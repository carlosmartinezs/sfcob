<?php

/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardUser.php 7634 2008-02-27 18:01:40Z fabien $
 */
class sfGuardUser extends PluginsfGuardUser
{
	public function PassEsDistintaA3Ultimas($passwordNva,$id)
	{
		if (!$salt = $this->getSalt())
	    {
	      $salt = md5(rand(100000, 999999).$this->getUsername());
	      $this->setSalt($salt);
	    }
	    
	    $algorithm = sfConfig::get('app_sf_guard_plugin_algorithm_callable', 'sha1');
    	$algorithmAsStr = is_array($algorithm) ? $algorithm[0].'::'.$algorithm[1] : $algorithm;
    	
    	if (!is_callable($algorithm))
    	{
     		throw new sfException(sprintf('The algorithm callable "%s" is not callable.', $algorithmAsStr));
    	}
    	$this->setAlgorithm($algorithmAsStr);
    	
		$EncryptedPasswordNva = call_user_func_array($algorithm, array($salt.$passwordNva));
		
		$c = new Criteria();
		$c->add(PerfilPeer::SF_GUARD_USER_ID,$id);
		$perfilBd = PerfilPeer::doSelectOne($c);
		
		$pass1 = $perfilBd->getPassword1();
		$pass2 = $perfilBd->getPassword2();
		$pass3 = $perfilBd->getPassword3();
		
		if($EncryptedPasswordNva != $pass1 && $EncryptedPasswordNva != $pass2 && $EncryptedPasswordNva != $pass3)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
