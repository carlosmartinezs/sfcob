<?php use_helper('Javascript');?>
<?php if($sf_request->isXmlHttpRequest()):?>
<div id="div-ajax">
<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
<h2 style="text-align:center;">Ingreso a SfCob</h2>
  <table>
  	<tbody>
  		<tr>
  			<th>Usuario</th>
  			<td><?php echo $form['username'] ?></td>
  			<td><?php echo $form['username']->renderError() ?></td>
  		</tr>
  		<tr>
  			<th>Contrase&ntilde;a</th>
  			<td><?php echo $form['password'] ?></td>
  			<td><?php echo $form['password']->renderError() ?></td>
  		</tr>
  		<tr>
  			<th>Recordarme?</th>
  			<td><?php echo $form['remember'] ?></td>
  			<td><?php echo $form['remember']->renderError() ?></td>
  		</tr>
  		<tr>
  			<td><input type="submit" value="Ingresar" class="login boton_con_imagen"/></td>
  			<td class="small-font">
  			<?php echo link_to('Olvid&eacute; mi contrase&ntilde;a','home/pedirusername'); ?>
			</td>
  		</tr>
  	</tbody>
  </table>
</form>
</div>
<?php else:?>
<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
<h2 style="text-align:center;">Ingreso a SfCob</h2>
  <table>
  	<tbody>
  		<tr>
  			<th>Usuario</th>
  			<td><?php echo $form['username'] ?></td>
  			<td><?php echo $form['username']->renderError() ?></td>
  		</tr>
  		<tr>
  			<th>Contrase&ntilde;a</th>
  			<td><?php echo $form['password'] ?></td>
  			<td><?php echo $form['password']->renderError() ?></td>
  		</tr>
  		<tr>
  			<th>Recordarme?</th>
  			<td><?php echo $form['remember'] ?></td>
  			<td><?php echo $form['remember']->renderError() ?></td>
  		</tr>
  		<tr>
  			<td><input type="submit" value="Ingresar" class="login boton_con_imagen"/></td>
  			<td class="small-font">
  			<?php echo link_to('Olvid&eacute; mi contrase&ntilde;a','home/pedirusername'); ?>
			</td>
  		</tr>
  	</tbody>
  </table>
</form>
<?php endif;?>

