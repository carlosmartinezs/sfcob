<div class="content-info-bar">
<?php if ($sf_user->isAuthenticated()): ?>
<div>
<div style="float: left;">
	<?php use_helper('Javascript') ?>
	<script src="/js/reloj.js" type="text/javascript">
	</script>
	<?php echo ' ' . Funciones::fecha() ?>
</div>
<div style="float: right;">
	<?php echo "Bienvenido(a), " . $sf_user->getUsername() . ". " ?>
	<?php echo link_to(image_tag('infobar/home.png').' Inicio','@homepage'); ?>
	<?php if(!$sf_user->getAttribute('caduca_pass',false)):?>
	 | 
	<?php echo link_to_remote(image_tag('infobar/user.png').' Perfil', array(
		'update' => 'modulo',
		'url' => '@user_profile',
		'loading' => visual_effect('appear','loading').
					visual_effect('highlight','modulo',array('duration' => 0.5)),
		'complete' => visual_effect('fade','loading'),
	)); ?> |
	<?php echo link_to_remote(image_tag('infobar/agenda.png',array('alt'=>'agenda','size'=>'24x24')).' Agenda', array(
		'update' => 'modulo',
		'url' => 'agenda/index',
		'loading' => visual_effect('appear','loading').
					visual_effect('highlight','modulo',array('duration' => 0.5)),
		'complete' => visual_effect('fade','loading'),
	)); ?> 
	<?php endif;?>
	| 
	<?php echo link_to(image_tag('infobar/logout.png',array('size' => '24x24')).' Salir','@sf_guard_signout'); ?>

</div>
<div style="clear:both;"></div>
<div style="float: left;">
<!-- Indicadores Economicos -->
  		Dolar Obs.:<script type="text/javascript">
		if(typeof(arrValores) != "undefined")
		if(typeof(arrValores[6])=="object")
		document.write(' $' + formatear_numero(arrValores[55].valor2));
		</script>
		|
		UF:<script type="text/javascript">
		if(typeof(arrValores) != "undefined")
		if(typeof(arrValores[4])=="object")
		document.write(' $' + formatear_numero(arrValores[4].valor2));
		</script>
		|
		UTM:<script type="text/javascript">
		if(typeof(arrValores) != "undefined")
		if(typeof(arrValores[6])=="object")
		document.write(' $' + formatear_numero(arrValores[5].valor2));
		</script>
		|
		IPC:
		<script type="text/javascript">
			if(typeof(arrValores) != "undefined")
				if(typeof(arrValores[6])=="object")
					document.write(' ' + formatear_numero(arrValores[23].valor2) + '%');
		</script>
|	<!-- Fin Indicadores Economicos -->
</div>
	<?php if($sf_user->hasCredential('abogado')):?>
		<div style="float:right">
			<?php include_component('home', 'valores') ?>
		</div>
	<?php endif;?>
<?php endif; ?>
</div>
<div id="imprimir-modulo" style="clear: both;">
	<?php echo button_to_function('Imprimir Modulo','window.print();',array('class'=>'boton_con_imagen imprimir'))?>
</div>
</div>

<?php include_component('home', 'tareasvencidas') ?>
