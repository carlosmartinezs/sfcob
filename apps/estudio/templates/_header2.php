<div class="content-info-bar2">
<?php if ($sf_user->isAuthenticated()): ?>
	<?php use_helper('Javascript') ?>
	<?php echo "Bienvenido(a), " . $sf_user->getUsername() . ". " ?>
	<?php echo link_to(image_tag('infobar/home.png').' Inicio','@homepage'); ?> | 
	<?php echo link_to_remote(image_tag('infobar/user.png').' Perfil', array(
		'update' => 'modulo',
		'url' => '@user_profile',
		'loading' => visual_effect('appear','loading').
					visual_effect('highlight','modulo',array('duration' => 0.5)),
		'complete' => visual_effect('fade','loading'),
	)); ?> |
	<?php echo link_to_remote(image_tag('infobar/agenda.png',array('alt'=>'agenda','size'=>'24x24')).' Agenda', array(
		'update' => 'modulo',
		'url' => 'agenda/index',
		'loading' => visual_effect('appear','loading').
					visual_effect('highlight','modulo',array('duration' => 0.5)),
		'complete' => visual_effect('fade','loading'),
	)); ?> | 
	<?php echo link_to(image_tag('infobar/logout.png',array('size' => '24x24')).' Salir','@sf_guard_signout'); ?>
<?php endif; ?>
</div>