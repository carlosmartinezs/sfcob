<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php use_helper('Javascript') ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>

<?php include_http_metas() ?>
<?php include_metas() ?>
<?php include_title() ?>
<link rel="shortcut icon" href="/images/favicon.ico" />
</head>
<body>
	<div id="main-div">
		<div id="header">
			<?php include_partial('global/header') ?>
		</div>
		<div id="info-bar">
			<?php include_partial('global/infobar') ?>
		</div>
		<div id="contenedor">
			<div id="modulo1">
			<?php if($sf_user->getAttribute('caduca_pass',false)):?>
			<div  id="info">
			<h4 class="informacion">Debe cambiar su contrase&ntilde;a:
			Han pasado 30 d&iacute;as o m&aacute;s desde el &uacute;ltimo 
			cambio de su contrase&ntilde;a.</h4>
			</div>
			<?php endif;?>
			<div id="acciones_perfil" style="clear: right">
				<?php echo $sf_content ?>
			</div>
			</div>
		</div>
		<div id="footer">
			<?php include_partial('global/footer') ?>
		</div>
		<div id="loading" style="display:none">
			<?php echo image_tag('loading.gif',array('size' => "150x150", 'alt' =>"Loading")) ?>
		</div>
	</div>
</body>
</html>