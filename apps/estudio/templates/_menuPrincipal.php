<?php use_helper('Javascript') ?>
	<?php if ($sf_user->hasCredential('administrador')): ?>
		<h2 id="user1"  class="toggle right mod-usuarios">Usuarios</h2>
	<div id="user-menu" class="content">
		<ul>
			<!-- <li><?php /*echo link_to_remote('Gesti&oacute;n de Usuarios', array(
			'script'	=> 'true',
			'update' 	=> 'modulo',
			'url'    	=> 'usuario/index',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesUs', 'class' => 'linkMenu')) */ ?>
			</li>  -->
			<li><?php echo link_to('Gesti&oacute;n de Usuarios','sfGuardUser/index')?></li>
			<li><?php echo link_to('Gesti&oacute;n de Grupos','sfGuardGroup/index')?></li>
			<li><?php echo link_to('Gesti&oacute;n de Roles','sfGuardPermission/index')?></li>
		</ul>
	</div>	
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
		<h2 id="admin1"  class="toggle right mod-administracion">Administraci&oacute;n</h2>
	<div id="administracion-menu" class="content">
		<ul>
			<?php if ($sf_user->hasCredential('abogado')):?>
			<li><?php echo link_to_remote('Gesti&oacute;n de Acreedores', array(
			'update' 	=> 'modulo',
			'url'    	=> 'administracion/indexacreedor',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesAc', 'class' => 'linkMenu')) ?></li>
			<?php endif; ?>
			<li><?php echo link_to_remote('Gesti&oacute;n de Deudores', array(
			'update' 	=> 'modulo',
			'url'    	=> 'administracion/indexdeudor',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesDeu', 'class' => 'linkMenu')) ?></li>
			<li>
			<?php if ($sf_user->hasCredential('abogado')):?>
			<?php echo link_to_remote('Gesti&oacute;n de Tribunales', array(
			'update' 	=> 'modulo',
			'url'    	=> 'administracion/indextribunal',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesTrib', 'class' => 'linkMenu')) ?>
			</li>
			<?php endif; ?>
		</ul>
	</div>	
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
		<h2 id="juicio1"  class="toggle right mod-juicios">Juicios</h2>
	<div id="juicios-menu" class="content">
	<!-- <div id="juicios-menu" class="content" style="display:none"> -->
		<ul>
			<li><?php echo link_to_remote('Gesti&oacute;n de Causas', array(
			'update' 	=> 'modulo',
			'url'    	=> 'juicios/indexcausa',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesCau', 'class' => 'linkMenu')) ?></li>
			<li><?php echo link_to_remote('Gesti&oacute;n de Diligencias', array(
			'update' 	=> 'modulo',
			'url'    	=> 'juicios/indexdiligencia',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesDili', 'class' => 'linkMenu')) ?></li>
			<li><?php echo link_to_remote('Gesti&oacute;n de Abonos', array(
			'update' 	=> 'modulo',
			'url'    	=> 'juicios/indexabono',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesAbo', 'class' => 'linkMenu')) ?></li>
			<li><?php echo link_to_remote('Gesti&oacute;n de Gastos', array(
			'update' 	=> 'modulo',
			'url'    	=> 'juicios/indexgasto',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesGast', 'class' => 'linkMenu')) ?></li>
		</ul>
	</div>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador') || $sf_user->hasCredential('acreedor')): ?>
	<h2 id="informe1"  class="toggle right mod-informes">Informes</h2>
	<div id="informes_menu" class="content">
		<ul><?php if ($sf_user->hasCredential('acreedor') || $sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
			<li><?php echo link_to_remote('Gesti&oacute;n de Informes', array(
			'update' 	=> 'modulo',
			'url'    	=> 'informes/indexinforme',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesInf', 'class' => 'linkMenu')) ?></li>
			<?php endif; ?>
			<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
			<li><?php echo link_to_remote('Gesti&oacute;n de Cartas', array(
			'update' 	=> 'modulo',
			'url'    	=> 'informes/indexcarta',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesCart', 'class' => 'linkMenu')) ?></li>
			<?php endif; ?>
			<?php if ($sf_user->hasCredential('procurador') || $sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
			<li><?php echo link_to_remote('Gesti&oacute;n de Escritos', array(
			'update' 	=> 'modulo',
			'url'    	=> 'informes/indexescrito',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesEsc', 'class' => 'linkMenu')) ?></li>
			<?php endif; ?>
		</ul>
	</div>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('administrador')): ?>
		<h2 id="mant1"  class="toggle right mod-mantencion">Mantenci&oacute;n</h2>
	<div id="mantencion-menu" class="content">
	<!-- <div id="mantencion-menu" class="content" style="display:none"> -->
		<ul>
			<li><?php echo link_to_remote('Gesti&oacute;n de Tipos de Gastos', array(
			'update' 	=> 'modulo',
			'url'    	=> 'mantencion/indextipogasto',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesTipGas', 'class' => 'linkMenu')) ?></li>
			<li><?php echo link_to_remote('Gesti&oacute;n de Tipos de Pagos', array(
			'update' 	=> 'modulo',
			'url'    	=> 'mantencion/indextipopago',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesTipPag', 'class' => 'linkMenu')) ?></li>
			<li><?php echo link_to_remote('Gesti&oacute;n de Tipos de Garant&iacute;as', array(
			'update' 	=> 'modulo',
			'url'    	=> 'mantencion/indextipogarantia',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesTipGar', 'class' => 'linkMenu')) ?>
			</li>
			<li><?php echo link_to_remote('Gesti&oacute;n de Tipos de Documentos', array(
			'update' 	=> 'modulo',
			'url'    	=> 'mantencion/indextipodocumento',
			'loading'	=> visual_effect('appear','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading'),
			),array('id' => 'gesTipDoc', 'class' => 'linkMenu')) ?>
			</li>
		</ul>
	</div>	
	<?php endif; ?>
