<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
<?php include_http_metas() ?>
<?php include_metas() ?>
<?php include_title() ?>
<link rel="shortcut icon" href="/images/favicon.ico" />
<script src="http://www.bci.cl/common/include/valores.js" type="text/javascript" />
</head>
<body>
	<div id="main-div">
		<div id="header">
			<?php include_partial('global/header') ?>
		</div>
		 
		<div id="info-bar">
			<?php include_partial('global/infobar') ?>
		</div>
		<div id="contenedor">
			<div id="menu-bar">
				<?php if ($sf_user->isAuthenticated()): ?>
				<?php include_partial('global/menuPrincipal')?>
				<?php endif; ?>
            </div>
			<div id="modulo">
				<?php echo $sf_content ?>
			</div>
		</div>
		<div id="footer">
			<?php include_partial('global/footer') ?>
		</div>
		<div id="loading" style="display:none">
			<?php echo image_tag('loading.gif',array('size' => "150x150", 'alt' =>"Loading")) ?>
		</div>
	</div>
</body>
</html>