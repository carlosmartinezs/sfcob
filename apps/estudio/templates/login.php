<?php use_helper('Javascript');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>

<?php include_http_metas() ?>
<?php include_metas() ?>
<?php include_title() ?>
<link rel="shortcut icon" href="/images/favicon.ico" />

</head>
<body>
	<div id="main-login">
		<div id="modulo-login">
		<?php if($sf_user->getAttribute('cambio_pass_success',false)):?>
			<?php $sf_user->getAttributeHolder()->remove('cambio_pass_success');?>
			<script type="text/javascript">
				alert('Se ha cambiado su contraseņa exitosamente. Ingrese nuevamente.');
			</script>
			<?php if($sf_user->getAttribute('caduca_pass',false)): ?>
				<?php $sf_user->getAttributeHolder()->remove('caduca_pass');?>
			<?php endif;?>
		<?php endif;?>
			<?php echo $sf_content ?>
		</div>
		<div id="footer">
			<?php include_partial('global/footer') ?>
		</div>
	</div>
</body>
</html>