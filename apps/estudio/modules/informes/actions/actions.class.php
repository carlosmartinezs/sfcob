<?php

/**
 * informes actions.
 *
 * @package    sgcj
 * @subpackage informes
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 9301 2008-05-27 01:08:46Z dwhittle $
 */
class informesActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex()
  {
  }
  
  public function executeIndexinforme()
  {
  }
  
  public function executeInformeliquidacion($request)
  {
  	if($request->hasParameter('imprimir') && $request->getParameter('imprimir') == 'imprimir'){
  		$this->setLayout('reporte');
  		$this->imprimir = $request->hasParameter('imprimir');
  		$this->getResponse()->addStylesheet('estudio', 'first', array('media' => 'print'));
  	}
  	
  	$iddeudor = $request->getParameter('iddeudor');
  	$this->deudor = DeudorPeer::retrieveByPK($iddeudor);
  	
  	$this->causasList = $this->deudor->getCausas();
  	
  }  
  
  public function executeReportecausaacreedor($request){
  	$this->acreedor = AcreedorPeer::retrieveByPK($request->getParameter('id'));
  	$c = new Criteria();
  	$c->addJoin(AcreedorPeer::ID,ContratoPeer::ACREEDOR_ID);
  	$c->addJoin(ContratoPeer::ID,CausaPeer::CONTRATO_ID);
  	$c->add(AcreedorPeer::ID,$request->getParameter('id'));
  	$this->causasList = CausaPeer::doSelect($c);
  	$this->accion = '';
  	$this->setTemplate('reportecausa');
  }
  
  public function executeReportecausa($request)
  {
  	$this->acreedor = AcreedorPeer::retrieveByPK($request->getParameter('idacreedor'));
  	
  	if($request->hasParameter('activa') || $request->hasParameter('terminada') || $request->hasParameter('castigada') || $request->hasParameter('incobrable') || $request->hasParameter('suspendida')){
  		
  		$filtroCausa = null;
  		$filtroCausa[0] = 0;
  		$filtroCausa[1] = 0;
  		$filtroCausa[2] = 0;
  		$filtroCausa[3] = 0;
  		$filtroCausa[4] = 0;
  		
  		if ($request->hasParameter('activa')){$filtroCausa[0] = 1;} 
  		if ($request->hasParameter('terminada')){$filtroCausa[1] = 1;}
  		if ($request->hasParameter('castigada')){$filtroCausa[2] = 1;}
  		if ($request->hasParameter('incobrable')){$filtroCausa[3] = 1;}
  		if ($request->hasParameter('suspendida')){$filtroCausa[4] = 1;}
  		
  		$this->estados = $filtroCausa;
  		
  		$this->causasList = CausaPeer::getCausasAcreedorFiltroDeEstado($this->acreedor, $filtroCausa);
  	
  	}else{
  		
  		$this->formFiltro = new FiltroEstadoCausaForm();
  		$this->formFiltro->setDefault('activa',true);
  		$this->setTemplate('filtroestadocausa');
  		
  	}
  }
   
  public function executeGenerareportecausa($request)
  {
  	//$this->setLayout('reporte');
	//$this->getResponse()->addStylesheet('estudio', 'first', array('media' => 'print'));
	
  	if($request->hasParameter('id_causa')){
  		
  		$this->causa = CausaPeer::retrieveByPK($request->getParameter('id_causa'));
  		$this->diligenciaList = DiligenciaPeer::getDilicenciasCausaPorFechaConLimite($this->causa,22);
  		
  	}else if($request->hasParameter('id_acreedor')){
  		
  		$filtroCausa = null;
  		
  		$filtroCausa[0] = 0;
  		$filtroCausa[1] = 0;
  		$filtroCausa[2] = 0;
  		$filtroCausa[3] = 0;
  		$filtroCausa[4] = 0;
  		
  		if ($request->hasParameter('activa')){$filtroCausa[0] = 1;} 
  		if ($request->hasParameter('terminada')){$filtroCausa[1] = 1;}
  		if ($request->hasParameter('castigada')){$filtroCausa[2] = 1;}
  		if ($request->hasParameter('incobrable')){$filtroCausa[3] = 1;}
  		if ($request->hasParameter('suspendida')){$filtroCausa[4] = 1;}
  		
  		$this->acreedor = AcreedorPeer::retrieveByPK($request->getParameter('id_acreedor'));
  		$this->causasList = CausaPeer::getCausasAcreedorFiltroDeEstado($this->acreedor,$filtroCausa);
  		$this->setTemplate('resumencausas');
  		
  	}
  }
  
  public function executeIndexcarta()
  {
  }

  public function executeRedactarcarta($request)
  {
  	if($request->hasParameter('id_carta') && $request->hasParameter('id_causa')){
  		
  		$this->carta = FormatoPeer::retrieveByPK($request->getParameter('id_carta'));
  		$this->causa = CausaPeer::retrieveByPK($request->getParameter('id_causa'));
  		
  		if($request->hasParameter('imprimir')){
  			
  			$this->imprimir = $request->getParameter('imprimir');
  			$this->setLayout('reporte');
  			$this->getResponse()->addStylesheet('estudio', 'first', array('media' => 'print'));
  			$diligencia = new Diligencia();
  			$diligencia->setDescripcion($this->carta->getTiponombre().' '.'\''.$this->carta.'\'');
  			$diligencia->setFecha('now');
  			$diligencia->setPeriodoCausaId($this->causa->getPeriodoCausaActual()->getId());
  			$diligencia->save();
  			
  			$diligenciaFormato = new DiligenciaFormato();
  			$diligenciaFormato->setDiligenciaId($diligencia->getId());
  			$diligenciaFormato->setFormatoId($this->carta->getId());
  			$diligenciaFormato->save();
  			
  		}else if($request->hasParameter('no_imprime') && $request->getParameter('no_imprime') == 'no_imprime'){
  			$this->no_imprime = $request->getParameter('no_imprime');
  		}
  		
  		$this->setTemplate('carta');
  		
  	}else{
  		
  		$this->formFiltroCarta = new FiltroTipoCartaForm();
  		$this->formFiltroCarta->setDefault('tipo_carta','1');
  		
  	}
  }
  
  public function executeShowcartaenviada()
  {
  }
  
  public function executeListformatocarta()
  {
  	$c = new Criteria();
  	$c->add(FormatoPeer::TIPO, '0');
  	$this->formatoList = FormatoPeer::doSelect($c);
  }
  
  public function executeShowformato($request)
  {
  	$this->formato = FormatoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->formato);
  }
  
  public function executeIndexescrito()
  {
  }
  
  public function executeRedactarescrito($request)
  {
    if($request->hasParameter('id_escrito') && $request->hasParameter('id_causa')){
  		
  		$this->escrito = FormatoPeer::retrieveByPK($request->getParameter('id_escrito'));
  		$this->causa = CausaPeer::retrieveByPK($request->getParameter('id_causa'));
  		
  		if($request->hasParameter('imprimir')){
  			
  			$this->imprimir = $request->getParameter('imprimir');
  			$this->setLayout('reporte');
  			$this->getResponse()->addStylesheet('estudio', 'first', array('media' => 'print'));
  			
  			$diligencia = new Diligencia();
  			$diligencia->setDescripcion($this->escrito->getTiponombre().' '.'\''.$this->escrito.'\'');
  			$diligencia->setFecha('now');
  			$diligencia->setPeriodoCausaId($this->causa->getPeriodoCausaActual()->getId());
  			$diligencia->save();
  			
  			$diligenciaFormato = new DiligenciaFormato();
  			$diligenciaFormato->setDiligenciaId($diligencia->getId());
  			$diligenciaFormato->setFormatoId($this->escrito->getId());
  			$diligenciaFormato->save();
  			
  		}else if($request->hasParameter('no_imprime') && $request->getParameter('no_imprime') == 'no_imprime'){
  			$this->no_imprime = $request->getParameter('no_imprime');
  		}
  		
  		$this->setTemplate('escrito');
  		
  	}else{
  		
  		$this->formFiltroEscrito = new FiltroTipoEscritoForm();
  		$this->formFiltroEscrito->setDefault('tipo_escrito','5');
  		
  	}
  }
  
  public function executeShowescritoenviado()
  {
  }
  
  public function executeListformatoescrito()
  {
  	$c = new Criteria();
  	$c->add(FormatoPeer::TIPO, '1');
  	$this->formatoList = FormatoPeer::doSelect($c);
  }  

  public function executeSearchformato($request){
  	$this->accion = $request->getParameter('accion');
  	
  	if($this->accion == 'cartaenviada'){
  		$this->form = new SearchCartaForm();
  	}else if(($this->accion == 'escritoenviado')){
  		$this->form = new SearchEscritoForm();
  	}
  }
  
  public function executeSearchformatoupdate($request){
  	
  	$this->diligencia_formatoList = DiligenciaFormatoPeer::doSelect(DiligenciaFormatoPeer::getCriteria($request));
  	
  	$this->accion = $request->getParameter('accion');

  	$this->setTemplate('listdiligenciaformato');
  }
}


