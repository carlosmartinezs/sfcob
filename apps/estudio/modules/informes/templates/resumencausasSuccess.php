<div id="resumen-causas">
<h2>Resumen Causas: <?php echo $acreedor ?></h2>
	<table>
		<tbody>
			<?php foreach($causasList as $causa): ?>
				<tr>
					<th>Causa:</th>
					<td colspan="3"> <?php echo $causa ?></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<table>
							<thead>
								<tr>
									<th>N&ordm;</th>
									<th>Rol</th>
									<th>Estado</th>
									<th>Exhorto</th>
									<th>Causa Base</th>
									<th>&Uacute;ltima Diligencia</th>
									<th>Fecha</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?php echo $causa->getId() ?></td>
									<td><?php echo $causa->getRol() ?></td>
									<td><?php echo $causa->getEstadoCausaActivo()->getEstado()->getNombre() ?></td>
									<td><?php echo ($causa->getHerenciaExhorto() ? 'Si' : 'No') ?></td>
									<td><?php echo ($causa->getHerenciaExhorto() ? $causa->getCausaBase() : '-') ?></td>
									<td>
									<?php $ultimaDiligencia = $causa->getUltimaDiligencia();
										echo (!$ultimaDiligencia ? 'No Posee Diligencias' : $ultimaDiligencia->getDescripcion()) ?>
									</td>
									<td><?php echo (!$ultimaDiligencia ? '-' : Funciones::fecha2($ultimaDiligencia->getFecha('d-m-Y2'))) ?></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			<?php endforeach;?>
		</tbody>
	</table>
</div>