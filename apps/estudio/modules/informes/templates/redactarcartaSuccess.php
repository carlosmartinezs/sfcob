<?php use_helper('Javascript') ?>
<h2>Seleccione tipo de carta</h2>
<div id="form-tipo-carta">
<?php 
	echo 	button_to_remote('Cobranza Aval',array(
				'update' => 'acciones-carta',
				'url' => 'juicios/searchcausa?accion=redactarcarta&tipo_carta=1',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
						visual_effect('slideDown','acciones-carta'),
			),array('class' => 'carta boton_con_imagen'));
	echo 	button_to_remote('Cobranza Deudor',array(
				'update' => 'acciones-carta',
				'url' => 'juicios/searchcausa?accion=redactarcarta&tipo_carta=2',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
						visual_effect('slideDown','acciones-carta'),
			),array('class' => 'carta boton_con_imagen'));
?>
</div>
<div id="acciones-carta" style="display:none">
</div>



