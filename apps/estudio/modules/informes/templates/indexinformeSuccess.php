<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Informes</h1>

<ul class="menu-modulo">
	<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
	<li>
		<?php echo link_to_remote('Generar Liquidaci&oacute;n', array(
    		'update' => 'acciones_informe',
    		'url'    => 'administracion/searchdeudor?accion=liquidacion',
			'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
		)) ?></li>
	<?php endif; ?>
	<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('acreedor')): ?>
		<?php if($sf_user->hasCredential('acreedor') && $acreedor = $sf_user->getAcreedor()): ?>
			<?php //$acreedor = $sf_user->getAcreedor()?>
			<li><?php echo link_to_remote('Generar Reporte de Causas', array(
	    		'update' => 'acciones_informe',
	    		'url'    => 'informes/reportecausa?idacreedor='.$acreedor->getId(),
				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
				'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
			)) ?></li>
		<?php else: ?>
		<li><?php echo link_to_remote('Generar Reporte de Causas', array(
	    		'update' => 'acciones_informe',
	    		'url'    => 'administracion/searchacreedor?accion=reportecausas',
				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
				'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
			)) ?></li>
		<?php endif; ?>
	<?php endif; ?>
</ul>
<div id="acciones_informe"  style="clear: right"></div>