<div id="liquidacion-causas">
<h2>Liquidaci&oacute;n de Causas: <?php echo $deudor ?></h2>
	<div class="reporte_">
		<h2 style="text-align: center;margin:0;" class="tddestacado">Resumen</h2>
		<table id="<?php echo 'deudor_'.$deudor->getId() ?>" style="margin: 0; padding: 0;width:100%;">
			<thead>
				<tr>
					<td colspan="9" class="tddestacado">
						<h3 style="margin:0;">Total: <?php echo count($causasList).' causa'.(count($causasList) > 1 ? 's' : '')?></h3>
					</td> 
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="3" class="tddestacado"><strong>Deuda Total</strong></td>
					<td colspan="3" class="tddestacado"><strong>Abono Total</strong></td>
					<td colspan="3" class="tddestacado"><strong>Total Pendiente</strong></td>
				</tr>
				<tr>
					<td colspan="3" class="tdcentrado">$ <?php echo $deudor->getTotalDeuda() ?></td>
					<td colspan="3" class="tdcentrado">$ <?php echo $deudor->getTotalAbono() ?></td>
					<td colspan="3" class="tdcentrado">$ <?php echo $deudor->getDeudaPendiente()?></td>
				</tr>
				<tr>
					<td class="tddestacado"><strong>Monto</strong></td>
					<td class="tddestacado"><strong>Inter&eacute;s</strong></td>
					<td class="tddestacado"><strong>Gastos</strong></td>
					<td class="tddestacado"><strong>Monto</strong></td>
					<td class="tddestacado"><strong>Inter&eacute;s</strong></td>
					<td class="tddestacado"><strong>Gastos</strong></td>
					<td class="tddestacado"><strong>Monto</strong></td>
					<td class="tddestacado"><strong>Inter&eacute;s</strong></td>
					<td class="tddestacado"><strong>Gastos</strong></td>
				</tr>
				<tr>
					<td class="tdcentrado">$ <?php echo $deudor->getTotalDeudaMonto() ?></td>
					<td class="tdcentrado">$ <?php echo $deudor->getTotalDeudaInteres() ?></td>
					<td class="tdcentrado">$ <?php echo $deudor->getTotalDeudaGastosCobranza() ?></td>
					<td class="tdcentrado">$ <?php echo $deudor->getTotalAbonoMonto() ?></td>
					<td class="tdcentrado">$ <?php echo $deudor->getTotalAbonoInteres() ?></td>
					<td class="tdcentrado">$ <?php echo $deudor->getTotalAbonoGastosCobranza() ?></td>
					<td class="tdcentrado">$ <?php echo $deudor->getDeudaPendienteMonto() ?></td>
					<td class="tdcentrado">$ <?php echo $deudor->getDeudaPendienteInteres() ?></td>
					<td class="tdcentrado">$ <?php echo $deudor->getDeudaPendienteGastosCobranza() ?></td>
				</tr>
			</tbody>
		</table>
		<h2 style="text-align:center;margin:10px 0;" class="tddestacado">Detalle por Causa</h2>
			<?php foreach($causasList as $causa): ?>
				<table id="<?php echo 'causa_'.$causa->getId() ?>" style="margin:0;padding:0;width:100%;">
					<thead>
						<tr>
							<td class="tddestacado" colspan="9">Causa: <?php echo $causa->getId()?> - Rol: <?php echo $causa->getRol()?></td>
						</tr>
					</thead>
					<tbody>
					   <tr>
					   		<!-- <th>Acreedor:</th> -->
					   		<td colspan="9"><?php echo $causa->getCaratula() ?></td>
					   </tr>
				   		<?php if($causa->getDeuda()->getMonto() == 0):?>
				   			<tr>
				   				<td colspan="9" class="aviso">
				   					NOTA: Esta causa tiene una deuda por monto $ 0 (cero),
				   					esto quiere decir, que en dicha causa no se han ingresado 
				   					documentos que respalden la deuda que existe.
				   				</td>
				   			</tr>
				   		<?php else:?>
						<tr>
					    	<th class="tdcentrado  tddestacado" colspan="3">Cuant&iacute;a</th>
					    	<th class="tdcentrado  tddestacado" colspan="3">Abonos</th>
					    	<th class="tdcentrado  tddestacado" colspan="3">Pendiente</th>
						</tr>
						<tr>
					    	<td colspan="3">$ <?php echo $causa->getCuantia()?></td>
					    	<td colspan="3">$ <?php echo $causa->getTotalAbono()?></td>
					    	<td colspan="3">$ <?php echo $causa->getDeudaPendiente()?></td>
						</tr>
						<tr>
					    	<th class="tdcentrado tddestacado">Monto</th>
					    	<th class="tdcentrado tddestacado">Inter&eacute;s</th>
					    	<th class="tdcentrado tddestacado">Gastos</th>
					    	<th class="tdcentrado tddestacado">Monto</th>
					    	<th class="tdcentrado tddestacado">Inter&eacute;s</th>
					    	<th class="tdcentrado tddestacado">Gastos</th>
					    	<th class="tdcentrado tddestacado">Monto</th>
					    	<th class="tdcentrado tddestacado">Inter&eacute;s</th>
					    	<th class="tdcentrado tddestacado">Gastos</th>
					    </tr>
					    <tr>
					    	<td>$ <?php echo $causa->getDeudaMonto()?></td>
					    	<td>$ <?php echo $causa->getDeudaInteres()?></td>
					    	<td>$ <?php echo $causa->getDeudaGastosCobranza()?></td>
					    	<td>$ <?php echo $causa->getAbonoMonto()?></td>
					    	<td>$ <?php echo $causa->getAbonoInteres()?></td>
					    	<td>$ <?php echo $causa->getAbonoGastosCobranza()?></td>
					    	<td>$ <?php echo $causa->getDeudaPendienteMonto()?></td>
					    	<td>$ <?php echo $causa->getDeudaPendienteInteres()?></td>
					    	<td>$ <?php echo $causa->getDeudaPendienteGastosCobranza()?></td>
					    </tr>
						<tr>
							<td colspan="9" style="padding: 0;">
								<table id="abonos-<?php echo $causa->getId() ?>" style="margin:0;padding: 0;width:100%;">
									<thead>
										<tr>
											<th colspan="5" class="tddestacado">Detalle Abonos</th>
										</tr>
										<tr>
											<th class="tddestacado">Fecha</th>
											<th class="tddestacado">Monto</th>
											<th class="tddestacado">Contra</th>
											<th class="tddestacado">Motivo</th>
											<th class="tddestacado">N&ordm; Recibo</th>
										</tr>
									</thead>
									<tbody>
									<?php $abonos = $causa->getAbonos() ?>
									<?php if(count($abonos) > 0): ?>
										<?php foreach($abonos as $abono):?>
										<tr>
											<td class="tdcentrado"><?php echo Funciones::fecha2($abono->getFecha('d-m-Y')) ?></td>
											<td class="tdcentrado">$ <?php echo $abono->getValor() ?></td>
											<td class="tdcentrado"><?php echo $abono->getTipoAbono() ?></td>
											<td class="tdcentrado"><?php echo $abono ?></td>
											<td class="tdcentrado"><?php echo $abono->getNroRecibo() ?></td>
										</tr>
										<?php endforeach; ?>
									<?php else: ?>
										<tr>
											<td colspan="5" class="aviso" >NO HAY ABONOS</td>
										</tr>
									<?php endif;?>
									</tbody>
								</table>
							</td>
						</tr>
						<?php endif;?>
					</tbody>
				</table>
				<br />	
			<?php endforeach;?>
	</div>
</div>