<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Escritos</h1>
<ul class="menu-modulo">
	<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
	<li>
		<?php echo link_to_remote('Redactar Escrito', array(
    		'update' => 'acciones_escrito',
    		'url'    => 'informes/redactarescrito',
		    'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
		)) ?></li>
	<?php endif; ?>
	<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
	<li>
		<?php echo link_to_remote('Visualizar Escrito Enviado', array(
    		'update' => 'acciones_escrito',
    		'url'    => 'informes/searchformato?accion=escritoenviado',
		)) ?></li>
	<?php endif; ?>
	<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
	<li>
		<?php echo link_to_remote('Visualizar Formato de Escrito', array(
    		'update' => 'acciones_escrito',
    		'url'    => 'informes/listformatoescrito',
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_escrito"  style="clear: right"></div>