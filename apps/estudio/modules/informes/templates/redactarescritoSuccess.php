<?php use_helper('Javascript') ?>
<h2>Seleccione tipo de escrito</h2>
<div id="form-tipo-escrito">
<?php
			echo button_to_remote('Fuerza P&uacute;blica Embargo',array(
				'update' => 'acciones-escrito',
				'url' => 'juicios/searchcausa?accion=redactarescrito&tipo_escrito=3',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
						visual_effect('slideDown','acciones-escrito'),
			),array('class' => 'escrito boton_con_imagen'));
			echo button_to_remote('Fuerza P&uacute;blica Retiro Especies',array(
				'update' => 'acciones-escrito',
				'url' => 'juicios/searchcausa?accion=redactarescrito&tipo_escrito=4',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
						visual_effect('slideDown','acciones-escrito'),
			),array('class' => 'escrito boton_con_imagen'));
			echo button_to_remote('Notif. art. 44',array(
				'update' => 'acciones-escrito',
				'url' => 'juicios/searchcausa?accion=redactarescrito&tipo_escrito=5',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
						visual_effect('slideDown','acciones-escrito'),
			),array('class' => 'escrito boton_con_imagen'));
			echo button_to_remote('Notif. art. 52',array(
				'update' => 'acciones-escrito',
				'url' => 'juicios/searchcausa?accion=redactarescrito&tipo_escrito=6',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
						visual_effect('slideDown','acciones-escrito'),
			),array('class' => 'escrito boton_con_imagen'));
			
			echo button_to_remote('Se Exhorte',array(
				'update' => 'acciones-escrito',
				'url' => 'juicios/searchcausa?accion=redactarescrito&tipo_escrito=7',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
						visual_effect('slideDown','acciones-escrito'),
			),array('class' => 'escrito boton_con_imagen'));
			?>
</div>
<div id="acciones-escrito" style="display:none">
</div>