<?php use_helper('Javascript') ?>
<h2>Lista de <?php echo ($accion == 'cartaenviada' ? 'Cartas Enviadas' : 'Escritos Enviados') ?></h2>
<?php	$mostrar_button = true;
	switch($accion):
		case 'cartaenviada': 
			$modulo = 'acciones_carta';
			$url = 'informes/redactarcarta';
			break;
		case 'escritoenviado':
			$modulo = 'acciones_escrito';
			$url = 'informes/redactarescrito';
			break;
		default: break;
	endswitch;
?>
<table>
  <thead>
    <tr>
      <th class="tddestacado">Diligencia</th>
      <th class="tddestacado"><?php echo ($accion == 'cartaenviada' ? 'Carta' : 'Escrito') ?></th>
      <th class="tddestacado">Fecha</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  	<?php if(count($diligencia_formatoList) > 0): ?>
	    <?php foreach ($diligencia_formatoList as $diligencia_formato): ?>
		    <tr>
		      <td><?php echo $diligencia_formato->getDiligencia() ?></td>
		      <td><?php echo $diligencia_formato->getFormato() ?></td>
		      <td><?php echo Funciones::fecha2($diligencia_formato->getFecha('d-m-Y')) ?></td>
		      <td><?php 
		      		if($mostrar_button):
		      			$id = $diligencia_formato->getFormato()->getId();
		      			$id_causa = $diligencia_formato->getDiligencia()->getPeriodoCausa()->getCausa()->getId();
		      			echo button_to_remote('Ver',array(
		      			'update' => $modulo,
		      			'url' 	=> $url.($accion == 'cartaenviada' ? '?id_carta=' : '?id_escrito=').$id.'&id_causa='.$id_causa.'&no_imprime=no_imprime',
		      			'loading' => visual_effect('appear','loading').
								visual_effect('highlight','list-search-formato-enviado', array('duration' => 0.5)).
								visual_effect('fade','list-search-formato-enviado', array('duration' => 0.5)),
		      			'complete' => visual_effect('fade','loading', array('duration' => 1.0)),
		      			),array('class' => 'detalles boton_con_imagen')); 
		      		endif;?></td>
		    </tr>
	    <?php endforeach; ?>
    <?php else: ?>
    	<tr>
    		<td class="aviso" colspan="4">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td>
    	</tr>
    <?php endif; ?>
  </tbody>
</table>
<?php 
	echo button_to_function('Cerrar ventana',
	visual_effect('appear','loading').
	visual_effect('highlight','list-search-formato-enviado', array('duration' => 0.5)).
	visual_effect('fade','list-search-formato-enviado', array('duration' => 0.5)).
	visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cancelar boton_con_imagen'));
?>