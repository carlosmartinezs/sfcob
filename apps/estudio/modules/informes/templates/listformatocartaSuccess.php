<?php use_helper('Javascript') ?>

<h2>Lista de Cartas</h2>

<table>
  <thead>
    <tr>
      <th class="tddestacado">Tipo</th>
      <th class="tddestacado">Nombre</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($formatoList as $formato): ?>
    <tr>
      <td><?php echo $formato->getTiponombre()?></td>
      <td><?php echo $formato->getNombre() ?></td>
      <td><?php echo button_to_remote('Visualizar', array(
    		'update' => 'acciones_carta',
    		'url'    => 'informes/showformato?id='.$formato->getId(),
		),array('class' => 'detalles boton_con_imagen')) ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>