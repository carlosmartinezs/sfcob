<?php use_helper('Javascript') ?>
<h2>Reporte de Causas: <?php echo $acreedor ?></h2>
<table>
	<thead>
<?php if(count($causasList) > 0): ?>
		<tr>
			<th class="tddestacado">Rol</th>
			<th class="tddestacado">Nombre</th>
			<th class="tddestacado">Estado Actual</th>
			<th class="tddestacado">Acci&oacute;n</th>
		</tr>
	</thead>
	<tbody>
  <?php foreach($causasList as $causa): ?>
	<tr>
		<td><?php echo $causa->getRol()?></td>
		<td><?php echo $causa ?></td>
		<td><?php echo $causa->getEstadoCausaActivo()->getEstado()->getNombre()?></td>
		<td>
		<?php 
		echo link_to_remote('Detalle',array(
					'update' => 'acciones_informe', 
					'url'	=>'informes/generareportecausa?id_causa='.$causa->getId(),
					'loading'	=> visual_effect('appear','loading'),
					'complete'	=> visual_effect('fade','loading')),
				array('class' => 'detalles boton_con_imagen'));
		echo button_to_function('+',"new Effect.toggle('preview_".$causa->getRol()."','slide');
								if(this.value == '+')
								{
									this.value = '-';
								}
								else
								{
									this.value = '+';
								}",array('class'=>'boton_con_imagen','style'=>'padding-left:5px;'))?>
		</td>
	</tr>
	<tr>
		<td colspan="5" style="padding:0;">
			<div id="<?php echo 'preview_'.$causa->getRol() ?>" style="padding:0;display:none;">
			<h2 style="margin:0;"><?php echo 'Reporte para: '.$causa->getAcreedor().'<br />Causa: "'.$causa.'"' ?></h2>
	<table style="margin: 0 auto;padding:0;width:100%;">
			<tbody>
				<tr>
	      			<th>Materia:</th>
	      			<td><?php echo $causa->getMateria() ?></td>
	      			<th>Competencia:</th>
	      			<td><?php echo $causa->getCompetencia() ?></td>
	    		</tr>
	    		<tr>
	      			<th>Fecha de Inicio:</th>
	      			<td><?php echo Funciones::fecha2($causa->getFechaInicio('d-m-Y')) ?></td>
	      			<th>Fecha de T&eacute;rmino:</th>
	      			<td><?php echo $causa->getFechaTermino('d-m-Y') ? Funciones::fecha2($causa->getFechaTermino('d-m-Y')) : 'No Estipulada' ?></td>
	    		</tr>
	    		<tr>
	      			<th>Periodo:</th>
	      			<td><?php echo $causa->getPeriodoCausaActual()->getPeriodo() ?></td>
	      			<th>&Uacute;ltima Diligencia:</th>
	      			<?php if($causa->getUltimaDiligencia()):?>
	      				<td><?php echo $causa->getUltimaDiligencia()->getDescripcion()?></td>
	    			<?php else:?>
	    				<td class="aviso"><?php echo 'NO POSEE DILIGENCIAS'?></td>
	    			<?php endif;?>
	    		</tr>
	    		<tr>
	    			<th>Cuant&iacute;a:</th>
	      			<td>$ <?php echo $causa->getCuantia()?></td>
	      			<th>Tribunal Actual:</th>
	      			<td><?php echo $causa->getTribunalActual()?></td>
	      		</tr>
	      		<tr>
	      			<th class="tddestacado" colspan="4">Documentos</th>
	      		</tr>
	      		<?php if(count($causa->getDocumentos()) > 0): ?>
	      		<?php $documentos = $causa->getDocumentos() ?>
	      		<tr>
	      			<td colspan="4" style="padding: 0;">
	      				<table style="margin: 0 auto;padding:0; width:100%;">
	      					<thead>
	      						<tr>
	      							<th class="tddestacado">Tipo</th>
	      							<th class="tddestacado">Emisi&oacute;n</th>
	      							<th class="tddestacado">Vencimiento</th>
	      							<th class="tddestacado">Monto</th>
	      							<th class="tddestacado">N&uacute;mero</th>
	      							<th class="tddestacado">Descripci&oacute;n</th>
	      						</tr>
	      					</thead>
	      					<tbody>
	      					<?php foreach($documentos as $documento): ?>
	      					<tr>
	      						<td><?php echo $documento->getTipoDocumento()?></td>
	      						<td><?php echo Funciones::fecha2($documento->getFechaEmision('d-m-Y'))?></td>
	      						<td><?php echo Funciones::fecha2($documento->getFechaVencimiento('d-m-Y'))?></td>
	      						<td>$ <?php echo $documento->getMonto()?></td>
	      						<td><?php echo $documento->getNroDocumento()?></td>
	      						<td><?php echo $documento->getDescripcion()?></td>
	      					</tr>
	      					<?php endforeach; ?>
	      					</tbody>
	      				</table>
	      			</td>
	      		</tr>
	      		<?php else: ?>
	      		<tr>
	      			<td colspan="4" class="aviso">ESTA CAUSA NO POSEE DOCUMENTOS</td>
	      		</tr>
	      		<?php endif; ?>
	      		<tr>
	      			<th class="tddestacado" colspan="4">&Uacute;ltimas diligencias</th>
	      		</tr>
	      		<tr>
				<td colspan="4" style="padding: 0;">
				<?php $diligenciaList = $causa->getDiligencias()?>
				<?php if(count($diligenciaList) > 0):?>
					<table style="margin: 0 auto; padding:0; width:100%;">
						<thead>
							<tr>
								<th class="tddestacado">Fecha</th>
								<th class="tddestacado">Descripci&oacute;n</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($diligenciaList as $diligencia):?>
								<tr>
									<td><?php echo Funciones::fecha2($diligencia->getFecha('d-m-Y')); ?></td>
									<td><?php echo $diligencia->getDescripcion(); ?></td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				<?php else: ?>	
					<strong>NO HAY DILIGENCIAS EN ESTA CAUSA</strong>	
				<?php endif; ?>
				</td>
			</tr>
			</tbody>
		</table>
			</div>
		</td>
	</tr>
  <?php endforeach; ?>
	</tbody>
</table>
<?php else: ?>
<div id="no-causas">
<span class="aviso"><strong>NO POSEE CAUSAS EN LOS ESTADOS ELEGIDOS</strong></span> 
	<?php if($sf_user->isAcreedor()):?>
		<?php echo button_to_remote('Buscar otra vez', array(
					'update' => 'acciones_informe',
		    		'url'    => 'informes/reportecausa?idacreedor='.$acreedor->getId(),
					'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
					),array('class' => 'buscar boton_con_imagen')) ?>
	<?php else:?>
		<?php echo button_to_remote('Buscar otra vez', array(
					'update' => 'acciones_informe',
		    		'url'    => 'administracion/searchacreedor?accion=reportecausas',
					'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
					),array('class' => 'buscar boton_con_imagen')) ?>
	<?php endif; ?>
</div>
<?php endif; ?>