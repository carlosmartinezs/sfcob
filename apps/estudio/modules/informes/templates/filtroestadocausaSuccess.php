<?php use_helper('Javascript') ?>
<div id="filtro-causa">
<?php echo form_remote_tag(array(
    'update'   	=> 'filtro-causa',
    'url'      	=> 'informes/reportecausa?idacreedor='.$acreedor->getId(),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('highlight','filtro-causa',array('duration' => 1.0)).
					visual_effect('fade','loading'),
))?>
  <table>
  	<thead>
  		<tr>
  			<td colspan="11" class="tddestacado" style="padding:0;height:40px;">
  			<h2 class="aviso" style="text-align: left;">Seleccione al menos un filtro</h2>
  			</td>
  		</tr>
  	</thead>
    <tbody>
      <?php echo $formFiltro->renderGlobalErrors() ?>
      <tr>
        <th><label for="filtroestadocausa_activa">Activa</label></th>
        <td>
          <?php echo $formFiltro['activa']->renderError() ?>
          <?php echo $formFiltro['activa'] ?>
        </td>
        <th><label for="filtroestadocausa_terminada">Terminada</label></th>
        <td>
          <?php echo $formFiltro['terminada']->renderError() ?>
          <?php echo $formFiltro['terminada'] ?>
        </td>
        <th><label for="filtroestadocausa_castigada">Castigada</label></th>
        <td>
          <?php echo $formFiltro['castigada']->renderError() ?>
          <?php echo $formFiltro['castigada'] ?>
        </td>
        <th><label for="filtroestadocausa_incobrable">Incobrable</label></th>
        <td>
          <?php echo $formFiltro['incobrable']->renderError() ?>
          <?php echo $formFiltro['incobrable'] ?>
        </td>
        <th><label for="filtroestadocausa_suspendida">Suspendida</label></th>
        <td>
          <?php echo $formFiltro['suspendida']->renderError() ?>
          <?php echo $formFiltro['suspendida'] ?>
        </td>
      	<td>
        	<input type="submit" value="Filtrar" class="buscar boton_con_imagen"/>
      	</td>
      </tr>
    </tbody>
  </table>
</form>
</div>