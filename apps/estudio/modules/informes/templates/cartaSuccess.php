<?php
if(!isset($imprimir) && !isset($no_imprime)):
	echo link_to('Imprimir',
		'informes/redactarcarta?id_carta='.$carta->getId().'&id_causa='.$causa->getId().'&imprimir=imprimir',
		array(
			'class' => 'imprimir boton_con_imagen',
			'target' => '_blank', 
			'confirm' => "Se guardar&aacute; una diligencia en la causa ".$causa.", con relaci&oacute;n a la carta que solicita imprimir. Esta acci&oacute;n es irrevertible. Pulse 'Aceptar' para continuar con la impresi&oacute;n, o 'Cancelar' para volver a la visualizaci&oacute;n de la carta."
		));
endif;
?>
<div id="header-carta">
	<table>
		<tr>
			<td rowspan="2" class="imagen"><?php echo image_tag('logo-abogado.jpg',array('size' => '64x64'))?></td>
			<td><strong>LEONARDO &Aacute;VILA SANDOVAL</strong></td>		
		</tr>
		<tr>
			<td>Abogado</td>
		</tr>
		<tr>
			<td colspan="2" class="texto_derecha"><?php echo Funciones::fecha() ?></td>
		</tr>
	</table>
</div>
<div id="cuerpo-carta">
<?php	switch($carta->getId()):
		case 1:
			echo sprintf($carta->getCuerpo(),$causa->getId(),$causa->getAcreedor(),$causa->getDeudor(),$causa->getDeudaMonto());
			break;
		case 2:
			echo sprintf($carta->getCuerpo(),$causa->getDeudor(),$causa->getDeudor()->getDomicilio(1)->getDireccionParaReporte(),$causa->getId(),$causa->getAcreedor(),$causa->getDeudaMonto());
			break;
		default:
			echo "NO HAY CARTA";
			break;
	endswitch;
?>
</div>
<div id="footer-carta">
	<table>
		<tr>
			<td>LEONARDO &Aacute;VILA SANDOVAL</td>
		</tr>
		<tr>
			<td>Abogado</td>
		</tr>
	</table>
</div>
