<?php use_helper('Javascript') ?>
<div id="reporte-causa">
<h2><?php echo 'Reporte para: '.$causa->getAcreedor().'<br />Causa: "'.$causa.'"' ?></h2>
	<table style="margin: 10px auto;">
			<tbody>
				<tr>
	      			<th>Materia:</th>
	      			<td><?php echo $causa->getMateria() ?></td>
	      			<th>Competencia:</th>
	      			<td><?php echo $causa->getCompetencia() ?></td>
	    		</tr>
	    		<tr>
	      			<th>Fecha de Inicio:</th>
	      			<td><?php echo Funciones::fecha2($causa->getFechaInicio('d-m-Y')) ?></td>
	      			<th>Fecha de T&eacute;rmino:</th>
	      			<td><?php echo $causa->getFechaTermino('d-m-Y') ? Funciones::fecha2($causa->getFechaTermino('d-m-Y')) : 'No Estipulada' ?></td>
	    		</tr>
	    		<tr>
	      			<th>Periodo:</th>
	      			<td><?php echo $causa->getPeriodoCausaActual()->getPeriodo() ?></td>
	      			<th>&Uacute;ltima Diligencia:</th>
	      			<?php if($causa->getUltimaDiligencia()):?>
	      				<td><?php echo $causa->getUltimaDiligencia()->getDescripcion()?></td>
	    			<?php else:?>
	    				<td class="aviso"><?php echo 'NO POSEE DILIGENCIAS'?></td>
	    			<?php endif;?>
	    		</tr>
	    		<tr>
	    			<th>Cuant&iacute;a:</th>
	      			<td>$ <?php echo $causa->getCuantia()?></td>
	      			<th>Tribunal Actual:</th>
	      			<td><?php echo $causa->getTribunalActual()?></td>
	      		</tr>
	      		<tr>
	      			<th class="tddestacado" colspan="4">Documentos</th>
	      		</tr>
	      		<?php if(count($causa->getDocumentos()) > 0): ?>
	      		<?php $documentos = $causa->getDocumentos() ?>
	      		<tr>
	      			<td colspan="4" style="padding: 0;">
	      				<table style="margin: 0 auto; width: 686px;">
	      					<thead>
	      						<tr>
	      							<th class="tddestacado">Tipo</th>
	      							<th class="tddestacado">Emisi&oacute;n</th>
	      							<th class="tddestacado">Vencimiento</th>
	      							<th class="tddestacado">Monto</th>
	      							<th class="tddestacado">N&uacute;mero</th>
	      							<th class="tddestacado">Descripci&oacute;n</th>
	      						</tr>
	      					</thead>
	      					<tbody>
	      					<?php foreach($documentos as $documento): ?>
	      					<tr>
	      						<td><?php echo $documento->getTipoDocumento()?></td>
	      						<td><?php echo Funciones::fecha2($documento->getFechaEmision('d-m-Y'))?></td>
	      						<td><?php echo Funciones::fecha2($documento->getFechaVencimiento('d-m-Y'))?></td>
	      						<td>$ <?php echo $documento->getMonto()?></td>
	      						<td><?php echo $documento->getNroDocumento()?></td>
	      						<td><?php echo $documento->getDescripcion()?></td>
	      					</tr>
	      					<?php endforeach; ?>
	      					</tbody>
	      				</table>
	      			</td>
	      		</tr>
	      		<?php else: ?>
	      		<tr>
	      			<td colspan="4" class="aviso">ESTA CAUSA NO POSEE DOCUMENTOS</td>
	      		</tr>
	      		<?php endif; ?>
	      		<tr>
	      			<th class="tddestacado" colspan="4">&Uacute;ltimas diligencias</th>
	      		</tr>
	      		<tr>
				<td colspan="4" style="padding: 0;">
				<?php if(count($diligenciaList) > 0):?>
					<table style="margin: 0 auto; width: 686px;">
						<thead>
							<tr>
								<th class="tddestacado">Fecha</th>
								<th class="tddestacado">Descripci&oacute;n</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($diligenciaList as $diligencia):?>
								<tr>
									<td><?php echo Funciones::fecha2($diligencia->getFecha('d-m-Y')); ?></td>
									<td><?php echo $diligencia->getDescripcion(); ?></td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				<?php else: ?>	
					<strong>NO HAY DILIGENCIAS EN ESTA CAUSA</strong>	
				<?php endif; ?>
				</td>
			</tr>
			</tbody>
		</table>
</div>