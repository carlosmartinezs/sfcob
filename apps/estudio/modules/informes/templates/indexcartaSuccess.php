<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Cartas</h1>
<ul class="menu-modulo">
	<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
	<li>
		<?php echo link_to_remote('Redactar Carta', array(
    		'update' => 'acciones_carta',
    		'url'    => 'informes/redactarcarta',
		    'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
		)) ?></li>
	<?php endif; ?>
	<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
	<li>
		<?php echo link_to_remote('Visualizar Carta Enviada', array(
    		'update' => 'acciones_carta',
    		'url'    => 'informes/searchformato?accion=cartaenviada',
			'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
		)) ?></li>
	<?php endif; ?>
	<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
	<li>
		<?php echo link_to_remote('Visualizar Formato de Carta', array(
    		'update' => 'acciones_carta',
    		'url'    => 'informes/listformatocarta',
			'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_carta"  style="clear: right"></div>