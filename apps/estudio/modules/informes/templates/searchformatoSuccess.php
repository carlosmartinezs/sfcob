<?php use_helper('Javascript') ?>
<h2>
        <?php 	switch($accion):
					case 'cartaenviada':
						echo "Buscar Carta Enviada";
						$return = 'informes/indexcarta';
						break;
					case 'escritoenviado':
						echo "Buscar Escrito Enviado";
						$return = 'informes/indexescrito';
						break;
					default:
						echo "Buscar";
						$return = 'informes/index';
						break;;
				endswitch;
        ?>
</h2>
<div id="form_search_formato_enviado">
	<?php echo form_remote_tag(array(
    	'update'   	=> 'list-search-formato-enviado',
    	'url'      	=> 'informes/searchformatoupdate?accion='.$accion,
		'loading'	=> visual_effect('appear','loading'),
		'complete'	=> visual_effect('appear','list-search-formato-enviado').
						visual_effect('highlight','list-search-formato-enviado',array('duration' => 1.0)).
						visual_effect('fade','loading'),
	))?>
  <table>
    <tfoot>
      	<tr>
      		<td class="tdcentrado" colspan="2">
      		<input type="submit" value="Buscar" class="buscar boton_con_imagen"/>
      			<?php echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> $return,
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
      			),array('class' => 'cancelar boton_con_imagen')); ?>
        		
      		</td>
    	</tr>
    </tfoot>
    <tbody>
      <tr>
      	<th>Tipo <?php echo ($accion == 'cartaenviada' ? 'Carta':'Escrito')?></th>
      	<td><?php echo $form['id'] ?></td>
      </tr>
      <tr>
      	<th>Periodo</th>
      	<td><?php echo $form['periodo'] ?>
      	</td>
      </tr>
      <tr>
      	<th>Deudor</th>
      	<td><?php echo $form['id_deudor'] ?>
      	<div id="deudor">
      	<input type="text" name="deudor_id" disabled="disabled"></input>
          &nbsp;<?php echo button_to_remote('Buscar', array(
          				'update'	=> 'list-search-deudor',
          				'url'		=> 'administracion/searchdeudor?accion=buscarcarta',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('appear', 'list-search-deudor').
          							   visual_effect('highlight','list-search-deudor',array('duration' => 1.0)).
          							   visual_effect('fade','loading', array('duration' => 0)), 
          				),array('class' => 'buscar boton_con_imagen')); ?>
          </div>     	
      	</td>
      </tr>
      <tr>
      	<th>Desde</th>
      	<td><?php echo $form['desde'] ?></td>
      </tr>
      <tr>
      	<th>Hasta</th>
      	<td><?php echo $form['hasta'] ?><input type="hidden" name="tipo_formato" value="<?php echo $accion ?>"/></td>
      </tr>
    </tbody>
  </table>
</form>
</div>
<div id="list-search-deudor" class="pop-up" style="display:none"></div>
<div id="list-search-formato-enviado" class="pop-up" style="display:none"></div>