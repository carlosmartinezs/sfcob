<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Deudores</h1>
<ul class="menu-modulo">
	<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
	<li>
		<?php if($sf_user->hasCredential('abogado')):?>
		<?php echo link_to_remote('Ingresar Deudor', array(
    		'update' => 'acciones_deudor',
    		'url'    => 'administracion/createdeudor',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
		<?php endif;?>
	<li>
		<?php if($sf_user->hasCredential('abogado')):?>
		<?php echo link_to_remote('Editar Deudor', array(
    		'update' => 'acciones_deudor',
    		'url'    => 'administracion/searchdeudor?accion=edit',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
		<?php endif;?>
	<li>
		<?php echo link_to_remote('Visualizar Deudor', array(
    		'update' => 'acciones_deudor',
    		'url'    => 'administracion/searchdeudor?accion=show',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')):?>
		<?php echo link_to_remote('Ingresar Domicilio', array(
    		'update' => 'acciones_deudor',
    		'url'    => 'administracion/searchdeudor?accion=creardomicilio',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
		<?php endif;?>
	<li>
		<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')):?>
		<?php echo link_to_remote('Editar Domicilio', array(
    		'update' => 'acciones_deudor',
    		'url'    => 'administracion/searchdeudor?accion=editardomicilio',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
		<?php endif;?>
	<li>
		
		<?php echo link_to_remote('Ver Domicilio', array(
    		'update' => 'acciones_deudor',
    		'url'    => 'administracion/searchdeudor?accion=verdomicilio',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_deudor"  style="clear: right"></div>