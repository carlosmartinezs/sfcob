<?php use_helper('Javascript') ?>
<h2>Ver Domicilio: <?php echo $deudor_domicilio->getNombreDeudor() ?></h2>
<table>
 <tfoot>
      <tr>
        <td class="tdcentrado" colspan="4">
          <?php if (!$deudor_domicilio->isNew() && ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') )): 
          			         					
          			echo button_to_remote('A&ntilde;adir Tel&eacute;fono', array(
          				'update'	=> 'acciones_deudor',
          				'url'		=> 'administracion/edittelefono?iddeudordomicilio='.$deudor_domicilio->getId(),
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
          				'complete'	=> visual_effect('fade','loading',array('duration' => 0)),
          				),array('class' => 'anadir boton_con_imagen'));
          				
          			if(count($deudor_domicilio->getTelefonoDomicilios()) > 0):
	          				echo button_to_remote('Editar Tel&eacute;fonos', array(
	          				'update'	=> 'list-search-telefono',
	          				'url'		=> 'administracion/searchtelefonodomicilioupdate?accion=edittelefono&iddeudordomicilio='.$deudor_domicilio->getId(),
	          				'loading'	=> visual_effect('appear','loading',array('duration' => 0)),
	          				'complete'	=> visual_effect('appear','list-search-telefono').
											visual_effect('highlight','list-search-telefono',array('duration' => 1.0)).
											visual_effect('fade','loading'),
							),array('class' => 'editar boton_con_imagen')); 
          			endif;

          			echo button_to_remote('Editar Domicilio', array(
          				'update'	=> 'acciones_deudor',
          				'url'		=> 'administracion/editdeudordomicilio?iddeudordomicilio='.$deudor_domicilio->getId().'&iddeudor='.$deudor_domicilio->getDeudorId(),
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
          				'complete'	=> visual_effect('fade','loading',array('duration' => 0)),
          				),array('class' => 'editar boton_con_imagen'));
						
          			echo button_to_remote('Borrar Domicilio', array(
    						'update'   => 'modulo',
    						'url'      => 'administracion/deletedeudordomicilio?id='.$deudor_domicilio->getId(),
    						'confirm'  => 'Est&aacute; seguro que desea eliminar el domicilio del deudor?',
            				'loading'  => visual_effect('appear','loading'),
							'complete' => visual_effect('fade','loading')."alert('Se ha eliminado el domicilio')",
          					),array('class' => 'eliminar boton_con_imagen')); 
          	endif;?>
          <?php echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'administracion/indexdeudor',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)), 
               ),array('class' => 'cancelar boton_con_imagen')); ?>
        </td>
      </tr>
    </tfoot>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="4">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Deudor:</th>
      <td><?php echo $deudor_domicilio->getNombreDeudor() ?></td>
    </tr>
    <tr>
      <th>Calle:</th>
      <td><?php echo $deudor_domicilio->getCalle() ?></td>
    </tr>
    <tr>
      <th>N&uacute;mero:</th>
      <td><?php echo $deudor_domicilio->getNumero() ?></td>
    </tr>
    <tr>
      <th>Departamento:</th>
      <td><?php echo $deudor_domicilio->getDepto() ?></td>
    </tr>
    <tr>
      <th>Villa / Poblaci&oacute;n:</th>
      <td><?php echo $deudor_domicilio->getVilla() ?></td>
    </tr>
    <tr>
      <th>Comuna:</th>
      <td><?php echo $deudor_domicilio->getComuna() ?></td>
    </tr>
    <tr>
      <th>Provincia:</th>
      <td><?php echo $deudor_domicilio->getComuna()->getCiudad() ?></td>
    </tr>
    <tr>
      <th>Regi&oacute;n:</th>
      <td><?php echo $deudor_domicilio->getComuna()->getCiudad()->getRegion() ?></td>
    </tr>
    <tr>
      <th>Tel&eacute;fonos:</th>
      <td><?php echo $deudor_domicilio->getTelefono() ?>
      <?php if(count($telefonos = $deudor_domicilio->getTelefonoDomicilios()) > 0): ?>
      <?php foreach($telefonos as $telefono): echo ' ,'.$telefono; endforeach; ?>
      <?php 
      	/*echo button_to_function(
      		'Ver Tel&eacute;fonos',
      		"
      		Effect.toggle('lista-fonos-domicilio','slide',{duration: 0.5});
      		
      		if($('boton_toggle').value == 'Ver Tel&eacute;fonos')
      		{
      			$('boton_toggle').value = 'Ocultar Tel&eacute;fonos';
      		}
      		else
      		{
      			$('boton_toggle').value = 'Ver Tel&eacute;fonos';
      		};
      		",
      		array('id'=>'boton_toggle','class' => 'detalles boton_con_imagen')
      	)*/
      ?>
      <?php /*echo button_to_remote('Ver Tel&eacute;fonos', array(
          				'update'	=> 'list-search-telefono',
          				'url'		=> 'administracion/searchtelefonodomicilioupdate?accion=showtelefono&iddeudordomicilio='.$deudor_domicilio->getId(),
          				'loading'	=> visual_effect('appear','loading',array('duration' => 0)),
          				'complete'	=> visual_effect('appear','list-search-telefono').
										visual_effect('highlight','list-search-telefono',array('duration' => 1.0)).
										visual_effect('fade','loading'),
						),array('class' => 'detalles boton_con_imagen'));*/ ?>
	  <?php endif; ?>
	  </td>
    </tr>
   <!--  <tr>
    	<td style="padding:0;">
    		<div id="lista-fonos-domicilio" style="display:none;">
		    	<div>
		    	<?php // foreach($telefonos as $telefono): echo ' ,'.$telefono; endforeach;?>
		    	</div>
		    </div>
    	</td>
    </tr>--> 
  </tbody>
</table>
<div id="list-search-telefono" class="pop-up" style="display:none"></div>
<!-- <div id="list-search-telefono" class="pop-up" style="display:none"></div> -->