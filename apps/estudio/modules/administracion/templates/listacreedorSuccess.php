<?php use_helper('Javascript') ?>
<div>
<h2>Lista de Acreedores</h2>
<table>
  <thead>
    <tr>
    <th class="tddestacado">N&ordm; Interno</th>
      <th class="tddestacado">Rut</th>
      <th class="tddestacado">Nombre</th>
      <th class="tddestacado">Regi&oacute;n</th>
		<th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  	<?php if(count($acreedorList) > 0):?>
    <?php foreach ($acreedorList as $acreedor): ?>
    <tr>
      <td><?php echo $acreedor->getId() ?></td>
      <td><?php echo $acreedor->getRutAcreedor() ?></td>
      <td><?php echo $acreedor ?></td>
      <td><?php echo $acreedor->getComuna()->getCiudad()->getRegion() ?></td>
      <td class="tdcentrado"><?php switch($accion):
					case 'show':
						echo button_to_remote('Detalles', array(
							'update'	=> 'acciones_acreedor',
							'url'		=> 'administracion/showacreedor?id='. ($accion == 'reportecausas' ? '' : $acreedor->getId()),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
											visual_effect('fade','list-search-acreedor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						), array('class' => 'detalles boton_con_imagen'));
      					break;
					case 'edit':
						echo button_to_remote('Editar', array(
							'update'	=> 'acciones_acreedor',
							'url'		=> 'administracion/editacreedor?id='. ($accion == 'reportecausas' ? '' : $acreedor->getId()),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
											visual_effect('fade','list-search-acreedor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'editar boton_con_imagen'));
						break;
					case 'crearcontrato':
						if($acreedor->tieneContratoActivo()){
							echo button_to_remote('Crear Contrato', array(
								'update'	=> 'acciones_acreedor',
								'url'		=> 'administracion/editcontrato?idacreedor='. ($accion == 'reportecausas' ? '' : $acreedor->getId()),
								'loading'	=> visual_effect('appear','loading').
												visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
												visual_effect('fade','list-search-acreedor', array('duration' => 0.5)),
								'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
								'confirm' => "".$acreedor." ya posee un contrato vigente. Al guardar un contrato nuevo, el existente quedar&aacute; inactivo y las nuevas causas ser&aacute;n asociadas al nuevo contrato. Esta acci&oacute;n es irreversible S&Oacute;LO si se guarda el nuevo contrato. Desea continuar?",
							),array('class' => 'anadir boton_con_imagen'));
						}else{
							echo button_to_remote('Crear Contrato', array(
								'update'	=> 'acciones_acreedor',
								'url'		=> 'administracion/editcontrato?idacreedor='. ($accion == 'reportecausas' ? '' : $acreedor->getId()),
								'loading'	=> visual_effect('appear','loading').
												visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
												visual_effect('fade','list-search-acreedor', array('duration' => 0.5)),
								'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => 'anadir boton_con_imagen'));
						}
						
						break;
					case 'vercontrato':
						if($acreedor->tieneContratoActivo()){
							echo button_to_remote('Ver Contrato', array(
								'update'	=> 'acciones_acreedor',
								'url'		=> 'administracion/showcontrato?idacreedor='. ($accion == 'reportecausas' ? '' : $acreedor->getId()),
								'loading'	=> visual_effect('appear','loading').
												visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
												visual_effect('fade','list-search-acreedor', array('duration' => 0.5)),
								'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => 'detalles boton_con_imagen'));
						}else{
							echo button_to_function('Ver Contrato',"alert('Este acreedor no posee contrato')",array('class' => 'detalles boton_con_imagen'));
						}
						break;
					case 'desactivacontrato':
						if($acreedor->tieneContratoActivo()){
							echo button_to_remote('Desactivar Contrato', array(
								'update'	=> 'acciones_acreedor',
								'url'		=> 'administracion/desactivarcontrato?idacreedor='. ($accion == 'reportecausas' ? '' : $acreedor->getId()),
								'confirm'	=> "Est&aacute; seguro que desea desactivar el contrato?",
								'loading'	=> visual_effect('appear','loading').
												visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
												visual_effect('fade','list-search-acreedor', array('duration' => 0.5)),
								'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => 'eliminar boton_con_imagen'));
						}else{
							echo button_to_function('Desactivar Contrato',"alert('Este acreedor no posee contrato')",array('class' => 'eliminar boton_con_imagen'));
						}
						break;
					case 'reportecausas':
						if(count($acreedor->getCausas()) > 0)
						{
							echo button_to_remote('Reporte de Causas', array(
								'update'	=> 'acciones_informe',
								'url'		=> 'informes/reportecausa?idacreedor='. $acreedor->getId().
												($estados[0] ? '&activa=1' : '').
												($estados[1] ? '&terminada=1' : '').
												($estados[2] ? '&castigada=1' : '').
												($estados[3] ? '&incobrable=1' : '').
												($estados[4] ? '&suspendida=1' : '').($accion == 'reportecausas' ? '' : $acreedor->getId()),
								'loading'	=> visual_effect('appear','loading').
												visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
												visual_effect('fade','list-search-acreedor', array('duration' => 0.5)),
								'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => 'reporte boton_con_imagen'));
						}
						else
						{
							echo button_to_function('Reporte de Causas',"alert('Este acreedor no posee causas')",array('class' => 'reporte boton_con_imagen'));
						}
						
						break;
					case 'crearcausa':
						if($acreedor->tieneContratoActivo()){
							echo button_to_remote('Llevar Acreedor', array(
							'update'	=> 'contrato',
							'url'		=> 'juicios/traercontrato?idacreedor='. $acreedor->getId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
											visual_effect('fade','list-search-acreedor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => 'llevar boton_con_imagen'));
						}else{
							echo button_to_function('Llevar Acreedor',"alert('Este acreedor no posee contrato')",array('class' => 'llevar boton_con_imagen'));
						}
						break;
					case 'buscarcausaacreedor':
						if($acreedor->tieneContratoActivo()){
							echo button_to_remote('Llevar Acreedor', array(
							'update'	=> 'contrato',
							'url'		=> 'juicios/traercontrato?idacreedor='. $acreedor->getId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
											visual_effect('fade','list-search-acreedor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => 'llevar boton_con_imagen'));
						}else{
							echo button_to_function('Llevar Acreedor',"alert('Este acreedor no posee contrato')",array('class' => 'llevar boton_con_imagen'));
						}
						break;
					default : 
						break;
				endswitch;
      		?>
      </td>
    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr><td class="aviso" colspan="5">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td></tr>
    <?php endif;?>
  </tbody>
</table>

<?php echo button_to_function('Cerrar ventana',
	visual_effect('appear','loading').
	visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
	visual_effect('fade','list-search-acreedor', array('duration' => 0.5)).
	visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cerrar boton_con_imagen'));
?>
</div>