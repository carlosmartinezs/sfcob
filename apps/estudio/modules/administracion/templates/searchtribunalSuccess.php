<?php use_helper('Javascript', 'Object') ?>
<div id="form_search_tribunal">
<h2>
        <?php 	switch($accion):
					case 'edit':
						echo "Editar Tribunal: Buscar";
						break;
					case 'show':
						echo "Ver Tribunal: Buscar";
						break;
					case 'crearcausa':
						echo "Ingresar Causa: Buscar Tribunal";
						break;
					case 'editcausa':
						echo "Editar Causa: Buscar Tribunal";
						break;
					default:
						echo "Buscar Tribunal";
						break;;
				endswitch;
        ?>
</h2>

	<?php echo form_remote_tag(array(
    	'update'   	=> 'list-search-tribunal',
    	'url'      	=> 'administracion/searchtribunalupdate',
		'loading'	=> visual_effect('appear','loading'),
		'complete'	=> visual_effect('appear','list-search-tribunal').
						visual_effect('highlight','list-search-tribunal',array('duration' => 1.0)).
						visual_effect('fade','loading'),
	))?>
  <table>
    <tfoot>
      	<tr>
      		<td class="tdcentrado" colspan="4">
      		<input type="submit" value="Buscar" class="buscar boton_con_imagen"/>
      			<?php 	switch($accion):
					case 'edit':
						echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'administracion/indextribunal',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
						),array('class' => 'cancelar boton_con_imagen'));
						break;
					case 'show':
						echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'administracion/indextribunal',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
						),array('class' => 'cancelar boton_con_imagen'));
						break;
					case 'crearcausa':break;
					case 'editcausa':break;
					default:break;;
				endswitch; ?>
        		
      		</td>
    	</tr>
    </tfoot>
    <tbody>
      <tr>
        <th><label for="tribunal_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
          <?php echo $form['accion'] ?>
        </td>
      </tr>
      <tr>
      <th><label for="region_id">Regi&oacute;n</label><?php echo ' '.'*'; ?></th>
        <td colspan="5">
		  <?php
 				$lista = array();
 				$regionList = RegionPeer::doSelect(new Criteria());
  				foreach ($regionList as $region)
  				{
    				$lista[$region->getId()] = $region->getNombre();
  				} 
  				$this->lista = $lista;
  				echo select_tag('region_id',
  				options_for_select($lista,
  					'getId',array('include_custom' => '-- Seleccione Regi&oacute;n --')),array(
    				'onchange' => remote_function(array(
      				'update' => "ciudades-segun-region",
      				'with'   => "'region_id='+value",
  					'script' => true,
      				'url'    => '/administracion/creaselectciudad',
      				'loading'	=> visual_effect('appear','loading'),
					'complete'	=> visual_effect('fade','loading'),
    			))));
		    ?>
        </td>
       </tr>
       <tr>
        <th><label for="acreedor_ciudad_id">Provincia</label><?php echo ' '.'*'; ?></th>
        <td>
          <div id="ciudades-segun-region">
          <?php echo input_tag('acreedor_ciudad_id','',array('readonly' => 'readonly')); ?>
          </div>
        </td>
        <th><label for="comuna_id">Comuna</label><?php echo ' '.'*'; ?></th>
        <td>
          <div id="comunas-segun-ciudades">
          <?php echo input_tag('comuna','',array('readonly' => 'readonly'));?>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</form>
<?php 	switch($accion):
			case 'edit':break;
			case 'show':break;
			case 'crearcausa':
				echo button_to_function('Cerrar ventana',
					visual_effect('appear','loading').
					visual_effect('highlight','list-search-tribunal', array('duration' => 0.5)).
					visual_effect('fade','list-search-tribunal', array('duration' => 0.5)).
					visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cerrar boton_con_imagen'));
				break;
			case 'editcausa':
				echo button_to_function('Cerrar ventana',
					visual_effect('appear','loading').
					visual_effect('highlight','list-search-tribunal', array('duration' => 0.5)).
					visual_effect('fade','list-search-tribunal', array('duration' => 0.5)).
					visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cerrar boton_con_imagen'));
				break;
			default:break;;
		endswitch; ?>
</div>
<div id="list-search-tribunal" class="pop-up" style="display:none"></div>