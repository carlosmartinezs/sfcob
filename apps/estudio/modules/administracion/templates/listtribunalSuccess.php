<?php use_helper('Javascript')?>
<div>
<h2>Lista de Tribunales</h2>
<table>
  <thead>
    <tr>
      <th class="tddestacado">Nombre</th>
      <th class="tddestacado">Ciudad</th>
	  <th class="tddestacado">Provincia</th>
	  <th class="tddestacado">Regi&oacute;n</th>
	  <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  <?php if(count($tribunalList) > 0):?>
    <?php foreach ($tribunalList as $tribunal): ?>
    <tr>
      <td><?php echo $tribunal->getNombre() ?></td>
      <td><?php echo $tribunal->getComuna()?></td>
      <td><?php echo $tribunal->getComuna()->getCiudad() ?></td>
      <td><?php echo $tribunal->getComuna()->getCiudad()->getRegion() ?></td>
      <td class="tdcentrado"><?php switch($accion):
					case 'show':
      					echo button_to_remote('Detalles', array(
      						'update' => 'acciones_tribunal',
      						'url' => 'administracion/showtribunal?id='.$tribunal->getId(),
      						'loading'=> visual_effect('appear','loading').
										visual_effect('highlight','list-search-tribunal', array('duration' => 0.5)).
										visual_effect('fade','list-search-tribunal', array('duration' => 0.5)),
							'complete' => visual_effect('fade','loading', array('duration' => 1.0)),
      					),array('class' => 'detalles boton_con_imagen'));
      					break;
					case 'edit':
						echo button_to_remote('Editar', array(
      						'update' => 'acciones_tribunal',
      						'url' => 'administracion/edittribunal?id='.$tribunal->getId(),
      						'loading'=> visual_effect('appear','loading').
										visual_effect('highlight','list-search-tribunal', array('duration' => 0.5)).
										visual_effect('fade','list-search-tribunal', array('duration' => 0.5)),
							'complete' => visual_effect('fade','loading', array('duration' => 1.0)),
      					),array('class' => 'editar boton_con_imagen')); 
						break;
					case 'crearcausa':
						echo button_to_remote('Llevar Tribunal', array(
      						'update' => 'tribunal',
      						'url' => 'juicios/traertribunal?idtribunal='.$tribunal->getId(),
      						'loading'=> visual_effect('appear','loading').
										visual_effect('highlight','list-search-tribunal', array('duration' => 0.5)).
										visual_effect('fade','list-search-tribunal', array('duration' => 0.5)),
							'complete' => visual_effect('fade','loading', array('duration' => 1.0)),
      					),array('class' => 'llevar boton_con_imagen'));
      					break;
					case 'editcausa':
						echo button_to_remote('Llevar Tribunal', array(
      						'update' => 'causa_tribunal',
      						'url' => 'juicios/traertribunaledit?idtribunal='.$tribunal->getId(),
      						'loading'=> visual_effect('appear','loading').
										visual_effect('highlight','list-search-tribunal', array('duration' => 0.5)).
										visual_effect('fade','list-search-tribunal', array('duration' => 0.5)),
							'complete' => visual_effect('fade','loading', array('duration' => 1.0)),
      					),array('class' => 'llevar boton_con_imagen'));
      					break;
					default : 
						echo "Otra accion";
				endswitch;
      		?>
     </td>
    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr><td class="aviso" colspan="5">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td></tr>
    <?php endif;?>
  </tbody>
</table>
<?php echo button_to_function('Cerrar ventana',
	visual_effect('appear','loading').
	visual_effect('highlight','list-search-tribunal', array('duration' => 0.5)).
	visual_effect('fade','list-search-tribunal', array('duration' => 0.5)).
	visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cerrar boton_con_imagen'));
?>
</div>