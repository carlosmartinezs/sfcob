<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Acreedores</h1>
<ul class="menu-modulo">
	<?php if($sf_user->hasCredential('abogado')): ?>
	<li>
		<?php echo link_to_remote('Ingresar Acreedor', array(
    		'update' => 'acciones_acreedor',
    		'url'    => 'administracion/createacreedor',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
	<li>
		<?php echo link_to_remote('Editar Acreedor', array(
    		'update' => 'acciones_acreedor',
    		'url'    => 'administracion/searchacreedor?accion=edit',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php echo link_to_remote('Visualizar Acreedor', array(
    		'update' => 'acciones_acreedor',
    		'url'    => 'administracion/searchacreedor?accion=show',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php echo link_to_remote('Crear Contrato', array(
    		'update' => 'acciones_acreedor',
    		'url'    => 'administracion/searchacreedor?accion=crearcontrato',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php echo link_to_remote('Ver Contrato', array(
    		'update' => 'acciones_acreedor',
    		'url'    => 'administracion/searchacreedor?accion=vercontrato',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php echo link_to_remote('Desactivar Contrato', array(
    		'update' => 'acciones_acreedor',
    		'url'    => 'administracion/searchacreedor?accion=desactivacontrato',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_acreedor" style="clear: right"></div>