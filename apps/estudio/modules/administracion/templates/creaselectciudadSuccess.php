<?php 
use_helper('DateForm');
use_helper('Object');	
use_helper('Javascript');

 $lista = array();
 $ciudadList = CiudadPeer::doSelectByRegion($region_id);
  foreach ($ciudadList as $ciudad)
  {
    $lista[$ciudad->getId()] = $ciudad->getNombre();
  } 
  $this->lista = $lista;
  
	if($region_id){
		  	echo select_tag('acreedor_ciudad_id',
  					options_for_select($lista,
  						'getId',array('include_custom' => '-- Seleccione Provincia --')),array(
    					'onchange' => remote_function(array(
      					'update' => "comunas-segun-ciudades",
      					'with'   => "'ciudad_id=' + value",
  						'script' => true,
      					'url'    => '/administracion/creaselectcomuna',
      					'loading'	=> visual_effect('appear','loading'),
						'complete'	=> visual_effect('fade','loading'),
    				))));
		}else{
			 echo select_tag('acreedor_ciudad_id', options_for_select(array(
  				'selected'  => 'Seleccione',
				), 'selected'));
		}
?>