<?php use_helper('Javascript', 'Object', 'DateForm') ?>
<?php $acreedor = $form->getObject() ?>
<?php if($pathinfo == '/administracion/createacreedor' ): ?>
<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_acreedor',
    'url'      	=> 'administracion/updateacreedor'.(!$acreedor->isNew() ? '?id='.$acreedor->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading').
					visual_effect('appear','ingresa-contrato'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
<h2>Ingresar Acreedor</h2>
<?php else: ?>
	<?php if(isset($perfil) && $perfil):?>
		<?php echo form_remote_tag(array(
		    'update'   	=> 'acciones_perfil',
		    'url'      	=> 'administracion/updateacreedor'.(!$acreedor->isNew() ? '?id='.$acreedor->getId() : ''),
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
		    'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
		))?>
		<h2>Editar Datos</h2>
	<?php else:?>
		<?php echo form_remote_tag(array(
		    'update'   	=> 'acciones_acreedor',
		    'url'      	=> 'administracion/updateacreedor'.(!$acreedor->isNew() ? '?id='.$acreedor->getId() : ''),
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
		    'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
		))?>
		<h2>Editar Acreedor: <?php echo $acreedor?></h2>
	<?php endif;?>
<?php endif; ?>
<div><?php //echo $form->renderGlobalErrors() ?></div>
<table>
    <tbody>
      <tr>
        <th><label for="acreedor_clase_giro_id">Clase de Giro *</label></th>
        <td colspan="5">
		  <?php 
		  		echo $form['clase_giro']->renderError();
		  		$lista2 = array();
 				$clasegiroList = ClaseGiroPeer::doSelect(new Criteria());
  				foreach ($clasegiroList as $clasegiro)
  				{
    				$lista2[$clasegiro->getId()] = $clasegiro->getNombre();
  				} 
  				
  				$this->lista2 = $lista2;
  				
  				if(isset($editar) && $editar == true){
  					echo select_tag('clase_giro_id',
  					options_for_select($lista2,
  										$claseGiroId,
  										array('include_custom' => '-- Seleccione Clase de Giro --')),
  					array('onchange' => remote_function(array(
		      					'update' => "giros-segun-clase",
		      					'with'   => "'clase_giro_id='+value",
		      					'url'    => '/administracion/creaselectgiro',
		      					'loading'	=> visual_effect('appear','loading'),
								'complete'	=> visual_effect('fade','loading'),))
  							));
  				}else{
  					echo select_tag('clase_giro_id',
  					options_for_select($lista2,
  										'getId',
  										array('include_custom' => '-- Seleccione Clase de Giro --')),
  					array('onchange' => remote_function(array(
		      					'update' => "giros-segun-clase",
		      					'with'   => "'clase_giro_id='+value",
		      					'url'    => '/administracion/creaselectgiro',
		      					'loading'	=> visual_effect('appear','loading'),
								'complete'	=> visual_effect('fade','loading'),))
  							));
  				}
	  			 ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_giro_id">Giro</label><?php echo ' '.'*'; ?></th>
        <td colspan="5">
          <div id="giros-segun-clase">
          <?php echo $form['giro_id']->renderError();
          echo $form['giro_id'];
				if(isset($editar) && $editar == true){
					echo input_hidden_tag('giro',$giroId);
					echo input_tag('giroName',$giroName,array('readonly' => 'readonly','size' => 105));
				}else{
					echo input_tag('giro','',array('readonly' => 'readonly'));
				}
          ?>
          </div>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_rut_acreedor"><strong>Rut * </strong></label><span class="small-font">(Ej: 12.345.678-9)</span></th>
        <td>
        <?php if($errorRutAc == true){
        	    echo '<ul class="error_list">
    					<li>Rut Inv&aacute;lido.</li>
  					  </ul>';
              }else{
              	echo $form['rut_acreedor']->renderError();
              }
        ?>
        	<?php echo $form['rut_acreedor'] ?> 
        </td>
        <th><label for="acreedor_nombres">Nombres</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['nombres']->renderError() ?>
          <?php echo $form['nombres'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_apellido_paterno">Apellido Paterno</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['apellido_paterno']->renderError() ?>
          <?php echo $form['apellido_paterno'] ?>
        </td>
        <th><label for="acreedor_apellido_materno">Apellido Materno</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['apellido_materno']->renderError() ?>
          <?php echo $form['apellido_materno'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_direccion">Direcci&oacute;n</label><?php echo ' '.'*'; ?></th>
        <td colspan="5">
          <?php echo $form['direccion']->renderError() ?>
          <?php echo $form['direccion'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_region_id">Regi&oacute;n</label><?php echo ' '.'*'; ?></th>
        <td colspan="5">
		  <?php echo $form['region']->renderError();
 				$lista = array();
 				$regionList = RegionPeer::doSelect(new Criteria());
  				foreach ($regionList as $region)
  				{
    				$lista[$region->getId()] = $region->getNombre();
  				} 
  					$this->lista = $lista;
  					if(isset($editar) && $editar == true){
  						echo select_tag('region_id',
	  					options_for_select($lista,
	  						$regionId,array('include_custom' => '-- Seleccione Regi&oacute;n --')),array(
	    					'onchange' => remote_function(array(
	      					'update' => "ciudades-segun-region",
	      					'with'   => "'region_id='+value",
	  						'script' => true,
	      					'url'    => '/administracion/creaselectciudad',
	      					'loading'	=> visual_effect('appear','loading'),
							'complete'	=> visual_effect('fade','loading'),
	    				))));
  					}else{
	  					echo select_tag('region_id',
	  					options_for_select($lista,
	  						'getId',array('include_custom' => '-- Seleccione Regi&oacute;n --')),array(
	    					'onchange' => remote_function(array(
	      					'update' => "ciudades-segun-region",
	      					'with'   => "'region_id='+value",
	  						'script' => true,
	      					'url'    => '/administracion/creaselectciudad',
	      					'loading'	=> visual_effect('appear','loading'),
							'complete'	=> visual_effect('fade','loading'),
	    				))));
  					}
		    ?>
        </td>
      </tr>
      <tr>
      	<th><label for="acreedor_ciudad_id">Provincia</label><?php echo ' '.'*'; ?></th>
        <td>
          <div id="ciudades-segun-region">
          <?php echo $form['ciudad']->renderError();
				if(isset($editar) && $editar == true){
					echo input_hidden_tag('acreedor_ciudad_id',$ciudadId);
					echo input_tag('ciudadName',$ciudad,array('readonly' => 'readonly'));
				}else{
					echo input_tag('ciudadName','',array('readonly' => 'readonly'));
				}
          ?>
          </div>
        </td>
        <th><label for="acreedor_comuna_id">Comuna</label><?php echo ' '.'*'; ?></th>
        <td>
          <div id="comunas-segun-ciudades">
          <?php echo $form['comuna_id']->renderError();
				if(isset($editar) && $editar == true){
					echo input_hidden_tag('comuna',$comunaId);
					echo input_tag('comunaName',$comuna,array('readonly' => 'readonly'));
				}else{
					echo input_tag('comunaName','',array('readonly' => 'readonly'));
				}
          ?>
          </div>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_telefono">Tel&eacute;fono *</label></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>
        </td>
        <th><label for="acreedor_fax">Fax</label></th>
        <td>
          <?php echo $form['fax']->renderError() ?>
          <?php echo $form['fax'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_email">Email *</label></th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_rep_legal">Representante<br />Legal</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['rep_legal']->renderError() ?>
          <?php echo $form['rep_legal'] ?>
        </td>
        <th><label for="acreedor_rut_rep_legal">Rut Representante<br />Legal</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php if($errorRutRep == true){
        	    echo '<ul class="error_list">
    					<li>Rut Inv&aacute;lido.</li>
  					  </ul>';
              }else{
              	echo $form['rut_rep_legal']->renderError();
              }
        ?>
          <?php echo $form['rut_rep_legal'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="4">
        <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
			<?php echo button_to_remote('Cancelar', array(
			'update' => 'modulo',
			'url' => 'administracion/indexacreedor',
			'loading' => visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
			),array('class' => 'cancelar boton_con_imagen'))?>
          
        </td>
      </tr>
    </tfoot>
</table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>