<?php use_helper('Javascript') ?>
<?php if(isset($perfil) && $perfil):?>
	<h2>Sus Datos</h2>
<?php else:?>
	<h2>Ver Acreedor</h2>
<?php endif;?>
<table>
  <tbody>
  	<?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="4">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
   	<?php if($sf_user->getAttribute('contrato_desactivado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('contrato_desactivado');?>
	  	<tr>
	      <td colspan="4">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Contrato desactivado.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Giro:</th>
      <td colspan="3"><?php echo $acreedor->getGiro() ?></td>
    </tr>
    <tr>
      <th>Rut:</th>
      <td colspan="3"><?php echo $acreedor->getRutAcreedor() ?></td>
    </tr>
    <tr>
      <th>Nombre:</th>
      <td colspan="3"><?php echo $acreedor->getNombreCompleto() ?></td>
    </tr>
   <tr>
      <th>Direcci&oacute;n:</th>
      <td colspan="3"><?php echo $acreedor->getDireccion() . ', '.
      	$acreedor->getComuna() . ', '.
      	$acreedor->getComuna()->getCiudad() . ', ' .
      	$acreedor->getComuna()->getCiudad()->getRegion() 
      	?></td>
   </tr>
   <tr>
      <th>Tel&eacute;fono:</th>
      <td><?php echo $acreedor->getTelefono() ?></td>
      <th>Fax:</th>
      <td><?php echo $acreedor->getFax() ?></td>
    </tr>
    <tr>
      <th>Email:</th>
      <td><?php echo $acreedor->getEmail() ?></td>
    </tr>
    <tr>
      <th>Representante<br />Legal:</th>
      <td><?php echo $acreedor->getRepLegal() ?></td>
      <th>Rut Representante<br />Legal:</th>
      <td><?php echo $acreedor->getRutRepLegal() ?></td>
    </tr>
  	<tr>
		<td class="tdcentrado" colspan="4">
	  	  <?php if(isset($perfil) && $perfil):?>
	  	  <?php else:?>
	  	  			<?php echo button_to_remote('Editar', array(
					'update'	=>'acciones_acreedor',
					'url'		=>'administracion/editacreedor?id='.$acreedor->getId(),
					'loading'=> visual_effect('appear','loading'),
					'complete' => visual_effect('fade','loading', array('duration' => 1.0)).
								visual_effect('highlight','acciones_acreedor', array('duration' => 0.5)),
					),array('class' => 'editar boton_con_imagen'))?>
	  	  <?php endif;?>
	    <?php if($acreedor->tieneContratoActivo()): ?>
			    <?php if(isset($perfil) && $perfil):?>
					<?php echo button_to_remote('Ver Contrato Vigente', array(
							'update'	=>'acciones_perfil',
							'url'		=>'administracion/showcontrato?idacreedor='.$acreedor->getId(),
							'loading'=> visual_effect('appear','loading'),
							'complete' => visual_effect('fade','loading', array('duration' => 1.0)).
							                            visual_effect('highlight','acciones_perfil', array('duration' => 0.5)),
							               ),array('class' => 'detalles boton_con_imagen'))?>
			    <?php else:?>
				    <?php echo button_to_remote('Ver Contrato Vigente', array(
							'update'	=>'acciones_acreedor',
							'url'		=>'administracion/showcontrato?idacreedor='.$acreedor->getId(),
							'loading'=> visual_effect('appear','loading'),
							'complete' => visual_effect('fade','loading', array('duration' => 1.0)).
							                            visual_effect('highlight','acciones_acreedor', array('duration' => 0.5)),
							               ),array('class' => 'detalles boton_con_imagen'))?>
			    <?php endif;?>
				<?php if($sf_user->hasCredential('abogado')):?>
  					<?php echo button_to_remote('Desactivar Contrato', array(
									'update'	=> 'acciones_acreedor',
									'url'		=> 'administracion/desactivarcontrato?idacreedor='.$acreedor->getId(),
									'confirm'	=> "Esta seguro que desea desactivar el contrato?",
									'loading'	=> visual_effect('appear','loading'),
									'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)).
													visual_effect('highlight','acciones_acreedor', array('duration' => 0.5)),
									),array('class' => 'eliminar boton_con_imagen'))?>
  				<?php endif;?>
		<?php else: ?>
	  		<?php if($sf_user->hasCredential('abogado')):?>
				<?php echo button_to_remote('Crear Contrato', array(
					'update'	=>'acciones_acreedor',
					'url'		=>'administracion/editcontrato?idacreedor='.$acreedor->getId(),
					'loading'=> visual_effect('appear','loading').
								visual_effect('highlight','ingresa-contrato', array('duration' => 0.5)),
					'complete' => visual_effect('fade','loading', array('duration' => 1.0)),
					),array('class' => 'anadir boton_con_imagen'))?>
	  		<?php endif;?>
		<?php endif; ?>
		</td>
	</tr>
  </tbody>
</table>