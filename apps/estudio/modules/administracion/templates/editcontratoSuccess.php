<?php use_helper('Javascript') ?>
<?php $contrato = $form->getObject() ?>
<h2>Crear Contrato</h2>
<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_acreedor',
    'url'      	=> 'administracion/updatecontrato?acreedorName='.$acreedorName.(!$contrato->isNew() ? '&id='.$contrato->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading').
					visual_effect('appear','ingresa-contrato'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
<div><?php echo $form->renderGlobalErrors() ?></div>
<table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="2">
			<input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
			<?php echo button_to_remote('Cancelar', array(
			'update' => 'modulo',
			'url' => 'administracion/indexacreedor',
			'loading' => visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
			),array('class' => 'cancelar boton_con_imagen'))?>
          
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <th><label for="contrato_acreedor_id">Acreedor</label></th>
        <td>
          <?php echo $form['acreedor_id']->renderError() ?>
          <?php echo $form['acreedor_id'] ?>
          <?php echo $acreedorName ?>
        </td>
      </tr>
      <tr>
        <th><label for="contrato_fecha_inicio">Fecha de Inicio *</label></th>
        <td>
          <?php echo $form['fecha_inicio']->renderError() ?>
          <?php echo $form['fecha_inicio'] ?> 
        </td>
      </tr>
      <tr>
        <th><label for="contrato_fecha_termino">Fecha de T&eacute;rmino *</label></th>
        <td>
          <?php echo $form['fecha_termino']->renderError() ?>
          <?php echo $form['fecha_termino'] ?> 
        </td>
      </tr>
      <tr>
        <th><label for="contrato_porcentaje_honorario">Porcentaje de Honorarios *</label><br />
        </th>
        <td>
          <?php echo $form['porcentaje_honorario']->renderError() ?>
          <?php echo $form['porcentaje_honorario'] ?> &#37; <span class="small-font">Para decimal use `.` (punto)</span>
        </td>
      </tr>
      <tr>
        <th><label for="contrato_estado">Estado</label></th>
        <td>
          <?php echo $form['estado']->renderError() ?>
          <?php echo $form['estado'] ?>
		  <?php echo 'Activo'?>
          <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
