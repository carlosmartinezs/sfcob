<?php use_helper('Javascript') ?>
<?php $telefono_domicilio = $form->getObject() ?>
<h2><?php echo $telefono_domicilio->isNew() ? 'Ingresar' : 'Editar' ?> Tel&eacute;fono en Domicilio</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_deudor',
    'url'      	=> 'administracion/updatetelefono?deudordomicilio='.$deudordomicilio.(!$telefono_domicilio->isNew() ? '&id='.$telefono_domicilio->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="2">
        <?php if (!$telefono_domicilio->isNew()): ?>
            <?php echo button_to_remote('Borrar Tel&eacute;fono', array(
    					'update'   => 'acciones_deudor',
    					'url'      => 'administracion/deletetelefono?id='.$telefono_domicilio->getId(),
    					'confirm'  => 'Esta seguro que desea eliminar el tel&eacute;fono del domicilio?',
            			'loading'  => visual_effect('appear','loading'),
						'complete' => visual_effect('fade','loading').
										"alert('Se ha eliminado el tel&eacute;fono')",
          		),array('class' => 'eliminar boton_con_imagen')); ?>
          <?php endif; ?>
        <input type="submit" value="Guardar" class="guardar boton_con_imagen" />
        	<?php echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'administracion/indexdeudor',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
        	),array('class' => 'cancelar boton_con_imagen')); ?>
          
          
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="telefono_domicilio_deudor_domicilio_id">Domicilio de Deudor:</label></th>
        <td>
          <?php echo $form['deudor_domicilio_id']->renderError() ?>
          <?php echo $form['deudor_domicilio_id'] ?>
          <?php echo $deudordomicilio ?>
        </td>
      </tr>
      <tr>
        <th><label for="telefono_domicilio_telefono">Tel&eacute;fono</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
