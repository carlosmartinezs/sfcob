<?php use_helper('Javascript')?>
<h2>Ver Tel&eacute;fono</h2>
<table>
 <tfoot>
 	<tr>
 		<td class="tdcentrado" colspan="2">
 		<?php 
 		echo button_to_remote('A&ntilde;adir Tel&eacute;fono', array(
          				'update'	=> 'acciones_deudor',
          				'url'		=> 'administracion/edittelefono?iddeudordomicilio='.$telefono_domicilio->getDeudorDomicilioId(),
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
          				'complete'	=> visual_effect('fade','loading',array('duration' => 0)),
          				),array('class' => 'anadir boton_con_imagen'));
          				
 		echo button_to_remote('Editar', array(
							'update'	=> 'acciones_deudor',
							'url'		=> 'administracion/edittelefono?id='.$telefono_domicilio->getId().'&iddeudordomicilio='.$telefono_domicilio->getDeudorDomicilioId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-telefono', array('duration' => 0.5)).
											visual_effect('fade','list-search-telefono', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'editar boton_con_imagen'));
		
		echo button_to_remote('Borrar', array(
    						'update'   => 'acciones_deudor',
    						'url'      => 'administracion/deletetelefono?id='.$telefono_domicilio->getId(),
    						'confirm'  => 'Est&aacute; seguro que desea eliminar el tel&eacute;fono asociado al domicilio?',
            				'loading'  => visual_effect('appear','loading'),
							'complete' => visual_effect('fade','loading').
											"alert('Se ha eliminado el tel&eacute;fono')",
          					),array('class' => 'eliminar boton_con_imagen')); 
         
        echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'administracion/indexdeudor',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)), 
               ),array('class' => 'cancelar boton_con_imagen'));
 		
 		?>
 		</td>
 	</tr>
 </tfoot>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="2">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Domicilio de Deudor:</th>
      <td><?php echo $telefono_domicilio->getDomicilio() ?></td>
    </tr>
    <tr>
      <th>Tel&eacute;fono:</th>
      <td><?php echo $telefono_domicilio->getTelefono() ?></td>
    </tr>
  </tbody>
</table>
