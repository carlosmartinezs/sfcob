<?php use_helper('Javascript', 'Object', 'DateForm') ?>
<div id="form_search_acreedor">
<h2>
        <?php 	switch($accion):
					case 'edit':
						echo "Editar Acreedor: Buscar";
						break;
					case 'show':
						echo "Ver Acreedor: Buscar";
						break;
					case 'crearcontrato':
						echo "Crear Contrato: Buscar Acreedor";
						break;
					case 'vercontrato':
						echo "Ver Contrato: Buscar Acreedor";
						break;
					case 'desactivacontrato':
						echo "Desactivar Contrato: Buscar Acreedor";
						break;
					case 'reportecausas':
						echo "Reporte de Causas: Buscar Acreedor";
						break;
					case 'crearcausa':
						echo "Ingresar Causa: Buscar Acreedor";
						break;
					case 'buscarcausaacreedor':
						echo "Buscar Causa: Buscar Acreedor";
						break;
					default:
						echo "Buscar Acreedor";
						break;
				endswitch;
        ?>
</h2>
	<?php echo form_remote_tag(array(
	    'update'   	=> 'list-search-acreedor',
	    'url'      	=> 'administracion/searchacreedorupdate',
		'loading'	=> visual_effect('appear','loading'),
		'complete'	=> visual_effect('appear','list-search-acreedor').
						visual_effect('highlight','list-search-acreedor',array('duration' => 1.0)).
						visual_effect('fade','loading'),
	))?>
  <table>
    <tfoot>
      	<tr>
      		<td class="tdcentrado" colspan="6">
      		<input type="submit" value="Buscar" class="buscar boton_con_imagen"/>
      		<?php switch($accion):
					case 'reportecausas':
						echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'informes/indexinforme',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)), 
      					),array('class' => 'cancelar boton_con_imagen'));
						break;
					case 'crearcausa':break;
					case 'buscarcausaacreedor':break;
					default:
						echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'administracion/indexacreedor',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)), 
      					),array('class' => 'cancelar boton_con_imagen'));
						break;
				endswitch;
				?>
        		
      		</td>
    	</tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="acreedor_clase_giro_id">Clase de Giro</label></th>
        <td colspan="5">
		  <?php $lista2 = array();
 				$clasegiroList = ClaseGiroPeer::doSelect(new Criteria());
  				foreach ($clasegiroList as $clasegiro)
  				{
    				$lista2[$clasegiro->getId()] = $clasegiro->getNombre();
  				} 
  				
  				$this->lista2 = $lista2;
  			
  				echo select_tag('clase_giro_id',
  				options_for_select($lista2,
  									'getId',
  									array('include_custom' => '-- Seleccione Clase de Giro --')),
  				array('onchange' => remote_function(array(
	      					'update' => "giros-segun-clase",
	      					'with'   => "'clase_giro_id='+value",
	      					'url'    => '/administracion/creaselectgiro',
	      					'loading'	=> visual_effect('appear','loading'),
							'complete'	=> visual_effect('fade','loading'),))
  						));
	  			 ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_giro_id">Giro</label></th>
        <td colspan="5">
          <div id="giros-segun-clase">
          <?php echo input_tag('giro','',array('readonly' => 'readonly'))?>
          </div>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_rut_acreedor">Rut </label><span class="small-font">(Ej: 12.345.678-9)</span></th>
        <td>
          <?php echo $form['rut_acreedor']->renderError() ?>
          <?php echo $form['rut_acreedor'] ?>
        </td>
        <th><label for="acreedor_nombres">Nombres</label></th>
        <td>
          <?php echo $form['nombres']->renderError() ?>
          <?php echo $form['nombres'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_apellido_paterno">Apellido Paterno</label></th>
        <td>
          <?php echo $form['apellido_paterno']->renderError() ?>
          <?php echo $form['apellido_paterno'] ?>
        </td>
        <th><label for="acreedor_apellido_materno">Apellido Materno</label></th>
        <td>
          <?php echo $form['apellido_materno']->renderError() ?>
          <?php echo $form['apellido_materno'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_direccion">Direcci&oacute;n</label></th>
        <td colspan="3">
          <?php echo $form['direccion']->renderError() ?>
          <?php echo $form['direccion'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_region_id">Regi&oacute;n</label></th>
        <td colspan="5">
		  <?php $lista = array();
 				$regionList = RegionPeer::doSelect(new Criteria());
  				foreach ($regionList as $region)
  				{
    				$lista[$region->getId()] = $region->getNombre();
  				} 
  				$this->lista = $lista;
  				echo select_tag('region_id',
  				options_for_select($lista,
  					'getId',array('include_custom' => '-- Seleccione Regi&oacute;n --')),array(
    				'onchange' => remote_function(array(
      				'update' => "ciudades-segun-region",
      				'with'   => "'region_id='+value",
  					'script' => true,
      				'url'    => '/administracion/creaselectciudad',
      				'loading'	=> visual_effect('appear','loading'),
					'complete'	=> visual_effect('fade','loading'),
    			))));
		    ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_ciudad_id">Provincia</label></th>
        <td>
          <div id="ciudades-segun-region">
          <?php echo input_tag('ciudadName','',array('readonly' => 'readonly'))?>
          </div>
        </td>
        <th><label for="acreedor_comuna_id">Comuna</label></th>
        <td>
          <div id="comunas-segun-ciudades">
          <?php echo input_tag('comunaName','',array('readonly' => 'readonly')) ?>
          </div>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_telefono">Tel&eacute;fono</label></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>
        </td>
        <th><label for="acreedor_fax">Fax</label></th>
        <td>
          <?php echo $form['fax']->renderError() ?>
          <?php echo $form['fax'] ?>
        </td>
        <th><label for="acreedor_email">Email</label></th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_rep_legal">Representante<br />Legal</label></th>
        <td>
          <?php echo $form['rep_legal']->renderError() ?>
          <?php echo $form['rep_legal'] ?>
        </td>
        <th><label for="acreedor_rut_rep_legal">Rut Representante<br />Legal</label></th>
        <td>
          <?php echo $form['rut_rep_legal']->renderError() ?>
          <?php echo $form['rut_rep_legal'] ?>
          <?php echo $form['accion']?>
        </td>
      </tr>
      <?php if($accion == 'reportecausas'): ?>
      <tr><td colspan="2"><strong>* Con causas en estado:</strong></td></tr>
      <tr>
        <th><label for="filtroestadocausa_activa">Activa</label></th>
        <td>
          <?php echo $formFiltroEstado['activa']->renderError() ?>
          <?php echo $formFiltroEstado['activa'] ?>
        </td>
        <th><label for="filtroestadocausa_terminada">Terminada</label></th>
        <td>
          <?php echo $formFiltroEstado['terminada']->renderError() ?>
          <?php echo $formFiltroEstado['terminada'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="filtroestadocausa_castigada">Castigada</label></th>
        <td>
          <?php echo $formFiltroEstado['castigada']->renderError() ?>
          <?php echo $formFiltroEstado['castigada'] ?>
        </td>
        <th><label for="filtroestadocausa_incobrable">Incobrable</label></th>
        <td>
          <?php echo $formFiltroEstado['incobrable']->renderError() ?>
          <?php echo $formFiltroEstado['incobrable'] ?>
        </td>
        <th><label for="filtroestadocausa_suspendida">Suspendida</label></th>
        <td>
          <?php echo $formFiltroEstado['suspendida']->renderError() ?>
          <?php echo $formFiltroEstado['suspendida'] ?>
        </td>
      </tr>
      <?php endif;?>
    </tbody>
  </table>
</form>
<?php switch($accion):
		case 'reportecausas':break;
		case 'crearcausa':
			echo button_to_function('Cerrar ventana',
				visual_effect('appear','loading').
				visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
				visual_effect('fade','list-search-acreedor', array('duration' => 0.5)).
				visual_effect('fade','loading', array('duration' => 1.0)
				),array('class' => 'cerrar boton_con_imagen'));
			break;
		case 'buscarcausaacreedor':
			echo button_to_function('Cerrar ventana',
				visual_effect('appear','loading').
				visual_effect('highlight','list-search-acreedor', array('duration' => 0.5)).
				visual_effect('fade','list-search-acreedor', array('duration' => 0.5)).
				visual_effect('fade','loading', array('duration' => 1.0)
				),array('class' => 'cerrar boton_con_imagen'));
			break;
		default:break;
	endswitch;?>
</div>
<div id="list-search-acreedor" class="pop-up" style="display:none">
</div>