<?php use_helper('Javascript')?>
<h2>Ver Tribunal</h2>
<table>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="6">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Nombre:</th>
      <td colspan="5"><?php echo $tribunal->getNombre() ?></td>
    </tr>
    <tr>
      <th>Direcci&oacute;n:</th>
      <td colspan="5"><?php echo $tribunal->getDireccion() ?></td>
    </tr>
    <tr>
      <th>Comuna:</th>
      <td><?php echo $tribunal->getComuna() ?></td>
      <th>Provincia:</th>
      <td><?php echo $tribunal->getComuna()->getCiudad() ?></td>
      <th>Regi&oacute;n</th>
      <td><?php echo $tribunal->getComuna()->getCiudad()->getRegion() ?>
      </td>
     </tr>
     <tr>
     <td><?php echo button_to_remote('Editar', array(
      						'update' => 'acciones_tribunal',
      						'url' => 'administracion/edittribunal?id='.$tribunal->getId(),
      						'loading'=> visual_effect('appear','loading').
										visual_effect('highlight','list-search-tribunal', array('duration' => 0.5)).
										visual_effect('fade','list-search-tribunal', array('duration' => 0.5)),
							'complete' => visual_effect('fade','loading', array('duration' => 1.0)),
      					),array('class' => 'editar boton_con_imagen'))?></td>
     </tr>
  </tbody>
</table>

