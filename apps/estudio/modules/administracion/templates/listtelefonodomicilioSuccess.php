<?php use_helper('Javascript');?>
<div>
<h2>Lista de Tel&eacute;fonos en <?php echo $deudor_domicilio; ?></h2>
<table>
  <thead>
    <tr>
      <th class="tddestacado">Tel&eacute;fono</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  <?php if(count($telefono_domicilioList) > 0):?>
    <?php foreach ($telefono_domicilioList as $telefono_domicilio): ?>
    <tr>
      <td><?php echo $telefono_domicilio->getTelefono() ?></td>
      <td class="tdcentrado"><?php $mostrar_button = true;
      			switch ($accion):
					case 'showtelefono':
						$value_button = 'Detalles';
						$update = 'acciones_deudor';
						$url = 'administracion/showtelefono?id=';
						$class_css = 'detalles boton_con_imagen';
						break;
					case 'edittelefono':
						$value_button = 'Editar';
						$update = 'acciones_deudor';
						$url = 'administracion/edittelefono?id=';
						$class_css = 'editar boton_con_imagen';
						break;
					default : 
						$mostrar_button = false;
						break;
				endswitch;
				if ($mostrar_button):
					echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url.$telefono_domicilio->getId().'&iddeudordomicilio='.$deudordomicilio->getId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-telefono', array('duration' => 0.5)).
											visual_effect('fade','list-search-telefono', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
				endif;
      		?>
      </td>
    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr><td class="aviso">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td></tr>
    <?php endif;?>
  </tbody>
</table>
<?php 
	echo button_to_function('Cerrar ventana',
	visual_effect('appear','loading').
	visual_effect('highlight','list-search-telefono', array('duration' => 0.5)).
	visual_effect('fade','list-search-telefono', array('duration' => 0.5)).
	visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cerrar boton_con_imagen'));
?>
</div>