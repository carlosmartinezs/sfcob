<?php use_helper('Javascript');?>
<h2>Lista de Domicilios de Deudor</h2>
<table>
  <thead>
    <tr>
      <th class="tddestacado">Calle</th>
      <th class="tddestacado">N&uacute;mero</th>
      <th class="tddestacado">Comuna</th>
      <th class="tddestacado">Provincia</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  <?php if(count($deudor_domicilioList) > 0):?>
    <?php foreach ($deudor_domicilioList as $deudor_domicilio): ?>
    <tr>
      <td><?php echo $deudor_domicilio->getCalle() ?></td>
      <td><?php echo $deudor_domicilio->getNumero() ?></td>
      <td><?php echo $deudor_domicilio->getComuna() ?></td>
      <td><?php echo $deudor_domicilio->getComuna()->getCiudad() ?></td>
      <td class="tdcentrado"><?php $mostrar_button = true;
      			switch($accion):
					case 'show':
						$value_button = 'Detalles';
						$update = 'acciones_deudor';
						$url = 'administracion/showdeudordomicilio?id=';
						$class_css = 'detalles boton_con_imagen';
						break;
					case 'edit':
						$value_button = 'Editar';
						$update = 'acciones_deudor';
						$url = 'administracion/editdeudordomicilio?iddeudordomicilio=';
						$class_css = 'editar boton_con_imagen';
						break;
					case 'editardomicilio':
						$value_button = 'Editar Domicilio';
						$update = 'acciones_deudor';
						$url = 'administracion/editdeudordomicilio?iddeudordomicilio=';
						$class_css = 'editar boton_con_imagen';
						break;
					case 'verdomicilio':
						$value_button = 'Detalles de Domicilio';
						$update = 'acciones_deudor';
						$url = 'administracion/showdeudordomicilio?id=';
						$class_css = 'detalles boton_con_imagen';
						break;
					default : 
						$mostrar_button = false;
						break;
				endswitch;
				
				if($mostrar_button):
					echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url.$deudor_domicilio->getId().'&iddeudor='.$deudor_domicilio->getDeudorId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
											visual_effect('fade','list-search-deudor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
				endif;
				
      		?>
      </td>
    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr><td class="aviso" colspan="5">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td></tr>
    <?php endif;?>
  </tbody>
</table>

<?php 
	if($accion != "show" && $accion != "edit"):
	echo button_to_function('Cerrar ventana',
	visual_effect('appear','loading').
	visual_effect('highlight','list-search-deudordomicilio', array('duration' => 0.5)).
	visual_effect('fade','list-search-deudordomicilio', array('duration' => 0.5)).
	visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cerrar boton_con_imagen'));
	endif;
?>