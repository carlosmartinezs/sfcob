<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Tribunales</h1>
<ul  class="menu-modulo">
	<?php if($sf_user->hasCredential('abogado')): ?>
	<li>
		<?php echo link_to_remote('Ingresar Tribunal', array(
    		'update' => 'acciones_tribunal',
    		'url'    => 'administracion/createtribunal',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php echo link_to_remote('Editar Tribunal', array(
    		'update' => 'acciones_tribunal',
    		'url'    => 'administracion/searchtribunal?accion=edit',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php echo link_to_remote('Visualizar Tribunal', array(
    		'update' => 'acciones_tribunal',
    		'url'    => 'administracion/searchtribunal?accion=show',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_tribunal"  style="clear: right"></div>