<?php use_helper('Javascript', 'Object', 'DateForm') ?>
<?php $tribunal = $form->getObject() ?>
<h2><?php echo $tribunal->isNew() ? 'Ingresar' : 'Editar' ?> Tribunal</h2>
<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_tribunal',
    'url'      	=> 'administracion/updatetribunal'.(!$tribunal->isNew() ? '?id='.$tribunal->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="4">
        <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
          	<?php echo button_to_remote('Cancelar', array(
          		'update'	=> 'modulo',
          		'url'		=> 'administracion/indextribunal',
          		'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
				'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
          	),array('class' => 'cancelar boton_con_imagen')); ?>
          
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <th><label for="tribunal_nombre">Nombre</label><?php echo ' '.'*'; ?></th>
        <td colspan="3">
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
	  <tr>
	  	<th><label for="tribunal_region_id">Regi&oacute;n</label><?php echo ' '.'*'; ?></th>
        <td colspan="3"><?php 
        		echo $form['region']->renderError();
 				$lista = array();
 				$regionList = RegionPeer::doSelect(new Criteria());
  				foreach ($regionList as $region)
  				{
    				$lista[$region->getId()] = $region->getNombre();
  				} 
  					$this->lista = $lista;
  					if(isset($editar) && $editar == true){
  						echo select_tag('region_id',
	  					options_for_select($lista,
	  						$regionId,array('include_custom' => '-- Seleccione Regi&oacute;n --')),array(
	    					'onchange' => remote_function(array(
	      					'update' => "ciudades-segun-region",
	      					'with'   => "'region_id='+value",
	  						'script' => true,
	      					'url'    => '/administracion/creaselectciudad',
	      					'loading'	=> visual_effect('appear','loading'),
							'complete'	=> visual_effect('fade','loading'),
	    				))));
  					}else{
	  					echo select_tag('region_id',
	  					options_for_select($lista,
	  						'getId',array('include_custom' => '-- Seleccione Regi&oacute;n --')),array(
	    					'onchange' => remote_function(array(
	      					'update' => "ciudades-segun-region",
	      					'with'   => "'region_id='+value",
	  						'script' => true,
	      					'url'    => '/administracion/creaselectciudad',
	      					'loading'	=> visual_effect('appear','loading'),
							'complete'	=> visual_effect('fade','loading'),
	    				))));
  					}
		    ?>
        </td>
      </tr>
      <tr>
        <th><label for="tribunal_ciudad_id">Provincia</label><?php echo ' '.'*'; ?></th>
        <td>
			<div id="ciudades-segun-region">
			<?php echo $form['ciudad']->renderError();
				if(isset($editar) && $editar == true){
					echo input_hidden_tag('acreedor_ciudad_id',$ciudadId);
					echo input_tag('ciudadName',$ciudad,array('readonly' => 'readonly'));
				}else{
					echo input_tag('ciudadName','',array('readonly' => 'readonly'));
				}
            ?>
			</div>
        </td>
        <th><label for="tribunal_comuna_id">Comuna</label><?php echo ' '.'*'; ?></th>
        <td><div id="comunas-segun-ciudades">
        <?php echo $form['comuna_id']->renderError();
				if(isset($editar) && $editar == true){
					echo input_hidden_tag('comuna',$comunaId);
					echo input_tag('comunaName',$comuna,array('readonly' => 'readonly'));
				}else{
					echo input_tag('comunaName','',array('readonly' => 'readonly'));
				}
          ?>
        </div>
        <?php echo $form['id'] ?>
        </td>
      </tr>
      <tr>
       <th>Direcci&oacute;n *</th>
        <td colspan="3">
        <?php echo $form['direccion']->renderError()?>
        <?php echo $form['direccion'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
