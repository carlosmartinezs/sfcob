	<?php use_helper('Javascript', 'Object', 'DateForm');?>
<?php $deudor_domicilio = $form->getObject() ?>
<h2><?php echo $deudor_domicilio->isNew() ? 'Ingresar' : 'Editar' ?> Domicilio a Deudor</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_deudor',
    'url'      	=> 'administracion/updatedeudordomicilio?deudor='.$deudor.(!$deudor_domicilio->isNew() ? '&id='.$deudor_domicilio->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
    'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="4">
        <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
          <?php echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'administracion/indexdeudor',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)), 
               ),array('class' => 'cancelar boton_con_imagen')); ?>
          <?php if (!$deudor_domicilio->isNew() && ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') )): 
          			         					
          			echo button_to_remote('A&ntilde;adir Tel&eacute;fono', array(
          				'update'	=> 'acciones_deudor',
          				'url'		=> 'administracion/edittelefono?iddeudordomicilio='.$deudor_domicilio->getId(),
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
          				'complete'	=> visual_effect('fade','loading',array('duration' => 0)),
          				),array('class' => 'anadir boton_con_imagen'));
          				
          			if(count($deudor_domicilio->getTelefonoDomicilios()) > 0):
	          				echo button_to_remote('Editar Tel&eacute;fonos', array(
	          				'update'	=> 'list-search-telefono',
	          				'url'		=> 'administracion/searchtelefonodomicilioupdate?accion=edittelefono&iddeudordomicilio='.$deudor_domicilio->getId(),
	          				'loading'	=> visual_effect('appear','loading',array('duration' => 0)),
	          				'complete'	=> visual_effect('appear','list-search-telefono').
											visual_effect('highlight','list-search-telefono',array('duration' => 1.0)).
											visual_effect('fade','loading'),
							),array('class' => 'editar boton_con_imagen')); 
          			endif;
          			echo button_to_remote('Borrar Domicilio', array(
    						'update'   => 'modulo',
    						'url'      => 'administracion/deletedeudordomicilio?id='.$deudor_domicilio->getId(),
    						'confirm'  => 'Est&aacute; seguro que desea eliminar el domicilio del deudor?',
            				'loading'  => visual_effect('appear','loading'),
							'complete' => visual_effect('fade','loading')."alert('Se ha eliminado el domicilio')",
          					),array('class' => 'eliminar boton_con_imagen')); 
          		endif;?>
          
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <th><label for="deudor_domicilio_deudor_id">Deudor</label></th>
        <td colspan="3">
          <?php echo $form['deudor_id']->renderError() ?>
          <?php echo $form['deudor_id'] ?>
          <?php echo $deudor ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_domicilio_region_id">Regi&oacute;n</label><?php echo ' '.'*'; ?></th>
        <td colspan="3">
          <?php echo $form['region']->renderError();
 				$lista = array();
 				$regionList = RegionPeer::doSelect(new Criteria());
  				foreach ($regionList as $region)
  				{
    				$lista[$region->getId()] = $region->getNombre();
  				} 
  					$this->lista = $lista;
  					if(isset($editar) && $editar == true){
  						echo select_tag('region_id',
	  					options_for_select($lista,
	  						$regionId,array('include_custom' => '-- Seleccione Regi&oacute;n --')),array(
	    					'onchange' => remote_function(array(
	      					'update' => "ciudades-segun-region",
	      					'with'   => "'region_id='+value",
	  						'script' => true,
	      					'url'    => '/administracion/creaselectciudad',
	      					'loading'	=> visual_effect('appear','loading'),
							'complete'	=> visual_effect('fade','loading'),
	    				))));
  					}else{
	  					echo select_tag('region_id',
	  					options_for_select($lista,
	  						'getId',array('include_custom' => '-- Seleccione Regi&oacute;n --')),array(
	    					'onchange' => remote_function(array(
	      					'update' => "ciudades-segun-region",
	      					'with'   => "'region_id='+value",
	  						'script' => true,
	      					'url'    => '/administracion/creaselectciudad',
	      					'loading'	=> visual_effect('appear','loading'),
							'complete'	=> visual_effect('fade','loading'),
	    				))));
  					}  ?>
        </td>
     </tr>
     <tr>
        <th><label for="deudor_domicilio_cuidad_id">Provincia</label><?php echo ' '.'*'; ?></th>
        <td>
          <div id="ciudades-segun-region">
          <?php echo $form['ciudad']->renderError();
				if(isset($editar) && $editar == true){
					echo input_hidden_tag('acreedor_ciudad_id',$ciudadId);
					echo input_tag('ciudadName',$ciudad,array('readonly' => 'readonly'));
				}else{
					echo input_tag('ciudadName','',array('readonly' => 'readonly'));
				}
          ?>
          </div>
        </td>
        <th><label for="deudor_domicilio_comuna_id">Comuna</label><?php echo ' '.'*'; ?></th>
        <td>
          <div id="comunas-segun-ciudades">
          <?php echo $form['comuna_id']->renderError();
				if(isset($editar) && $editar == true){
					echo input_hidden_tag('comuna',$comunaId);
					echo input_tag('comunaName',$comuna,array('readonly' => 'readonly'));
				}else{
					echo input_tag('comunaName','',array('readonly' => 'readonly'));
				}
          ?>
          </div>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_domicilio_calle">Calle</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['calle']->renderError() ?>
          <?php echo $form['calle'] ?>
        </td>
        <th><label for="deudor_domicilio_numero">N&uacute;mero</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['numero']->renderError() ?>
          <?php echo $form['numero'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_domicilio_depto">Departamento</label></th>
        <td>
          <?php echo $form['depto']->renderError() ?>
          <?php echo $form['depto'] ?>
        </td>
        <th><label for="deudor_domicilio_villa">Villa / Poblaci&oacute;n</label></th>
        <td>
          <?php echo $form['villa']->renderError() ?>
          <?php echo $form['villa'] ?>
        </td>
     </tr>
     <tr>
        <th><label for="deudor_domicilio_telefono">Tel&eacute;fono</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
<div id="list-search-telefono" class="pop-up" style="display:none"></div>