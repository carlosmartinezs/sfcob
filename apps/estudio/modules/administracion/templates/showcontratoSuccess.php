<?php if(isset($perfil) && $perfil):?>
	<?php if($contrato->getEstado()):?>
		<h2>Su Contrato Vigente</h2>
	<?php else:?>
		<h2>Su Contrato hasta <?php echo $contrato->getFechaTermino('d-m-Y')?></h2>
	<?php endif;?>
<?php else:?>
	<h2>Ver Contrato</h2>
<?php endif;?>
<table>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="4">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Acreedor:</th>
      <td colspan="3"><?php echo $contrato->getNombreAcreedor() ?></td>
    </tr>
    <tr>
      <th>Fecha de Inicio:</th>
      <td><?php echo Funciones::fecha2($contrato->getFechaInicio('d-m-Y')) ?></td>
      <th>Fecha de T&eacute;rmino:</th>
      <td><?php echo $contrato->getFechaTermino() ? Funciones::fecha2($contrato->getFechaTermino('d-m-Y')) : 'No estipulada' ?></td>
    </tr>
    <tr>
      <th>Honorarios:</th>
      <td><?php echo $contrato->getPorcentajeHonorario() . "%" ?></td>
      <th>Estado:</th>
      <td><?php echo ($contrato->getEstado() ? 'Activo' : 'Cancelado') ?></td>
    </tr>
  </tbody>
</table>