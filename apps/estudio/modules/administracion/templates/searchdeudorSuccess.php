<?php use_helper('Javascript') ?>
<div id="form_search_deudor">
<h2>
        <?php 	switch($accion):
					case 'edit':
						echo "Editar Deudor: Buscar";
						break;
					case 'show':
						echo "Ver Deudor: Buscar";
						break;
					case 'creardomicilio':
						echo "Ingresar Domicilio: Buscar Deudor";
						break;
					case 'editardomicilio':
						echo "Editar Domicilio: Buscar Deudor";
						break;
					case 'verdomicilio':
						echo "Ver Domicilio: Buscar Deudor";
						break;
					case 'liquidacion':
						echo "Generar Liquidaci&oacute;n: Buscar Deudor";
      					break;
					case 'crearcausa':
						echo "Ingresar Causa: Buscar Deudor";
						break;
					case 'buscarcausadeudor':
						echo "Buscar Causa: Buscar Deudor";
						break;
					default:
						echo "Buscar Deudor";
						break;;
				endswitch;
        ?>
</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'list-search-deudor',
    'url'      	=> 'administracion/searchdeudorupdate',
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('appear','list-search-deudor').
					visual_effect('highlight','list-search-deudor',array('duration' => 1.0)).
					visual_effect('fade','loading'),
))?>
  <table>
    <tfoot>
      	<tr>
      		<td class="tdcentrado" colspan="4">
      		<input type="submit" value="Buscar" class="buscar boton_con_imagen"/>
      			<?php switch($accion):
					case 'juicios':
						echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'juicios/index',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
						),array('class' => 'cancelar boton_con_imagen'));
						break;
					case 'liquidacion':
						echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'informes/indexinforme',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
						),array('class' => 'cancelar boton_con_imagen'));
      					break;
					case 'crearcausa':break;
					case 'buscarcausadeudor':break;
					default:
						echo button_to_remote('Cancelar', array(
          				'update'	=> 'modulo',
          				'url'		=> 'administracion/indexdeudor',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
						),array('class' => 'cancelar boton_con_imagen'));
						break;
					endswitch;
				?>
      		</td>
    	</tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="deudor_rut_deudor">Rut </label><span class="small-font">(Ej: 12.345.678-9)</span></th>
        <td>
          <?php echo $form['rut_deudor']->renderError() ?>
          <?php echo $form['rut_deudor'] ?>
        </td>
        <th><label for="deudor_nombres">Nombres</label></th>
        <td>
          <?php echo $form['nombres']->renderError() ?>
          <?php echo $form['nombres'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_apellido_paterno">Apellido Paterno</label></th>
        <td>
          <?php echo $form['apellido_paterno']->renderError() ?>
          <?php echo $form['apellido_paterno'] ?>
        </td>
        <th><label for="deudor_apellido_materno">Apellido Materno</label></th>
        <td>
          <?php echo $form['apellido_materno']->renderError() ?>
          <?php echo $form['apellido_materno'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_celular">Tel&eacute;fono Celular</label></th>
        <td>
          <?php echo $form['celular']->renderError() ?>
          <?php echo $form['celular'] ?>
        </td>
        <th><label for="deudor_email">Email</label></th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>
		  <?php echo $form['accion'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
<?php switch($accion):
		case 'juicios':break;
		case 'liquidacion':break;
		case 'crearcausa':
			echo button_to_function('Cerrar ventana',
				visual_effect('appear','loading').
				visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
				visual_effect('fade','list-search-deudor', array('duration' => 0.5)).
				visual_effect('fade','loading', array('duration' => 1.0)),
				array('class' => 'cerrar boton_con_imagen'));
			break;
		case 'buscarcausadeudor':
			echo button_to_function('Cerrar ventana',
				visual_effect('appear','loading').
				visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
				visual_effect('fade','list-search-deudor', array('duration' => 0.5)).
				visual_effect('fade','loading', array('duration' => 1.0)),
				array('class' => 'cerrar boton_con_imagen'));
			break;
		default:break;
	endswitch;?>
</div>
<div id="list-search-deudor" class="pop-up" style="display:none"></div>