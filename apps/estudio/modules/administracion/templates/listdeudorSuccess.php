<?php use_helper('Javascript');?>
<div>
<h2>Lista de Deudores</h2>
<table>
  <thead>
    <tr>
      <th class="tddestacado">Rut</th>
      <th class="tddestacado">Nombres</th>
      <th class="tddestacado">Apellidos</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  <?php if(count($deudorList) > 0):?>
    <?php foreach ($deudorList as $deudor): ?>
    <tr>
      <td><?php echo $deudor->getRutDeudor() ?></td>
      <td><?php echo $deudor->getNombres() ?></td>
      <td><?php echo $deudor->getApellidoPaterno().' '.$deudor->getApellidoMaterno() ?></td>
      <td class="tdcentrado"><?php switch ($accion):
					case 'show':
						echo button_to_remote('Detalles', array(
							'update'	=> 'acciones_deudor',
							'url'		=> 'administracion/showdeudor?id='.$deudor->getId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
											visual_effect('fade','list-search-deudor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'detalles boton_con_imagen'));
						break;
					case 'edit':
						echo button_to_remote('Editar', array(
							'update'	=> 'acciones_deudor',
							'url'		=> 'administracion/editdeudor?id='.$deudor->getId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
											visual_effect('fade','list-search-deudor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'editar boton_con_imagen'));
						break;
					case 'creardomicilio':
						echo button_to_remote('A&ntilde;adir Domicilio', array(
							'update'	=> 'acciones_deudor',
							'url'		=> 'administracion/createdeudordomicilio?iddeudor='.$deudor->getId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
											visual_effect('fade','list-search-deudor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'anadir boton_con_imagen'));
						break;
					case 'editardomicilio':
						if(count($deudor->getDeudorDomicilios()) > 0)
						{
							echo button_to_remote('Ver Domicilios', array(
								'update'	=> 'acciones_deudor',
								'url'		=> 'administracion/searchdeudordomicilioupdate?accion=edit&iddeudor='.$deudor->getId(),
								'loading'	=> visual_effect('appear','loading').
												visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
												visual_effect('fade','list-search-deudor', array('duration' => 0.5)),
								'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => 'detalles boton_con_imagen'));
						}
						else
						{
							echo button_to_function('Ver Domicilios',"alert('Este deudor no posee domicilios')",array('class' => 'detalles boton_con_imagen'));
						}
						
						break;
					case 'verdomicilio':
						if(count($deudor->getDeudorDomicilios()) > 0)
						{
							echo button_to_remote('Ver Domicilio', array(
								'update'	=> 'acciones_deudor',
								'url'		=> 'administracion/searchdeudordomicilioupdate?accion=show&iddeudor='.$deudor->getId(),
								'loading'	=> visual_effect('appear','loading').
												visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
												visual_effect('fade','list-search-deudor', array('duration' => 0.5)),
								'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
								),array('class' => 'detalles boton_con_imagen'));
						}
						else
						{
							echo button_to_function('Ver Domicilio',"alert('Este deudor no posee domicilios')",array('class' => 'detalles boton_con_imagen'));
						}
						
						break;
					case 'liquidacion':
						if(count($deudor->getCausas()) > 0)
						{
							echo button_to_remote('Generar Liquidaci&oacute;n', array(
								'update'	=> 'acciones_informe',
								'url'		=> 'informes/informeliquidacion?iddeudor='.$deudor->getId(),
								'loading'	=> visual_effect('appear','loading').
												visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
												visual_effect('fade','list-search-deudor', array('duration' => 0.5)),
								'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => 'reporte boton_con_imagen'));
						}
						else
						{
							echo button_to_function('Generar Liquidaci&oacute;n',"alert('Este deudor no tiene causas')",array('class' => 'reporte boton_con_imagen'));
						}
						break;
					case 'crearcausa':
						echo button_to_remote('Llevar Deudor', array(
							'update'	=> 'deudor',
							'url'		=> 'juicios/traerdeudor?iddeudor='.$deudor->getId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
											visual_effect('fade','list-search-deudor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'llevar boton_con_imagen'));
						break;
					case 'buscarcausadeudor':
						echo button_to_remote('Llevar Deudor', array(
							'update'	=> 'deudor',
							'url'		=> 'juicios/traerdeudor?iddeudor='.$deudor->getId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
											visual_effect('fade','list-search-deudor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'llevar boton_con_imagen'));
						break;
					case 'buscarcarta':
						echo button_to_remote('Llevar Deudor', array(
							'update'	=> 'deudor',
							'url'		=> 'juicios/traerdeudor?iddeudor='.$deudor->getId(),
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
											visual_effect('fade','list-search-deudor', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'llevar boton_con_imagen'));
						break;
					default : 
						break;
				endswitch;
      		?>
      </td>
    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr><td class="aviso" colspan="4">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td></tr>
    <?php endif;?>
  </tbody>
</table>
<?php 
	echo button_to_function('Cerrar ventana',
	visual_effect('appear','loading').
	visual_effect('highlight','list-search-deudor', array('duration' => 0.5)).
	visual_effect('fade','list-search-deudor', array('duration' => 0.5)).
	visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cerrar boton_con_imagen'));
?>
</div>