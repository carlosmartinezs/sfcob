<?php use_helper('Javascript') ?>
<h2>Ver Deudor</h2>
<table>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="4">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Rut:</th>
      <td><?php echo $deudor->getRutDeudor() ?></td>
    </tr>
    <tr>
      <th>Nombres:</th>
      <td><?php echo $deudor->getNombres() ?></td>
    </tr>
    <tr>
      <th>Apellido Paterno:</th>
      <td><?php echo $deudor->getApellidoPaterno() ?></td>
    </tr>
    <tr>
      <th>Apellido Materno:</th>
      <td><?php echo $deudor->getApellidoMaterno() ?></td>
    </tr>
    <tr>
      <th>Tel&eacute;fono Celular:</th>
      <td><?php echo $deudor->getCelular() ?></td>
    </tr>
    <tr>
      <th>Email:</th>
      <td><?php echo $deudor->getEmail() ?></td>
    </tr>
    <tr>
      <td class="tdcentrado" colspan="2">
      <?php echo button_to_remote('Ver Domicilios', array(
		'update'	=> 'list-search-deudordomicilio',
		'url'		=> 'administracion/searchdeudordomicilioupdate?accion=verdomicilio&iddeudor='.$deudor->getId(),
		'loading'	=> visual_effect('appear','loading'),
		'complete'	=> visual_effect('appear','list-search-deudordomicilio').
						visual_effect('highlight','list-search-deudordomicilio',array('duration' => 1.0)).
						visual_effect('fade','loading'),
		),array('class' => 'detalles boton_con_imagen')) ?>
		<?php if($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')):?>
	  <?php echo button_to_remote('A&ntilde;adir Domicilio', array(
		'update'	=> 'acciones_deudor',
		'url'		=> 'administracion/createdeudordomicilio?iddeudor='.$deudor->getId(),
		'loading'	=> visual_effect('appear','loading'),
		'complete'	=> visual_effect('appear','acciones_deudor').
						visual_effect('highlight','acciones_deudor',array('duration' => 1.0)).
						visual_effect('fade','loading'),
		),array('class' => 'anadir boton_con_imagen')) ?>
		<?php endif;?>
	  </td>
    </tr>
  </tbody>
</table>
<div id="list-search-deudordomicilio" class="pop-up" style="display:none"></div>