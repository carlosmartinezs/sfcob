<?php use_helper('Javascript', 'Object', 'DateForm') ?>
<?php $deudor = $form->getObject() ?>
<?php if($pathinfo == '/administracion/createdeudor' ): ?>
<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_deudor',
    'url'      	=> 'administracion/updatedeudor'.(!$deudor->isNew() ? '?id='.$deudor->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
<h2>Ingresar Deudor</h2>
<?php else: ?>
<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_deudor',
    'url'      	=> 'administracion/updatedeudor'.(!$deudor->isNew() ? '?id='.$deudor->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
<h2>Editar Deudor: <?php echo $deudor?></h2>
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="4">
        <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
          <?php echo button_to_remote('Cancelar', array(
          		'update'	=> 'modulo',
          		'url'		=> 'administracion/indexdeudor',
          		'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
				'complete'	=> visual_effect('fade','loading', array('duration' => 0)), 
          		),array('class' => 'cancelar boton_con_imagen')); ?>
        <?php 
        	if($pathinfo != '/administracion/createdeudor' ):
	      		echo button_to_remote('A&ntilde;adir Domicilio', array(
		    		'update' => 'acciones_deudor',
		    		'url'    => 'administracion/createdeudordomicilio?iddeudor='. $deudor->getId(),
					'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
				),array('class' => 'anadir boton_con_imagen'));	
      			echo button_to_remote('Editar Domicilios', array(
					'update'	=> 'list-search-deudordomicilio',
					'url'		=> 'administracion/searchdeudordomicilioupdate?accion=editardomicilio&iddeudor='.$deudor->getId(),
					'loading'	=> visual_effect('appear','loading'),
					'complete'	=> visual_effect('appear','list-search-deudordomicilio').
									visual_effect('highlight','list-search-deudordomicilio',array('duration' => 1.0)).
									visual_effect('fade','loading'),
				),array('class' => 'editar boton_con_imagen'));
			endif; ?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="deudor_rut_deudor">Rut * </label><span class="small-font">(Ej: 12.345.678-9)</span></th>
        <td>
          <?php if($errorRutDeu == true){
        	    echo '<ul class="error_list">
    					<li>Rut inv&aacute;lido.</li>
  					  </ul>';
              }else{
              	echo $form['rut_deudor']->renderError();
              }
        ?>
          <?php echo $form['rut_deudor'] ?>
        </td>
        <th><label for="deudor_nombres">Nombres *</label></th>
        <td>
          <?php echo $form['nombres']->renderError() ?>
          <?php echo $form['nombres'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_apellido_paterno">Apellido Paterno</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['apellido_paterno']->renderError() ?>
          <?php echo $form['apellido_paterno'] ?>
        </td>
        <th><label for="deudor_apellido_materno">Apellido Materno</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['apellido_materno']->renderError() ?>
          <?php echo $form['apellido_materno'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_celular">Tel&eacute;fono *</label></th>
        <td>
          <?php echo $form['celular']->renderError() ?>
          <?php echo $form['celular'] ?>
        </td>
        <th><label for="deudor_email">Email</label></th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>

        <?php echo $form['id'] ?>
        <?php echo input_hidden_tag('la_accion',$la_accion) ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
<div id="list-search-deudordomicilio" class="pop-up" style="display:none"></div>