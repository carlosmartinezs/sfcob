<?php

/**
 * administracion actions.
 *
 * @package    sgcj
 * @subpackage administracion
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 9301 2008-05-27 01:08:46Z dwhittle $
 */
class administracionActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex()
  {
  }
  
  public function executeIndextribunal()
  {
  }
  
  public function executeListtribunal()
  {
  	$this->tribunalList = TribunalPeer::doSelect(new Criteria());
  }
  
  public function executeSearchtribunal($request)
  {
  	$this->accion = $request->getParameter('accion');
  	$this->form = new SearchTribunalForm();
  	$this->form->setDefault('accion',$request->getParameter('accion'));
  }
  
  public function executeSearchtribunalupdate($request)
  {
  	$this->tribunalList = TribunalPeer::doSelect(TribunalPeer::getCriteria($request));
  	$this->accion = $request->getParameter('accion');
	$this->setTemplate('listtribunal');
  }
  public function executeShowtribunal($request)
  {
    $this->tribunal = TribunalPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tribunal);
  }
  
  public function executeCreatetribunal()
  {
    $this->form = new TribunalForm();

    $this->setTemplate('edittribunal');
  }
  
  public function executeEdittribunal($request)
  {
    $this->form = new TribunalForm(TribunalPeer::retrieveByPk($request->getParameter('id')));
    $this->editar = true;
    $this->region = TribunalPeer::retrieveByPk($request->getParameter('id'))->getComuna()->getCiudad()->getRegion();
    $this->ciudad = TribunalPeer::retrieveByPk($request->getParameter('id'))->getComuna()->getCiudad();
    $this->comuna = TribunalPeer::retrieveByPk($request->getParameter('id'))->getComuna();
    
    $this->regionId = $this->region->getId();
    $this->ciudadId = $this->ciudad->getId();
    $this->comunaId = $this->comuna->getId();
  }
  
  public function executeUpdatetribunal($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TribunalForm(TribunalPeer::retrieveByPk($request->getParameter('id')));

    $datos = $request->getParameter('tribunal');
    
    $datos['nombre'] = trim($datos['nombre']);
    
	$datos['comuna_id'] = $request->getParameter('comuna');
	$datos['region'] = $request->getParameter('region_id');
	$datos['ciudad'] = $request->getParameter('acreedor_ciudad_id');
    
    $this->form->bind($datos);
    if ($this->form->isValid())
    {
      $this->tribunal = $this->form->save();
      $this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showtribunal');
    }else{
      $this->editar = true;
      
      $this->regionId = $request->getParameter('region_id');
      $this->ciudadId = $request->getParameter('acreedor_ciudad_id');
      $this->comunaId = $request->getParameter('comuna');
      
      $this->region = RegionPeer::retrieveByPK($this->regionId);
      $this->ciudad = CiudadPeer::retrieveByPK($this->ciudadId);
      $this->comuna = ComunaPeer::retrieveByPK($this->comunaId);
      
      $this->setTemplate('edittribunal');
    }
  }

  public function executeDeletetribunal($request)
  {
    $this->forward404Unless($tribunal = TribunalPeer::retrieveByPk($request->getParameter('id')));

    $tribunal->delete();

    $this->redirect('administracion/indextribunal');
  }
  
  public function executeIndexacreedor()
  {	
  }
  
  public function executeListacreedor()
  {
    $this->acreedorList = AcreedorPeer::doSelect(new Criteria());
  }
  
  public function executeSearchacreedor($request)
  {
  	$this->accion = $request->getParameter('accion');
  	$this->form = new SearchAcreedorForm();
  	
  	if($this->accion == 'reportecausas'){
  		$this->formFiltroEstado = new FiltroEstadoCausaForm();
  		$this->formFiltroEstado->setDefault('activa',true);
  	}
  	
  	$this->form->setDefault('accion',$request->getParameter('accion'));
  }
  
  public function executeSearchacreedorupdate($request)
  {
  	$this->acreedorList = AcreedorPeer::doSelect(AcreedorPeer::getCriteria($request));
  	$this->accion = $request->getParameter('accion');
  	
  	if($this->accion == 'reportecausas')
  	{
  		$filtroCausa = null;
  		$filtroCausa[0] = 0;
  		$filtroCausa[1] = 0;
  		$filtroCausa[2] = 0;
  		$filtroCausa[3] = 0;
  		$filtroCausa[4] = 0;
  		
  		if ($request->hasParameter('activa')){$filtroCausa[0] = 1;} 
  		if ($request->hasParameter('terminada')){$filtroCausa[1] = 1;}
  		if ($request->hasParameter('castigada')){$filtroCausa[2] = 1;}
  		if ($request->hasParameter('incobrable')){$filtroCausa[3] = 1;}
  		if ($request->hasParameter('suspendida')){$filtroCausa[4] = 1;}
  		
  		$this->estados = $filtroCausa;
  	}
  	
	$this->setTemplate('listacreedor');
  }
  
  public function executeShowacreedor($request)
  {
    $this->acreedor = AcreedorPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->acreedor);
  }
  
  public function executeCreateacreedor($request)
  {
    $this->form = new AcreedorForm();
    $this->setTemplate('editacreedor');
    $this->pathinfo = $request->getPathInfo();
    $this->errorRutAc = false;
  	$this->errorRutRep = false;
  }
  
  public function executeEditacreedor($request)
  {
  
  	$user = $this->getUser();
  	
  	if($user->isAcreedor())
  	{
  		$this->acreedor = $user->getAcreedor();
  		$this->perfil = true;
  		
  		$this->form = new AcreedorForm(AcreedorPeer::retrieveByPk($this->acreedor->getId()));
	    $this->pathinfo = $request->getPathInfo();
	    
	    $this->editar = true;
	    $this->errorRutAc = false;
	  	$this->errorRutRep = false;
	    
	  	
	    $giro = AcreedorPeer::retrieveByPk($this->acreedor->getId())->getGiro();
	    $this->claseGiroId = $giro->getClaseGiroId();
	    $this->giroId = $giro->getId();
	    $this->giroName = $giro->getNombre();
	    
	    $this->region = AcreedorPeer::retrieveByPk($this->acreedor->getId())->getComuna()->getCiudad()->getRegion();
	    $this->ciudad = AcreedorPeer::retrieveByPk($this->acreedor->getId())->getComuna()->getCiudad();
	    $this->comuna = AcreedorPeer::retrieveByPk($this->acreedor->getId())->getComuna();
	    
	    $this->regionId = $this->region->getId();
	    $this->ciudadId = $this->ciudad->getId();
	    $this->comunaId = $this->comuna->getId();
  	}
  	else
  	{
	    $this->form = new AcreedorForm(AcreedorPeer::retrieveByPk($request->getParameter('id')));
	    $this->pathinfo = $request->getPathInfo();
	    
	    $this->editar = true;
	    $this->errorRutAc = false;
	  	$this->errorRutRep = false;
	    
	  	
	    $giro = AcreedorPeer::retrieveByPk($request->getParameter('id'))->getGiro();
	    $this->claseGiroId = $giro->getClaseGiroId();
	    $this->giroId = $giro->getId();
	    $this->giroName = $giro->getNombre();
	    
	    $this->region = AcreedorPeer::retrieveByPk($request->getParameter('id'))->getComuna()->getCiudad()->getRegion();
	    $this->ciudad = AcreedorPeer::retrieveByPk($request->getParameter('id'))->getComuna()->getCiudad();
	    $this->comuna = AcreedorPeer::retrieveByPk($request->getParameter('id'))->getComuna();
	    
	    $this->regionId = $this->region->getId();
	    $this->ciudadId = $this->ciudad->getId();
	    $this->comunaId = $this->comuna->getId();
  	}
  }
  
  public function executeUpdateacreedor($request)
  {
  	$this->errorRutAc = false;
  	$this->errorRutRep = false;
  	
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new AcreedorForm(AcreedorPeer::retrieveByPk($request->getParameter('id')));
    $this->pathinfo = $request->getPathInfo();

	$datos = $request->getParameter('acreedor');
    
	$datos['nombres'] = trim($datos['nombres']);
	$datos['rut_acreedor'] = trim($datos['rut_acreedor']);
	$datos['rut_rep_legal'] = trim($datos['rut_rep_legal']);
	
	$rutAc_no_verificado = $datos['rut_acreedor'];
	$rutRep_no_verificado = $datos['rut_rep_legal'];
	
	$datos['rut_acreedor'] = Funciones::verificarRut($datos['rut_acreedor']);
	$datos['rut_rep_legal'] = Funciones::verificarRut($datos['rut_rep_legal']);
  
      if($datos['rut_acreedor'] == null && $rutAc_no_verificado != '')
      {
      	$this->errorRutAc = true;
      }
      else
      {
      	$datos['rut_acreedor'] = Funciones::parsearRut($datos['rut_acreedor']);
      }
      
      if($datos['rut_rep_legal'] == null && $rutRep_no_verificado != '')
      {
      	$this->errorRutRep = true;
      }
      else
      {
      	$datos['rut_rep_legal'] = Funciones::parsearRut($datos['rut_rep_legal']);
      }
      
	$datos['apellido_paterno'] = trim($datos['apellido_paterno']);
	$datos['apellido_materno'] = trim($datos['apellido_materno']);
	$datos['direccion'] = trim($datos['direccion']);
	$datos['telefono'] = trim($datos['telefono']);
	$datos['fax'] = trim($datos['fax']);
	$datos['email'] = trim($datos['email']);
	$datos['rep_legal'] = trim($datos['rep_legal']);
	
	
    $nombres = $datos['nombres'];
    
    if($nombres != null && $nombres != '' && $datos['apellido_paterno'] != null && $datos['apellido_paterno'] != ''){
    	$primeraLetra = $nombres[0];
	    $apellidoPaterno = $datos['apellido_paterno'];
	    $usuario = $primeraLetra.$apellidoPaterno;
	    $usuario = ereg_replace( "([     ]+)", "", $usuario); 
	    $usuario=strtolower($usuario);
	    
	    $c = new Criteria();
	    $c->add(sfGuardUserPeer::USERNAME, $usuario);
	    $usuarioBd = sfGuardUserPeer::doSelectOne($c);
	    
	    if($usuarioBd instanceof sfGuardUser)
	    {
			$datos['sf_guard_user_id'] = $usuarioBd->getId();
	    }
	    else
	    {
	    	$passGenerada = Funciones::generarRandomString(10);
	    	//$passGenerada = '1234';
	    	$this->getUser()->setAttribute('passgenerada'.$usuario, $passGenerada);
	    	$user = new sfGuardUser();
	    	$user->setUsername($usuario);
	    	$user->setPassword($passGenerada);
	    	$user->save();
	    	
	    	$permiso = new sfGuardUserPermission();
	    	$permiso->setUserId($user->getId());
	    	$permiso->setPermissionId(6);
			$permiso->save();
			
			$perfilUser = new Perfil();
	    	$perfilUser->setSfGuardUserId($user->getId());
	    	$perfilUser->setPassword1($user->getPassword());
	    	$perfilUser->setUltimoCambio('now');
	    	$perfilUser->save();
	    	
			$datos['sf_guard_user_id'] = $user->getId();
			$this->pwd = $user->getPassword();
	    }
    }
    
	$datos['comuna_id'] = $request->getParameter('comuna');
	$datos['giro_id'] = $request->getParameter('giro');
	$datos['clase_giro'] = $request->getParameter('clase_giro_id');
	$datos['region'] = $request->getParameter('region_id');
	$datos['ciudad'] = $request->getParameter('acreedor_ciudad_id');
    
    $this->form->bind($datos);
    
    if ($this->form->isValid())
    {
	    $user = $this->getUser();
	    if($user->isAcreedor())
	    {
	    	$this->perfil = true;
	    	$this->pwd = $user->getPassword();
	    }
	    $this->acreedor = $this->form->save();
	    
	    $c = new Criteria();
	    $c->add(PerfilPeer::SF_GUARD_USER_ID,$this->acreedor->getSfGuardUserId());
	 	$perfilAcreedor = PerfilPeer::doSelectOne($c);
	 	
	 	if(!$perfilAcreedor->getEnvioMail())
	 	{
	 		$gmail = new GMail();
		  	
		  	$gmail->setFrom("no-reply@sfcob.cl");
		  	$gmail->setFromName("SfCob (No Reply)");
		  	$gmail->subject("Ingreso a SfCob: ".$this->acreedor->getsfGuardUser()->getUsername());
		  	$gmail->altBody("
		  	
		  	Bienvenido a SfCob:
		  	
		  	user: ".$this->acreedor->getsfGuardUser()->getUsername()."
		  	pass: ".$this->getUser()->getAttribute('passgenerada'.$this->acreedor->getsfGuardUser()->getUsername())."
		  	
		  	");
		  	$gmail->msgHTML("
		  	
		  	Bienvenido a SfCob:<br />
		  	<br />
		  	Usuario: ".$this->acreedor->getsfGuardUser()->getUsername()."<br />
		  	Contrase&ntilde;a: ".$this->getUser()->getAttribute('passgenerada'.$this->acreedor->getsfGuardUser()->getUsername())."<br />
		  	<br />
		  	ingresa via : <a href= \"http://sgcj/estudio.php\">SfCob</a>
		  	
		  	");
		  	$gmail->AddAddress("carlos.f.martinez.s@gmail.com","Carlos");// reemplazar por linea de abajo
		  	//$gmail->AddAddress(".$this->acreedor->getEmail().",".$this->acreedor.");
		  	
			$gmail->IsHTML(true);
			
			if($gmail->Send())
			{
				$this->gmailError = "Error: " . $gmail->getGMail()->ErrorInfo;
			}
			else
			{
				$this->gmailSuccess = "Mensaje enviado correctamente";
				$perfilAcreedor->setEnvioMail(true);
				$perfilAcreedor->save();
			}
	 	}
		  	
		$user->setAttribute('guardado',true);
	    $this->setTemplate('showacreedor');
    }else{
    	
      $this->editar = true;
      
      $this->claseGiroId = $request->getParameter('clase_giro_id');
      $this->giroId = $request->getParameter('giro');
      $this->giroName = GiroPeer::retrieveByPK($this->giroId);
    
      $this->regionId = $request->getParameter('region_id');
      $this->ciudadId = $request->getParameter('acreedor_ciudad_id');
      $this->comunaId = $request->getParameter('comuna');
      
      $this->region = RegionPeer::retrieveByPK($this->regionId);
      $this->ciudad = CiudadPeer::retrieveByPK($this->ciudadId);
      $this->comuna = ComunaPeer::retrieveByPK($this->comunaId);
      
      if($datos['rut_acreedor'] == null && $rutAc_no_verificado != '')
      {
      	$datos['rut_acreedor'] = $rutAc_no_verificado;
      }
      
      if($datos['rut_rep_legal'] == null && $rutRep_no_verificado != '')
      {
      	$datos['rut_rep_legal'] = $rutRep_no_verificado;
      }
      
      $this->form->bind($datos);
      $this->setTemplate('editacreedor');
    }
  }

  public function executeDeleteacreedor($request)
  {
    $this->forward404Unless($acreedor = AcreedorPeer::retrieveByPk($request->getParameter('id')));

    $acreedor->delete();

    $this->redirect('administracion/indexacreedor');
  }
  
  public function executeIndexdeudor()
  {
  }
  public function executeListdeudor()
  {
    $this->deudorList = DeudorPeer::doSelect(new Criteria());
  }
  
  public function executeSearchdeudor($request)
  {
  	$this->accion = $request->getParameter('accion');
  	$this->form = new SearchDeudorForm();
  	$this->form->setDefault('accion',$request->getParameter('accion'));
  }
  
  public function executeSearchdeudorupdate($request)
  {
  	$this->deudorList = DeudorPeer::doSelect(DeudorPeer::getCriteria($request));
  	$this->accion = $request->getParameter('accion');
	$this->setTemplate('listdeudor');
  }

  public function executeShowdeudor($request)
  {
    $this->deudor = DeudorPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->deudor);
  }

  public function executeCreatedeudor($request)
  {
    $this->form = new DeudorForm();
    $this->setTemplate('editdeudor');
    $this->pathinfo = $request->getPathInfo();
    $this->la_accion = 'creardeudor';
    $this->errorRutDeu = false;
  }

  public function executeEditdeudor($request)
  {
    $this->form = new DeudorForm(DeudorPeer::retrieveByPk($request->getParameter('id')));
    $this->pathinfo = $request->getPathInfo();
    $this->la_accion = '';
    $this->errorRutDeu = false;
  }

  public function executeUpdatedeudor($request)
  {
  	$this->errorRutDeu = false;
    $this->forward404Unless($request->isMethod('post'));
    $this->la_accion = $request->getParameter('la_accion');
    $this->form = new DeudorForm(DeudorPeer::retrieveByPk($request->getParameter('id')));
	$this->pathinfo = $request->getPathInfo();
	
	$datos = $request->getParameter('deudor');
	
	$datos['rut_deudor'] = trim($datos['rut_deudor']);
	
	$rutDeu_no_verificado = $datos['rut_deudor'];
	
    $datos['rut_deudor'] = Funciones::verificarRut($datos['rut_deudor']);
  
      if($datos['rut_deudor'] == null && $rutDeu_no_verificado != '')
      {
      	$this->errorRutDeu = true;
      }
      else
      {
      	$datos['rut_deudor'] = Funciones::parsearRut($datos['rut_deudor']);
      }
      
	$datos['nombres'] = trim($datos['nombres']);
	$datos['apellido_paterno'] = trim($datos['apellido_paterno']);
	$datos['apellido_materno'] = trim($datos['apellido_materno']);
	$datos['celular'] = trim($datos['celular']);
	$datos['email'] = trim($datos['email']);
	
    $this->form->bind($datos);
    if ($this->form->isValid())
    {
      $this->deudor = $this->form->save();
      $this->getUser()->setAttribute('guardado',true);
	  $this->setTemplate('showdeudor');
    }
    else
    {
      if($this->la_accion == 'creardeudor'){
      	$this->pathinfo = '/administracion/createdeudor';
      }
      
      if($datos['rut_deudor'] == null && $rutDeu_no_verificado != '')
      {
      	$datos['rut_deudor'] = $rutDeu_no_verificado;
      }
      $this->form->bind($datos);
      $this->setTemplate('editdeudor');
    }
  }

  public function executeDeletedeudor($request)
  {
    $this->forward404Unless($deudor = DeudorPeer::retrieveByPk($request->getParameter('id')));

    $deudor->delete();

    $this->redirect('administracion/indexdeudor');
  }
  public function executeIndexdeudordomicilio()
  {
  }
  
	public function executeListdeudordomicilio()
  {
  	$this->deudor_domicilioList = DeudorDomicilioPeer::doSelect(new Criteria());
  }
  
  public function executeSearchdeudordomicilioupdate($request)
  {
  	$this->deudor_domicilioList = DeudorDomicilioPeer::getByIdDeudor($request->getParameter('iddeudor'));
  	$this->accion = $request->getParameter('accion');
	$this->setTemplate('listdeudordomicilio');
  }
  
  public function executeShowdeudordomicilio($request)
  {
    $this->deudor_domicilio = DeudorDomicilioPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->deudor_domicilio);
  }

  public function executeCreatedeudordomicilio($request)
  {
  	$this->deudor = DeudorPeer::retrieveByPK($request->getParameter('iddeudor'));
    $this->form = new DeudorDomicilioForm();
    $this->form->setDefault('deudor_id',$this->deudor->getId());
	$this->pathinfo = $request->getPathInfo();
    $this->setTemplate('editdeudordomicilio');
  }

  public function executeEditdeudordomicilio($request)
  {
  	$this->pathinfo = $request->getPathInfo();
    $this->form = new DeudorDomicilioForm(DeudorDomicilioPeer::retrieveByPK($request->getParameter('iddeudordomicilio')));
    
    $this->editar = true;
    
    $this->region = DeudorDomicilioPeer::retrieveByPK($request->getParameter('iddeudordomicilio'))->getComuna()->getCiudad()->getRegion();
    $this->ciudad = DeudorDomicilioPeer::retrieveByPK($request->getParameter('iddeudordomicilio'))->getComuna()->getCiudad();
    $this->comuna = DeudorDomicilioPeer::retrieveByPK($request->getParameter('iddeudordomicilio'))->getComuna();
    
    $this->regionId = $this->region->getId();
    $this->ciudadId = $this->ciudad->getId();
    $this->comunaId = $this->comuna->getId();

    $this->deudor = DeudorPeer::retrieveByPK($request->getParameter('iddeudor'))->__toString();
  }

  public function executeUpdatedeudordomicilio($request)
  {
    $this->forward404Unless($request->isMethod('post'));
	
    $this->form = new DeudorDomicilioForm(DeudorDomicilioPeer::retrieveByPk($request->getParameter('id')));

    $datos = $request->getParameter('deudor_domicilio');
    
    $datos['calle'] = trim($datos['calle']);
	$datos['numero'] = trim($datos['numero']);
	$datos['depto'] = trim($datos['depto']);
	$datos['villa'] = trim($datos['villa']);
	$datos['telefono'] = trim($datos['telefono']);
	
	$datos['comuna_id'] = $request->getParameter('comuna');
	$datos['region'] = $request->getParameter('region_id');
	$datos['ciudad'] = $request->getParameter('acreedor_ciudad_id');
    
    $this->form->bind($datos);

    if ($this->form->isValid())
    {
      $this->deudor_domicilio = $this->form->save();
      $this->getUser()->setAttribute('guardado',true);
	  $this->setTemplate('showdeudordomicilio');
    }
    else
    {
      $this->editar = true;
          
      $this->regionId = $request->getParameter('region_id');
      $this->ciudadId = $request->getParameter('acreedor_ciudad_id');
      $this->comunaId = $request->getParameter('comuna');
      
      $this->region = RegionPeer::retrieveByPK($this->regionId);
      $this->ciudad = CiudadPeer::retrieveByPK($this->ciudadId);
      $this->comuna = ComunaPeer::retrieveByPK($this->comunaId);
      
      $this->deudor = DeudorPeer::retrieveByPK($datos['deudor_id'])->__toString();
      $this->setTemplate('editdeudordomicilio');
    }
  }

  public function executeDeletedeudordomicilio($request)
  {
    $this->forward404Unless($deudor_domicilio = DeudorDomicilioPeer::retrieveByPk($request->getParameter('id')));

    $deudor_domicilio->delete();
    $this->setTemplate('indexdeudor');
  }
  public function executeListcontrato()
  {
  	$this->contratoList = ContratoPeer::doSelect(new Criteria());
  }
  
	public function executeShowcontrato($request)
  {
  	$criteria = new Criteria();
  	$criteria->add(ContratoPeer::ACREEDOR_ID, $request->getParameter('idacreedor'));
  	$criteria->add(ContratoPeer::ESTADO, true);
    $this->contrato = ContratoPeer::doSelectOne($criteria);
    $this->forward404Unless($this->contrato);
    $this->acreedor = AcreedorPeer::retrieveByPK($this->contrato->getAcreedorId());
  	
    $user = $this->getUser();
  	
  	if($user->isAcreedor())
  	{
  		//$this->acreedor = $user->getAcreedor();
  		//$this->setTemplate('showacreedor','administracion');
  		//$this->pwd = $user->getPassword();
  		$this->perfil = true;
  	}
  }
  
	public function executeCreatecontrato()
  {
    $this->form = new ContratoForm();

    $this->setTemplate('editcontrato');
  }

  public function executeEditcontrato($request)
  {
  	$this->form = new ContratoForm();
    $criteria = new Criteria();
    $criteria->add(AcreedorPeer::ID, $request->getParameter('idacreedor'));
    
    $this->form->setDefault('acreedor_id', AcreedorPeer::doSelectOne($criteria)->getId());
    $this->form->setDefault('estado', true);
    $this->form->setDefault('fecha_inicio',strtotime("now"));
    
    $fecha['day'] = date('d');
    $fecha['month'] = date('m');
    $fecha['year'] = date('Y') + 1;
    
    $this->form->setDefault('fecha_termino',$fecha);
    
    $this->acreedorName = AcreedorPeer::doSelectOne($criteria);
  }

  public function executeUpdatecontrato($request)
  {
    $this->forward404Unless($request->isMethod('post'));
    $this->form = new ContratoForm(ContratoPeer::retrieveByPk($request->getParameter('id')));
    $datos = $request->getParameter('contrato');
    
    $this->cambio_fecha_termino = false;
    
    if($datos['fecha_termino']['day'] == '' && $datos['fecha_termino']['month'] == '' && $datos['fecha_termino']['year'] == '')
    {
    	$datos['fecha_termino']['day'] = date('d');
    	$datos['fecha_termino']['month'] = date('m');
    	$datos['fecha_termino']['year'] = date('Y') + 1;

    	$this->cambio_fecha_termino = true;
    }
    
    $this->form->bind($datos);
    
    if ($this->form->isValid())
    {
      $acreedor = AcreedorPeer::retrieveByPK($this->form->getValue('acreedor_id'));
      
      if($acreedor->tieneContratoActivo()){
      	$contrato = $acreedor->getContratoActivo();
      	$contrato->setEstado(false);
      	$contrato->setFechaTermino("now");
      	$contrato->save();
      }
      
	  /*if($this->cambio_fecha_termino)
	  {
	    $datos['fecha_termino'] = null;
	    $this->form->bind($datos);
	  }*/
	  
      $this->contrato = $this->form->save();
      $this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showcontrato');
    }
    else
    {
      /*if($this->cambio_fecha_termino)
	  {
	    $datos['fecha_termino'] = null;
	    $this->form->bind($datos);
	  }*/
	  
      $this->acreedorName = $request->getParameter('acreedorName');
      $this->setTemplate('editcontrato');	
    }
  }

  public function executeDesactivarcontrato($request)
  {
    $this->acreedor = AcreedorPeer::retrieveByPK($request->getParameter('idacreedor'));
  	
    $contrato = $this->acreedor->getContratoActivo();
    $contrato->setFechaTermino("now");
    $contrato->setEstado(false);
	$contrato->save();
	$this->getUser()->setAttribute('contrato_desactivado',true);
	$this->setTemplate('showacreedor');
  }
  
  public function executeIndextelefono()
  {
  }
  
  public function executeSearchtelefonodomicilioupdate($request)
  {
  	$criteria = new Criteria();
    $criteria->add(DeudorDomicilioPeer::ID, $request->getParameter('iddeudordomicilio'));
    $this->deudordomicilio = DeudorDomicilioPeer::retrieveByPK($request->getParameter('iddeudordomicilio'));
  	$this->deudor_domicilio = DeudorDomicilioPeer::doSelectOne($criteria)->__toString();
  	$this->telefono_domicilioList = TelefonoDomicilioPeer::getByIdDeudorDomicilio($request->getParameter('iddeudordomicilio'));
  	$this->accion = $request->getParameter('accion');
	$this->setTemplate('listtelefonodomicilio');
  }
  public function executeShowtelefono($request)
  {
  	$this->telefono_domicilio = TelefonoDomicilioPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->telefono_domicilio);
  }

  public function executeCreatetelefono()
  {
    $this->form = new TelefonoDomicilioForm();

    $this->setTemplate('edittelefono');
  }

  public function executeEdittelefono($request)
  {  	
    $this->form = new TelefonoDomicilioForm(TelefonoDomicilioPeer::retrieveByPk($request->getParameter('id')));
    $criteria = new Criteria();
    $criteria->add(DeudorDomicilioPeer::ID, $request->getParameter('iddeudordomicilio'));
    $this->form->setDefault('deudor_domicilio_id', DeudorDomicilioPeer::doSelectOne($criteria)->getId());
    $this->deudordomicilio = DeudorDomicilioPeer::doSelectOne($criteria)->__toString();
  }

  public function executeUpdatetelefono($request)
  {    
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TelefonoDomicilioForm(TelefonoDomicilioPeer::retrieveByPk($request->getParameter('id')));
	
    $datos = $request->getParameter('telefono_domicilio');
    
    $datos['telefono'] = trim($datos['telefono']);
    
    $this->form->bind($datos);
    if ($this->form->isValid())
    {
      $this->telefono_domicilio = $this->form->save();
      $this->getUser()->setAttribute('guardado',true);
	  $this->setTemplate('showtelefono');
    }else{
      $this->deudordomicilio = $request->getParameter('deudordomicilio');
      $this->setTemplate('edittelefono');
    }
  }
	public function executeDeletetelefono($request)
  {
    $this->forward404Unless($telefono_domicilio = TelefonoDomicilioPeer::retrieveByPk($request->getParameter('id')));

    $telefono_domicilio->delete();
	$this->deudor_domicilio = DeudorDomicilioPeer::retrieveByPk($telefono_domicilio->getDeudorDomicilioId());
    $this->setTemplate('showdeudordomicilio');
  }
  public function executeDelete($request)
  {
    $this->forward404Unless($telefono_domicilio = TelefonoDomicilioPeer::retrieveByPk($request->getParameter('id')));

    $telefono_domicilio->delete();
	$this->deudor_domicilio = DeudorDomicilioPeer::retrieveByPk($request->getParameter('id'));
    $this->redirect('telefonodomicilio/index');
  }
  
  public function executeCreaselectgiro($request)
  {
   $this->clase_giro_id = $request->getParameter('clase_giro_id');
  }
  
  public function executeCreaselectciudad($request)
  {
   $this->region_id = $request->getParameter('region_id');
  }
  
  public function executeCreaselectcomuna($request)
  {
   $this->ciudad_id = $request->getParameter('ciudad_id');
  }
}
