<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Roles de Usuario</h1>

<ul class="menu-modulo">
	<li>
		<?php echo link_to_remote('Crear Rol', array(
    		'update' => 'acciones_rol',
    		'url'    => 'sfGuardPermission/create',
		)) ?></li>
	<li>
		<?php echo link_to_remote('Editar Rol', array(
    		'update' => 'acciones_rol',
    		'url'    => 'sfGuardPermission/edit',
		)) ?></li>
	<li>
		<?php echo link_to_remote('Visualizar Rol', array(
    		'update' => 'acciones_rol',
    		'url'    => 'sfGuardPermission/list',
		)) ?></li>
</ul>
<div id="acciones_rol"  style="clear: right"></div>