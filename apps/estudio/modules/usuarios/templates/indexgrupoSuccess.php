<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Grupos de Usuario</h1>

<ul class="menu-modulo">
	<li>
		<?php echo link_to_remote('Crear Grupo de Usuario', array(
    		'update' => 'acciones_grupo',
    		'url'    => 'sfGuardGroup/create',
		)) ?></li>
	<li>
		<?php echo link_to_remote('Editar Grupo de Usuario', array(
    		'update' => 'acciones_grupo',
    		'url'    => 'sfGuardGroup/edit',
		)) ?></li>
	<li>
		<?php echo link_to_remote('Visualizar Grupo de Usuario', array(
    		'update' => 'acciones_grupo',
    		'url'    => 'sfGuardGroup/list',
		)) ?></li>
</ul>
<div id="acciones_grupo"  style="clear: right"></div>