<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Usuarios</h1>

<ul class="menu-modulo">
	<li>
		<?php echo link_to_remote('Crear Usuario', array(
    		'update' => 'acciones_usuario',
    		'url'    => 'sfGuardUser/create',
		)) ?></li>
	<li>
		<?php echo link_to_remote('Editar Usuario', array(
    		'update' => 'acciones_usuario',
    		'url'    => 'sfGuardUser/edit',
		)) ?></li>
	<li>
		<?php echo link_to_remote('Visualizar Usuario', array(
    		'update' => 'acciones_usuario',
    		'url'    => 'sfGuardUser/list',
		)) ?></li>
</ul>
<div id="acciones_usuario"  style="clear: right"></div>