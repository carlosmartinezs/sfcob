<?php use_helper('Javascript', 'Object'); ?>
<?php 
	switch($accion)
	{
		case 'edit':
			echo "<h2>Tareas postergables</h2>";
			break;
		case 'vencidas':
			echo "<h2>Tareas que vencieron</h2>";
			break;
		case 'delete':
			echo "<h2>Tareas a eliminar</h2>";
			break;
		case 'realizadas':
			echo "<h2>Tareas realizadas</h2>";
			break;
	}
?>
<?php $count = count($agendaList)?>
<table>
  <thead>
  <?php if($accion == 'tareasvencidas'):?>
  	<tr>
		<th colspan="4" class="tddestacado"><h2>Tareas vencidas</h2></th>
	</tr>
  <?php endif;?>
    <tr>
      <th class="tddestacado">Fecha - Hora</th>
      <th class="tddestacado">Descripci&oacute;n</th>
      <th class="tddestacado">Realizada</th>
      <?php if($accion != 'realizadas'):?>
      <th class="tddestacado">Acci&oacute;n</th>
      <?php endif;?>
    </tr>
  </thead>
  <tbody>
  <?php if(count($agendaList) > 0):?>
    <?php foreach ($agendaList as $agenda): ?>
		    <tr>
		    	<td class="tdcentrado"><?php 
		    			echo Funciones::fecha2($agenda->getFecha('d-m-Y'));
				    	echo $agenda->getFecha('\ \a\ \l\a\s H:i').' hrs.';?></td>
		      	<td><?php echo $agenda->getDescripcion() ?></td>
		      	<td class="tdcentrado"><?php echo ($agenda->getRealizada() ? image_tag('accept.png',array('size'=>'16x16')) : image_tag('delete.png',array('size'=>'16x16'))) ?></td>
		     	<?php if($accion != 'realizadas'):?>
		     	<td class="tdcentrado"><?php 
					switch($accion)
					{
						case 'edit':
							echo button_to_remote('Postergar', array(
								'update' => 'acciones_agenda',
								'url' => 'agenda/edit?id='.$agenda->getId(),
								'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
								'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
								),array('class' => 'editar boton_con_imagen'));
							break;
						case 'vencidas':
							echo button_to_remote('Realizada', array(
								'update' => 'acciones_agenda',
								'url' => 'agenda/realizada?id='.$agenda->getId().'&accion=vencidas',
								'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
								'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
								'confirm' => 'La tarea se marcar&aacute; como \'Realizada\', esta acci&oacute;n es irreversible. Desea continuar?',
								),array('class' => 'aceptar boton_con_imagen'));?>&nbsp;<?php 
							echo button_to_remote('Postergar', array(
								'update' => 'acciones_agenda',
								'url' => 'agenda/edit?id='.$agenda->getId().'&accion=vencidas',
								'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
								'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
								),array('class' => 'editar boton_con_imagen'));
							break;
						case 'delete':
							echo button_to_remote('Eliminar', array(
								'update' => 'acciones_agenda',
								'url' => 'agenda/delete?id='.$agenda->getId(),
								'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
								'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
								),array('class' => 'eliminar boton_con_imagen'));
							break;
						case 'tareasvencidas':
							if($count == 1)
			    			{
			    				echo button_to_remote('Realizada', array(
									'update' => 'vencidas',
									'url' => 'agenda/realizada?id='.$agenda->getId().'&accion=tareasvencidas',
									'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
									'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)).
			    									visual_effect('fade','vencidas', array('duration' => 0.5)),
									'confirm' => 'La tarea se marcar&aacute; como \'Realizada\', esta acci&oacute;n es irreversible. Desea continuar?',
									),array('class' => 'aceptar boton_con_imagen'));?>&nbsp;<?php
								echo button_to_remote('Postergar', array(
										'update' => 'vencidas',
										'url' => 'agenda/edit?id='.$agenda->getId().'&accion=tareasvencidas&count=1',
										'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
										'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
										),array('class' => 'editar boton_con_imagen'));
			    			}
			    			else
			    			{
			    				echo button_to_remote('Realizada', array(
									'update' => 'vencidas',
									'url' => 'agenda/realizada?id='.$agenda->getId().'&accion=tareasvencidas',
									'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
									'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
									'confirm' => 'La tarea se marcar&aacute; como \'Realizada\', esta acci&oacute;n es irreversible. Desea continuar?',
									),array('class' => 'aceptar boton_con_imagen'));?>&nbsp;<?php 
								echo button_to_remote('Postergar', array(
									'update' => 'vencidas',
									'url' => 'agenda/edit?id='.$agenda->getId().'&accion=tareasvencidas',
									'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
									'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
									),array('class' => 'editar boton_con_imagen'));
			    			}
			    			break;
					 } ?>
		    	</td>
		    	<?php endif;?>
		    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr><td class="aviso" colspan="4">NO SE ENCONTRARON TAREAS</td></tr>
    <?php endif;?>
  </tbody>
</table>