<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $agenda->getId() ?></td>
    </tr>
    <tr>
      <th>Perfil:</th>
      <td><?php echo $agenda->getPerfilId() ?></td>
    </tr>
    <tr>
      <th>Descripcion:</th>
      <td><?php echo $agenda->getDescripcion() ?></td>
    </tr>
    <tr>
      <th>Fecha:</th>
      <td><?php echo $agenda->getFecha() ?></td>
    </tr>
    <tr>
      <th>Realizada:</th>
      <td><?php echo $agenda->getRealizada() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('agenda/edit?id='.$agenda->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('agenda/index') ?>">List</a>
