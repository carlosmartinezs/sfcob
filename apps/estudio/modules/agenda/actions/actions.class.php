<?php

/**
 * agenda actions.
 *
 * @package    sgcj
 * @subpackage agenda
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class agendaActions extends sfActions
{
  public function executeIndex()
  {
  	$c = new Criteria();
  	$c->add(AgendaPeer::PERFIL_ID,$this->getUser()->getPerfil()->getId());
  	//$c->add(AgendaPeer::REALIZADA,false);
  	$c->addAscendingOrderByColumn(AgendaPeer::FECHA);
    $this->agendaList = AgendaPeer::doSelect($c);
  }

  public function executeList($request)
  {
  	$this->accion = $request->getParameter('accion');
  	
  	$c = new Criteria();
  	$c->add(AgendaPeer::PERFIL_ID,$this->getUser()->getPerfil()->getId());
  	$c->add(AgendaPeer::REALIZADA,false);
  	
  	if($this->accion == 'vencidas' || $this->accion == 'tareasvencidas')
  	{
  		$c->add(AgendaPeer::FECHA,date('Y-m-d'),Criteria::LESS_EQUAL);
  	}
  	else if($this->accion == 'delete')
  	{
  		$c->add(AgendaPeer::FECHA,date('Y-m-d'),Criteria::GREATER_EQUAL);
  	}
  	else if ($this->accion == 'realizadas')
  	{
  		$c->add(AgendaPeer::REALIZADA,true);
  	}
  	
  	$c->addAscendingOrderByColumn(AgendaPeer::FECHA);
    $this->agendaList = AgendaPeer::doSelect($c);
    
    
  }
  
  public function executeShow($request)
  {
    $this->agenda = AgendaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->agenda);
  }

  public function executeCreate()
  {
    $this->form = new AgendaForm();
	$this->form->setDefault('fecha_actual',date('Y-m-d H:i'));
    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new AgendaForm(AgendaPeer::retrieveByPk($request->getParameter('id')));
    $this->form->setDefault('fecha_actual',date('Y-m-d H:i'));
    $this->accion = $request->getParameter('accion');
    if($this->request->hasParameter('count'))
    {
    	$this->count = $this->request->getParameter('count');
    }
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new AgendaForm(AgendaPeer::retrieveByPk($request->getParameter('id')));

    $datos = $request->getParameter('agenda');
    $datos['perfil_id'] = $this->getUser()->getPerfil()->getId();
    $datos['realizada'] = false;
    
    $this->form->bind($datos);
    if ($this->form->isValid())
    {
      $agenda = $this->form->save();
      
	    if($request->getParameter('accion')== 'vencidas')
	    {   
	    	$this->accion = 'vencidas';
	    	$this->forward('agenda','list');
	    }

    	if($request->getParameter('accion')== 'tareasvencidas')
	    {   
	    	$this->accion = 'tareavencidas';
	    	$this->forward('agenda','list');
	    }
      
      $this->accion = 'edit';
      $this->forward('agenda','list');
    }
    
	$this->accion = $request->getParameter('accion');
    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($agenda = AgendaPeer::retrieveByPk($request->getParameter('id')));

    $agenda->delete();

    $this->accion = 'delete';
    $this->forward('agenda','list');
    
  }
  
  public function executeRealizada($request)
  {
  	$this->accion = $request->getParameter('accion');
  	$this->agenda = AgendaPeer::retrieveByPk($request->getParameter('id'));
  	$this->agenda->setRealizada(true);
  	$this->agenda->save();
  	
  	if($this->accion == 'vencidas')
    {
    	$this->forward('agenda','list');
    }
    
  	if($this->accion == 'tareasvencidas')
    {   
    	$this->forward('agenda','list');
    }
  }
  
  public function executeTareasrealizadas()
  {
  	$this->getUser()->getId();
  }
}
