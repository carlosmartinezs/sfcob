<?php

/**
 * usuario actions.
 *
 * @package    sgcj
 * @subpackage usuario
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class usuarioActions extends sfActions
{
  public function executeIndex($request)
  {
  	$this->accion = $request->getParameter('accion');
    $this->sf_guard_userList = sfGuardUserPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->sf_guard_user = sfGuardUserPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->sf_guard_user);
  }

  public function executeCreate()
  {
    $this->form = new sfGuardUserForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
  	if($request->hasParameter('usuario'))
  	{
  		$c = new Criteria();
  		$c->add(sfGuardUserPeer::USERNAME,$request->getParameter('usuario'));
  		$sfGuardUser = sfGuardUserPeer::doSelectOne($c);
  		
  		if($sfGuardUser instanceof sfGuardUser)
  		{
  			$this->form = new sfGuardUserForm(sfGuardUserPeer::retrieveByPk($sfGuardUser->getId()));
  		}
  		else
  		{
  			return $this->renderText('
  			<div style="border: 1px solid #1C384D; margin: 10px 10px 20px; padding: 10px;background: #EEEEEE;">
  				<span class="aviso">No se encontr&oacute; el usuario.</span>
  			</div>
  			');
  		}
  		
  	}
  	else
  	{
  		$this->form = new sfGuardUserForm(sfGuardUserPeer::retrieveByPk($request->getParameter('id')));
  	}
    
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new sfGuardUserForm(sfGuardUserPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('sf_guard_user'));
    if ($this->form->isValid())
    {
      $sf_guard_user = $this->form->save();

      $this->redirect('usuario/edit?id='.$sf_guard_user->getId());
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($sf_guard_user = sfGuardUserPeer::retrieveByPk($request->getParameter('id')));

    $sf_guard_user->delete();

    $this->redirect('usuario/index');
  }
  
  public function executeAutocompletar($request)
  {
  	$c = new Criteria();
  	$c->add(sfGuardUserPeer::USERNAME,$request->getParameter('usuario').'%',Criteria::LIKE);
  	$this->usuarioList =sfGuardUserPeer::doSelect($c);
  }
  
  public function executeEditarAutocompletar($request)
  {
  }
}
