<?php use_helper('Javascript')?>
<?php $sf_guard_user = $form->getObject() ?>
<h2><?php echo $sf_guard_user->isNew() ? 'Ingresar Usuario' : 'Editar Usuario: '.$sf_guard_user->getUsername() ?> </h2>

<!--  
<form action="<?php //echo url_for() ?>" method="post" <?php //$form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
-->
<?php echo form_remote_tag(array(
	'script' 	=> 'true',
    'update'   	=> ($sf_guard_user->isNew() ? 'acciones_usuario' : 'autocompletar'),
    'url'      	=> 'usuario/update'.(!$sf_guard_user->isNew() ? '?id='.$sf_guard_user->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading').
					visual_effect('appear','ingresa-contrato'),
))?>  
  <table>
    <tfoot>
      <tr>
        <td colspan="2" class="tdcentrado">
          
          <input type="submit" value="Guardar" class="guardar boton_con_imagen" />
          <?php echo button_to_remote('Cancelar',array(
		          	'script' 	=> 'true',
		    		'update'   	=> 'acciones_usuario',
		    		'url'      	=> 'usuario/list',
					'loading'	=> visual_effect('appear','loading'),
					'complete'	=> visual_effect('fade','loading'),
		          ),array('class'=>'cancelar boton_con_imagen'))?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['username']->renderLabel() ?></th>
        <td>
          <?php echo $form['username']->renderError() ?>
          <?php echo $form['username'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['password']->renderLabel() ?></th>
        <td>
          <?php echo $form['password']->renderError() ?>
          <?php echo $form['password'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['password_again']->renderLabel() ?></th>
        <td>
          <?php echo $form['password_again']->renderError() ?>
          <?php echo $form['password_again'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
