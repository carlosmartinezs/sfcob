<?php use_helper('Javascript')?>
<div id="acciones_usuario" style="clear: right">
<?php echo form_remote_tag(array(
	'script' 	=> 'true',
    'update'   	=> 'autocompletar',
    'url'      	=> 'usuario/edit',
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading').
					visual_effect('appear','ingresa-contrato'),
),array('style'=>'border: 1px solid #1C384D; margin: 10px 10px 20px; padding: 10px;background: #EEEEEE;'))?>
  Buscar usuario por nombre:
  <?php echo input_auto_complete_tag('usuario','',
    'usuario/autocompletar',
    array('autocomplete' => 'off'),
    array('use_style'    => true,'indicator'=>'indicator')) ?>
  	
  <input type="submit" value="Buscar" class="buscar boton_con_imagen"/>
  <span id="indicator" style="display: none"><?php echo image_tag('large-loading.gif',array('size'=>'16x16'))?>Buscando...</span>
</form>
<div id="autocompletar">

</div>
</div>