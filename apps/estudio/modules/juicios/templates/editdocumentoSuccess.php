<?php use_helper('Javascript', 'Object') ?>
<?php $documento = $form->getObject() ?>
<h2><?php echo $documento->isNew() ? 'Ingresar' : 'Editar' ?> Documento</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_causa',
    'url'      	=> 'juicios/updatedocumento'.(!$documento->isNew() ? '?id='.$documento->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
<div><?php echo $form->renderGlobalErrors() ?></div>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="2">
        <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
          <?php echo button_to_remote('Cancelar', array(
			'update' => 'modulo',
			'url' => 'juicios/indexcausa',
			'loading' => visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
			),array('class' => 'cancelar boton_con_imagen'))?>
          
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <th><label for="documento_causa_id">Causa</label></th>
        <td>
          <?php echo $form['causa_id']->renderError() ?>
          <?php echo input_hidden_tag('causa_id', $causaId);
          		echo $nombreCausa; ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_tipo_documento_id">Tipo de Documento *</label></th>
        <td>
          <?php echo $form['tipo_documento_id']->renderError() ?>
          <?php echo $form['tipo_documento_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_nro_documento">N&uacute;mero de Documento *</label></th>
        <td>
          <?php echo $form['nro_documento']->renderError() ?>
          <?php echo $form['nro_documento'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_fecha_emision">Fecha de Emisi&oacute;n *</label></th>
        <td>
          <?php echo $form['fecha_emision']->renderError() ?>
          <?php echo $form['fecha_emision'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_fecha_vencimiento">Fecha de Vencimiento *</label></th>
        <td>
          <?php echo $form['fecha_vencimiento']->renderError() ?>
          <?php echo $form['fecha_vencimiento'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_monto">Monto Total * </label><br/>
        <span class="small-font">(S&oacute;lo N&uacute;meros)</span></th>
        <td>
          <?php echo $form['monto']->renderError() ?>
          <?php echo $form['monto'] ?>
        </td>
      </tr>
      <tr>
      	<th><label for="documento_interes">Porcentaje de Inter&eacute;ses * </label>
      	</th>
      	<td>
      	  <?php echo $form['interes']->renderError() ?>
          <?php echo $form['interes'] ?>
          <?php echo "%" ?>
          <span class="small-font">Para decimal use `.` (punto)</span>
      	</td>
      </tr>
      <tr>
        <th><label for="documento_descripcion">Descripci&oacute;n *</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
