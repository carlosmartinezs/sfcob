<?php
use_helper('DateForm');
use_helper('Object');	
use_helper('Javascript');

echo input_hidden_tag('tribunal_id', $tribunal->getId());
echo input_tag('tribunal', $tribunal,array('readonly' => 'readonly'));
	
	echo button_to_remote('Cambiar', array(
          'update'	=> 'list-search-tribunal',
          'url'		=> 'administracion/searchtribunal?accion=crearcausa',
          'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
		  'complete'	=> visual_effect('appear', 'list-search-tribunal').
         				   visual_effect('highlight','list-search-tribunal',array('duration' => 1.0)).
          				   visual_effect('fade','loading', array('duration' => 0)),
          ),array('class' => 'cambiar boton_con_imagen')); 
?>	