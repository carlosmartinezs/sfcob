<?php use_helper('Javascript') ?>
<h2>Lista de Documentos: <?php echo $causa?></h2>

<table>
  <thead>
    <tr>
      <th class="tddestacado">Rol Causa</th>
      <th class="tddestacado">Tipo de Documento</th>
      <th class="tddestacado">Monto</th>
      <th class="tddestacado">Inter&eacute;s</th>
      <th class="tddestacado">N&deg; de Documento</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  <?php if(count($documentoList) > 0):?>
		  	<?php if ($accion == 'showdocumento'): ?>
		    <?php foreach ($documentoList as $documento): ?>
		    <tr>
		      <td><?php echo $documento->getCausa()->getRol() ?></td>
		      <td><?php echo $documento->getTipoDocumento() ?></td>
		      <td>$ <?php echo $documento->getMonto() ?></td>
		      <td><?php echo $documento->getInteres() ?> &#37;</td>
		      <td><?php echo $documento->getNroDocumento() ?></td>
		      <td class="tdcentrado"><?php echo button_to_remote('Ver', array(
					'update' => 'acciones_causa',
					'url' => 'juicios/showdocumento?id='.$documento->getId(),
					'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
					),array('class' => 'detalles boton_con_imagen'));?></td> 
		    </tr>
		    <?php endforeach; ?>
		    <?php endif; ?>
		    <?php if ($accion == 'deletedocumento'): ?>
		    <?php foreach ($documentoList as $documento): ?>
		    <tr>
		      <td><?php echo $documento->getCausa()->getRol() ?></td>
		      <td><?php echo $documento->getTipoDocumento() ?></td>
		      <td>$ <?php echo $documento->getMonto() ?></td>
		      <td><?php echo $documento->getInteres() ?> %</td>
		      <td><?php echo $documento->getNroDocumento() ?></td>
		      <td><?php echo button_to_remote('Eliminar', array(
								'update' => 'modulo',
								'url' => 'juicios/deletedocumento?id='.$documento->getId().'&causaid='.$causa->getId(),
		      					'confirm'  => 'Est&aacute; seguro que desea eliminar el documento de la causa?',
								'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
								'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)).
		      									"alert('Se ha eliminado el documento')",)
		      ,array('class' => 'eliminar boton_con_imagen'));?></td>
		    </tr>
		    <?php endforeach; ?>
		    <?php endif; ?>
    <?php else:?>
    <tr><td class="aviso" colspan="6">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td></tr>
    <?php endif;?>
  </tbody>
</table>