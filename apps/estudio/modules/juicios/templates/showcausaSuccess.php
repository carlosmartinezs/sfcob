<?php use_helper('Javascript') ?>
<h2>Ver Causa</h2>
<table>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="6">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Inicio:</th>
      <td colspan="2"><?php echo Funciones::fecha2($causa->getFechaInicio('d-m-Y')) ?></td>
      <th>T&eacute;rmino:</th>
      <td colspan="2"><?php echo $causa->getFechaTermino('d-m-Y') ? Funciones::fecha2($causa->getFechaTermino('d-m-Y')) : 'No estipulada.';  ?></td>
    </tr>
    
    <tr>
      <th>Car&aacute;tula</th>
      <td colspan="5"><?php echo $causa->getCaratula()?></td>
    </tr>
    <tr>
      <th>Honorarios:</th>
      <td colspan="2">$ <?php echo $causa->getDeuda()->getHonorarios() ?></td>
      <th>Gastos:</th>
      <td colspan="2">$ <?php echo $causa->getGastos()?></td>
    </tr>
	<tr>
	  <th>Periodo Actual:</th>
      <td colspan="5"><?php echo $causa->getPeriodoCausaActual()->getPeriodo() ?></td>
	</tr>
	
    <tr>
      <th>Rol:</th>
      <td colspan="2"><?php echo $causa->getRol() ?></td>
      <th>Tipo:</th>
      <td colspan="2"><?php echo $causa->getTipoCausa() ?></td>
    </tr>
    
    <tr>
      <th>Herencia Exhorto:</th>
      <td colspan="2"><?php echo ($causa->getHerenciaExhorto() ? 'Si' : 'No')?></td>
      <th>Causa Base:</th>
      <td colspan="2"><?php echo ($causa->getCausaExhorto() ? $causa->getCausaExhorto() : '-') ?></td>
    </tr>
    
    <tr>
      <th>Acreedor:</th>
      <td colspan="5"><?php echo $causa->getAcreedor() ?></td>
    </tr>
    
    <tr>
      <th>Deudor:</th>
      <td colspan="5"><?php echo $causa->getNombreDeudor() ?></td>
    </tr>
    
    <tr>
      <th>Estado Actual:</th>
      <td colspan="2"><?php echo $causa->getEstadoCausaActivo()->getEstado() ?></td>
      <th>Tribunal:</th>
      <td colspan="2"><?php echo $causa->getTribunalActual() ?></td>
    </tr>
    
    <tr>
      
      <th>Materia:</th>
      <td colspan="2"><?php echo $causa->getNombreMateria() ?></td>
      <th>Competencia:</th>
      <td colspan="2"><?php echo $causa->getCompetencia() ?></td>
    </tr>
    <tr>
      <td colspan="6">
        <table style="border:1px solid #1C384D; margin: 5px auto; width: 100%;padding:0;">
          	<tr>
    			<th class="tdcentrado" colspan="9" style="border:0;background:#1C384D;color:white;">Saldos</th>
		    </tr>
		    <tr>
		    	<th class="tdcentrado" colspan="3">Cuant&iacute;a</th>
		    	<th class="tdcentrado" colspan="3">Abonos</th>
		    	<th class="tdcentrado" colspan="3">Pendiente</th>
		    </tr>
		    <tr>
		    	<td colspan="3">$ <?php echo $causa->getCuantia()?></td>
		    	<td colspan="3">$ <?php echo $causa->getTotalAbono()?></td>
		    	<td colspan="3">$ <?php echo $causa->getDeudaPendiente()?></td>
		    </tr>
		    <tr>
		    	<th class="tdcentrado">Monto</th>
		    	<th class="tdcentrado">Inter&eacute;s</th>
		    	<th class="tdcentrado">Gastos</th>
		    	<th class="tdcentrado">Monto</th>
		    	<th class="tdcentrado">Inter&eacute;s</th>
		    	<th class="tdcentrado">Gastos</th>
		    	<th class="tdcentrado">Monto</th>
		    	<th class="tdcentrado">Inter&eacute;s</th>
		    	<th class="tdcentrado">Gastos</th>
		    </tr>
		    <tr>
		    	<td>$ <?php echo $causa->getDeudaMonto()?></td>
		    	<td>$ <?php echo $causa->getDeudaInteres()?></td>
		    	<td>$ <?php echo $causa->getDeudaGastosCobranza()?></td>
		    	<td>$ <?php echo $causa->getAbonoMonto()?></td>
		    	<td>$ <?php echo $causa->getAbonoInteres()?></td>
		    	<td>$ <?php echo $causa->getAbonoGastosCobranza()?></td>
		    	<td>$ <?php echo $causa->getDeudaPendienteMonto()?></td>
		    	<td>$ <?php echo $causa->getDeudaPendienteInteres()?></td>
		    	<td>$ <?php echo $causa->getDeudaPendienteGastosCobranza()?></td>
		    </tr>
        </table>
      </td>
    </tr>
  </tbody>
  <?php if(!$causa->estaTerminada()):?>
  <tfoot>
  	<tr>
  		<td class="tdcentrado" colspan="6">
  			<?php 
  					echo button_to_remote('Editar', array(
							'update'	=> 'acciones_causa',
							'url'		=> 'juicios/editcausa?id='.$causa->getId(),
							'loading'	=> visual_effect('appear','loading'),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'editar boton_con_imagen'));
						
  					echo button_to_remote('A&ntilde;adir Documento', array(
						'update'	=> 'acciones_causa',
						'url'		=> 'juicios/createdocumento?idcausa='.$causa->getId(),
						'loading'	=> visual_effect('appear','loading'),
						'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'anadir boton_con_imagen'));
					echo button_to_remote('A&ntilde;adir Garant&iacute;a', array(
						'update'	=> 'acciones_causa',
						'url'		=> 'juicios/creategarantia?idcausa='.$causa->getId(),
						'loading'	=> visual_effect('appear','loading'),
						'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => 'anadir boton_con_imagen'));
			?>
  		</td>
  	</tr>
  </tfoot>
  <?php endif;?>
</table>
