<?php use_helper('Javascript', 'Object'); ?>
<?php $abono = $form->getObject() ?>
<h2><?php echo $abono->isNew() ? 'Ingresar' : 'Editar' ?> Abono</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_abono',
    'url'      	=> 'juicios/updateabono'.(!$abono->isNew() ? '?id='.$abono->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
<div><?php echo $form->renderGlobalErrors() ?></div>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="2">
        <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
          <?php echo button_to_remote('Cancelar', array(
			'update' => 'modulo',
			'url' => 'juicios/indexabono',
			'loading' => visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
			),array('class' => 'cancelar boton_con_imagen'))?>
          
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <th><label for="abono_deuda_id">Causa</label></th>
        <td>
          <?php echo $form['causa_id']->renderError() ?>
          <?php echo input_hidden_tag('causa_id', $causaId);
          		echo $nombreCausa; ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_tipo_abono_id">Tipo de Abono * </label></th>
        <td>
          <?php echo $form['tipo_abono_id']->renderError() ?>
          <?php echo $form['tipo_abono_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_tipo_pago_id">Tipo de Pago * </label></th>
        <td>
          <?php echo $form['tipo_pago_id']->renderError() ?>
          <?php echo $form['tipo_pago_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_deuda_id">Deuda</label></th>
        <td>
        	<?php echo input_hidden_tag('deuda_id',$deudaId);
        		  echo 'Monto Base Pendiente: $ '.$montoDeudaPendiente; ?>
        </td>
      </tr>
      <tr>
      	<th><label for="abono_deuda_id"></label></th>
      		<td>
         	<?php echo 'Inter&eacute;s Pendiente: $ '.$interesDeudaPendiente; ?>
      		</td>
      </tr>
      <tr>
      	<th><label for="abono_deuda_id"></label></th>
      		<td>
         	<?php echo 'Gastos Cobranza Pendiente: $ '.$gastosCobranzaDeudaPendiente; ?>
      		</td>
      </tr>
      <tr>
        <th><label for="abono_valor">Monto a Abonar * </label><br/>
        <span class="small-font">(S&oacute;lo N&uacute;meros)</span></th>
        <td>
          <?php echo $form['valor']->renderError() ?>
          <?php echo $form['valor'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_fecha">Fecha * </label></th>
        <td>
          <?php echo $form['fecha']->renderError() ?>
          <?php echo $form['fecha'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_motivo">Motivo * </label></th>
        <td>
          <?php echo $form['motivo']->renderError() ?>
          <?php echo $form['motivo'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_nro_recibo">N&uacute;mero de Recibo * </label><br/>
        <span class="small-font">(S&oacute;lo N&uacute;meros)</span></th>
        <td>
          <?php echo $form['nro_recibo']->renderError() ?>
          <?php echo $form['nro_recibo'] ?>

        <?php echo $form['id'] ?>
        <?php echo $form['pendiente'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>