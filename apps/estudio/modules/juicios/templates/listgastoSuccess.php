<?php use_helper('Javascript') ?>
<h2>Lista de Gastos</h2>

<table>
  <thead>
    <tr>
      <th class="tddestacado">Tipo de Gasto</th>
      <th class="tddestacado">Monto</th>
      <th class="tddestacado">Fecha</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  	<?php if(count($gastoList) > 0):?>
    <?php foreach ($gastoList as $gasto): ?>
    <tr>
      <td><?php echo $gasto->getTipoGasto() ?></td>
      <td>$ <?php echo $gasto->getMonto() ?></td>
      <td><?php echo Funciones::fecha2($gasto->getFecha('d-m-Y')) ?></td>
      <td class="tdcentrado"><?php if($accion == 'editgasto'):
      				echo button_to_remote('Editar Gasto', array(
						'update' => 'acciones_gasto',
						'url' => 'juicios/editgasto?id='.$gasto->getId().'&diligencia_id='.$gasto->getDiligenciaId(),
						'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
						),array('class' => 'editar boton_con_imagen'));
      			elseif($accion == 'showgasto'):
      				echo button_to_remote('Ver Gasto', array(
						'update' => 'acciones_gasto',
						'url' => 'juicios/showgasto?id='.$gasto->getId().'&diligencia_id='.$gasto->getDiligenciaId(),
						'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
						'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
						),array('class' => 'detalles boton_con_imagen'));
      			endif; ?>
    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr>
    	<td colspan="4" class="aviso">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td>
    </tr>
    <?php endif;?>
  </tbody>
</table>
