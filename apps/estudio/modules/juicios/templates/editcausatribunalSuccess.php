<?php use_helper('Javascript', 'Object') ?>
<?php $causa_tribunal = $form->getObject() ?>
<h2>Editar Tribunal de Causa</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'tribunal',
    'url'      	=> 'juicios/updatecausatribunal',
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','form-cambio-tribunal', array('duration' => 0.5)).
				   visual_effect('highlight','form-cambio-tribunal',array('duration' => 1.0)).
				   visual_effect('fade','loading', array('duration' => 0.5)),
	'confirm'	=> "Los cambios realizados se ver&aacute;n reflejados al momento de guardar la causa.",
))?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo button_to_function('Cerrar ventana',
				visual_effect('appear','loading').
				visual_effect('highlight','form-cambio-tribunal', array('duration' => 0.5)).
				visual_effect('fade','form-cambio-tribunal', array('duration' => 0.5)).
				visual_effect('fade','loading', array('duration' => 1.0)),
				array('class' => 'cerrar boton_con_imagen'));?>
          <input type="submit" value="Aceptar" class="aceptar boton_con_imagen" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="causa_tribunal_causa_id">Causa</label></th>
        <td>
          <?php echo input_hidden_tag('causa_id', $causa->getId());
          		echo $causa; ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_tribunal_tribunal_id">Tribunal *</label></th>
        <td>
        <div id="causa_tribunal">
          	<?php echo input_tag('tribunal',$tribunal,array('readonly' => 'readonly'));
          			echo button_to_remote('Buscar', array(
          				'update'	=> 'list-search-tribunal',
          				'url'		=> 'administracion/searchtribunal?accion=editcausa',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('appear', 'list-search-tribunal').
          							   visual_effect('highlight','list-search-tribunal',array('duration' => 1.0)).
          							   visual_effect('fade','loading', array('duration' => 0)), )); ?></div>
        </td>
      </tr>
      <tr>
        <th><label for="causa_tribunal_motivo">Motivo *</label></th>
        <td>
          <?php echo $form['motivo']->renderError() ?>
          <?php echo $form['motivo'] ?>
          
          <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
