<?php use_helper('Javascript') ?>
<h2>Lista de Diligencias</h2>

<table>
  <thead>
    <tr>
      <th class="tddestacado">N&ordm; Causa</th>
      <th class="tddestacado">Rol Causa</th>
      <th class="tddestacado">Periodo</th>
      <th class="tddestacado">Fecha</th>
      <th class="tddestacado">Descripci&oacute;n</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  	<?php if(count($diligenciaList) > 0):?>
    <?php foreach ($diligenciaList as $diligencia): ?>
    <tr>
      <td><?php echo $diligencia->getPeriodoCausa()->getCausa()->getId() ?></td>
      <td><?php echo $diligencia->getPeriodoCausa()->getCausa()->getRol() ?></td>
      <td><?php echo $diligencia->getPeriodoCausa()->getPeriodo()->getNombre() ?></td>
      <td><?php echo Funciones::fecha2($diligencia->getFecha('d-m-Y')) ?></td>
      <td><?php echo $diligencia->getDescripcion() ?></td>
      <td class="tdcentrado"><?php if ($accion == 'editdiligencia'): ?>
		      	<?php echo button_to_remote('Editar', array(
					'update' => 'acciones_diligencia',
					'url' => 'juicios/editdiligencia?id='.$diligencia->getId().'&idcausa='.$causa->getId(),
					'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
					),array('class' => 'editar boton_con_imagen'));?>
			<?php endif; ?>
			<?php if ($accion == 'showdiligencia'): ?>
				<?php echo button_to_remote('Ver', array(
					'update' => 'acciones_diligencia',
					'url' => 'juicios/showdiligencia?id='.$diligencia->getId().'&causa_id='.$causa->getId(),
					'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
					),array('class' => 'detalles boton_con_imagen'));?>
			<?php endif; ?>
			<?php if ($accion == 'creagasto'): ?>
				<?php echo button_to_remote('A&ntilde;adir Gasto', array(
					'update' => 'acciones_gasto',
					'url' => 'juicios/creategasto?iddiligencia='.$diligencia->getId().'&idcausa='.$causa->getId(),
					'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
					),array('class' => 'anadir boton_con_imagen'));?>
			<?php endif; ?>
			<?php if ($accion == 'editgasto'): ?>
				<?php echo button_to_remote('Ver Gastos', array(
					'update' => 'acciones_gasto',
					'url' => 'juicios/listgasto?accion=editgasto&iddiligencia='.$diligencia->getId().'&idcausa='.$causa->getId(),
					'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
					),array('class' => 'detalles boton_con_imagen'));?>
			<?php endif; ?>
    		<?php if ($accion == 'showgasto'): ?>
    			<?php echo button_to_remote('Ver Gastos', array(
					'update' => 'acciones_gasto',
					'url' => 'juicios/listgasto?accion=showgasto&iddiligencia='.$diligencia->getId().'&idcausa='.$causa->getId(),
					'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
					),array('class' => 'detalles boton_con_imagen'));?>
			<?php endif; ?>
		</td>
    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr><td class="aviso" colspan="6">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td></tr>
    <?php endif;?>
  </tbody>
</table>
