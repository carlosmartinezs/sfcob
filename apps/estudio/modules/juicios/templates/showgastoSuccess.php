<h2>Ver Gasto</h2>
<table>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="4">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Tipo de Gasto:</th>
      <td><?php echo $gasto->getTipoGasto() ?></td>
    </tr>
    <tr>
      <th>Diligencia:</th>
      <td><?php echo $gasto->getDiligencia()->getDescripcion() ?></td>
    </tr>
    <tr>
      <th>Monto:</th>
      <td>$ <?php echo $gasto->getMonto() ?></td>
    </tr>
    <tr>
      <th>Fecha:</th>
      <td><?php echo Funciones::fecha2($gasto->getFecha('d-m-Y')) ?></td>
    </tr>
    <tr>
      <th>Motivo:</th>
      <td><?php echo $gasto->getMotivo() ?></td>
    </tr>
  </tbody>
</table>