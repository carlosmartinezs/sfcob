<?php use_helper('Javascript','Object') ?>
<h2>
        <?php 	switch($accion):
        			case 'edit':
						echo "Editar Causa: Buscar";
						break;
					case 'show':
						echo "Ver Causa: Buscar";
						break;
					case 'redactarcarta':
						echo "Redactar Carta: Buscar Causa";
						break;
					case 'redactarescrito':
						echo "Redactar Escrito: Buscar Causa";
						break;
					case 'crearcausa':
						echo "Crear Causa: Buscar Causa Base";
						break;
					case 'editcausa':
						echo "Editar Causa: Buscar Causa Base";
						break;
					case 'creadocumento':
						echo "Ingresar Documento: Buscar Causa";
						break;
					case 'showdocumento':
						echo "Ver Documento: Buscar Causa";
						break;
					case 'deletedocumento':
						echo "Eliminar Documento: Buscar Causa";
						break;
					case 'creagarantia':
						echo "Ingresar Garant&iacute;a: Buscar Causa";
						break;
					case 'editgarantia':
						echo "Editar Garant&iacute;a: Buscar Causa";
						break;
					case 'showgarantia':
						echo "Ver Garant&iacute;a: Buscar Causa";
						break;
					case 'creadiligencia':
						echo "Ingresar Diligencia: Buscar Causa";
						break;
					case 'editdiligencia':
						echo "Editar Diligencia: Buscar Causa";
						break;
					case 'showdiligencia':
						echo "Ver Diligencia: Buscar Causa";
						break;
					case 'creaabono':
						echo "Ingresar Abono: Buscar Causa";
						break;
					case 'editabono':
						echo "Editar Abono: Buscar Causa";
						break;
					case 'deleteabono':
						echo "Eliminar Abono: Buscar Causa";
						break;
					case 'showabono':
						echo "Ver Abono: Buscar Causa";
						break;
					case 'creagasto':
						echo "Ingresar Gasto: Buscar Causa";
						break;
					case 'editgasto':
						echo "Editar Gasto: Buscar Causa";
						break;
					case 'showgasto':
						echo "Ver Gasto: Buscar Causa";
						break;
					default:
						echo "Buscar Causa";
						break;;
				endswitch;
        ?>
</h2>
<div id="form_search_causa">
<?php 	switch($accion):
			case 'edit':
				$parametros = '?accion='.$accion;
				break;
			case 'show':
				$parametros = '?accion='.$accion;
				break;
			case 'redactarcarta':
				$parametros = '?accion='.$accion.'&id_carta='.$id_carta;
				break;
			case 'redactarescrito':
				$parametros = '?accion='.$accion.'&id_escrito='.$id_escrito;
				break;
			case 'crearcausa':
				$parametros = '?accion='.$accion;
				break;
			case 'editcausa':
				$parametros = '?accion='.$accion;
				break;
			case 'creadocumento':
				$parametros = '?accion='.$accion;
				break;
			case 'showdocumento':
				$parametros = '?accion='.$accion;
				break;
			case 'deletedocumento':
				$parametros = '?accion='.$accion;
				break;
			case 'creagarantia':
				$parametros = '?accion='.$accion;
				break;
			case 'editgarantia':
				$parametros = '?accion='.$accion;
				break;
			case 'showgarantia':
				$parametros = '?accion='.$accion;
				break;
			case 'creadiligencia':
				$parametros = '?accion='.$accion;
				break;
			case 'editdiligencia':
				$parametros = '?accion='.$accion;
				break;
			case 'showdiligencia':
				$parametros = '?accion='.$accion;
				break;
			case 'creaabono':
				$parametros = '?accion='.$accion;
				break;
			case 'editabono':
				$parametros = '?accion='.$accion;
				break;
			case 'deleteabono':
				$parametros = '?accion='.$accion;
				break;
			case 'showabono':
				$parametros = '?accion='.$accion;
				break;
			case 'creagasto':
				$parametros = '?accion='.$accion;
				break;
			case 'editgasto':
				$parametros = '?accion='.$accion;
				break;
			case 'showgasto':
				$parametros = '?accion='.$accion;
				break;
			default:
				$parametros = '';
				break;;
		endswitch;
?>
<?php echo form_remote_tag(array(
    'update'   	=> 'list-search-causa',
    'url'      	=> 'juicios/searchcausaupdate'.$parametros,
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('appear','list-search-causa').
					visual_effect('highlight','list-search-causa',array('duration' => 1.0)).
					visual_effect('fade','loading'),
))?>
<div><?php echo $form->renderGlobalErrors() ?></div>
  <table>
    <tfoot>
      	<tr>
      		<td class="tdcentrado" colspan="4">
      		<input type="submit" value="Buscar" class="buscar boton_con_imagen"/>
      			<?php 
      			if($accion != 'crearcausa' && $accion != 'editcausa')
      			{
      				switch($accion):
					case 'redactarcarta':
						$return = 'informes/indexcarta';
						break;
					case 'redactarescrito':
						$return = 'informes/indexcarta';
						break;
					case 'creadiligencia':
						$return = 'juicios/indexdiligencia';
						break;
					case 'editdiligencia':
						$return = 'juicios/indexdiligencia';
						break;
					case 'showdiligencia':
						$return = 'juicios/indexdiligencia';
						break;
					case 'creaabono':
						$return = 'juicios/indexabono';
						break;
					case 'editabono':
						$return = 'juicios/indexabono';
						break;
					case 'showabono':
						$return = 'juicios/indexabono';
						break;
					case 'deleteabono':
						$return = 'juicios/indexabono';
						break;
					case 'creagasto':
						$return = 'juicios/indexgasto';
						break;
					case 'editgasto':
						$return = 'juicios/indexgasto';
						break;
					case 'showagasto':
						$return = 'juicios/indexgasto';
						break;
					default:
						$return = 'juicios/indexcausa';
						break;
					endswitch;
					
					if(isset($pop_up) && $pop_up == true)
      				{
      					echo button_to_function('Cerrar ventana',
							visual_effect('appear','loading').
							visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
							visual_effect('fade','list-search-causa', array('duration' => 0.5)).
							visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cerrar boton_con_imagen'));	
      				}
      				else
      				{
					    echo button_to_remote('Cancelar', array(
	          				'update'	=> 'modulo',
	          				'url'		=> $return,
	          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
							'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
					    ),array('class' => 'cancelar boton_con_imagen'));
      				}
      			}
      			else
      			{
      					echo button_to_function('Cerrar ventana',
							visual_effect('appear','loading').
							visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
							visual_effect('fade','list-search-causa', array('duration' => 0.5)).
							visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cerrar boton_con_imagen'));	
      			}?>
        		
      		</td>
    	</tr>
    </tfoot>
    <tbody>
      <tr>
      	<th><label for="causa_id">N&uacute;mero Interno</label></th>
      	<td>
      		<?php echo $form['id']->renderError() ?>
      		<?php echo $form['id'] ?>
      	</td>
      	<th><label for="causa_periodo">Periodo</label></th>
      	<td>
      		<?php echo $form['periodo']->renderError() ?>
      		<?php echo $form['periodo'] ?>
      	</td>
      </tr>
      <tr>
        <th><label for="causa_tipo_causa_id">Tipo Causa</label></th>
        <td>
          <?php echo $form['tipo_causa_id']->renderError() ?>
          <?php echo $form['tipo_causa_id'] ?>
        </td>
        <th><label for="causa_competencia">Competencia</label></th>
        <td>
          <?php echo $form['competencia']->renderError() ?>
          <?php echo $form['competencia'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_rol">Rol</label></th>
        <td>
          <?php echo $form['rol']->renderError() ?>
          <?php echo $form['rol'] ?>
		  <?php echo $form['accion'] ?>
        </td>
        <th><label for="causa_materia_id">Materia</label></th>
        <td>
          <?php echo $form['materia_id']->renderError() ?>
          <?php echo $form['materia_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_contrato">Contrato</label></th>
        <td>
          <div id="contrato">
	          <?php echo $form['searchcontrato']->renderError() ?>
	          <?php echo $form['searchcontrato'] ?>
	          <?php 
	          	if(isset($acreedor))
	          	{
	          		echo input_hidden_tag('contrato_id',$contrato_id);
	          		echo input_tag('nombreAcreedor',$acreedor,array('readonly' => 'readonly'));
	          	}
	          	else
	          	{
	          		echo input_tag('nombreAcreedor','',array('disabled' => 'disabled'));
	          	}
	          	
	            echo button_to_remote(
	            		'Buscar', 
	            		array(
	          				'update'	=> 'list-search-acreedor',
	          				'url'		=> 'administracion/searchacreedor?accion=buscarcausaacreedor',
	          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
							'complete'	=> visual_effect('appear', 'list-search-acreedor').
	          							   visual_effect('highlight','list-search-acreedor',array('duration' => 1.0)).
	          							   visual_effect('fade','loading', array('duration' => 0)),
	          		),array('class' => 'buscar boton_con_imagen')); 
	          ?>
	      </div>
        </td>
        <th><label for="causa_deudor">Deudor</label></th>
        <td>
          <div id="deudor">
	          <?php echo $form['searchdeudor']->renderError();
	          
	          		echo $form['searchdeudor'];
	          
	          		if(isset($deudor))
		          	{
		          		echo input_hidden_tag('deudor_id',$deudor_id);
		          		echo input_tag('nombreDeudor',$deudor,array('readonly' => 'readonly'));
		          	}
		          	else
		          	{
		          		echo input_tag('nombreDeudor','',array('disabled' => 'disabled'));
		          	}
		          	
		          	echo button_to_remote('Buscar', array(
	          				'update'	=> 'list-search-deudor',
	          				'url'		=> 'administracion/searchdeudor?accion=buscarcausadeudor',
	          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
							'complete'	=> visual_effect('appear', 'list-search-deudor').
	          							   visual_effect('highlight','list-search-deudor',array('duration' => 1.0)).
	          							   visual_effect('fade','loading', array('duration' => 0)),
	          		),array('class' => 'buscar boton_con_imagen'));
	          
	          ?>
	      </div>
        </td>
      </tr>
      <tr>
        <th><label for="causa_fecha_inicio">Desde</label></th>
        <td>
          <?php echo $form['desde']->renderError() ?>
          <?php echo $form['desde'] ?>
        </td>
        <th><label for="causa_fecha_termino">Hasta</label></th>
        <td>
          <?php echo $form['hasta']->renderError() ?>
          <?php echo $form['hasta'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_herencia_exhorto">Herencia Exhorto</label></th>
        <td>
          <?php echo $form['herencia_exhorto']->renderError() ?>
          <?php echo $form['herencia_exhorto'] ?>
        </td>
        <th><label for="causa_estado">Estado</label></th>
        <td>
          <?php echo $form['estado']->renderError() ?>
          <?php echo $form['estado'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
</div>
<div id="list-search-deudor" class="pop-up" style="display:none"></div>
<div id="list-search-acreedor" class="pop-up" style="display:none"></div>
<div id="list-search-causa" class="pop-up" style="display:none"></div>