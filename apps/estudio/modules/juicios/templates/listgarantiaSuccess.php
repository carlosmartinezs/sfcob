<?php use_helper('Javascript') ?>
<h2>Lista de Garant&iacute;as</h2>

<table>
  <thead>
    <tr>
      <th class="tddestacado">Rol Causa</th>
      <th class="tddestacado">Tipo de garant&iacute;a</th>
      <th class="tddestacado">Valor</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  <?php if(count($garantiaList) > 0):?>
		  	<?php if ($accion == 'showgarantia'): ?>
		    <?php foreach ($garantiaList as $garantia): ?>
		    <tr>
		      <td><?php echo $garantia->getCausa()->getRol() ?></td>
		      <td><?php echo $garantia->getTipoGarantia() ?></td>
		      <td>$ <?php echo $garantia->getValor() ?></td>
		      <td class="tdcentrado"><?php echo button_to_remote('Ver', array(
					'update' => 'acciones_causa',
					'url' => 'juicios/showgarantia?id='.$garantia->getId(),
					'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
					),array('class' => 'detalles boton_con_imagen'));?> </td>
		    </tr>
		    <?php endforeach; ?>
		    <?php endif; ?>
		    <?php if ($accion == 'editgarantia'): ?>
		    <?php foreach ($garantiaList as $garantia): ?>
		    <tr>
		      <td><?php echo $garantia->getCausa()->getRol() ?></td>
		      <td><?php echo $garantia->getTipoGarantia() ?></td>
		      <td><?php echo $garantia->getValor() ?></td>
		      <td><?php echo button_to_remote('Editar', array(
					'update' => 'acciones_causa',
					'url' => 'juicios/editgarantia?id='.$garantia->getId().'&idcausa='.$causa->getId(),
					'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
					'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
					),array('class' => 'editar boton_con_imagen'));?></td>
		    </tr>
		    <?php endforeach; ?>
		    <?php endif; ?>
    <?php else:?>
    <tr><td class="aviso" colspan="4">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td></tr>
    <?php endif;?>
  </tbody>
</table>