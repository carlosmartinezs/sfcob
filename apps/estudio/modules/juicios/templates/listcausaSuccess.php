<?php use_helper('Javascript') ?>
<h2>Lista de Causas</h2>
<table>
  <thead>
    <tr>
      <th class="tddestacado">N&ordm;</th>
      <th class="tddestacado">Tipo</th>
      <th class="tddestacado">Rol</th>
      <th class="tddestacado">Car&aacute;tula</th>
      <th class="tddestacado">Inicio</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  <?php if(count($causaList) > 0):?>
    <?php foreach ($causaList as $causa): ?>
    <tr>
      <td><?php echo $causa->getId() ?></td>
      <td><?php echo $causa->getTipoCausa() ?></td>
      <td><?php echo $causa->getRol() ?></td>
      <td><?php echo $causa->getCaratula() ?></td>
      <td><?php echo Funciones::fecha2($causa->getFechaInicio('d-m-Y')) ?></td>
      <td class="tdcentrado"><?php $mostrar_button = true;
      			switch($accion):
					case 'edit':
						$value_button = 'Editar';
						$update = 'acciones_causa';
						$url = 'juicios/editcausa?id='.$causa->getId();
						$class_css = 'editar boton_con_imagen';
						break;
					case 'show':
						$value_button = 'Detalles';
						$update = 'acciones_causa';
						$url = 'juicios/showcausa?id='.$causa->getId();
						$class_css = 'detalles boton_con_imagen';
						break;
					case 'redactarcarta':
						$value_button = 'Redactar';
						$update = 'acciones_carta';
						$url = 'informes/redactarcarta?id_carta='.$id_carta.'&id_causa='.$causa->getId();
						$class_css = 'reporte boton_con_imagen';
      					break;
      				case 'redactarescrito':
						$value_button = 'Redactar';
						$update = 'acciones_escrito';
						$url = 'informes/redactarescrito?id_escrito='.$id_escrito.'&id_causa='.$causa->getId();
						$class_css = 'reporte boton_con_imagen';
      					break;
      				case 'crearcausa':
      					$value_button = 'Llevar Causa';
      					$update = 'causa-exhorto';
      					$url = 'juicios/traercausaexhorto?accion=crearcausa&id_causa='.$causa->getId();
      					$class_css = 'llevar boton_con_imagen';
      					break;
      				case 'editcausa':
      					$value_button = 'llevar Causa';
      					$update = 'causa-exhorto';
      					$url = 'juicios/traercausaexhorto?accion=editcausa&id_causa='.$causa->getId();
      					$class_css = 'llevar boton_con_imagen';
      					break;
      				case 'creadocumento':
      					$value_button = 'A&ntilde;adir Documento';
      					$update = 'acciones_causa';
      					$url = 'juicios/createdocumento?idcausa='.$causa->getId();
      					$class_css = 'anadir boton_con_imagen';
	      				break;
      				case 'showdocumento':
      					$value_button = 'Ver Documentos';
      					$update = 'acciones_causa';
      					$url = 'juicios/listdocumento?accion=showdocumento&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
      				case 'deletedocumento':
      					$value_button = 'Ver Documentos';
      					$update = 'acciones_causa';
      					$url = 'juicios/listdocumento?accion=deletedocumento&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
      				case 'creagarantia':
      					$value_button = 'A&ntilde;adir Garant&iacute;a';
      					$update = 'acciones_causa';
      					$url = 'juicios/creategarantia?idcausa='.$causa->getId();
      					$class_css = 'anadir boton_con_imagen';
      					break;
      				case 'editgarantia':
      					$value_button = 'Editar Garant&iacute;as';
      					$update = 'acciones_causa';
      					$url = 'juicios/listgarantia?accion=editgarantia&idcausa='.$causa->getId();
      					$class_css = 'editar boton_con_imagen';
      					break;
      				case 'showgarantia':
      					$value_button = 'Ver Garant&iacute;as';
      					$update = 'acciones_causa';
      					$url = 'juicios/listgarantia?accion=showgarantia&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
      				case 'creadiligencia':
      					$value_button = 'A&ntilde;adir Diligencia';
      					$update = 'acciones_diligencia';
      					$url = 'juicios/creatediligencia?idcausa='.$causa->getId();
      					$class_css = 'anadir boton_con_imagen';
      					break;
      				case 'editdiligencia':
      					$value_button = 'Ver Diligencias';
      					$update = 'acciones_diligencia';
      					$url = 'juicios/listdiligencia?accion=editdiligencia&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
      				case 'showdiligencia':
      					$value_button = 'Ver Diligencias';
      					$update = 'acciones_diligencia';
      					$url = 'juicios/listdiligencia?accion=showdiligencia&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
      				case 'creaabono':
      					$value_button = 'Abonar';
      					$update = 'acciones_abono';
      					$url = 'juicios/createabono?idcausa='.$causa->getId();
      					$class_css = 'abonar boton_con_imagen';
      					break;
      				case 'editabono':
      					$value_button = 'Ver Abonos';
      					$update = 'acciones_abono';
      					$url = 'juicios/listabono?accion=editabono&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
      				case 'deleteabono':
      					$value_button = 'Ver Abonos';
      					$update = 'acciones_abono';
      					$url = 'juicios/listabono?accion=deleteabono&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
      				case 'showabono':
      					$value_button = 'Ver Abonos';
      					$update = 'acciones_abono';
      					$url = 'juicios/listabono?accion=showabono&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
      				case 'creagasto':
      					$value_button = 'Ver Diligencias';
      					$update = 'acciones_gasto';
      					$url = 'juicios/listdiligencia?accion=creagasto&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
      				case 'editgasto':
      					$value_button = 'Ver Diligencias';
      					$update = 'acciones_gasto';
      					$url = 'juicios/listdiligencia?accion=editgasto&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
      				case 'showgasto':
      					$value_button = 'Ver Diligencias';
      					$update = 'acciones_gasto';
      					$url = 'juicios/listdiligencia?accion=showgasto&idcausa='.$causa->getId();
      					$class_css = 'detalles boton_con_imagen';
      					break;
					default : 
						$mostrar_button = false;
						break;
				endswitch;
				
				if ($mostrar_button){
					if($accion == 'redactarcarta')
					{
						if(count($causa->getDocumentos()) > 0)
						{
								echo button_to_remote($value_button, array(
									'update'	=> $update,
									'url'		=> $url,
									'loading'	=> visual_effect('appear','loading').
													visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
													visual_effect('fade','list-search-causa', array('duration' => 0.5)),
									'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
									),array('class' => $class_css));
							
						}
						else
						{
							echo button_to_function($value_button,"alert('Esta causa no posee documentos que respalden la deuda.')",array('class' => $class_css));
						}
					}elseif($accion == 'creadocumento'){
						if($causa->getEstadoCausaActivo()->getEstado()->getId() != 2 && $causa->getEstadoCausaActivo()->getEstado()->getId() != 4){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => $class_css));
						}else{
							echo button_to_function($value_button, 
	      						"alert('No puede a&ntilde;adir documentos a esta causa ya que su estado es \"".$causa->getEstadoCausaActivo()->getEstado()."\"')",array('class' => $class_css));
	      				
						}
					}elseif($accion == 'deletedocumento'){
						if($causa->getEstadoCausaActivo()->getEstado()->getId() != 2 && $causa->getEstadoCausaActivo()->getEstado()->getId() != 4){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
						}else{
							echo button_to_function($value_button, 
	      						"alert('No puede eliminar documentos de esta causa ya que su estado es \"".$causa->getEstadoCausaActivo()->getEstado()."\"')",array('class' => $class_css));
						}
					}elseif($accion == 'showdocumento'){
						if(count($causa->getDocumentos()) > 0){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
						}else{
							echo button_to_function('Ver Documentos',
								"alert('Esta causa no posee documentos')",array('class' => $class_css));
						}
					}elseif($accion == 'creagarantia'){
						if($causa->getEstadoCausaActivo()->getEstado()->getId() != 2 && $causa->getEstadoCausaActivo()->getEstado()->getId() != 4){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
						}else{
							echo button_to_function($value_button, 
	      						"alert('No puede a&ntilde;adir garant&iacute;as a esta causa ya que su estado es \"".$causa->getEstadoCausaActivo()->getEstado()."\"')",array('class' => $class_css));
						}
					}elseif($accion == 'editgarantia'){
						if($causa->getEstadoCausaActivo()->getEstado()->getId() != 2 && $causa->getEstadoCausaActivo()->getEstado()->getId() != 4){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
						}else{
							echo button_to_function($value_button, 
	      						"alert('No puede editar garant&iacute;as de esta causa ya que su estado es \"".$causa->getEstadoCausaActivo()->getEstado()."\"')",array('class' => $class_css));
						}
					}elseif($accion == 'showgarantia'){
						if(count($causa->getGarantias()) > 0){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
						}else{
							echo button_to_function($value_button,
								"alert('Esta causa no posee garant&iacute;as')",array('class' => $class_css));
						}
					}elseif($accion == 'creadiligencia'){
						if($causa->getEstadoCausaActivo()->getEstado()->getId() != 2 && $causa->getEstadoCausaActivo()->getEstado()->getId() != 4){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
						}else{
							echo button_to_function($value_button,
								"alert('No puede a&ntilde;adir diligencias en esta causa ya que su estado es \"".$causa->getEstadoCausaActivo()->getEstado()."\"')",array('class' => $class_css));
						}
					}elseif($accion == 'editdiligencia'){
						if($causa->getEstadoCausaActivo()->getEstado()->getId() != 2 && $causa->getEstadoCausaActivo()->getEstado()->getId() != 4){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
						}else{
							echo button_to_function($value_button,
								"alert('No puede editar diligencias de esta causa ya que su estado es \"".$causa->getEstadoCausaActivo()->getEstado()."\"')",array('class' => $class_css));
						}
					}elseif($accion == 'creaabono'){
						if(($causa->getEstadoCausaActivo()->getEstado()->getId() == 5) && ($causa->getDeudaPendiente() > 0)){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));		
						}else{
							echo button_to_function($value_button,
								"alert('Para realizar abonos en la causa, el estado de &eacute;sta debe ser \"Suspendida\" y debe tener una deuda total pendiente mayor a 0. El estado actual de esta causa es \"".$causa->getEstadoCausaActivo()->getEstado()."\" y su deuda total pendiente es de \$".$causa->getDeudaPendiente()."')",array('class' => $class_css));			
						}
					}elseif($accion == 'editabono'){
						if($causa->getEstadoCausaActivo()->getEstado()->getId() != 2 && $causa->getEstadoCausaActivo()->getEstado()->getId() != 4){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
						}else{
							echo button_to_function($value_button,
								"alert('No puede editar abonos de esta causa ya que su estado es \"".$causa->getEstadoCausaActivo()->getEstado()."\"')",array('class' => $class_css));
						}
					}elseif($accion == 'showabono'){
						echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => $class_css));
							
					}else if($accion == 'deleteabono'){
					if(!$causa->estaTerminada())
						{
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => $class_css));
						}
						else
						{
							echo button_to_function($value_button,"alert('No puede eliminar abonos de esta causa ya que su estado es \"Terminada\".')",array('class' => $class_css));
						}
					}elseif($accion == 'creagasto'){
						if($causa->getEstadoCausaActivo()->getEstado()->getId() != 2 && $causa->getEstadoCausaActivo()->getEstado()->getId() != 4){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
						),array('class' => $class_css));
						}else{
							echo button_to_function($value_button,
								"alert('No puede ingresar gastos a esta causa ya que su estado es \"".$causa->getEstadoCausaActivo()->getEstado()."\"')",array('class' => $class_css));
						}
					}elseif($accion == 'editgasto'){
						if($causa->getEstadoCausaActivo()->getEstado()->getId() != 2 && $causa->getEstadoCausaActivo()->getEstado()->getId() != 4){
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => $class_css));
						}else{
							echo button_to_function($value_button,
								"alert('No puede editar gastos de esta causa ya que su estado es \"".$causa->getEstadoCausaActivo()->getEstado()."\"')",array('class' => $class_css));
						}
					}elseif($accion=='redactarescrito'){
							if(count($causa->getDeudor()->getDeudorDomicilios()) > 0)
							{
								echo button_to_remote($value_button, array(
									'update'	=> $update,
									'url'		=> $url,
									'loading'	=> visual_effect('appear','loading').
													visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
													visual_effect('fade','list-search-causa', array('duration' => 0.5)),
									'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
									),array('class' => $class_css));
							}
							else
							{
								echo button_to_function($value_button,"alert('El deudor asociado a la causa no posee al menos un domicilio, el cual es requerido para este escrito.')",array('class' => $class_css));
							}	
					}
					else if($accion=='show'){
						echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => $class_css));
					}
					else{
						if(!$causa->estaTerminada())
						{
							echo button_to_remote($value_button, array(
							'update'	=> $update,
							'url'		=> $url,
							'loading'	=> visual_effect('appear','loading').
											visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
											visual_effect('fade','list-search-causa', array('duration' => 0.5)),
							'complete' 	=> visual_effect('fade','loading', array('duration' => 1.0)),
							),array('class' => $class_css));
						}
						else
						{
							echo button_to_function($value_button,"alert('No puede editar esta causa ya que su estado es \"Terminada\".')",array('class' => $class_css));
						}
					}
				}
      		?>
      </td>
    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr><td class="aviso" colspan="6">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td></tr>
    <?php endif;?>
  </tbody>
</table>
<?php echo button_to_function('Cerrar ventana',
	visual_effect('appear','loading').
	visual_effect('highlight','list-search-causa', array('duration' => 0.5)).
	visual_effect('fade','list-search-causa', array('duration' => 0.5)).
	visual_effect('fade','loading', array('duration' => 1.0)),array('class' => 'cancelar boton_con_imagen'));
?>
