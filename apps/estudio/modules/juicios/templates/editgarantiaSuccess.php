<?php use_helper('Javascript', 'Object') ?>
<?php $garantia = $form->getObject() ?>
<h2><?php echo $garantia->isNew() ? 'Ingresar' : 'Editar' ?> Garant&iacute;a</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_causa',
    'url'      	=> 'juicios/updategarantia?editar='.$editar.(!$garantia->isNew() ? '&id='.$garantia->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
<?php echo input_hidden_tag('editar',$editar); ?>
<div><?php echo $form->renderGlobalErrors() ?></div>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="2">
        <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
          <?php echo button_to_remote('Cancelar', array(
			'update' => 'modulo',
			'url' => 'juicios/indexcausa',
			'loading' => visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
			),array('class' => 'cancelar boton_con_imagen'))?>
          
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <th><label for="garantia_causa_id">Causa * </label></th>
        <td>
          <?php echo $form['causa_id']->renderError() ?>
          <?php echo input_hidden_tag('causa_id', $causaId);
          		echo $nombreCausa; ?>
        </td>
      </tr>
      <tr>
        <th><label for="garantia_tipo_garantia_id">Tipo de Garant&iacute;a * </label></th>
        <td>
          <?php echo $form['tipo_garantia_id']->renderError() ?>
          <?php echo $form['tipo_garantia_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="garantia_valor">Valor * </label><span class="small-font">(S&oacute;lo N&uacute;meros)</span></th>
        <td>
          <?php echo $form['valor']->renderError() ?>
          <?php echo $form['valor'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
    <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
