<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Causas</h1>
<ul class="menu-modulo">
	<?php if ($sf_user->hasCredential('abogado')):?>
	<li>
		<?php echo link_to_remote('Ingresar Causa', array(
    		'update' => 'acciones_causa',
    		'url'    => 'juicios/createcausa',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?>
	</li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')):?>
	<li>
		<?php echo link_to_remote('Editar Causa', array(
    		'update' => 'acciones_causa',
    		'url'    => 'juicios/searchcausa?accion=edit',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?>
	</li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
	<li>
		<?php echo link_to_remote('Visualizar Causa', array(
    		'update' => 'acciones_causa',
    		'url'    => 'juicios/searchcausa?accion=show',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?>
	</li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
	<li>
		<?php echo link_to_function('Documentos'," Effect.toggle('menu-documentos', 'slide', { delay: 0.15 })"); ?>
	</li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
	<li>
		<?php echo link_to_function('Garant&iacute;as'," Effect.toggle('menu-garantias', 'slide', { delay: 0.15 })"); ?>
	</li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('procurador')):?>
	<li>
			<?php echo link_to_remote('Ver Documento', array(
	    		'update' => 'acciones_causa',
	    		'url'    => 'juicios/searchcausa?accion=showdocumento',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
								visual_effect('highlight','modulo',array('duration' => 0.5)),
			)) ?>
		</li>
		<li>
			<?php echo link_to_remote('Ver Garant&iacute;a', array(
	    		'update' => 'acciones_causa',
	    		'url'    => 'juicios/searchcausa?accion=showgarantia',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
								visual_effect('highlight','modulo',array('duration' => 0.5)),
			)) ?>
		</li>
	<?php endif; ?>
</ul>
<ul id="menu-documentos" class="menu-modulo" style="display:none; clear:right;margin-top: -11px;">
	<li>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
	<?php endif; ?>
	</li>
		<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
		<li>
			<?php echo link_to_remote('Ingresar Documento', array(
	    		'update' => 'acciones_causa',
	    		'url'    => 'juicios/searchcausa?accion=creadocumento',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
								visual_effect('highlight','modulo',array('duration' => 0.5)),
			)) ?>
		</li>
		<?php endif; ?>
		<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
		<li>
			<?php echo link_to_remote('Ver Documento', array(
	    		'update' => 'acciones_causa',
	    		'url'    => 'juicios/searchcausa?accion=showdocumento',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
								visual_effect('highlight','modulo',array('duration' => 0.5)),
			)) ?>
		</li>
		<?php endif; ?>
		<?php if ($sf_user->hasCredential('abogado')): ?>
		<li>
			<?php echo link_to_remote('Eliminar Documento', array(
	    		'update' => 'acciones_causa',
	    		'url'    => 'juicios/searchcausa?accion=deletedocumento',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
								visual_effect('highlight','modulo',array('duration' => 0.5)),
			)) ?>
		</li>
		<?php endif; ?>		
</ul>
<ul id="menu-garantias" class="menu-modulo" style="display:none; clear:right;margin-top: -11px;">
	<li>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
	<?php endif; ?>
	</li>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
		<li>
			<?php echo link_to_remote('Ingresar Garant&iacute;a', array(
	    		'update' => 'acciones_causa',
	    		'url'    => 'juicios/searchcausa?accion=creagarantia',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
								visual_effect('highlight','modulo',array('duration' => 0.5)),
			)) ?>
		</li>
		<?php endif; ?>
		<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')): ?>
		<li>
			<?php echo link_to_remote('Editar Garant&iacute;a', array(
	    		'update' => 'acciones_causa',
	    		'url'    => 'juicios/searchcausa?accion=editgarantia',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
								visual_effect('highlight','modulo',array('duration' => 0.5)),
			)) ?>
		</li>
		<?php endif; ?>
		<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
		<li>
			<?php echo link_to_remote('Ver Garant&iacute;a', array(
	    		'update' => 'acciones_causa',
	    		'url'    => 'juicios/searchcausa?accion=showgarantia',
				'loading'	=> visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading').
								visual_effect('highlight','modulo',array('duration' => 0.5)),
			)) ?>
		</li>
		<?php endif; ?>
</ul>
<div id="acciones_causa"  style="clear: right"></div>
