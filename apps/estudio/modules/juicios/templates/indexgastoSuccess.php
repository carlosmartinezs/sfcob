<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Gastos</h1>
<ul class="menu-modulo">
	<?php if ($sf_user->hasCredential('procurador') || $sf_user->hasCredential('secretaria')):?>
	<li>
		<?php echo link_to_remote('Ingresar Gasto', array(
    		'update' => 'acciones_gasto',
    		'url'    => 'juicios/searchcausa?accion=creagasto',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('secretaria')):?>
	<li>
		<?php echo link_to_remote('Editar Gasto', array(
    		'update' => 'acciones_gasto',
    		'url'    => 'juicios/searchcausa?accion=editgasto',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
	<li>
		<?php echo link_to_remote('Visualizar Gasto', array(
    		'update' => 'acciones_gasto',
    		'url'    => 'juicios/searchcausa?accion=showgasto',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_gasto"  style="clear: right"></div>