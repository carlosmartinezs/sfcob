<?php use_helper('Javascript', 'Object') ?>
<?php $causa = $form->getObject() ?>
<h2><?php echo $causa->isNew() ? 'Ingresar' : 'Editar' ?> Causa</h2>
<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_causa',
    'url'      	=> 'juicios/updatecausa'.(!$causa->isNew() ? '?id='.$causa->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'	=> "Se guardar&aacute;n los datos de la causa. Desea continuar?",
))?>
<?php echo input_hidden_tag('editar',$editar); ?>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="4">
        	<input type="submit" value="Guardar" class="guardar boton_con_imagen" />
          <?php echo button_to_remote('Cancelar', array(
			'update' => 'modulo',
			'url' => 'juicios/indexcausa',
			'loading' => visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
			),array('class' => 'cancelar boton_con_imagen'))?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php if(isset($editar) && $editar == true && $estadoId && $estadoCausa && $periodoCausaId && $periodoCausa):?>
      <tr>
      	<th><label for="causa_estado">Estado</label></th>
      	<td>
      		<div id="estado_causa">
      		<?php if(isset($errorEstado) && $errorEstado):?>
      			<ul>
      				<li>Debe seleccionar un estado</li>
      			</ul>
      		<?php endif;?>
      		<?php echo input_hidden_tag('estado',$estadoId);
      			if(isset($motivoEstado)):
      				echo input_hidden_tag('motivo_estado', $motivoEstado);
      			endif;
      		echo input_tag('estadoCausa',$estadoCausa,array('readonly' => 'readonly')); ?>
      		<?php if($sf_user->hasCredential('abogado')):?>
      		<?php	if($estadoId != 2 && $estadoId != 4): 
      					echo button_to_remote('Cambiar', array(
          					'update'	=> 'form-cambio-estado',
          					'url'		=> 'juicios/editestadocausa?estado_id='.$estadoId.'&causa_id='.$causa->getId(),
          					'loading'	=> visual_effect('appear', 'form-cambio-estado').
          								   visual_effect('highlight','form-cambio-estado',array('duration' => 1.0)).
          								   visual_effect('appear','loading', array('duration' => 0)),
		  					'complete'	=> visual_effect('fade','loading', array('duration' => 0)), 
          				),array('class' => 'cambiar boton_con_imagen'));
      				endif;?>
      		<?php endif;?></div>
      	</td>
      	<th><label for="causa_periodo">Periodo</label></th>
      	<td>
      		<div id="periodo_causa">
      		<?php echo input_hidden_tag('periodo_causa_id',$periodoCausaId); 
      		echo input_tag('periodoCausa',$periodoCausa,array('readonly' => 'readonly')); ?>
      		<?php if($periodoCausaId == '1' || $periodoCausaId == '2'): ?>
      		<?php	  echo button_to_remote('Cambiar', array(
          			'update'	=> 'periodo_causa',
          			'url'		=> 'juicios/editperiodocausa?causa_id='.$causa->getId(),
          			'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
		  			'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
      				'confirm'	=> "Esta acci&oacute;n es irreversible y cambiar&aacute; el periodo actual de la causa al periodo siguiente. Dicho cambio se realizar&aacute; al momento de guardar la causa.",
      				),array('class' => 'cambiar boton_con_imagen'))?>
      		<?php endif; ?></div>
      	</td>
      </tr>
      <?php endif; ?>
      <tr>
        <th><label for="causa_tipo_causa_id">Tipo de Causa</label><?php echo ' '.'*'; ?></th>
        <td>
			<?php 
 				$lista = array();
 				$tipoCausaList = TipoCausaPeer::doSelect(new Criteria());
  				foreach ($tipoCausaList as $tipoCausa)
  				{
    				$lista[$tipoCausa->getId()] = $tipoCausa->getNombre();
  				} 
  					$this->lista = $lista;
		  
  			if(isset($editar) && $editar == true && isset($tipoCausaId) && $tipoCausaId >= 1){
  				//echo "id tipo causa: ".$tipoCausaId;
		  		echo select_tag('tipo_causa_id',
  					options_for_select($lista,
  										$tipoCausaId,
  						array('include_custom' => '-- Seleccione --')),array(
    					'onchange' => remote_function(array(
      					'update' => "causa-exhorto",
      					'with'   => "'tipo_causa_id='+value",
  						'script' => true,
      					'url'    => '/juicios/creainputcausaexhorto?editar='.$editar,
      					'loading'	=> visual_effect('appear','loading'),
						'complete'	=> visual_effect('fade','loading'),
    				))));
  			}else{
  				if(isset($tipoCausaId) && ($tipoCausaId < 1))
  				{
  				?>
  					<ul class="error_list">
  						<li>Seleccione tipo de causa.</li>
  					</ul>
  				<?php
  				}
  				echo select_tag('tipo_causa_id',
  					options_for_select($lista,
  						'getId',array('include_custom' => '-- Seleccione --')),array(
    					'onchange' => remote_function(array(
      									'update' => "causa-exhorto",
      									'with'   => "'tipo_causa_id='+value",
  										'script' => true,
      									'url'    => '/juicios/creainputcausaexhorto?editar='.$editar,
      									'loading'	=> visual_effect('appear','loading'),
										'complete'	=> visual_effect('fade','loading'),
    				))));
  			}?>
        </td>
        <th><label for="causa_causa_exhorto">Causa Base</label><?php echo ' '.'**'; ?></th>
        <td>
        	<?php echo $form['causa_exhorto']->renderError() ?>
        	<div id="causa-exhorto">
          	<?php if(isset($editar) && $editar == true && $tipoCausaId == 2)
          		{
          			if($causaExhorto instanceof Causa)
          			{
          				echo input_hidden_tag('causa_exhorto_id', $causaExhorto->getId());
						echo input_tag('causa_exhorto_caratula', $causaExhorto->getRol(), array('readonly' => 'readonly'));
          				echo button_to_remote('Cambiar C. Base', array(
          				'update'	=> 'list-search-causa',
          				'url'		=> 'juicios/searchcausa?accion=editcausa',
         				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
		  				'complete'	=> visual_effect('appear', 'list-search-causa').
          							   visual_effect('highlight','list-search-causa',array('duration' => 1.0)). 
									   visual_effect('fade','loading', array('duration' => 0))),array('class'=>'cambiar boton_con_imagen'));
          			}
          			else
          			{
          				?>
  					<ul class="error_list">
  						<li>Busque una causa base.</li>
  					</ul>
  				<?php
          				echo input_hidden_tag('causa_exhorto_id','');
						echo input_tag('causa_exhorto_caratula','CLICK BUSCAR CAUSA', array('disabled' => 'disabled'));
          				echo button_to_remote('Buscar', array(
          				'update'	=> 'list-search-causa',
          				'url'		=> 'juicios/searchcausa?accion=editcausa',
         				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
		  				'complete'	=> visual_effect('appear', 'list-search-causa').
          							   visual_effect('highlight','list-search-causa',array('duration' => 1.0)). 
									   visual_effect('fade','loading', array('duration' => 0))),array('class'=>'buscar boton_con_imagen'));
          			}
          			
          		}
          		else
          		{
          			echo input_hidden_tag('causa_exhorto_id','');
          			echo input_tag('causa-exhorto','SIN CAUSA BASE',array('disabled' => 'disabled'));
          		}
          	?>
          </div>
        </td>
      </tr>
      <tr>
        <th><label for="causa_rol">Rol</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['rol']->renderError() ?>
          <?php echo $form['rol'] ?>
        </td>
        <th><label for="causa_deudor_id">Deudor</label><?php echo ' '.'*'; ?></th>
        <td>
        
        <div id="deudor">
        <?php echo $form['deudor_id']->renderError() ?>
          <?php if(isset($editar) && $editar == true && isset($deudorId) && isset($deudor)){
      			echo input_hidden_tag('deudor_id', $deudorId);
				echo input_tag('deudor',$deudor,array('readonly' => 'readonly'));
          
          			?><?php echo button_to_remote('Cambiar', array(
          				'update'	=> 'list-search-deudor',
          				'url'		=> 'administracion/searchdeudor?accion=crearcausa',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('appear', 'list-search-deudor').
          							   visual_effect('highlight','list-search-deudor',array('duration' => 1.0)).
          							   visual_effect('fade','loading', array('duration' => 0)), 
          			),array('class' => 'cambiar boton_con_imagen')); 
          		}else{
          		?><input type="text" name="deudor_id" disabled="disabled"></input>
          <?php echo button_to_remote('Buscar', array(
          				'update'	=> 'list-search-deudor',
          				'url'		=> 'administracion/searchdeudor?accion=crearcausa',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('appear', 'list-search-deudor').
          							   visual_effect('highlight','list-search-deudor',array('duration' => 1.0)).
          							   visual_effect('fade','loading', array('duration' => 0)), 
          				),array('class' => 'buscar boton_con_imagen')); ?>
          <?php } ?></div>
        </td>      
      </tr>
      <tr>
        <th><label for="causa_contrato_id">Acreedor</label><?php echo ' '.'*'; ?></th>
        <td>
        
        <div id="contrato">
        <?php echo $form['contrato_id']->renderError() ?>
          <?php if(isset($editar) && $editar == true && isset($contratoId) && isset($nombreAcreedor)){
      			echo input_hidden_tag('contrato_id', $contratoId);
				echo input_tag('nombreAcreedor',$nombreAcreedor,array('readonly' => 'readonly'));
          
          			?><?php echo button_to_remote('Cambiar', array(
          				'update'	=> 'list-search-acreedor',
          				'url'		=> 'administracion/searchacreedor?accion=crearcausa',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('appear', 'list-search-acreedor').
          							   visual_effect('highlight','list-search-acreedor',array('duration' => 1.0)).
          							   visual_effect('fade','loading', array('duration' => 0)),
          			),array('class' => 'cambiar boton_con_imagen')); 
          		}else{
          		?><input type="text" name="contrato_id" disabled="disabled"></input>
          		<?php echo button_to_remote('Buscar', array(
          				'update'	=> 'list-search-acreedor',
          				'url'		=> 'administracion/searchacreedor?accion=crearcausa',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('appear', 'list-search-acreedor').
          							   visual_effect('highlight','list-search-acreedor',array('duration' => 1.0)).
          							   visual_effect('fade','loading', array('duration' => 0)),
          				),array('class' => 'buscar boton_con_imagen')); ?>
          <?php } ?></div>
        </td>
        <th><label for="causa_materia_id">Materia</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['materia_id']->renderError() ?>
          <?php echo $form['materia_id'] ?>
        </td>  
      </tr>
      <tr>
        <th><label for="causa_competencia">Competencia</label><?php echo ' '.'*'; ?></th>
        <td>
          <?php echo $form['competencia_id']->renderError() ?>
          <?php echo $form['competencia_id'] ?>
        </td>
        <th><label for="causa_tribunal">Tribunal</label><?php echo ' '.'*'; ?></th>
      	<td>
      	
      	<div id="tribunal">
      	<?php echo $form['tribunal']->renderError() ?>
      	<?php if(isset($editar) && $editar == true && $tribunalId && $tribunalNombre){
      			echo input_hidden_tag('tribunal_id', $tribunalId);
      			if(isset($motivoTribunal)):
      				echo input_hidden_tag('motivo_tribunal', $motivoTribunal);
      			endif;
				echo input_tag('tribunalNombre',$tribunalNombre,array('readonly' => 'readonly')); ?>
	
				<?php echo button_to_remote('Cambiar', array(
          			'update'	=> 'form-cambio-tribunal',
          			'url'		=> 'juicios/editcausatribunal?tribunal_id='.$tribunalId.'&causa_id='.$causa->getId(),
          			'loading'	=> visual_effect('appear', 'form-cambio-tribunal').
          						   visual_effect('highlight','form-cambio-tribunal',array('duration' => 1.0)).
          						   visual_effect('appear','loading', array('duration' => 0)),
		  			'complete'	=> visual_effect('fade','loading', array('duration' => 0)),
          			),array('class' => 'cambiar boton_con_imagen')); 
      		}else{
      		?><input type="text" name="tribunal" disabled="disabled"></input>
          	<?php echo button_to_remote('Buscar', array(
          				'update'	=> 'list-search-tribunal',
          				'url'		=> 'administracion/searchtribunal?accion=crearcausa',
          				'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
						'complete'	=> visual_effect('appear', 'list-search-tribunal').
          							   visual_effect('highlight','list-search-tribunal',array('duration' => 1.0)).
          							   visual_effect('fade','loading', array('duration' => 0)),
          				),array('class' => 'buscar boton_con_imagen')); 
      			}  ?></div>
      	</td>
      </tr>
      <tr>
        <th><label for="causa_fecha_inicio">Fecha de Inicio</label><?php echo ' '.'*'; ?></th>
        <td colspan="3">
          <?php echo $form['fecha_inicio']->renderError() ?>
          <?php echo $form['fecha_inicio'] ?>
          
          <?php echo $form['id'] ?>
        </td>
      </tr>     
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      	<p>**: Obligatorio s&oacute;lo si la causa es de tipo <strong>Exhorto</strong>.</p>
      </td>
    </tr>
  </table>
</form>
<div id="list-search-deudor" class="pop-up" style="display:none"></div>
<div id="list-search-acreedor" class="pop-up" style="display:none"></div>
<div id="form-cambio-tribunal" class="pop-up" style="display:none"></div>
<div id="list-search-tribunal" class="pop-up" style="display:none"></div>
<div id="form-cambio-estado" class="pop-up" style="display:none"></div>
<div id="form-cambio-tribunal" class="pop-up" style="display:none"></div>
<div id="list-search-causa" class="pop-up" style="display:none"></div>