<h2>Ver Documento</h2>
<table>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="4">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Causa:</th>
      <td><?php echo $documento->getCausa() ?></td>
    </tr>
    <tr>
      <th>Tipo de Documento:</th>
      <td><?php echo $documento->getTipoDocumento() ?></td>
    </tr>
    <tr>
      <th>N&uacute;mero de Documento:</th>
      <td><?php echo $documento->getNroDocumento() ?></td>
    </tr>
    <tr>
      <th>Fecha de Emisi&oacute;n:</th>
      <td><?php echo Funciones::fecha2($documento->getFechaEmision('d-m-Y')) ?></td>
    </tr>
    <tr>
      <th>Fecha de Vencimiento:</th>
      <td><?php echo Funciones::fecha2($documento->getFechaVencimiento('d-m-Y')) ?></td>
    </tr>
    <tr>
      <th>Monto:</th>
      <td>$ <?php echo $documento->getMonto() ?></td>
    </tr>
    <tr>
      <th>Inter&eacute;s:</th>
      <td><?php echo $documento->getInteres() ?> %</td>
    </tr>
    <tr>
      <th>Descripci&oacute;n:</th>
      <td><?php echo $documento->getDescripcion() ?></td>
    </tr>
  </tbody>
</table>
