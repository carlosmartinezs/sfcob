<?php use_helper('Javascript', 'Object') ?>
<?php $diligencia = $form->getObject() ?>
<h2><?php echo $diligencia->isNew() ? 'Ingresar' : 'Editar' ?> Diligencia</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_diligencia',
    'url'      	=> 'juicios/updatediligencia'.(!$diligencia->isNew() ? '?id='.$diligencia->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="2">
        <input type="submit" value="Guardar" class="guardar boton_con_imagen" />
          <?php echo button_to_remote('Cancelar', array(
			'update' => 'modulo',
			'url' => 'juicios/indexdiligencia',
			'loading' => visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
			),array('class' => 'cancelar boton_con_imagen'))?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <th><label for="diligencia_causa_id">Causa *</label></th>
        <td>
          <?php echo $form['causa_id']->renderError() ?>
		  <?php echo input_hidden_tag('causa_id', $causaId);
		  		echo $nombreCausa; ?>
        </td>
      </tr>
      <tr>
        <th><label for="diligencia_periodo_causa_id">Periodo *</label></th>
        <td>
          <?php echo $form['periodo_id']->renderError() ?>
          <?php echo input_hidden_tag('periodo_id', $periodoId);
          		echo $nombrePeriodo; ?>
        </td>
      </tr>
      <tr>
        <th><label for="diligencia_fecha">Fecha *</label></th>
        <td>
          <?php echo $form['fecha']->renderError() ?>
          <?php echo $form['fecha'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="diligencia_descripcion">Descripci&oacute;n *</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
