<?php use_helper('Javascript') ?>
<h2>Lista de Abonos</h2>

<table>
  <thead>
    <tr>
      <th class="tddestacado">Rol Causa</th>
      <th class="tddestacado">Tipo de Abono</th>
      <th class="tddestacado">Tipo de Pago</th>
      <th class="tddestacado">Valor</th>
      <th class="tddestacado">Fecha</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  	<?php if(isset($hayMensajeEliminar) && $hayMensajeEliminar == true):?>
  	<tr>
      <td colspan="6" class="informacion" style="padding-left:35px;">Abono eliminado exitosamente.</td></tr>
  	<?php endif;?>
    <?php if(count($abonoList) > 0):?>
    <?php foreach ($abonoList as $abono): ?>
    <tr>
      <td><?php echo $abono->getDeuda()->getCausa()->getRol() ?></td>
      <td><?php echo $abono->getTipoAbono() ?></td>
      <td><?php echo $abono->getTipoPago() ?></td>
      <td>$ <?php echo $abono->getValor() ?></td>
      <td><?php echo Funciones::fecha2($abono->getFecha('d-m-Y')) ?></td>
      <?php if ($accion == 'showabono'): ?>
      <td class="tdcentrado"><?php echo button_to_remote('Ver', array(
			'update' => 'acciones_abono',
			'url' => 'juicios/showabono?id='.$abono->getId().'&idcausa='.$causa->getId(),
			'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
			),array('class' => 'detalles boton_con_imagen'));?></td> 
	  <?php endif; ?>
	  <?php if ($accion == 'editabono'): ?>
      <td class="tdcentrado"><?php echo button_to_remote('Editar', array(
			'update' => 'acciones_abono',
			'url' => 'juicios/editabono?id='.$abono->getId().'&idcausa='.$causa->getId(),
			'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
			),array('class' => 'editar boton_con_imagen'));?></td> 
	  <?php endif; ?>
	  <?php if ($accion == 'deleteabono'): ?>
	  <td class="tdcentrado"><?php echo button_to_remote('Eliminar', array(
			'update' => 'acciones_abono',
			'url' => 'juicios/deleteabono?id='.$abono->getId().'&idcausa='.$causa->getId(),
			'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
	  		'confirm'	=> 'Est&aacute; seguro de eliminar &eacute;ste abono?'
			),array('class' => 'eliminar boton_con_imagen'));?></td> 
	  <?php endif; ?>
    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr><td class="aviso" colspan="6">NO EXISTEN REGISTROS RELACIONADOS CON LOS CRITERIOS DE B&Uacute;SQUEDA</td></tr>
    <?php endif;?>
  </tbody>
</table>
