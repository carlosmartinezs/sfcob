<h2>Ver Diligencia</h2>
<table>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="4">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Causa:</th>
      <td><?php echo $diligencia->getPeriodoCausa()->getCausa()->getCaratula() ?></td>
    </tr>
    <tr>
      <th>Periodo:</th>
      <td><?php echo $diligencia->getPeriodoCausa()->getPeriodo()->getNombre() ?></td>
    </tr>
    <tr>
      <th>Fecha:</th>
      <td><?php echo $diligencia->getFecha('d-m-Y') ?></td>
    </tr>
    <tr>
      <th>Descripci&oacute;n:</th>
      <td><?php echo $diligencia->getDescripcion() ?></td>
    </tr>
  </tbody>
</table>
