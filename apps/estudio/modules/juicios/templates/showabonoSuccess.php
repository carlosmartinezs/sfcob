<?php use_helper('Javascript') ?>
<h2>Ver Abono</h2>
<table>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="4">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Causa:</th>
      <td colspan="3"><?php echo $abono->getDeuda()->getCausa()->getCaratula() ?></td>
    </tr>
    <tr>
      <th>Tipo de Abono:</th>
      <td><?php echo $abono->getTipoAbono() ?></td>
      <th>Tipo de Pago:</th>
      <td><?php echo $abono->getTipoPago() ?></td>
    </tr>
    <tr>
      <th>Fecha:</th>
      <td><?php echo Funciones::fecha2($abono->getFecha('d-m-Y')) ?></td>
      <th>N&uacute;mero de Recibo:</th>
      <td><?php echo $abono->getNroRecibo() ?></td>
    </tr>
    <tr>
    	<th>Valor:</th>
      <td colspan="3">$ <?php echo $abono->getValor() ?></td>
    </tr>
    <tr><th>Motivo:</th>
      <td colspan="3"><?php echo $abono->getMotivo() ?></td></tr>
    <tr>
    	<td class="tdcentrado" colspan="6">      
      		<?php echo button_to_remote('Editar',array(
      			'update'	=>	'acciones_abono',
      			'url'		=>	'juicios/editabono?id='.$abono->getId().'&idcausa='.$abono->getDeuda()->getCausa()->getId(),
      			'loading' 	=> 	visual_effect('appear','loading', array('duration' => 0.5)),
				'complete'	=> 	visual_effect('fade','loading', array('duration' => 0.5))),
				array('class' => 'editar boton_con_imagen'))?>
			<?php echo button_to_remote('Eliminar', array(
				'update' => 'acciones_abono',
				'url' => 'juicios/deleteabono?id='.$abono->getId().'&idcausa='.$abono->getDeuda()->getCausa()->getId(),
				'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
				'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
		  		'confirm'	=> 'Est&aacute; seguro de eliminar &eacute;ste abono?'
				),array('class' => 'eliminar boton_con_imagen'))?>
	  </td>
    </tr>
  </tbody>
</table>
