<?php
use_helper('DateForm');
use_helper('Object');	
use_helper('Javascript');

echo input_hidden_tag('deudor_id', $deudor->getId());
echo input_tag('deudor',$deudor,array('readonly' => 'readonly'));
	
	?>&nbsp;<?php echo button_to_remote('Cambiar', array(
    	'update'	=> 'list-search-deudor',
    	'url'		=> 'administracion/searchdeudor?accion=buscarcausadeudor',
    	'loading'	=> visual_effect('appear','loading', array('duration' => 0)),
		'complete'	=> visual_effect('appear', 'list-search-deudor').
        			   visual_effect('highlight','list-search-deudor',array('duration' => 1.0)).
        			   visual_effect('fade','loading', array('duration' => 0)), 
      ),array('class' => 'cambiar boton_con_imagen')); 
?>	