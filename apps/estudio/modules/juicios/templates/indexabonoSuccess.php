<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Abonos</h1>
<ul class="menu-modulo">
	<?php if ($sf_user->hasCredential('secretaria')):?>
	<li>
		<?php echo link_to_remote('Ingresar Abono', array(
    		'update' => 'acciones_abono',
    		'url'    => 'juicios/searchcausa?accion=creaabono',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('secretaria')):?>
	<li><?php echo link_to_remote('Editar Abono', array(
    		'update' => 'acciones_abono',
    		'url'    => 'juicios/searchcausa?accion=editabono',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
	<li>
		<?php echo link_to_remote('Visualizar Abono', array(
    		'update' => 'acciones_abono',
    		'url'    => 'juicios/searchcausa?accion=showabono',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('secretaria')):?>
	<li><?php echo link_to_remote('Eliminar Abono', array(
    		'update' => 'acciones_abono',
    		'url'    => 'juicios/searchcausa?accion=deleteabono',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_abono"  style="clear: right"></div>