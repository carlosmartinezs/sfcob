<?php use_helper('Javascript') ?>
<h1>Gesti&oacute;n de Diligencias</h1>
<ul class="menu-modulo">
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('procurador') || $sf_user->hasCredential('secretaria')):?>
	<li>
		<?php echo link_to_remote('Ingresar Diligencia', array(
    		'update' => 'acciones_diligencia',
    		'url'    => 'juicios/searchcausa?accion=creadiligencia',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria')):?>
	<li>
		<?php echo link_to_remote('Editar Diligencia', array(
    		'update' => 'acciones_diligencia',
    		'url'    => 'juicios/searchcausa?accion=editdiligencia',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')): ?>
	<li>
		<?php echo link_to_remote('Visualizar Diligencia', array(
    		'update' => 'acciones_diligencia',
    		'url'    => 'juicios/searchcausa?accion=showdiligencia',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_diligencia"  style="clear: right"></div>