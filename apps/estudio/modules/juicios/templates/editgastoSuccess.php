<?php use_helper('Javascript', 'Object') ?>
<?php $gasto = $form->getObject() ?>
<h2><?php echo $gasto->isNew() ? 'Ingresar' : 'Editar' ?> Gasto</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_gasto',
    'url'      	=> 'juicios/updategasto'.(!$gasto->isNew() ? '?id='.$gasto->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="2">
        <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
          <?php echo button_to_remote('Cancelar', array(
			'update' => 'modulo',
			'url' => 'juicios/indexgasto',
			'loading' => visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
			),array('class'=>'cancelar boton_con_imagen'))?>
          
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="gasto_tipo_gasto_id">Tipo de Gasto * </label></th>
        <td>
          <?php echo $form['tipo_gasto_id']->renderError() ?>
          <?php echo $form['tipo_gasto_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="gasto_diligencia_id">Diligencia</label><br/>
        </th>
        <td>
          <?php echo $form['diligencia_id']->renderError() ?>
          <?php echo input_hidden_tag('diligencia_id', $diligenciaId);
          		echo $diligenciaDesc;?>
        </td>
      </tr>
      <tr>
        <th><label for="gasto_monto">Monto * </label><span class="small-font">(S&oacute;lo N&uacute;meros)</span></th>
        <td>
          <?php echo $form['monto']->renderError() ?>
          <?php echo $form['monto'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="gasto_fecha">Fecha * </label></th>
        <td>
          <?php echo Funciones::fecha(); ?>
        </td>
      </tr>
      <tr>
        <th><label for="gasto_motivo">Motivo * </label></th>
        <td>
          <?php echo $form['motivo']->renderError() ?>
          <?php echo $form['motivo'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
