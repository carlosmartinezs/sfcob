<h2>Ver Garant&iacute;a</h2>
<table>
  <tbody>
  <?php if($sf_user->getAttribute('guardado',false)):?>
  		<?php $sf_user->getAttributeHolder()->remove('guardado');?>
	  	<tr>
	      <td colspan="4">
	      	<div class="informacion" style="margin: 0px 0px 0px -5px ! important; padding: 0px 35px;width: 91%;" >
	      	Datos guardados con &eacute;xito.
	      	</div>
	      </td>
	    </tr>
   	<?php endif; ?>
    <tr>
      <th>Causa:</th>
      <td><?php echo $garantia->getCausa()->getRol().": ".$garantia->getCausa()->getCaratula() ?></td>
    </tr>
    <tr>
      <th>Tipo de Garant&iacute;a:</th>
      <td><?php echo $garantia->getTipoGarantia() ?></td>
    </tr>
    <tr>
      <th>Valor:</th>
      <td>$ <?php echo $garantia->getValor() ?></td>
    </tr>
  </tbody>
</table>
