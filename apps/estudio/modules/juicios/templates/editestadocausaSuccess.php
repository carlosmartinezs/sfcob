<?php use_helper('Javascript', 'Object') ?>
<?php $estado_causa = $form->getObject() ?>
<h2>Editar Estado de Causa</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'estado_causa',
    'url'      	=> 'juicios/updateestadocausa',
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','form-cambio-estado', array('duration' => 0.5)).
				   visual_effect('highlight','form-cambio-estado',array('duration' => 1.0)).
				   visual_effect('fade','loading', array('duration' => 0.5)),
	'confirm'	=> "Los cambios realizados se ver&aacute;n reflejados al momento de guardar la causa.",
))?>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="2">
        <input type="submit" value="Aceptar" class="aceptar boton_con_imagen"/>
        <?php echo button_to_function('Cerrar ventana',
				visual_effect('appear','loading').
				visual_effect('highlight','form-cambio-estado', array('duration' => 0.5)).
				visual_effect('fade','form-cambio-estado', array('duration' => 0.5)).
				visual_effect('fade','loading', array('duration' => 1.0)),
				array('class' => 'cerrar boton_con_imagen'));?>
          
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="estado_causa_causa_id">Causa</label></th>
        <td>
          <?php echo input_hidden_tag('causa_id', $causa->getId());
          		echo $causa; ?>
        </td>
      </tr>
      <tr>
        <th><label for="estado_causa_estado_id">Estado *</label></th>
        <td>
          <?php echo select_tag('estado_id',
  					options_for_select($lista,
  										'getId',
  										array('include_custom' => '-- Seleccione --'))); ?>
        </td>
      </tr>
      <tr>
        <th><label for="estado_causa_motivo">Motivo del Cambio *</label></th>
        <td>
          <?php echo $form['motivo']->renderError() ?>
          <?php echo $form['motivo'] ?>
          
          <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
