<?php

/**
 * juicios actions.
 *
 * @package    sgcj
 * @subpackage juicios
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 9301 2008-05-27 01:08:46Z dwhittle $
 */
class juiciosActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex()
  {
  }
  
  public function executeIndexcausa()
  {
  }
  
  public function executeListcausa()
  {
  	$c = new Criteria();
  	/*$c->addDescendingOrderByColumn(CausaPeer::FECHA_INICIO);*/
    $this->causaList = CausaPeer::doSelect($c);
  }
  
  public function executeSearchcausa($request)
  {
  	$this->accion = $request->getParameter('accion');

  	switch($this->accion){
  		case 'redactarcarta': 
  			$this->id_carta = $request->getParameter('tipo_carta'); break;
  		case 'redactarescrito': 
  			$this->id_escrito = $request->getParameter('tipo_escrito'); break;
  		default: break;   		
  	}
  	
  	$this->form = new SearchCausaForm();
  	$this->form->setDefault('accion',$this->accion);
  }
  
  public function executeSearchcausaupdate($request)
  {
  	$this->accion = $request->getParameter('accion');
  	$this->form = new SearchCausaForm();
  	
  	$datos = array(
  		'estado'		   => $request->getParameter('estado'),
  		'id'			   => $request->getParameter('id'),
		'accion'		   => $request->getParameter('accion'),
		'tipo_causa_id'    => $request->getParameter('tipo_causa_id'),
      	'searchdeudor'     => $request->getParameter('searchdeudor'),
      	'materia_id'       => $request->getParameter('materia_id'),
      	'searchcontrato'   => $request->getParameter('searchcontrato'),
      	'rol'              => $request->getParameter('rol'),
      	'competencia'      => $request->getParameter('competencia'),
      	'desde'    		   => $request->getParameter('desde'),
      	'hasta'            => $request->getParameter('hasta'),
      	'herencia_exhorto' => $request->getParameter('herencia_exhorto'),
  	    'periodo'          => $request->getParameter('periodo'),
  	);
  	
  	if($request->hasParameter('contrato_id'))
  	{
  		$datos['searchcontrato'] = $request->getParameter('contrato_id');
  	}
  	
    if($request->hasParameter('deudor_id'))
  	{
  		$datos['searchdeudor'] = $request->getParameter('deudor_id');
  	}
  	
  	$fecha_hasta = $request->getParameter('hasta');
  	
  	if($fecha_hasta['day'] == '' && $fecha_hasta['month'] == '' && $fecha_hasta['year'] == '')
  	{
  		$fecha_hasta['day'] = date('d');
  		$fecha_hasta['month'] = date('m');
  		$fecha_hasta['year'] = date('Y');
  		
  		$datos['hasta'] = $fecha_hasta;
  	}
  	
  	$this->form->bind($datos);
  	
  	if($this->form->isValid())
  	{
  		$this->causaList = CausaPeer::doSelect(CausaPeer::getCriteria($datos));
	  	
	  	if($request->hasParameter('id_carta') && $this->accion == 'redactarcarta'){
	
	  		$this->id_carta = $request->getParameter('id_carta');
	  		
	  	}else if($request->hasParameter('id_escrito') && $this->accion == 'redactarescrito'){
	  		
	  		$this->id_escrito = $request->getParameter('id_escrito');;
	  	}
	  	
		$this->setTemplate('listcausa');
  	}
  	else
  	{
  		$this->pop_up = true;
  		
	  	if($request->hasParameter('contrato_id'))
	  	{
	  		$this->contrato_id = $request->getParameter('contrato_id');
	  		$this->acreedor = ContratoPeer::retrieveByPK($this->contrato_id)->getAcreedor();
	  	}
	  	
  	    if($request->hasParameter('deudor_id'))
	  	{
	  		$this->deudor_id = $request->getParameter('deudor_id');
	  		$this->deudor = DeudorPeer::retrieveByPK($this->deudor_id);
	  	}
	  	
  		$this->setTemplate('searchcausa');
  	}
  }
  
  public function executeShowcausa($request)
  {
    $this->causa = CausaPeer::retrieveByPk($request->getParameter('id'));
    $this->deuda = $this->causa->getDeuda();
    $this->forward404Unless($this->causa);
  }

  public function executeCreatecausa()
  {
    $this->form = new CausaForm();
    $this->setTemplate('editcausa');
    $this->editar = false;
  }

  public function executeEditcausa($request)
  {
    $this->form = new CausaForm(CausaPeer::retrieveByPk($request->getParameter('id')));
    $this->editar = true;
    $causa = CausaPeer::retrieveByPk($request->getParameter('id'));
    $tribunal = $causa->getCausaTribunalActual()->getTribunal();
    $this->tribunalId = $tribunal->getId();
    $this->tribunalNombre = $tribunal->__toString();
    $this->estadoCausa = $causa->getEstadoCausaActivo()->getEstado();
    $this->estadoId = $causa->getEstadoCausaActivo()->getEstado()->getId();
    $this->periodoCausa = $causa->getPeriodoCausaActual()->getPeriodo();
    $this->periodoCausaId = $causa->getPeriodoCausaActual()->getPeriodoId();
    $this->tipoCausaId = $causa->getTipoCausaId();
    $this->deudor = $causa->getNombreDeudor();
    $this->deudorId = $causa->getDeudor()->getId();    
    $this->nombreAcreedor = $causa->getAcreedor();
    $acreedorId = $causa->getAcreedor()->getId();
    $acreedor = AcreedorPeer::retrieveByPk($acreedorId);
    $this->contratoId = $causa->getContrato()->getId();
    $this->causaExhorto = $causa->getCausaExhorto();
  }

  public function executeUpdatecausa($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new CausaForm(CausaPeer::retrieveByPk($request->getParameter('id')));
    
    $datos = $request->getParameter('causa');
    $datos['tipo_causa_id'] = $request->getParameter('tipo_causa_id');
    
    if($datos['tipo_causa_id'] == '2'){
    	$datos['herencia_exhorto'] = true;
    	$datos['causa_exhorto'] = $request->getParameter('causa_exhorto_id');
    }else{
    	$datos['herencia_exhorto'] = false;
    }
    
    $datos['deudor_id'] = $request->getParameter('deudor_id');
	$datos['contrato_id'] = $request->getParameter('contrato_id');
	$datos['tribunal'] = $request->getParameter('tribunal_id');
	
	$editar = $request->getParameter('editar');
	
	$this->estadoId = $request->getParameter('estado');
  	$this->estadoCausa = $request->getParameter('estadoCausa');
  	
  	$this->periodoCausaId = $request->getParameter('periodo_causa_id');
  	$this->periodoCausa = $request->getParameter('periodoCausa');
	
	if(isset($editar) && $editar == true)
	{		
		if($request->hasParameter('periodo_causa_id')){
			$c = new Criteria();
			$causa_id = $request->getParameter('id');
	  		$c->add(PeriodoCausaPeer::CAUSA_ID, $causa_id);
	  		$c->add(PeriodoCausaPeer::ACTIVO, true);
	  		$periodoCausaBd = PeriodoCausaPeer::doSelectOne($c);
	  		$this->periodoCausaBd = $periodoCausaBd;
	  		if($this->periodoCausaBd->getPeriodoId() != $request->getParameter('periodo_causa_id'))
	  		{
				$this->periodoCausaBd->setActivo(false);
	  			$periodo = new PeriodoCausa();
	  			$periodo->setCausaId($causa_id);
	  			$periodo->setPeriodoId($request->getParameter('periodo_causa_id'));
	  			$periodo->setFechaInicio("now");
	  			$periodo->setActivo(true);
	  			$this->periodo = $periodo;
	  		}
		}else{		
			$periodoCausa = new PeriodoCausa();
			$periodoCausa->setPeriodoId(1);
			$periodoCausa->setFechaInicio("now");
			$periodoCausa->setActivo(true);
		
			$deuda = new Deuda();
			$deuda->setMonto(0);
			$deuda->setInteres(0);
			$deuda->setHonorarios(0);
		}
		
		if($request->hasParameter('estado'))
		{
			if($request->getParameter('estado') != '')
			{
				$crit = new Criteria();
		  		$crit->add(EstadoCausaPeer::CAUSA_ID, $causa_id);
		  		$crit->add(EstadoCausaPeer::ACTIVO, true);
		  		$estadoCausaBd = EstadoCausaPeer::doSelectOne($crit);
		  		$this->estadoCausaBd = $estadoCausaBd;
		  		
		  		if($this->estadoCausaBd->getEstadoId() != $request->getParameter('estado'))
		  		{
		  			$this->estadoCausaBd->setActivo(false);
		  			$estado = new EstadoCausa();
		  			$estado->setCausaId($causa_id);
		  			$estado->setEstadoId($request->getParameter('estado'));
		  			$estado->setFecha("now");
		  			$estado->setMotivo($request->getParameter('motivo_estado'));
		  			$estado->setActivo(true);
		  			$this->estado = $estado;
		  			
		  			$this->errorEstado = false;
		  		}
			}
			else
			{
				$this->errorEstado = true;
			}
	  		
		}
		else
		{		
			$estadoCausa = new EstadoCausa();
			$estadoCausa->setEstadoId(1);
			$estadoCausa->setFecha("now");
			$estadoCausa->setMotivo("Inicio Causa");
			$estadoCausa->setActivo(true);
			
			$this->errorEstado = false;
		}
		
		if($request->hasParameter('motivo_tribunal'))
		{	
	  		$criteria = new Criteria();
	  		$criteria->add(CausaTribunalPeer::CAUSA_ID, $causa_id);
	  		$criteria->add(CausaTribunalPeer::ACTIVO, true);
	  		$causaTribunalBd = CausaTribunalPeer::doSelectOne($criteria);
	  		$this->causaTribunalBd = $causaTribunalBd;
	  		
	  		if($this->causaTribunalBd->getTribunalId() != $request->getParameter('tribunal_id'))
	  		{
	  			$this->causaTribunalBd->setActivo(false);
	  			$tribunal = new CausaTribunal();
	  			$tribunal->setCausaId($causa_id);
	  			$tribunal->setTribunalId($request->getParameter('tribunal_id'));
	  			$tribunal->setFechaCambio("now");
	  			$tribunal->setMotivo($request->getParameter('motivo_tribunal'));
	  			$tribunal->setActivo(true);
	  			$this->tribunal = $tribunal;
	  		}
		}
		else
		{
			$causaTribunal = new CausaTribunal();
			$causaTribunal->setTribunalId($request->getParameter('tribunal_id'));
			$causaTribunal->setFechaCambio("now");
			$causaTribunal->setMotivo("Tribunal Base");
			$causaTribunal->setActivo(true);
		}
		
		$this->form->bind($datos);
	    
	    if ($this->form->isValid() && !$this->errorEstado)
	    {
	    	if($datos['herencia_exhorto'] && $request->getParameter('causa_exhorto_id') > 0)
	    	{
	    		$this->editar = true;
	      
			      $this->estadoId = $request->getParameter('estado');
			      $this->estadoCausa = EstadoPeer::retrieveByPK($this->estadoId);
			      
			      $this->periodoCausaId = $request->getParameter('periodo_causa_id');
			      $this->periodoCausa = PeriodoPeer::retrieveByPK($this->periodoCausaId);
			      
			      $this->deudorId = $request->getParameter('deudor_id');
			      $this->deudor = DeudorPeer::retrieveByPK($this->deudorId);
			      
			      $this->contratoId = $request->getParameter('contrato_id');
			      
			      $contrato = ContratoPeer::retrieveByPK($this->contratoId);
			      
			      if ($contrato instanceof Contrato)
			      {
			      	$this->nombreAcreedor = $contrato->getAcreedor();
			      }
			      else
			      {
			      	$this->nombreAcreedor = '';
			      }
			      
			      $this->tribunalId = $request->getParameter('tribunal_id');
			      $this->tribunalNombre = TribunalPeer::retrieveByPK($this->tribunalId);	
			      
			      $this->tipoCausaId = $request->getParameter('tipo_causa_id');
		
			      $this->causaExhorto = CausaPeer::retrieveByPK($request->getParameter('causa_exhorto_id'));
			      $this->motivoTribunal = $request->getParameter('motivo_tribunal');
			      $this->motivoEstado = $request->getParameter('motivo_estado');
		
			      $this->setTemplate('editcausa');	
	    	}
	    	else
	    	{
	    		 $causa = $this->form->save();
			      $this->causa = $causa;
			      
			      if(isset($causaTribunal) && isset($deuda) && isset($estadoCausa) && isset($periodoCausa)){
				      $causaTribunal->setCausaId($causa->getId());
				      $estadoCausa->setCausaId($causa->getId());
				      $periodoCausa->setCausaId($causa->getId());
				      $deuda->setCausaId($causa->getId());
				      $causaTribunal->save();
				      $estadoCausa->save();
				      $periodoCausa->save();
				      $deuda->save();
			      }
			      
			      if($this->periodo){
				      $this->periodo->save();
			  		  $this->periodoCausaBd->save();
			      }
			      if($this->estado){
			  		  $this->estado->save();
			  		  $this->estadoCausaBd->save();
			      }
			      if($this->tribunal){
			  		  $this->tribunal->save();
			  		  $this->causaTribunalBd->save();
			      }
		  		  $this->getUser()->setAttribute('guardado',true);
			      $this->setTemplate('showcausa');
	    	}
	     
	    }else{
	      $this->editar = true;
	      
	      $this->estadoId = $request->getParameter('estado');
	      $this->estadoCausa = EstadoPeer::retrieveByPK($this->estadoId);
	      
	      $this->periodoCausaId = $request->getParameter('periodo_causa_id');
	      $this->periodoCausa = PeriodoPeer::retrieveByPK($this->periodoCausaId);
	      
	      $this->deudorId = $request->getParameter('deudor_id');
	      $this->deudor = DeudorPeer::retrieveByPK($this->deudorId);
	      
	      $this->contratoId = $request->getParameter('contrato_id');
	      
	      $contrato = ContratoPeer::retrieveByPK($this->contratoId);
	      
	      if ($contrato instanceof Contrato)
	      {
	      	$this->nombreAcreedor = $contrato->getAcreedor();
	      }
	      else
	      {
	      	$this->nombreAcreedor = '';
	      }
	      
	      
	      $this->tribunalId = $request->getParameter('tribunal_id');
	      $this->tribunalNombre = TribunalPeer::retrieveByPK($this->tribunalId);	
	      
	      $this->tipoCausaId = $request->getParameter('tipo_causa_id');

	      $this->causaExhorto = CausaPeer::retrieveByPK($request->getParameter('causa_exhorto_id'));
	      $this->motivoTribunal = $request->getParameter('motivo_tribunal');
	      $this->motivoEstado = $request->getParameter('motivo_estado');

	      $this->setTemplate('editcausa');	
	    }
	}
	else
	{
		$causaTribunal = new CausaTribunal();
		$causaTribunal->setTribunalId($request->getParameter('tribunal_id'));
		$causaTribunal->setFechaCambio("now");
		$causaTribunal->setMotivo("Tribunal Base");
		$causaTribunal->setActivo(true);
		
		$estadoCausa = new EstadoCausa();
		$estadoCausa->setEstadoId(1);
		$estadoCausa->setFecha("now");
		$estadoCausa->setMotivo("Inicio Causa");
		$estadoCausa->setActivo(true);
		
		$periodoCausa = new PeriodoCausa();
		$periodoCausa->setPeriodoId(1);
		$periodoCausa->setFechaInicio("now");
		$periodoCausa->setActivo(true);
		
		$deuda = new Deuda();
		$deuda->setMonto(0);
		$deuda->setInteres(0);
		$deuda->setHonorarios(0);
	    
	    $this->form->bind($datos);
	    
	    if ($this->form->isValid())
	    {
	    	if($datos['herencia_exhorto'] && $request->getParameter('causa_exhorto_id') > 0)
	    	{
	    		$this->editar = true;

			      $this->estadoId = $request->getParameter('estado');
			      $this->estadoCausa = EstadoPeer::retrieveByPK($this->estadoId);
			      
			      $this->periodoCausaId = $request->getParameter('periodo_causa_id');
			      $this->periodoCausa = PeriodoPeer::retrieveByPK($this->periodoCausaId);
			      
			      $this->tipoCausaId = $request->getParameter('tipo_causa_id');
			      
			      $this->deudorId = $request->getParameter('deudor_id');
			      $this->deudor = DeudorPeer::retrieveByPK($this->deudorId);
			      
			      $contrato = ContratoPeer::retrieveByPK($this->contratoId);
			      
			      if ($contrato instanceof Contrato)
			      {
			      	$this->nombreAcreedor = $contrato->getAcreedor();
			      }
			      else
			      {
			      	$this->nombreAcreedor = '';
			      }
			      
			      $this->tribunalId = $request->getParameter('tribunal_id');
			      $this->tribunalNombre = TribunalPeer::retrieveByPK($this->tribunalId);	 
		
			      $this->causaExhorto = CausaPeer::retrieveByPK($request->getParameter('causa_exhorto_id'));
			      
			      $this->setTemplate('editcausa');	
	    	}
	    	else
	    	{
	    		 $causa = $this->form->save();
			      $this->causa = $causa;
			      $causaTribunal->setCausaId($causa->getId());
			      $estadoCausa->setCausaId($causa->getId());
			      $periodoCausa->setCausaId($causa->getId());
			      $deuda->setCausaId($causa->getId());
			      $causaTribunal->save();
			      $estadoCausa->save();
			      $periodoCausa->save();
			      $deuda->save();
			      
			      $this->getUser()->setAttribute('guardado',true);
			      $this->setTemplate('showcausa');
	    	}
	      
	    }else{
	      $this->editar = true;

	      $this->estadoId = $request->getParameter('estado');
	      $this->estadoCausa = EstadoPeer::retrieveByPK($this->estadoId);
	      
	      $this->periodoCausaId = $request->getParameter('periodo_causa_id');
	      $this->periodoCausa = PeriodoPeer::retrieveByPK($this->periodoCausaId);
	      
	      $this->tipoCausaId = $request->getParameter('tipo_causa_id');
	      
	      $this->deudorId = $request->getParameter('deudor_id');
	      $this->deudor = DeudorPeer::retrieveByPK($this->deudorId);
	      
	      $contrato = ContratoPeer::retrieveByPK($this->contratoId);
	      
	      if ($contrato instanceof Contrato)
	      {
	      	$this->nombreAcreedor = $contrato->getAcreedor();
	      }
	      else
	      {
	      	$this->nombreAcreedor = '';
	      }
	      
	      $this->tribunalId = $request->getParameter('tribunal_id');
	      $this->tribunalNombre = TribunalPeer::retrieveByPK($this->tribunalId);	 

	      $this->causaExhorto = CausaPeer::retrieveByPK($request->getParameter('causa_exhorto_id'));
	      
	      $this->setTemplate('editcausa');	
	    }
	}
  }

  public function executeDeletecausa($request)
  {
    $this->forward404Unless($causa = CausaPeer::retrieveByPk($request->getParameter('id')));

    $causa->delete();

    $this->redirect('juicios/indexcausa');
  }
  
  public function executeIndexdiligencia()
  {
  }

  public function executeListdiligencia($request)
  {
  	$c = new Criteria();
  	$c->add(CausaPeer::ID, $request->getParameter('idcausa'));
  	$c->addJoin(PeriodoCausaPeer::CAUSA_ID, CausaPeer::ID);
  	$c->add(PeriodoCausaPeer::ACTIVO, true);
  	$this->diligenciaList = DiligenciaPeer::doSelectJoinPeriodoCausa($c);
  	$this->causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
	$accion = $request->getParameter('accion');
  	$this->accion = $accion;
  }

  public function executeShowdiligencia($request)
  {
    $this->diligencia = DiligenciaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->diligencia);
  }

  public function executeCreatediligencia($request)
  {
    $this->form = new DiligenciaForm();
	$causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
	$this->causa = $causa;
	$this->causaId = $request->getParameter('idcausa');
	$this->nombreCausa = $causa->getCaratula();
	$this->periodoId = $this->causa->getPeriodoCausaActual()->getPeriodoId();
	$this->nombrePeriodo = $this->causa->getPeriodoCausaActual()->getPeriodo()->getNombre();
    $this->setTemplate('editdiligencia');
  }

  public function executeEditdiligencia($request)
  {
    $this->form = new DiligenciaForm(DiligenciaPeer::retrieveByPk($request->getParameter('id')));
    $causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
	$this->causa = $causa;
	$this->causaId = $request->getParameter('idcausa');
	$this->nombreCausa = $this->causa->getCaratula();
	$this->periodoId = $this->causa->getPeriodoCausaActual()->getPeriodoId();
	$this->nombrePeriodo = $this->causa->getPeriodoCausaActual()->getPeriodo()->getNombre();
  }

  public function executeUpdatediligencia($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new DiligenciaForm(DiligenciaPeer::retrieveByPk($request->getParameter('id')));
	
    $this->causa = CausaPeer::retrieveByPK($request->getParameter('causa_id'));
    
    $datos = $request->getParameter('diligencia');
    
    $datos['causa_id'] = $request->getParameter('causa_id');
    $datos['periodo_id'] = $request->getParameter('periodo_id');
    
    $datos['periodo_causa_id'] = $this->causa->getPeriodoCausaActual()->getId();
    
    $this->form->bind($datos);
    
    if ($this->form->isValid())
    {
      $this->diligencia = $this->form->save();
      $this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showdiligencia');
    }else{
      $this->causaId = $request->getParameter('causa_id');
      $this->nombreCausa = $this->causa->getCaratula();
      $this->periodoId = $request->getParameter('periodo_id');
      $this->nombrePeriodo = $this->causa->getPeriodoCausaActual()->getPeriodo()->getNombre(); 
	  $this->setTemplate('editdiligencia');
    }
  }

  public function executeDeletediligencia($request)
  {
    $this->forward404Unless($diligencia = DiligenciaPeer::retrieveByPk($request->getParameter('id')));

    $diligencia->delete();

    $this->redirect('juicios/indexdiligencia');
  }
  
  public function executeIndexabono()
  {
  }
  
  public function executeListabono($request)
  {
    $this->causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
    $c = new Criteria();
    $c->add(AbonoPeer::DEUDA_ID,$this->causa->getDeuda()->getId());
    $this->abonoList = AbonoPeer::doSelect($c);
  	$this->accion = $request->getParameter('accion');
  }

  public function executeShowabono($request)
  {
    $this->abono = AbonoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->abono);
  }

  public function executeCreateabono($request)
  {
    $this->form = new AbonoForm();
    $causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
	$this->causa = $causa;
	$this->causaId = $request->getParameter('idcausa');
	$this->nombreCausa = $causa->getCaratula();
	$this->error = "";
	
	$c = new Criteria();
    $c->add(DeudaPeer::CAUSA_ID, $request->getParameter('idcausa'));
    $this->deuda = DeudaPeer::doSelectOne($c);
    $this->deudaId = $this->deuda->getId();
    $this->montoDeudaPendiente = ($this->deuda->getMonto()-$this->causa->getAbonoMonto());
    $this->interesDeudaPendiente = ($this->deuda->getInteres()-$this->causa->getAbonoInteres());
	$this->gastosCobranzaDeudaPendiente = ($this->deuda->getGastosCobranza()-$this->causa->getAbonoGastosCobranza());
    $this->setTemplate('editabono');
  }

  public function executeEditabono($request)
  {
    $this->form = new AbonoForm(AbonoPeer::retrieveByPk($request->getParameter('id')));
    $causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
	$this->causa = $causa;
	$this->causaId = $request->getParameter('idcausa');
	$this->nombreCausa = $this->causa->getCaratula();
	$this->error = "";
	$c = new Criteria();
    $c->add(DeudaPeer::CAUSA_ID, $request->getParameter('idcausa'));
    $this->deuda = DeudaPeer::doSelectOne($c);
    $this->deudaId = $this->deuda->getId();
    $this->montoDeudaPendiente = ($this->deuda->getMonto()-$this->causa->getAbonoMonto());
    $this->interesDeudaPendiente = ($this->deuda->getInteres()-$this->causa->getAbonoInteres());
    $this->gastosCobranzaDeudaPendiente = ($this->deuda->getGastosCobranza()-$this->causa->getAbonoGastosCobranza());
  }

  public function executeUpdateabono($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new AbonoForm(AbonoPeer::retrieveByPk($request->getParameter('id')));
    
    $causa = CausaPeer::retrieveByPK($request->getParameter('causa_id'));
	$this->causa = $causa;
    
    $datos = $request->getParameter('abono');
    $datos['causa_id'] = $request->getParameter('causa_id');
    $datos['deuda_id'] = $request->getParameter('deuda_id');
    
    $datos['fecha_actual'] = date('Y-m-d');
    $datos['fecha_inicio_causa'] = $this->causa->getFechaInicio('Y-m-d');
    
    $c = new Criteria();
    $c->add(DeudaPeer::CAUSA_ID, $request->getParameter('causa_id'));
    $this->deuda = DeudaPeer::doSelectOne($c);
    
    $this->montoDeudaPendiente = ($this->deuda->getMonto()-$this->causa->getAbonoMonto());
    $this->interesDeudaPendiente = ($this->deuda->getInteres()-$this->causa->getAbonoInteres());
    $this->gastosCobranzaDeudaPendiente = ($this->deuda->getGastosCobranza()-$this->causa->getAbonoGastosCobranza());
    
  	if($datos['tipo_abono_id'] == 3 ){
    	$datos['pendiente'] = $this->gastosCobranzaDeudaPendiente;
    }else if($datos['tipo_abono_id'] == 2 ){
    	$datos['pendiente'] = $this->interesDeudaPendiente;
    }else if($datos['tipo_abono_id'] == 1 ){
    	$datos['pendiente'] = $this->montoDeudaPendiente;
    }else{
    	$datos['pendiente'] = -1;
    }  	
    	
    $this->form->bind($datos);
    
    if ($this->form->isValid())
    {
      $this->abono = $this->form->save();
      
      if($datos['tipo_abono_id'] != 3)
      {
      	$porcentaje = $this->causa->getContrato()->getPorcentajeHonorario();
      	$this->deuda->setHonorarios($this->deuda->getHonorarios() + ($this->abono->getValor() * $porcentaje / 100));
      	$this->deuda->save();
      }
		$this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showabono');
    }else{
      $this->causaId = $request->getParameter('causa_id');
	  $this->nombreCausa = $causa->getCaratula();
      $this->deudaId = $this->deuda->getId();
      $this->setTemplate('editabono');
    }
  }

  public function executeDeleteabono($request)
  {
    $this->forward404Unless($abono = AbonoPeer::retrieveByPk($request->getParameter('id')));
	$this->causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
	
	$this->deuda = $this->causa->getDeuda();
  	
	if($abono->getTipoAbonoId()!= 3)
      {
      	$porcentaje = $this->causa->getContrato()->getPorcentajeHonorario();
      	$this->deuda->setHonorarios($this->deuda->getHonorarios() - ($abono->getValor() * $porcentaje / 100));
      	$this->deuda->save();
      }
      
    $abono->delete();

    $c = new Criteria();
    $c->add(AbonoPeer::DEUDA_ID,$this->causa->getDeuda()->getId());
    $this->abonoList = AbonoPeer::doSelect($c);
  	$this->accion = 'deleteabono';
  	$this->hayMensajeEliminar = true;
    $this->setTemplate('listabono');
   
  }

  public function executeIndexgasto()
  {
  }
  
  public function executeListgasto($request)
  {
  	$c = new Criteria();
  	$c->add(GastoPeer::DILIGENCIA_ID,$request->getParameter('iddiligencia'));
    $this->gastoList = GastoPeer::doSelect($c);
    $accion = $request->getParameter('accion');
  	$this->accion = $accion;
  }

  public function executeShowgasto($request)
  {
    $this->gasto = GastoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->gasto);
  }

  public function executeCreategasto($request)
  {
    $this->form = new GastoForm();
    
    $diligencia = DiligenciaPeer::retrieveByPK($request->getParameter('iddiligencia'));
    $this->diligenciaId = $request->getParameter('iddiligencia');
    $this->diligenciaDesc = $diligencia->getDescripcion();
    $this->setTemplate('editgasto');
  }

  public function executeEditgasto($request)
  {
    $this->form = new GastoForm(GastoPeer::retrieveByPk($request->getParameter('id')));
    $diligencia = DiligenciaPeer::retrieveByPK($request->getParameter('diligencia_id'));
    $this->diligenciaId = $request->getParameter('diligencia_id');
    $this->diligenciaDesc = $diligencia->getDescripcion();
  }

  public function executeUpdategasto($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new GastoForm(GastoPeer::retrieveByPk($request->getParameter('id')));
    
    $datos = $request->getParameter('gasto');
    $datos['diligencia_id'] = $request->getParameter('diligencia_id');
	$datos['fecha'] = strtotime("now");
    
    $diligencia = DiligenciaPeer::retrieveByPK($request->getParameter('diligencia_id'));
    
    $this->form->bind($datos);
    if ($this->form->isValid())
    {
      $this->gasto = $this->form->save();
	  $deuda = $diligencia->getPeriodoCausa()->getCausa()->getDeuda();
	  $deuda->setGastosCobranza($deuda->getGastosCobranza() + $this->gasto->getMonto());
	  $deuda->save();
	  $this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showgasto');
    }else{
	  
      $this->diligenciaId = $request->getParameter('diligencia_id');
      $this->diligenciaDesc = $diligencia->getDescripcion();
	  $this->setTemplate('editgasto');
    }
  }

  public function executeDeletegasto($request)
  {
    $this->forward404Unless($gasto = GastoPeer::retrieveByPk($request->getParameter('id')));

    $gasto->delete();

    $this->redirect('juicios/indexgasto');
  }
  
  public function executeCreainputcausaexhorto($request)
  {
  $this->editar = $request->getParameter('editar');
  $this->tipo_causa_id = $request->getParameter('tipo_causa_id');
  $this->pathInfo = $request->getPathInfo();	
  }
  
  public function executeTraerdeudor($request)
  {
  	$this->deudor = DeudorPeer::retrieveByPk($request->getParameter('iddeudor'));
  }
  
  public function executeTraercontrato($request)
  {
  	$contratoActivo = AcreedorPeer::retrieveByPk($request->getParameter('idacreedor'))->getContratoActivo();
  	$acreedor = AcreedorPeer::retrieveByPK($request->getParameter('idacreedor'));
  	$this->nombreAcreedor = $acreedor->__toString(); 
  	
  	if($contratoActivo != null)
  	{
  		$this->contratoId = $contratoActivo->getId();
  	}
  	else
  	{
  		$c = new Criteria();
  		$c->add(ContratoPeer::ACREEDOR_ID,$request->getParameter('idacreedor'));
  		$this->contratoId = ContratoPeer::doSelectOne($c)->getId();
  	} 	
  }
  
  public function executeTraertribunal($request)
  {
  	$this->tribunal = TribunalPeer::retrieveByPk($request->getParameter('idtribunal'));
  }
  
  public function executeTraercausaexhorto($request)
  {
  	$this->accion = $request->getParameter('accion');
  	$this->causaExhorto = CausaPeer::retrieveByPK($request->getParameter('id_causa'));
  }
  
  public function executeEditperiodocausa($request)
  {
  	$c = new Criteria();
  	$c->add(PeriodoCausaPeer::CAUSA_ID, $request->getParameter('causa_id'));
  	$c->add(PeriodoCausaPeer::ACTIVO, true);
  	$periodoCausaBd = PeriodoCausaPeer::doSelectOne($c);
  	$this->periodoId = $periodoCausaBd->getPeriodoId();
  	
  	if($this->periodoId == '1' || $this->periodoId == '2'){
  		$this->periodoId += 1;
  	}
  	$this->periodoNombre = PeriodoPeer::retrieveByPK($this->periodoId)->getNombre();
  }
  
  public function executeEditestadocausa($request)
  {
  	$this->form = new EstadoCausaForm(EstadoCausaPeer::retrieveByPk($request->getParameter('id')));
  	$this->causa = CausaPeer::retrieveByPK($request->getParameter('causa_id'));
  	$this->estado = EstadoPeer::retrieveByPK($request->getParameter('estado_id'));
  	$this->estadoId = $this->estado->getId();
  	
  	if($this->estadoId == 1)
  	{
  		$lista = array();
 				$estadosList = EstadoPeer::doSelect(new Criteria());
  				foreach ($estadosList as $estado)
  				{
  					if($estado->getId() == 3 || $estado->getId() == 5 || $estado->getId() == 2 )
  					{
    				$lista[$estado->getId()] = $estado->getNombre();
  					}
  				} 
  		$this->lista = $lista;
  	}elseif($this->estadoId == 3){
  		$lista = array();
 				$estadosList = EstadoPeer::doSelect(new Criteria());
  				foreach ($estadosList as $estado)
  				{
  					if($estado->getId() == 5 || $estado->getId() == 1 || $estado->getId() == 4 )
  					{
    				$lista[$estado->getId()] = $estado->getNombre();
  					}
  				} 
  		$this->lista = $lista;
  	}elseif($this->estadoId == 5){
  		$lista = array();
 				$estadosList = EstadoPeer::doSelect(new Criteria());
  				foreach ($estadosList as $estado)
  				{
  					if($estado->getId() == 1 || $estado->getId() == 2 )
  					{
    				$lista[$estado->getId()] = $estado->getNombre();
  					}
  				} 
  		$this->lista = $lista;
  	}
  }
  
  public function executeUpdateestadocausa($request)
  {
  	$datos = $request->getParameter('estado_causa');
  	$this->estadoNuevo = $request->getParameter('estado_id');
  	$this->motivoEstado = $datos['motivo'];
  	$this->estadoNuevoNombre = EstadoPeer::retrieveByPK($this->estadoNuevo);    
  }
  
  public function executeEditcausatribunal($request)
  {
  	$this->form = new CausaTribunalForm(CausaTribunalPeer::retrieveByPk($request->getParameter('id')));
  	$this->causa = CausaPeer::retrieveByPK($request->getParameter('causa_id'));
  	$this->tribunal = TribunalPeer::retrieveByPK($request->getParameter('tribunal_id'));
  	$this->tribunalId = $this->tribunal->getId();
  }
  
  public function executeTraertribunaledit($request)
  {
  	$this->tribunal = TribunalPeer::retrieveByPk($request->getParameter('idtribunal'));
  }
  
	public function executeUpdatecausatribunal($request)
  {
  	$datos = $request->getParameter('causa_tribunal');
  	$this->tribunalNuevo = $request->getParameter('tribunal_id');
  	$this->motivoTribunal = $datos['motivo'];
  	$this->tribunalNuevoNombre = TribunalPeer::retrieveByPK($this->tribunalNuevo);    
  }
  
  public function executeIndexdocumento()
  {
  }
  
  public function executeListdocumento($request)
  {
  	$c = new Criteria();
  	$c->add(DocumentoPeer::CAUSA_ID, $request->getParameter('idcausa'));
  	$c->addAscendingOrderByColumn(DocumentoPeer::FECHA_EMISION);
  	$this->documentoList = DocumentoPeer::doSelect($c);
  	$this->causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
  	$accion = $request->getParameter('accion');
  	$this->accion = $accion;
  }

  public function executeShowdocumento($request)
  {
    $this->documento = DocumentoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->documento);
  }

  public function executeCreatedocumento($request)
  {
    $this->form = new DocumentoForm();
	$causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
	$this->causa = $causa;
	$this->causaId = $request->getParameter('idcausa');
	$this->nombreCausa = $causa->getCaratula();
    $this->setTemplate('editdocumento');
  }

  public function executeEditdocumento($request)
  {
    $this->form = new DocumentoForm(DocumentoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdatedocumento($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new DocumentoForm(DocumentoPeer::retrieveByPk($request->getParameter('id')));
    $datos = $request->getParameter('documento');
    
    $this->causa = CausaPeer::retrieveByPK($request->getParameter('causa_id'));
    $datos['causa_id'] = $request->getParameter('causa_id');
    
    $c = new Criteria();
    $c->add(DeudaPeer::CAUSA_ID, $request->getParameter('causa_id'));
    $deuda = DeudaPeer::doSelectOne($c);
    
    $this->intereses = ($datos['monto']*$datos['interes'])/100;
    $this->intereses = floor($this->intereses);
    //$this->honorariosContrato = $this->causa->getContrato()->getPorcentajeHonorario();
    //$this->honorarios = (($datos['monto']+$this->intereses)*$this->honorariosContrato)/100;
        
    //$this->honorarios = floor($this->honorarios);
    
    $deuda->setMonto($deuda->getMonto()+$datos['monto']);
    $deuda->setInteres($deuda->getInteres()+$this->intereses);
    //$deuda->setHonorarios($deuda->getHonorarios()+$this->honorarios);
    
    $periodoCausaId = $this->causa->getPeriodoCausaActual()->getId();
    
    $diligencia = new Diligencia();
    $diligencia->setPeriodoCausaId($periodoCausaId);
    $diligencia->setFecha("now");

    $this->form->bind($datos);
    if ($this->form->isValid())
    {
      $this->documento = $this->form->save();
	  $deuda->save();
	  $diligencia->setDescripcion("Ingreso de documento ".$this->documento->getNroDocumento());
	  $diligencia->save();
	  $this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showdocumento');
    }else{
      $this->causaId = $request->getParameter('causa_id');
      $this->nombreCausa = $this->causa->getCaratula();
	  $this->setTemplate('editdocumento');
    }
  }

  public function executeDeletedocumento($request)
  {
    $this->forward404Unless($documento = DocumentoPeer::retrieveByPk($request->getParameter('id')));
    
    $causa = CausaPeer::retrieveByPK($request->getParameter('causaid'));
    $this->causa = $causa;
    $c = new Criteria();
    $c->add(DeudaPeer::CAUSA_ID, $request->getParameter('causaid'));
    $deuda = DeudaPeer::doSelectOne($c);
    
    $this->interesesDoc = $documento->getInteres();
    $this->deudaBase = $documento->getMonto();
    $this->intereses = ($documento->getMonto()*$this->interesesDoc)/100;
    //$this->honorariosContrato = $causa->getContrato()->getPorcentajeHonorario();
    //$this->honorarios = (($this->deudaBase+$this->intereses)*$this->honorariosContrato)/100;
    
    $this->intereses = floor($this->intereses);
    //$this->honorarios = floor($this->honorarios);
    
    $deuda->setMonto($deuda->getMonto()-$this->deudaBase);
    $deuda->setInteres($deuda->getInteres()-$this->intereses);
    //$deuda->setHonorarios($deuda->getHonorarios()-$this->honorarios);
    
    $periodoCausaId = $this->causa->getPeriodoCausaActual()->getId();
    
    $diligencia = new Diligencia();
    $diligencia->setPeriodoCausaId($periodoCausaId);
    $diligencia->setFecha("now");
    
    if($deuda && $diligencia){
    	$diligencia->setDescripcion("Eliminaci&oacute;n de documento: ".$documento->getNroDocumento().
    							"<br>Descripci&oacute;n: ".$documento->getDescripcion().
    							"<br>Tipo: ".$documento->getTipoDocumento().
    							"<br>Monto: ".$documento->getMonto());
    	$diligencia->save();
    	$deuda->save();
    	$documento->delete();	
    }
    
    $this->setTemplate('indexcausa');
  }
  
  public function executeIndexgarantia()
  {
  }

  public function executeListgarantia($request)
  {
  	$c = new Criteria();
  	$c->add(CausaPeer::ID, $request->getParameter('idcausa'));
  	$this->garantiaList = GarantiaPeer::doSelectJoinCausa($c);
  	$this->causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
	$accion = $request->getParameter('accion');
  	$this->accion = $accion;
  }
  public function executeShowgarantia($request)
  {
    $this->garantia = GarantiaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->garantia);
  }

  public function executeCreategarantia($request)
  {
    $this->form = new GarantiaForm();
	$causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
	$this->causa = $causa;
	$this->causaId = $request->getParameter('idcausa');
	$this->nombreCausa = $this->causa->getCaratula();
    $this->setTemplate('editgarantia');
    $this->editar = false;
  }

  public function executeEditgarantia($request)
  {
    $this->form = new GarantiaForm(GarantiaPeer::retrieveByPk($request->getParameter('id')));
    $this->editar = true;
    $causa = CausaPeer::retrieveByPK($request->getParameter('idcausa'));
	$this->causa = $causa;
	$this->causaId = $request->getParameter('idcausa');
	$this->nombreCausa = $this->causa->getCaratula();
  }

  public function executeUpdategarantia($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->causa = CausaPeer::retrieveByPK($request->getParameter('causa_id'));
    $this->form = new GarantiaForm(GarantiaPeer::retrieveByPk($request->getParameter('id')));

    $datos = $request->getParameter('garantia');
    $this->editar = $request->getParameter('editar');
    $datos['causa_id'] = $request->getParameter('causa_id');
    
    $periodoCausaId = $this->causa->getPeriodoCausaActual()->getId();
    
    $diligencia = new Diligencia();
    $diligencia->setPeriodoCausaId($periodoCausaId);
    $diligencia->setFecha("now");
    
    
    $this->form->bind($datos);
    if ($this->form->isValid())
    {
      $this->garantia = $this->form->save();
    	if($this->editar == false){
	  	$diligencia->setDescripcion("Ingreso de garantia ".$this->garantia->getId());
      }else{
      	$diligencia->setDescripcion("Edici&oacute;n de garantia ".$this->garantia->getId());
      }
      $diligencia->save();
      $this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showgarantia');
      
    }else{
      $this->causaId = $request->getParameter('causa_id');
      $this->nombreCausa = $this->causa->getCaratula();
	  $this->setTemplate('editgarantia');
    }
  }

  public function executeDeletegarantia($request)
  {
    $this->forward404Unless($garantia = GarantiaPeer::retrieveByPk($request->getParameter('id')));

    $garantia->delete();

    $this->setTemplate('indexcausa');
  }
  
  public function executeTerminarcausa($request)
  {
  	$causa = CausaPeer::retrieveByPK($request->getParameter('id'));
  	
  	$estadoCausaActual = $causa->getEstadoCausaActivo();
  	$estadoCausaActual->setActivo(false);
  	$estadoCausaActual->save();
  	
  	$estadoCausaNuevo = new EstadoCausa();
  	$estadoCausaNuevo->setCausa($causa);
  	$estadoCausaNuevo->setEstadoId(2);
  	$estadoCausaNuevo->setFecha("now");
  	$estadoCausaNuevo->setMotivo('Se termin&oacute; de cancelar la deuda');
  	$estadoCausaNuevo->setActivo(true);
  	$estadoCausaNuevo->save();
  	
  	$this->forward('juicios','causassaldadas');
  }
  
  public function executeCausassaldadas()
  {
  	$causaList = CausaPeer::doSelect(new Criteria);
    $this->causasSaldadasList = array();
    $i = 0;
    foreach($causaList as $causa)
    {
    	if($causa->estaSaldada() && !$causa->estaTerminada())
    	{
    		$this->causasSaldadasList[$i] = $causa;
    		$i++;
    	}
    }
  }
}