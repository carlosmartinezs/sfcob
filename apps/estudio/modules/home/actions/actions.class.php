<?php

/**
 * home actions.
 *
 * @package    sgcj
 * @subpackage home
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 9301 2008-05-27 01:08:46Z dwhittle $
 */
class homeActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex()
  {
  	if($this->getUser()->isAuthenticated())
	{
		$perfilUsuario = $this->getUser()->getPerfil();
		
		if(!($perfilUsuario instanceof Perfil))
		{
			$user = sfGuardUserPeer::retrieveByPK($this->getUser()->getId());
			
			$perfilUser = new Perfil();
	    	$perfilUser->setSfGuardUserId($user->getId());
	    	$perfilUser->setPassword1($user->getPassword());
	    	$perfilUser->setUltimoCambio('now');
	    	$perfilUser->save();
		}
	}
	
  	$perfilUsuario = $this->getUser()->getPerfil();
  	
	if($perfilUsuario->debeCambiarPass($this->getUser()))
	{
		$this->getUser()->setFlash('cambioPass', true);
		$this->getUser()->setAttribute('caduca_pass',true);
		$this->forward('home','cambiocontrasena');
	}
	
  	if($this->getUser()->getFlash('cambioPass'))
  	{
  		$this->getUser()->setFlash('cambioPass', false);
  	}
  	
    if(!$perfilUsuario->tienePregunta())
	{
		$this->getUser()->setFlash('crearPregunta', true);
		
		$this->forward('home','preguntasecreta');
	}
	
  	if($this->getUser()->getFlash('crearPregunta'))
  	{
  		$this->getUser()->setFlash('crearPregunta', false);
  	}

  }
  
  public function executeRequiredcredential()
  {
  	$this->getResponse()->setStatusCode(403);
  }
  
  public function executePerfil()
  {
  	$this->perfil = $this->getUser()->getPerfil();
  }
  
  public function executeCambiocontrasena()
  {
  	if($this->getUser()->getFlash('cambioPass'))
  	{
  		$this->setLayout('cambioPass');
  	}
  	//$this->setLayout('cambioPass');
  	$this->form = new CambioPassForm();
  }
  
  public function executeCambiocontrasenaupdate($request)
  {
  	
  	$this->form = new CambioPassForm();

  	$this->form->bind($request->getParameter('cambiocontrasena'));
  	
  	if($this->form->isValid())
  	{
  		$datos = $request->getParameter('cambiocontrasena');
  		$c = new Criteria();
  		$usuarioId = $this->getUser()->getId();
	    $c->add(sfGuardUserPeer::ID, $usuarioId);
	    $usuarioBd = sfGuardUserPeer::doSelectOne($c);
	    
	    $this->idUserCambio = $usuarioId;
	    
	    if($usuarioBd instanceof sfGuardUser && $usuarioBd->checkPassword($datos['pass_actual']))
	    {
	    	if($usuarioBd->PassEsDistintaA3Ultimas($datos['pass_nva1'],$usuarioBd->getId()))
	    	{
	    		$c = new Criteria();
				$c->add(PerfilPeer::SF_GUARD_USER_ID,$usuarioBd->getId());
				$perfilCambio = PerfilPeer::doSelectOne($c);
				
				$perfilCambio->setPassword3($perfilCambio->getPassword2());
				$perfilCambio->setPassword2($perfilCambio->getPassword1());
				$usuarioBd->setPassword($datos['pass_nva1']);
				$usuarioBd->save();
				$perfilCambio->setPassword1($usuarioBd->getPassword());
				$perfilCambio->setUltimoCambio('now');
				$perfilCambio->save();
				
				$this->getUser()->setAttribute('cambio_pass_success',true);
	    		$this->redirect('@sf_guard_signout');
	    		
	    	}
	    	else
	    	{
	    		$this->noDistinctPass = true;
	    		$this->setLayout('cambioPass');
	    		$this->setTemplate('cambiocontrasena');
	    	}
	    }
	    else
	    {
	    	$this->incorrectPass = true;
	    	$this->setLayout('cambioPass');
	    	$this->setTemplate('cambiocontrasena');
	    }
  	}
  	else
  	{
  		$this->setLayout('cambioPass');
  		$this->setTemplate('cambiocontrasena');
  	}
  }
  
  public function executeVerdatos()
  {
  	$user = $this->getUser();
  	
  	if($user->isAcreedor())
  	{
  		$this->acreedor = $user->getAcreedor();
  		$this->setTemplate('showacreedor','administracion');
  		$this->pwd = $user->getPassword();
  		$this->perfil = true;
  	}
  	else
  	{
  		$this->forward('perfil','show');
  	}
  }
  public function executeEditardatos($request)
  {
    $user = $this->getUser();
  	
  	if($user->isAcreedor())
  	{
  		$this->forward('administracion','editacreedor');
  	}
  	else
  	{
  		$this->forward('perfil','edit');
  	}
  }
  
  public function executePreguntasecreta()
  {
  	if($this->getUser()->getFlash('crearPregunta'))
  	{
  		$this->setLayout('crearPregunta');
  	}
  
  	$this->form = new PreguntaSecretaForm();
  	$c = new Criteria();
  	$c->add(PerfilPeer::SF_GUARD_USER_ID,$this->getUser()->getId());
  	$perfil = PerfilPeer::doSelectOne($c);
  	
  	$this->form->setDefault('pregunta',$perfil->getPregunta());
  	$this->form->setDefault('respuesta',$perfil->getRespuesta());
  }
  
  public function executePreguntasecretaupdate($request){
	
  	$this->form = new PreguntaSecretaForm();

  	$this->form->bind($request->getParameter('preguntasecreta'));
  	
  	if($this->form->isValid())
  	{
  		$datos = $request->getParameter('preguntasecreta');
  		
  		$c = new Criteria();
  		$c->add(PerfilPeer::SF_GUARD_USER_ID,$this->getUser()->getId());
  		$perfil = PerfilPeer::doSelectOne($c);
  		
  		$perfil->setPregunta($datos['pregunta']);
  		$perfil->setRespuesta($datos['respuesta']);
  		
  		$perfil->save();
  	}
  	else
  	{
  		$this->setTemplate('preguntasecreta');
  	}
  }
  
  public function executePedirusername(){
  
  	$this->setLayout('login');
  	
  	$this->form = new PedirUsernameForm();
  	
  }
  
  public function executeRecuperarcontrasena($request){
  	
  	$this->setLayout('login');
  	
  	$this->form = new PedirUsernameForm();
  	
  	$this->form->bind($request->getParameter('pedirusername'));
  	
  	if($this->form->isValid())
  	{
  		$datos = $request->getParameter('pedirusername');
  		$username = $datos['username'];
  		
  		$c = new Criteria();
  		$c->add(sfGuardUserPeer::USERNAME,$username);
  		
  		$bdUser = sfGuardUserPeer::doSelectOne($c);
  		
  		if($bdUser instanceof sfGuardUser)
  		{
  			$userGuardId = $bdUser->getId();
  			
  			$c = new Criteria();
  			$c->add(PerfilPeer::SF_GUARD_USER_ID,$userGuardId);
  			
  			$perfil = PerfilPeer::doSelectOne($c);
  			
  			$this->perfilId = $perfil->getId();
  			
  			$this->form = new RecuperarContrasenaForm();
  			
  			$this->pregunta = $perfil->getPregunta();
  			
  			if($this->pregunta == '')
  			{
  				$this->setTemplate('nohaypregunta');
  			}
  			else
  			{
  				$this->getUser()->setAttribute('username',$username);
  			}
  		}
  		else
  		{
  			$this->error_usuario = true;
  			$this->setTemplate('pedirusername');
  		}
  	}
  	else
  	{
  		$this->setTemplate('pedirusername');
  	}
  }
  
  public function executeRecuperarcontrasenaupdate($request){
  	
  		$this->setLayout('login');
  		
  		$this->form = new RecuperarContrasenaForm();
  		
  		$this->form->bind($request->getParameter('recuperarcontrasena'));
  		
  		if($this->form->isValid())
  		{
  			$respuestaBD = PerfilPeer::retrieveByPK($request->getParameter('id'))->getRespuesta();
  			$this->respuestaBD = $respuestaBD;
  			$datos = $request->getParameter('recuperarcontrasena');
  			
  			$respuestaUsuario = $datos['respuesta']; 
  			
  			if($respuestaBD == $respuestaUsuario)
  			{
  				$perfil = PerfilPeer::retrieveByPK($request->getParameter('id'));
  				
  				/*$c = new Criteria();
  				$c->add(sfGuardUserPeer::USERNAME,$this->getUser()->getAttribute('username'));
  				$sfGuardUser = sfGuardUserPeer::doSelectOne($c);*/
  				$sfGuardUser = $perfil->getsfGuardUser();
  				$email = null;
  				
  				if( $listAcreedor = $sfGuardUser->getAcreedors() != null)
  				{
  					foreach($listAcreedor as $acreedor)
  					{
  						$email = $acreedor->getEmail();
  						$nombre = $acreedor->getNombreCompleto();
  					}
  				}
  				else
  				{
  					$email = $perfil->getEmail();
  					$nombre = $perfil->getNombreCompleto();
  				}
  				
  				$this->newPass = Funciones::generarRandomString(10);
  				
  				$sfGuardUser->setPassword($this->newPass);
  				$sfGuardUser->save();
  				
  				$perfil->setPassword3($perfil->getPassword2());
  				$perfil->setPassword2($perfil->getPassword1());
  				$perfil->setPassword1($sfGuardUser->getPassword());
  				$perfil->save();
  				
  				if($email != null || $email != '')
  				{
		  			$gmail = new GMail();
				  	
				  	$gmail->setFrom("no-reply@sfcob.cl");
				  	$gmail->setFromName("SfCob (No Reply)");
				  	$gmail->subject("Cambio de contraseņa: ".$this->getUser()->getAttribute('username'));
				  	$gmail->altBody("
				  	
				  	Bienvenido a SfCob:
				  	
				  	user: ".$this->getUser()->getAttribute('username')."
				  	pass: ".$this->newPass."
				  	
				  	");
				  	
				  	$gmail->msgHTML("
				  	
				  	Su nueva contrase&ntilde;a para SfCob:<br />
				  	<br />
				  	Usuario: ".$this->getUser()->getAttribute('username')."<br />
				  	Contrase&ntilde;a: ".$this->newPass."<br />
				  	<br />
				  	ingresa via : <a href=\"http://sgcj/estudio.php\">SfCob</a>
				  	
				  	");
				  	$gmail->AddAddress("carlos.f.martinez.s@gmail.com","Carlos");// reemplazar por linea de abajo
				  	//$gmail->AddAddress(".$email.",".$nombre.");
				  	
					$gmail->IsHTML(true);
					
					if($gmail->Send())
					{
						$this->gmailError = "Error: " . $gmail->getGMail()->ErrorInfo;
					}
					else
					{
						$this->gmailSuccess = "Mensaje enviado correctamente";
					}
					
					$this->enpantalla = false;
  				}
  				else
  				{
  					$this->enpantalla = true;
  				}
  				
  				if($this->getUser()->getFlash('crearPregunta'))
			  	{
			  		$this->getUser()->setFlash('crearPregunta', false);
			  	}
  				
				$this->getUser()->getAttributeHolder()->remove('username');
				
  			}
  			else
  			{
  				$this->error_respuesta = true;
  				$this->pregunta = PerfilPeer::retrieveByPK($request->getParameter('id'))->getPregunta();
  				$this->perfilId = $request->getParameter('id');
  				$this->setTemplate('recuperarcontrasena');
  			}
  		}
  		else
  		{
  			$this->pregunta = PerfilPeer::retrieveByPK($request->getParameter('id'))->getPregunta();
  			$this->perfilId = $request->getParameter('id');
  			$this->setTemplate('recuperarcontrasena');
  		}
  		
  		/*if($this->pregunta == '')
  		{
  			$this->setTemplate('nohaypregunta');
  		}*/
  }
}
