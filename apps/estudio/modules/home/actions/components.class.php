<?php

class homeComponents extends sfComponents
{
	public function executeAcreedoressincontrato()
    {
    	$this->acreedorList = AcreedorPeer::doSelect(new Criteria());
    }
    
	public function executeVencimientocontrato()
    {
    	$acreedor = $this->getUser()->getAcreedor();
    	
    	if($acreedor->tieneContratoActivo())
    	{
    		$this->contrato = $acreedor->getContratoActivo();
    	}
    	else
    	{
    		$this->contrato = $acreedor->getUltimoContrato();
    	}
    }
    
    public function executeCausassindiligencias()
    {
    	$this->causaList = CausaPeer::doSelect(new Criteria());
    }
    
    public function executeCausassindocumentosacreedor()
    {
    	$c = new Criteria();
    	$c->addJoin(AcreedorPeer::ID,ContratoPeer::ACREEDOR_ID);
    	$c->addJoin(ContratoPeer::ID,CausaPeer::CONTRATO_ID);
    	$c->add(AcreedorPeer::ID,$this->getUser()->getAcreedor()->getId());
    	$this->causaList = CausaPeer::doSelect($c);
    }
    
    public function executeCausassindocumentos()
    {
    	$this->causaList = CausaPeer::doSelect(new Criteria());
    }
    
    public function executeTareasproximasavencer()
    {
    	$c = new Criteria();
	  	$c->add(AgendaPeer::PERFIL_ID,$this->getUser()->getPerfil()->getId());
	  	$c->add(AgendaPeer::REALIZADA,false);	  	
	  	$c->addAnd(AgendaPeer::FECHA,date('Y-m-d H:i'),Criteria::GREATER_THAN);
	  	$c->addAnd(AgendaPeer::FECHA,Funciones::dateAdd(5).' '.date('H:i'),Criteria::LESS_EQUAL);
	  	$c->addAscendingOrderByColumn(AgendaPeer::FECHA);
	    $this->agendaList = AgendaPeer::doSelect($c);
    }
    
    public function executeContratosavencer()
    {    	
    	$c = new Criteria();
    	$c->add(ContratoPeer::FECHA_TERMINO,date('Y-m-d'),Criteria::GREATER_EQUAL);
    	$c->addAnd(ContratoPeer::FECHA_TERMINO,Funciones::dateAdd(45), Criteria::LESS_EQUAL);
    	$c->addAnd(ContratoPeer::ESTADO,true);
    	$c->addAscendingOrderByColumn(ContratoPeer::FECHA_TERMINO);
    	
		$this->contratoList = ContratoPeer::doSelect($c);
    }
    
    public function executeTareasvencidas()
    {
    	$c = new Criteria();
	  	$c->add(AgendaPeer::PERFIL_ID,$this->getUser()->getPerfil()->getId());
	  	$c->add(AgendaPeer::REALIZADA,false);
	  	$c->add(AgendaPeer::FECHA,date('Y-m-d H:i'),Criteria::LESS_EQUAL);
	  	$c->addAscendingOrderByColumn(AgendaPeer::FECHA);
	    $this->agendaList = AgendaPeer::doSelect($c);
    }
    
	public function executeCausassaldadas()
    {
    	$causaList = CausaPeer::doSelect(new Criteria);
    	$this->causasSaldadasList = array();
    	$i = 0;
    	foreach($causaList as $causa)
    	{
    		if($causa->estaSaldada() && !$causa->estaTerminada())
    		{
    			$this->causasSaldadasList[$i] = $causa;
    			$i++;
    		}
    	}
    }
    
    public function executeCausasterminadasultimomes()
    {
    	$c = new Criteria;
    	$c->addJoin(EstadoCausaPeer::CAUSA_ID,CausaPeer::ID);
    	$c->add(EstadoCausaPeer::ACTIVO,true);
    	$c->add(EstadoCausaPeer::ESTADO_ID,2);
    	$c->add(EstadoCausaPeer::FECHA,date('Y-m-d',strtotime('-30 days')),Criteria::GREATER_EQUAL);
    	$this->causasTerminadasList = CausaPeer::doSelect($c);
    	
    }
    
	public function executeCausasterminadasacreedor()
    {
    	$this->acreedor = $this->getUser()->getAcreedor();
    	$c = new Criteria;
    	$c->add(ContratoPeer::ACREEDOR_ID,$this->acreedor->getId());
    	$c->addJoin(CausaPeer::CONTRATO_ID,ContratoPeer::ID);
    	$c->addJoin(EstadoCausaPeer::CAUSA_ID,CausaPeer::ID);
    	$c->add(EstadoCausaPeer::ACTIVO,true);
    	$c->add(EstadoCausaPeer::ESTADO_ID,2);
    	
    	$this->causasTerminadasList = CausaPeer::doSelect($c);
    	
    }
    
    public function executeValores()
    {
    	$this->honorarios = 0;
    	$this->gastos = 0;
    	
    	$causaList = CausaPeer::doSelect(new Criteria());
    	
    	foreach($causaList as $causa)
    	{
    		$this->gastos += $causa->gastosDelMes();
    		$this->honorarios += $causa->honorariosDelMes();
    	}
    }
    
	public function executeCausassindiligenciasacreedor()
    {
    	$c = new Criteria();
    	$c->add(ContratoPeer::ACREEDOR_ID,$this->getUser()->getAcreedor()->getId());
    	$c->addJoin(ContratoPeer::ID,CausaPeer::CONTRATO_ID);
    	$this->causaList = CausaPeer::doSelect($c);
    }
}