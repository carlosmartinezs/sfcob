<table>
	<thead>
	  	<tr>
	    	<th colspan="3" class="tddestacado">Contratos a vencer dentro de las pr&oacute;ximas 6 semanas.</th>
	    </tr>
	    <tr>
	    	<th class="tddestacado">Fecha T&eacute;rmino</th>
	    	<th class="tddestacado">N&ordm; Interno</th>
	    	<th class="tddestacado">Acreedor</th>
	    </tr>
  	</thead>
  <tbody>
  	<?php if(count($contratoList) > 0):?>
	    <?php foreach ($contratoList as $contrato): ?>
		    <tr>
		    	<td class="tdcentrado"><?php echo Funciones::fecha2($contrato->getFechaTermino('d-m-Y'))?></td>
		      	<td class="tdcentrado"><?php echo $contrato->getId() ?></td>
		      	<td><?php echo $contrato->getAcreedor()?></td>
		    </tr>
	    <?php endforeach; ?>
	<?php else:?>
		<tr>
			<td colspan="3" class="aviso">
				NO SE ENCONTRARON CONTRATOS PR&Oacute;XIMOS A VENCER.
			</td>
		</tr>
	<?php endif;?>
  </tbody>
</table>