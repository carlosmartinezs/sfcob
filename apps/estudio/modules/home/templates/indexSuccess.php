<?php if(!($sf_user->hasCredential('administrador')) || ($sf_user->hasCredential('abogado')) ):?>
<h1 class="informacion">Notificaciones</h1>
<?php endif;?>
<?php if(($sf_user->hasCredential('abogado'))):?>
	<?php include_component('home', 'contratosavencer') ?>
	<div id="saldadas">
		<?php include_component('home', 'causassaldadas') ?>
	</div>
<?php endif;?>
<?php if(($sf_user->hasCredential('abogado')) || ($sf_user->hasCredential('secretaria')) || ($sf_user->hasCredential('procurador')) || ($sf_user->hasCredential('junior'))): ?>
	<?php include_component('home', 'causasterminadasultimomes') ?>
	<?php include_component('home', 'acreedoressincontrato') ?>
	<?php include_component('home', 'causassindocumentos') ?>
	<?php include_component('home', 'causassindiligencias') ?>
<?php else: ?>
	<?php if($sf_user->hasCredential('acreedor') && ($sf_user->getAcreedor()->getUltimoContrato() instanceof Contrato)):?>
			<?php include_component('home', 'vencimientocontrato') ?>
			<div id="causas-terminadas-acreedor">
			<?php include_component('home','causasterminadasacreedor')?>
			</div>
			<?php include_component('home','causassindocumentosacreedor') ?>
			<?php include_component('home','causassindiligenciasacreedor') ?>
	<?php elseif ($sf_user->hasCredential('acreedor')):?>
<table>
  <thead>
  	<tr>
      <th class="tddestacado">Sin Contrato</th>
    </tr>
  </thead>
  <tbody>
		<tr>
			<td class="aviso">
				UD. NO POSEE UN CONTRATO. CONT&Aacute;CTESE CON SU ABOGADO.
			</td>
		</tr>
  </tbody>
</table>
	<?php endif;?>
<?php endif; ?>
<h1 class="informacion">Tareas</h1>
<?php include_component('home', 'tareasproximasavencer') ?>





