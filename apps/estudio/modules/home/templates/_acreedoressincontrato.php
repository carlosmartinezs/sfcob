<div id="acredores-sin-contrato">
	<table>
		<thead>
			<tr><th colspan="3" class="tddestacado">Acreedores sin Contrato</th></tr>
			<tr>
				<th class="tddestacado">N&ordm; Interno</th>
				<th class="tddestacado">Rut</th>
				<th class="tddestacado">Nombre</th>
			</tr>
		</thead>
		<tbody>
		<?php $count = 0 ?>
			<?php foreach($acreedorList as $acreedor):?>
				<?php if(!$acreedor->tieneContratoActivo()):?>
				<?php $count++ ?>
					<tr>
						<td class="tdcentrado"><?php echo $acreedor->getId() ?></td>
						<td class="tdcentrado"><?php echo $acreedor->getRutAcreedor() ?></td>
						<td class="tdcentrado"><?php echo $acreedor ?></td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php if($count== 0):?>
		<tr>
			<td colspan="3" class="aviso">
				NO SE ENCONTRARON ACREEDORES SIN CONTRATO.
			</td>
		</tr>
		<?php endif;?>
		</tbody>
	</table>
</div>