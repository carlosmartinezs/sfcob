<div style="background:#EEEEEE;border:#1C384D solid 1px;padding:10px">
	<div class="aviso" style="margin: 10px;">
		<div>
		<?php if(!$enpantalla):?>
		<h3>Su nueva contrase&ntilde;a ha sido enviada a su correo.</h3>
		<?php else:?>
		<h3>Su nueva contrase&ntilde;a es: <?php echo $newPass ?></h3>
		<?php endif;?>
		Ahora vuelva a la p&aacute;gina de login e ingrese con su nueva contrase&ntilde;a. Le recomendamos
		cambiar la contrase&ntilde;a por una que Ud. recuerde y adem&aacute;s cambiar su pregunta y respuesta secretas.<br />
		<?php if($enpantalla):?>
		Por favor agregue su email a su perfil de usuario, para as&iacute; enviarle su contrase&ntilde;a a dicha direcci&oacute;n la pr&oacute;xima vez.
		<?php endif;?>
		</div>
	</div>
	<div style="margin:0 auto;">
		<?php echo link_to('Ir al Login','@sf_guard_signin',array('class'=>'login boton_con_imagen'))?>
	</div>
</div>
