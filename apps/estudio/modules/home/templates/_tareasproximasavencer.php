<table>
  <thead>
  	<tr>
      <th colspan="3" class="tddestacado">Tareas dentro de los pr&oacute;ximos 5 d&iacute;as</th>
    </tr>
    <tr>
    <th class="tddestacado">Fecha</th>
      <th class="tddestacado">Descripci&oacute;n</th>
      
      <th class="tddestacado">Realizada</th>
    </tr>
  </thead>
  <tbody>
  	<?php if(count($agendaList) > 0):?>
	    <?php foreach ($agendaList as $agenda): ?>
		    <tr>
		    <td class="tdcentrado"><?php
		    echo Funciones::fecha2($agenda->getFecha('d-m-Y'));
				    	echo $agenda->getFecha('\ \a\ \l\a\s H:i \h\r\s\.')?></td>
		      <td><?php echo $agenda->getDescripcion() ?></td>
		      
		      <td class="tdcentrado"><?php echo ($agenda->getRealizada() ? image_tag('accept.png',array('size'=>'16x16')) : image_tag('delete.png',array('size'=>'16x16'))) ?></td>
		    </tr>
	    <?php endforeach; ?>
	<?php else:?>
		<tr>
			<td colspan="3" class="aviso">
				NO SE ENCONTRARON TAREAS PARA LOS PR&Oacute;XIMOS 5 D&Iacute;AS.
			</td>
		</tr>
	<?php endif;?>
  </tbody>
</table>