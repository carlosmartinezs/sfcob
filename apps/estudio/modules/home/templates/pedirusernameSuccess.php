<h2>Recuperar Contrase&ntilde;a :</h2>
<form action="<?php echo url_for('home/recuperarcontrasena') ?>" method="post">
  <table>
  	<tr>
  		<td colspan="2">
			<h4 class="informacion">Para generar su nueva contrase&ntilde;a, ingrese su "usuario", luego se le pedir&aacute; responder a su pregunta secreta.</h4>
		</td>
	</tr>
  	<?php if(isset($error_usuario)):?>
	  	<tr>
	  		<td colspan="2">
				<ul class="error_list"><li>Usuario ingresado no existe.</li></ul>
			</td>
		</tr>
	<?php endif;?>
    <?php echo $form ?>
    <tr>
      <td class="tdcentrado" colspan="2">
        <input type="submit" class="aceptar boton_con_imagen" value="Aceptar"/>
      	<?php echo button_to('Cancelar','@homepage',array('class'=>'cancelar boton_con_imagen'))?>
      </td>
    </tr>
  </table>
</form>
