<div id="causas-sin-diligencias">
	<table>
		<thead>
			<tr><th colspan="4" class="tddestacado">Causas sin diligencias hace m&aacute;s de un mes</th></tr>
			<tr>
				<th class="tddestacado">Fecha &uacute;ltima Diligencia</th>
				<th class="tddestacado">N&ordm;</th>
				<th class="tddestacado">Rol</th>
				<th class="tddestacado">Descripci&oacute;n</th>
			</tr>
		</thead>
		<tbody>
			<?php $numCausasSinDil = 0?>
			<?php foreach($causaList as $causa):?>
				<?php if($causa->masDeUnMesSinDiligencias()):?>
				<?php $numCausasSinDil++?>
				<tr>
				<td class="tdcentrado">
						<?php echo ((count($causa->getDiligencias()) > 0) ? Funciones::fecha2($causa->getUltimaDiligencia()->getFecha('d-m-Y')): Funciones::fecha2($causa->getFechaInicio('d-m-Y'))) ?>
					</td>
					<td class="tdcentrado">
						<?php echo $causa->getId()?>
					</td>
					<td class="tdcentrado">
						<?php echo $causa->getRol()?>
					</td>
					
					<td class="tdcentrado">
						<?php echo ((count($causa->getDiligencias()) > 0) ? $causa->getUltimaDiligencia()->getDescripcion(): 'Inicio de Causa') ?>
					</td>
				</tr>
				<?php endif; ?>
			<?php endforeach; ?>
			<?php if($numCausasSinDil < 1):?>
				<tr>
					<td class="aviso" colspan="4">NO SE ENCONTRARON CAUSAS SIN DILIGENCIAS HACE M&Aacute;S DE UN MES</td>
				</tr>
			<?php endif;?>
		</tbody>
	</table>
</div>