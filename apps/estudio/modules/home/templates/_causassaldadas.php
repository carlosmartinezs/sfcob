<?php use_helper('Javascript', 'Object'); ?>
<?php if(count($causasSaldadasList) > 0):?>
<?php $count = count($causasSaldadasList)?>
		<table>
		  <thead>
		  	<tr>
		      <th colspan="6" class="tddestacado">Causas con deuda saldada</th>
		    </tr>
		    <tr>
		      <th class="tddestacado">N&ordm;</th>
		      <th class="tddestacado">Tipo</th>
		      <th class="tddestacado">Rol</th>
		      <th class="tddestacado">Car&aacute;tula</th>
		      <th class="tddestacado">Inicio</th>
		      <th class="tddestacado">Acci&oacute;n</th>
		    </tr>
		  </thead>
		  <tbody>
		  <?php foreach($causasSaldadasList as $causa):?>
		    <tr>
		  	  <td><?php echo $causa->getId() ?></td>
		      <td><?php echo $causa->getTipoCausa() ?></td>
		      <td><?php echo $causa->getRol() ?></td>
		      <td><?php echo $causa->getCaratula() ?></td>
		      <td><?php echo Funciones::fecha2($causa->getFechaInicio('d-m-Y')) ?></td>
		      <td class="tdcentrado">
		      	<?php 
			      	if($count == 1):
			      		echo button_to_remote('Terminar',
			      			array(
			      				'update'=>'saldadas',
			      				'url'=>'juicios/terminarcausa?id='.$causa->getId(),
			      				'confirm'=>'Al aceptar, estar&aacute; dando termino al proceso de esta causa. Esta acci&oacute;n es irreversible. Desea continuar?',
			      				'loading'=>visual_effect('appear','loading'),
			      				'complete'=>visual_effect('fade','loading').
			      							visual_effect('fade','saldadas'),
			      			),array('class'=>'aceptar boton_con_imagen')
			      		);
			      	else:
			      		echo button_to_remote('Terminar',
			      			array(
			      				'update'=>'saldadas',
			      				'url'=>'juicios/terminarcausa?id='.$causa->getId(),
			      				'confirm'=>'Al aceptar, estar&aacute; dando termino al proceso de esta causa. Esta acci&oacute;n es irreversible. Desea continuar?',
			      				'loading'=>visual_effect('appear','loading'),
			      				'complete'=>visual_effect('fade','loading'),
			      			),array('class'=>'aceptar boton_con_imagen')
			      		);
			      	endif;
		      	?>
		      </td>
		    </tr>
		    <?php endforeach;?>
		  </tbody>
		</table>
<?php endif;?>