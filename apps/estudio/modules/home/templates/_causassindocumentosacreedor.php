<?php use_helper('Javascript') ?>
<div id="causas-sin-documentos">
	<table>
		<thead>
			<tr><th colspan="3" class="tddestacado">Causas sin documentos</th></tr>
			<tr>
				<th class="tddestacado">N&ordm; Interno</th>
				<th class="tddestacado">Rol</th>
				<th class="tddestacado">Car&aacute;tula</th>
			</tr>
		</thead>
		<tbody>
			<?php $numCausas = 0;?>
			<?php foreach($causaList as $causa):?>
				
				<?php if(count($causa->getDocumentos()) <= 0):?>
				<?php $numCausas++;?>
					<tr>
						<td class="tdcentrado">
							<?php echo $causa->getId() ?>
						</td>
						<td class="tdcentrado">
							<?php echo $causa->getRol() ?>
						</td>
						<td>
							<?php echo $causa ?>
						</td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
			<?php if($numCausas < 1):?>
				<tr>
					<td colspan="3" class="aviso">NO SE ENCONTRARON CAUSAS SIN DOCUMENTOS</td>
				</tr>
			<?php endif;?>
		</tbody>
	</table>
</div>