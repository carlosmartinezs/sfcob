<?php use_helper('Javascript', 'Object'); ?>
<?php if(count($agendaList) > 0):?>
<?php $count = count($agendaList)?>
	<div id="vencidas" class="pop-up">
	<h2>Tareas vencidas</h2>
		<table>
		  <thead>
		    <tr>
		    	<th class="tddestacado">Fecha</th>
		      	<th class="tddestacado">Descripci&oacute;n</th>
		      	<th class="tddestacado">Realizada</th>
		     	 <th class="tddestacado">Acci&oacute;n</th>
		    </tr>
		  </thead>
		  <tbody>
			    <?php foreach ($agendaList as $agenda): ?>
				    <tr>
				    	<td class="tdcentrado"><?php 
				    	
				    	echo Funciones::fecha2($agenda->getFecha('d-m-Y'));
				    	echo $agenda->getFecha('\ \a\ \l\a\s H:i').' hrs.';?></td>
				      	<td><?php echo $agenda->getDescripcion() ?></td>
				      	<td class="tdcentrado"><?php echo ($agenda->getRealizada() ? image_tag('accept.png',array('size'=>'16x16')) : image_tag('delete.png',array('size'=>'16x16'))) ?></td>
				    	<td class="tdcentrado">
				    		<?php
				    			
				    			if($count == 1)
				    			{
				    				echo button_to_remote('Realizada', array(
										'update' => 'vencidas',
										'url' => 'agenda/realizada?id='.$agenda->getId().'&accion=tareasvencidas',
										'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
										'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)).
				    									visual_effect('fade','vencidas', array('duration' => 0.5)),
										'confirm' => 'La tarea se marcar&aacute; como \'Realizada\', esta acci&oacute;n es irreversible. Desea continuar?',
										),array('class' => 'aceptar boton_con_imagen'));?>&nbsp;<?php 
									echo button_to_remote('Postergar', array(
										'update' => 'vencidas',
										'url' => 'agenda/edit?id='.$agenda->getId().'&accion=tareasvencidas&count=1',
										'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
										'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
										),array('class' => 'editar boton_con_imagen'));
				    			}
				    			else
				    			{
				    				echo button_to_remote('Realizada', array(
										'update' => 'vencidas',
										'url' => 'agenda/realizada?id='.$agenda->getId().'&accion=tareasvencidas',
										'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
										'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
										'confirm' => 'La tarea se marcar&aacute; como \'Realizada\', esta acci&oacute;n es irreversible. Desea continuar?',
										),array('class' => 'aceptar boton_con_imagen'));?>&nbsp;<?php 
									echo button_to_remote('Postergar', array(
										'update' => 'vencidas',
										'url' => 'agenda/edit?id='.$agenda->getId().'&accion=tareasvencidas',
										'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
										'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
										),array('class' => 'editar boton_con_imagen'));
								}
								
								
				    			
				    		?>
				    	</td>
				    </tr>
			    <?php endforeach; ?>
		  </tbody>
		</table>
	</div>
<?php endif;?>