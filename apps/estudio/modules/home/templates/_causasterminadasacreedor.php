<?php use_helper('Javascript', 'Object'); ?>
		<table>
		  <thead>
		  	<tr>
		      <th colspan="7" class="tddestacado">Causas terminadas de <?php echo $acreedor ?></th>
		    </tr>
		    <tr>
		      <th class="tddestacado">N&ordm;</th>
		      <th class="tddestacado">Tipo</th>
		      <th class="tddestacado">Rol</th>
		      <th class="tddestacado">Car&aacute;tula</th>
		      <th class="tddestacado">Inicio</th>
		      <th class="tddestacado">T&eacute;rmino</th>
		      <th class="tddestacado">Acci&oacute;n</th>
		    </tr>
		  </thead>
		  <tbody>
		  <?php if(count($causasTerminadasList) > 0):?>
		  <?php foreach($causasTerminadasList as $causa):?>
		    <tr>
		  	  <td><?php echo $causa->getId() ?></td>
		      <td><?php echo $causa->getTipoCausa() ?></td>
		      <td><?php echo $causa->getRol() ?></td>
		      <td><?php echo $causa->getCaratula() ?></td>
		      <td><?php echo Funciones::fecha2($causa->getFechaInicio('d-m-Y')) ?></td>
		      <td><?php echo Funciones::fecha2($causa->getEstadoCausaActivo()->getFecha('d-m-Y')) ?></td>
		      <td><?php echo button_to_function('Detalles',"Effect.toggle('detalle-causa','slide');var elemento = $(this);if(elemento.value=='Detalles'){elemento.value='Ocultar'}else{elemento.value='Detalles'};",
		      			array('class'=>'detalles boton_con_imagen')) ?></td>
		    </tr>
		    <tr>
		    	<td colspan="7" style="margin:0;padding:0;">
		    		<div id="detalle-causa" style="display:none;">
		    			<div>
					    	<table style="margin:0; width:100%;padding:0;">
							  <tbody>
							  	<tr>
							    	<th class="tdcentrado" colspan="9" style="border:0;background:#1C384D;color:white;">Detalles</th>
								</tr>
							    <tr>
							      <th>Inicio:</th>
							      <td colspan="2"><?php echo Funciones::fecha2($causa->getFechaInicio('d-m-Y')) ?></td>
							      <th>T&eacute;rmino:</th>
							      <td colspan="2"><?php echo $causa->getFechaTermino('d-m-Y') ? Funciones::fecha2($causa->getFechaTermino('d-m-Y')) : 'No estipulada.';  ?></td>
							    </tr>
							    
							    <tr>
							      <th>Car&aacute;tula</th>
							      <td colspan="5"><?php echo $causa->getCaratula()?></td>
							    </tr>
							    <tr>
							      <th>Honorarios:</th>
							      <td colspan="2">$ <?php echo $causa->getDeuda()->getHonorarios() ?></td>
							      <th>Gastos:</th>
							      <td colspan="2">$ <?php echo $causa->getGastos()?></td>
							    </tr>
								<tr>
								  <th>Periodo Actual:</th>
							      <td colspan="5"><?php echo $causa->getPeriodoCausaActual()->getPeriodo() ?></td>
								</tr>
								
							    <tr>
							      <th>Rol:</th>
							      <td colspan="2"><?php echo $causa->getRol() ?></td>
							      <th>Tipo:</th>
							      <td colspan="2"><?php echo $causa->getTipoCausa() ?></td>
							    </tr>
							    
							    <tr>
							      <th>Herencia Exhorto:</th>
							      <td colspan="2"><?php echo ($causa->getHerenciaExhorto() ? 'Si' : 'No')?></td>
							      <th>Causa Base:</th>
							      <td colspan="2"><?php echo ($causa->getCausaExhorto() ? $causa->getCausaExhorto() : '-') ?></td>
							    </tr>
							    
							    <tr>
							      <th>Acreedor:</th>
							      <td colspan="5"><?php echo $causa->getAcreedor() ?></td>
							    </tr>
							    
							    <tr>
							      <th>Deudor:</th>
							      <td colspan="5"><?php echo $causa->getNombreDeudor() ?></td>
							    </tr>
							    
							    <tr>
							      <th>Estado Actual:</th>
							      <td colspan="2"><?php echo $causa->getEstadoCausaActivo()->getEstado() ?></td>
							      <th>Tribunal:</th>
							      <td colspan="2"><?php echo $causa->getTribunalActual() ?></td>
							    </tr>
							    
							    <tr>
							      
							      <th>Materia:</th>
							      <td colspan="2"><?php echo $causa->getNombreMateria() ?></td>
							      <th>Competencia:</th>
							      <td colspan="2"><?php echo $causa->getCompetencia() ?></td>
							    </tr>
							    <tr>
							      <td colspan="6">
							        <table style="border:1px solid #1C384D; margin: 5px auto; width: 100%;padding:0;">
							          	<tr>
							    			<th class="tdcentrado" colspan="9" style="border:0;background:#1C384D;color:white;">Saldos</th>
									    </tr>
									    <tr>
									    	<th class="tdcentrado" colspan="3">Cuant&iacute;a</th>
									    	<th class="tdcentrado" colspan="3">Abonos</th>
									    	<th class="tdcentrado" colspan="3">Pendiente</th>
									    </tr>
									    <tr>
									    	<td colspan="3">$ <?php echo $causa->getCuantia()?></td>
									    	<td colspan="3">$ <?php echo $causa->getTotalAbono()?></td>
									    	<td colspan="3">$ <?php echo $causa->getDeudaPendiente()?></td>
									    </tr>
									    <tr>
									    	<th class="tdcentrado">Monto</th>
									    	<th class="tdcentrado">Inter&eacute;s</th>
									    	<th class="tdcentrado">Gastos</th>
									    	<th class="tdcentrado">Monto</th>
									    	<th class="tdcentrado">Inter&eacute;s</th>
									    	<th class="tdcentrado">Gastos</th>
									    	<th class="tdcentrado">Monto</th>
									    	<th class="tdcentrado">Inter&eacute;s</th>
									    	<th class="tdcentrado">Gastos</th>
									    </tr>
									    <tr>
									    	<td>$ <?php echo $causa->getDeudaMonto()?></td>
									    	<td>$ <?php echo $causa->getDeudaInteres()?></td>
									    	<td>$ <?php echo $causa->getDeudaGastosCobranza()?></td>
									    	<td>$ <?php echo $causa->getAbonoMonto()?></td>
									    	<td>$ <?php echo $causa->getAbonoInteres()?></td>
									    	<td>$ <?php echo $causa->getAbonoGastosCobranza()?></td>
									    	<td>$ <?php echo $causa->getDeudaPendienteMonto()?></td>
									    	<td>$ <?php echo $causa->getDeudaPendienteInteres()?></td>
									    	<td>$ <?php echo $causa->getDeudaPendienteGastosCobranza()?></td>
									    </tr>
							        </table>
							      </td>
							    </tr>
							  </tbody>
							</table>
		    			</div>
		    		</div>
		    	</td>
		    </tr>
		    <?php endforeach;?>
		    <?php else:?>
		    <tr>
		    	<td colspan="7" class="aviso">
		    		NO SE ECONTRARON CAUSAS TERMINADAS EN EL &Uacute;LTIMO MES
		    	</td>
		    </tr>
		    <?php endif;?>
		  </tbody>
		</table>