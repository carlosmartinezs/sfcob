<h2>Recuperar Contrase&ntilde;a :</h2>
<form action="<?php echo url_for('home/recuperarcontrasenaupdate?id='.$perfilId) ?>" method="post">
  <table>
  <tr>
  		<td colspan="2">
			<h4 class="informacion">Responda su pregunta secreta y se generar&aacute; su nueva contrase&ntilde;a.</h4>
		</td>
	</tr>
  <?php if(isset($error_respuesta)):?>
  	<tr><td colspan="2"><ul class="error_list"><li>Respuesta incorrecta.</li></ul></td></tr>
	<?php endif;?>
  <tr>
  	<th>Su Pregunta Secreta :</th>
  	<td><?php echo $pregunta?></td>
  </tr>
    <?php echo $form ?>
    <tr>
      <td colspan="2" class="tdcentrado">
        <input type="submit" class="aceptar boton_con_imagen" value="Aceptar"/>
        <?php echo button_to('Cancelar','@sf_guard_signin',array('class'=>'cancelar boton_con_imagen'))?>
      </td>
    </tr>
  </table>
</form>