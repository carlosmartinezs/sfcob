<div style="background:#EEEEEE;border:#1C384D solid 1px;padding:10px">
	<div class="aviso" style="margin: 10px;padding:10px 10px 10px 25px;">
		<div style="text-align:justify;">
		Lamentablemente no podemos restablecer su contrase&ntilde;a autom&aacute;ticamente en estos momentos,
		ya que su perfil de usuario no cuenta con una pregunta secreta, la cual es necesaria para este
		procedimiento. Favor de comunicarse con el administrador del sistema para restablecer su nueva
		contrase&ntilde;a de forma manual.<br /><br />
		Solicitamos, en cuanto tenga acceso nuevamente, crear su pregunta secreta en la
		secci&oacute;n <u>Perfil</u>.
		</div>
	</div>
	<div style="margin:0 auto;">
		<?php echo link_to('Ir al Login','@sf_guard_signin',array('class'=>'login boton_con_imagen'))?>
	</div>
</div>