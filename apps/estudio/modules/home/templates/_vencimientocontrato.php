<div id="vencimiento-contrato">
	<table>
		<thead>
			<tr><th colspan="4" class="tddestacado">Contrato Reciente</th></tr>
			<tr>
				<th class="tddestacado">N&ordm;</th>
				<th class="tddestacado">Fecha Inicio</th>
				<th class="tddestacado">Fecha T&eacute;rmino</th>
				<th class="tddestacado">Estado</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?php echo $contrato->getId() ?></td>
				<td><?php echo Funciones::fecha2($contrato->getFechaInicio('d-m-Y')) ?></td>
				<td><?php echo Funciones::fecha2($contrato->getFechaTermino('d-m-Y')) ?></td>
				<td><?php echo ($contrato->getEstado() ? 'Activo' : 'Terminado') ?></td>
			</tr>
		</tbody>
	</table>
</div>