<?php use_helper('Javascript') ?>
<h2>Cambio de Contrase&ntilde;a</h2>
<form action="<?php echo url_for('home/cambiocontrasenaupdate') ?>" method="post" onsubmit="if (confirm('Se cambiar&aacute; su antigua contrase&ntilde;a. Si el cambio se realiza con exito, deber&aacute; ingresar al sistema nuevamente. Desea continuar?')){return true;};return false;">
<?php /* echo form_remote_tag(array(
    'update'   	=> 'acciones_perfil',
    'url'      	=> 'home/cambiocontrasenaupdate',
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  	=> "Se cambiar&aacute; su antigua contrase&ntilde;a. Si el cambio se realiza con exito, deber&aacute; ingresar al sistema nuevamente. Desea continuar?",
))*/?>
  <table>
  	<?php if(isset($incorrectPass) && $incorrectPass):?>
		<tr>
			<td colspan="2" style="padding: 0;">
				<ul class="error_list">
	  				<li>Contrase&ntilde;a Actual Incorrecta</li>
	  			</ul>
			</td>
		</tr>
  	<?php endif;?>
  	<?php if(isset($noDistinctPass) && $noDistinctPass):?>
		<tr>
			<td colspan="2" style="padding: 0;">
				<ul class="error_list">
	  				<li>La contrase&ntilde;a nueva debe ser distinta a las 3 (tres) &uacute;ltimas.</li>
	  			</ul>
			</td>
		</tr>
  	<?php endif;?>
  	<?php if(!$sf_user->getFlash('cambioPass') || !isset($noDistinctPass)):?>
	<tr><td colspan="2" class="informacion" style="padding-left: 40px;">Recuerde que su nueva contrase&ntilde;a debe ser distinta a las 3(tres) &uacute;ltimas.</td></tr>
	<?php endif;?>
    <?php echo $form ?>
    <tr>
      <td class="tdcentrado" colspan="2">
       <input type="submit" class="cambiar boton_con_imagen" value="Cambiar"/>
 </td>
    </tr>
  </table>
</form>