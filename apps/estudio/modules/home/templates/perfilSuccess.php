<?php use_helper('Javascript') ?>
<h1>Perfil de <?php echo $sf_user->getUsername()?></h1>
<ul class="menu-modulo">
	<li>
		<?php echo link_to_remote('Ver Datos', array(
    		'update' => 'acciones_perfil',
    		'url'    => 'home/verdatos',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
	<?php if($sf_user->hasCredential('administrador') || $sf_user->hasCredential('abogado') || $sf_user->hasCredential('secretaria') || $sf_user->hasCredential('procurador')):?>
	<li>
		<?php echo link_to_remote('Editar Datos', array(
    		'update' => 'acciones_perfil',
    		'url'    => 'home/editardatos',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
	<?php endif; ?>
	<li>
	<?php //echo link_to('Cambiar Contrase&ntilde;a','home/cambiocontrasena');?>
		<?php echo link_to_remote('Cambiar Contrase&ntilde;a', array(
    		'update' => 'acciones_perfil',
    		'url'    => 'home/cambiocontrasena',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
	<li><?php echo link_to_remote('Pregunta Secreta', array(
    		'update' => 'acciones_perfil',
    		'url'    => 'home/preguntasecreta',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
</ul>
<div id="acciones_perfil" style="clear: right">
<?php if(!$sf_user->hasCredential('acreedor')):?>
<h2>Sus Datos</h2>
<table>
  <tbody>
    <tr>
      <th>Ultimo cambio de contrase&ntilde;a:</th>
      <td colspan="5"><?php echo Funciones::fecha2($perfil->getUltimoCambio('d-m-Y')) ?></td>
    </tr>
    <tr>
      <th>Nombre:</th>
      <td colspan="5"><?php echo $perfil->getNombreCompleto() ?></td>
    </tr>
    <tr>
      <th>Direcci&oacute;n:</th>
      <td colspan="5"><?php echo $perfil->getDireccion() ?></td>
    </tr>
    <tr>
      <th>Comuna:</th>
      <td><?php echo $perfil->getComuna() ?></td>
      <th>Provincia:</th>
      <td><?php echo ($perfil->getComuna() ? $perfil->getComuna()->getCiudad() : '')?></td>
      <th>Regi&oacute;n:</th>
      <td><?php echo ($perfil->getComuna() ? $perfil->getComuna()->getCiudad()->getRegion() : '') ?></td>
    </tr>
    <tr>
      <th>Telefono:</th>
      <td><?php echo $perfil->getTelefono() ?></td>
      <th>Fax:</th>
      <td colspan="3"><?php echo $perfil->getFax() ?></td>
    </tr>
    <tr>
      <th>Email:</th>
      <td colspan="5"><?php echo $perfil->getEmail() ?></td>
    </tr>
  </tbody>
</table>
<?php endif;?>
</div>