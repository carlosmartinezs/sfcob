<?php use_helper('Javascript') ?>
<h2>Pregunta Secreta</h2>
<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_perfil',
    'url'      	=> 'home/preguntasecretaupdate',
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  	=> "Se cambiar&aacute; su antigua pregunta secreta. Recuerde que &eacute;sta se pedir&aacute; cuando no recuerde su contrase&ntilde;a. Desea continuar?",
))?>
  <table>
    <?php echo $form ?>
    <tr>
      <td class="tdcentrado" colspan="2">
        <input type="submit" class="guardar boton_con_imagen" value="Guardar"/>
      </td>
    </tr>
  </table>
</form>