<?php use_helper('Javascript') ?>
<h1>Mantenci&oacute;n de Tipos de Pagos</h1>
<ul class="menu-modulo">
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('administrador')):?>
	<li>
		<?php echo link_to_remote('Ingresar Tipo de Pago', array(
    		'update' => 'acciones_tipo_pago',
    		'url'    => 'mantencion/createtipopago',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('administrador')):?>
	<li>
		<?php echo link_to_remote('Editar Tipo de Pago', array(
    		'update' => 'acciones_tipo_pago',
    		'url'    => 'mantencion/listtipopago?accion=edit',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('administrador')):?>
	<li>
		<?php echo link_to_remote('Visualizar Tipo de Pago', array(
    		'update' => 'acciones_tipo_pago',
    		'url'    => 'mantencion/listtipopago?accion=show',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_tipo_pago" style="clear: right"></div>