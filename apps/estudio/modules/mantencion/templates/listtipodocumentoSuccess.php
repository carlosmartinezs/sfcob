<?php use_helper('Javascript') ?>
<h2>Lista de Tipos de Documento</h2>

<table>
  <thead>
    <tr>
      <th class="tddestacado">Nombre</th>
      <th class="tddestacado">Descripci&oacute;n</th>
      <?php if ($accion == 'edit'): ?>
      <th class="tddestacado">Acci&oacute;n</th>
      <?php endif; ?>
    </tr>
  </thead>
  <tbody>
  	<?php if ($accion == 'edit'): ?>
    <?php foreach ($tipo_documentoList as $tipo_documento): ?>
    <tr>
      <td><?php echo $tipo_documento->getNombre() ?></td>
      <td><?php echo $tipo_documento->getDescripcion() ?></td>
      <td><?php echo button_to_remote('Editar', array(
			'update' => 'acciones_tipo_documento',
			'url' => 'mantencion/edittipodocumento?id='.$tipo_documento->getId(),
			'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
			),array('class' => 'editar boton_con_imagen'));?> 
     </td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
    <?php if ($accion == 'show'): ?>
    <?php foreach ($tipo_documentoList as $tipo_documento): ?>
    <tr>
      <td><?php echo $tipo_documento->getNombre() ?></td>
      <td><?php echo $tipo_documento->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
  </tbody>
</table>
