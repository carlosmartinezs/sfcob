<?php use_helper('Javascript') ?>
<h1>Mantenci&oacute;n de Tipos de Documentos</h1>
<ul class="menu-modulo">
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('administrador')):?>
	<li>
		<?php echo link_to_remote('Ingresar Tipo de Documento', array(
    		'update' => 'acciones_tipo_documento',
    		'url'    => 'mantencion/createtipodocumento',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('administrador')):?>
	<li>
		<?php echo link_to_remote('Editar Tipo de Documento', array(
    		'update' => 'acciones_tipo_documento',
    		'url'    => 'mantencion/listtipodocumento?accion=edit',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('administrador')):?>
	<li>
		<?php echo link_to_remote('Visualizar Tipo de Documento', array(
    		'update' => 'acciones_tipo_documento',
    		'url'    => 'mantencion/listtipodocumento?accion=show',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_tipo_documento" style="clear: right"></div>