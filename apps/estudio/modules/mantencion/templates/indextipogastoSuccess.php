<?php use_helper('Javascript') ?>
<h1>Mantenci&oacute;n de Tipos de Gastos</h1>
<ul class="menu-modulo">
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('administrador')):?>
	<li>
		<?php echo link_to_remote('Ingresar Tipo de Gasto', array(
    		'update' => 'acciones_tipo_gasto',
    		'url'    => 'mantencion/createtipogasto',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('administrador')):?>
	<li>
		<?php echo link_to_remote('Editar Tipo de Gasto', array(
    		'update' => 'acciones_tipo_gasto',
    		'url'    => 'mantencion/listtipogasto?accion=edit',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
	<?php if ($sf_user->hasCredential('abogado') || $sf_user->hasCredential('administrador')):?>
	<li>
		<?php echo link_to_remote('Visualizar Tipo de Gasto', array(
    		'update' => 'acciones_tipo_gasto',
    		'url'    => 'mantencion/listtipogasto?accion=show',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
</ul>
<div id="acciones_tipo_gasto" style="clear: right"></div>