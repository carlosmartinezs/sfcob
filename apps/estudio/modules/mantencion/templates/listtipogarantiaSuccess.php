<?php use_helper('Javascript') ?>
<h2>Lista de Tipos de Garant&iacute;a</h2>

<table>
  <thead>
    <tr>
      <th class="tddestacado">Nombre</th>
      <th class="tddestacado">Descripci&oacute;n</th>
      <th class="tddestacado">Acci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
  	<?php if ($accion == 'edit'): ?>
    <?php foreach ($tipo_garantiaList as $tipo_garantia): ?>
    <tr>
      <td><?php echo $tipo_garantia->getNombre() ?></td>
      <td><?php echo $tipo_garantia->getDescripcion() ?></td>
      <td><?php echo button_to_remote('Editar', array(
			'update' => 'acciones_tipo_garantia',
			'url' => 'mantencion/edittipogarantia?id='.$tipo_garantia->getId(),
			'loading' => visual_effect('appear','loading', array('duration' => 0.5)),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
			),array('class' => 'editar boton_con_imagen'));?> 
     </td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
    <?php if ($accion == 'show'): ?>
    <?php foreach ($tipo_garantiaList as $tipo_garantia): ?>
    <tr>
      <td><?php echo $tipo_garantia->getNombre() ?></td>
      <td><?php echo $tipo_garantia->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
  </tbody>
</table>
