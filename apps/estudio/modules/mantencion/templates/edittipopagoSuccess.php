<?php use_helper('Javascript') ?>
<?php $tipo_pago = $form->getObject() ?>
<h2><?php echo $tipo_pago->isNew() ? 'Ingresar' : 'Editar' ?> Tipo de Pago</h2>

<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_tipo_pago',
    'url'      	=> 'mantencion/updatetipopago'.(!$tipo_pago->isNew() ? '?id='.$tipo_pago->getId() : ''),
	'loading'	=> visual_effect('appear','loading', array('duration' => 0.5)),
	'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado"colspan="2">
        <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
        
         <?php echo button_to_remote('Cancelar', array(
			'update' => 'modulo',
			'url' => 'mantencion/indextipopago',
			'loading' => visual_effect('appear','loading',array('duration' => 0.5) ),
			'complete'	=> visual_effect('fade','loading', array('duration' => 0.5)),
			),array('class'=>'cancelar boton_con_imagen'))?>
          </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="tipo_pago_nombre">Nombre *</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="tipo_pago_descripcion">Descripci&oacute;n *</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
  	 <tr>
  	  <td>
      	<p>Los campos marcados con <strong>*</strong> son <strong>obligatorios</strong></p>
      </td>
    </tr>
  </table>
</form>
