<?php

/**
 * mantencion actions.
 *
 * @package    sgcj
 * @subpackage mantencion
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 9301 2008-05-27 01:08:46Z dwhittle $
 */
class mantencionActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex($request)
  {
  }
  
  public function executeIndextipogasto()
  {
  }

  public function executeListtipogasto($request)
  {
  	$this->tipo_gastoList = TipoGastoPeer::doSelect(new Criteria());
  	$accion = $request->getParameter('accion');
  	$this->accion = $accion;
  }
  
  public function executeShowtipogasto($request)
  {
    $this->tipo_gasto = TipoGastoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tipo_gasto);
  }

  public function executeCreatetipogasto()
  {
    $this->form = new TipoGastoForm();
    $this->setTemplate('edittipogasto');
  }

  public function executeEdittipogasto($request)
  {
    $this->form = new TipoGastoForm(TipoGastoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdatetipogasto($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TipoGastoForm(TipoGastoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('tipo_gasto'));
    if ($this->form->isValid())
    {
      $this->tipo_gasto = $this->form->save();
		$this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showtipogasto');
    }else{
	  $this->setTemplate('edittipogasto');
    }
  }

  public function executeDeletetipogasto($request)
  {
    $this->forward404Unless($tipo_gasto = TipoGastoPeer::retrieveByPk($request->getParameter('id')));

    $tipo_gasto->delete();

    $this->redirect('mantencion/indextipogasto');
  }
  
  public function executeIndextipopago()
  {
  }

  public function executeListtipopago($request)
  {
  	$this->tipo_pagoList = TipoPagoPeer::doSelect(new Criteria());
  	$accion = $request->getParameter('accion');
  	$this->accion = $accion;
  }
  
  public function executeShowtipopago($request)
  {
    $this->tipo_pago = TipoPagoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tipo_pago);
  }

  public function executeCreatetipopago()
  {
    $this->form = new TipoPagoForm();
    $this->setTemplate('edittipopago');
  }

  public function executeEdittipopago($request)
  {
    $this->form = new TipoPagoForm(TipoPagoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdatetipopago($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TipoPagoForm(TipoPagoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('tipo_pago'));
    if ($this->form->isValid())
    {
      $this->tipo_pago = $this->form->save();
		$this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showtipopago');
    }else{
	  $this->setTemplate('edittipopago');
    }
  }

  public function executeDeletetipopago($request)
  {
    $this->forward404Unless($tipo_pago = TipoPagoPeer::retrieveByPk($request->getParameter('id')));

    $tipo_pago->delete();

    $this->redirect('mantencion/indextipopago');
  }
  
  public function executeIndextipogarantia()
  {
  }

  public function executeListtipogarantia($request)
  {
  	$this->tipo_garantiaList = TipoGarantiaPeer::doSelect(new Criteria());
  	$accion = $request->getParameter('accion');
  	$this->accion = $accion;
  }
  
  public function executeShowtipogarantia($request)
  {
    $this->tipo_garantia = TipoGarantiaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tipo_garantia);
  }

  public function executeCreatetipogarantia()
  {
    $this->form = new TipoGarantiaForm();

    $this->setTemplate('edittipogarantia');
  }

  public function executeEdittipogarantia($request)
  {
    $this->form = new TipoGarantiaForm(TipoGarantiaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdatetipogarantia($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TipoGarantiaForm(TipoGarantiaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('tipo_garantia'));
    if ($this->form->isValid())
    {
      $this->tipo_garantia = $this->form->save();
		$this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showtipogarantia');
    }else{
      $this->setTemplate('edittipogarantia');
    }
  }

  public function executeDeletegarantia($request)
  {
    $this->forward404Unless($tipo_garantia = TipoGarantiaPeer::retrieveByPk($request->getParameter('id')));

    $tipo_garantia->delete();

    $this->redirect('mantencion/indextipogarantia');
  }
  
  public function executeIndextipodocumento()
  {
  }

  public function executeListtipodocumento($request)
  {
  	$this->tipo_documentoList = TipoDocumentoPeer::doSelect(new Criteria());
  	$accion = $request->getParameter('accion');
  	$this->accion = $accion;
  }
  
  public function executeShowtipodocumento($request)
  {
    $this->tipo_documento = TipoDocumentoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tipo_documento);
  }

  public function executeCreatetipodocumento()
  {
    $this->form = new TipoDocumentoForm();

    $this->setTemplate('edittipodocumento');
  }

  public function executeEdittipodocumento($request)
  {
    $this->form = new TipoDocumentoForm(TipoDocumentoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdatetipodocumento($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TipoDocumentoForm(TipoDocumentoPeer::retrieveByPk($request->getParameter('id')));
	$datos = $request->getParameter('tipo_documento');
    $this->form->bind($datos);
    if ($this->form->isValid())
    {
      $this->tipo_documento = $this->form->save();
		$this->getUser()->setAttribute('guardado',true);
      $this->setTemplate('showtipodocumento');
    }else{
      $this->setTemplate('edittipodocumento');
    }
  }

  public function executeDeletedocumento($request)
  {
    $this->forward404Unless($tipo_documento = TipoDocumentoPeer::retrieveByPk($request->getParameter('id')));

    $tipo_documento->delete();

    $this->redirect('mantencion/indextipodocumento');
  }
}
