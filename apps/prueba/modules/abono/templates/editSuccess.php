<?php use_helper('Javascript') ?>
<?php $abono = $form->getObject() ?>
<h1><?php echo $abono->isNew() ? 'Ingresar' : 'Editar' ?> Abono</h1>
<form action="<?php echo url_for('abono/update'.(!$abono->isNew() ? '?id='.$abono->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('abono/index') ?>">Cancelar</a>
          <?php if (!$abono->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'abono/delete?id='.$abono->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el abono?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="abono_deuda_id">Causa Actual</label></th>
        <td>
          <?php echo $form['deuda_id']->renderError() ?>
          <?php echo $form['deuda_id'] ?>
          este campo no debe tener combobox. debe mostrar la causa desde la que se llego para realizar el abono.
        </td>
      </tr>
      <tr>
        <th><label for="abono_tipo_abono_id">Tipo de Abono</label></th>
        <td>
          <?php echo $form['tipo_abono_id']->renderError() ?>
          <?php echo $form['tipo_abono_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_tipo_pago_id">Tipo de Pago</label></th>
        <td>
          <?php echo $form['tipo_pago_id']->renderError() ?>
          <?php echo $form['tipo_pago_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_deuda_id">Deuda</label></th>
        <td>
        	Base
        </td>
      </tr>
      <tr>
      	<th><label for="abono_deuda_id"></label></th>
      		<td>
         	Inter&eacute;s
      		</td>
      </tr>
      <tr>
        <th><label for="abono_valor">Monto a Abonar</label></th>
        <td>
          <?php echo $form['valor']->renderError() ?>
          <?php echo $form['valor'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_fecha">Fecha</label></th>
        <td>
          <?php echo $form['fecha']->renderError() ?>
          <?php echo $form['fecha'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_motivo">Motivo</label></th>
        <td>
          <?php echo $form['motivo']->renderError() ?>
          <?php echo $form['motivo'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="abono_nro_recibo">N&uacute;mero de Recibo</label></th>
        <td>
          <?php echo $form['nro_recibo']->renderError() ?>
          <?php echo $form['nro_recibo'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>