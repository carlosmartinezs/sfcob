<h1>Lista de Abonos</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <!--<th>Causa</th>  -->
      <th>Tipo de Abono</th>
      <th>Tipo de Pago</th>
      <th>Deuda</th>
      <th>Valor</th>
      <th>Fecha</th>
      <th>Motivo</th>
      <th>N&uacute;mero de Recibo</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($abonoList as $abono): ?>
    <tr>
      <td><a href="<?php echo url_for('abono/show?id='.$abono->getId()) ?>"><?php echo $abono->getId() ?></a></td>
      <td><?php echo $abono->getTipoAbonoId() ?></td>
      <td><?php echo $abono->getTipoPagoId() ?></td>
      <td><?php echo $abono->getDeudaId() ?></td>
      <td><?php echo $abono->getValor() ?></td>
      <td><?php echo $abono->getFecha() ?></td>
      <td><?php echo $abono->getMotivo() ?></td>
      <td><?php echo $abono->getNroRecibo() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('abono/create') ?>">Crear</a>
