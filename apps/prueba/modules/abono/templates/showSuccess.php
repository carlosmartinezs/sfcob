<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $abono->getId() ?></td>
    </tr>
    <tr>
      <th>Causa:</th>
      <td><pre>Mostrar la causa y los abonos que se han hecho a dicha causa</pre></td>
    </tr>
    <tr>
      <th>Tipo de Abono:</th>
      <td><?php echo $abono->getTipoAbonoId() ?></td>
    </tr>
    <tr>
      <th>Tipo de Pago:</th>
      <td><?php echo $abono->getTipoPagoId() ?></td>
    </tr>
    <tr>
      <th>Deuda:</th>
      <td><?php echo $abono->getDeudaId() ?></td>
    </tr>
    <tr>
      <th>Valor:</th>
      <td><?php echo $abono->getValor() ?></td>
    </tr>
    <tr>
      <th>Fecha:</th>
      <td><?php echo $abono->getFecha() ?></td>
    </tr>
    <tr>
      <th>Motivo:</th>
      <td><?php echo $abono->getMotivo() ?></td>
    </tr>
    <tr>
      <th>N&uacute;mero de Recibo:</th>
      <td><?php echo $abono->getNroRecibo() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('abono/edit?id='.$abono->getId()) ?>">Editar</a>
&nbsp;
<a href="<?php echo url_for('abono/index') ?>">Listar</a>
