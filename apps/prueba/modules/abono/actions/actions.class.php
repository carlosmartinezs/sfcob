<?php

/**
 * abono actions.
 *
 * @package    sgcj
 * @subpackage abono
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class abonoActions extends sfActions
{
  public function executeIndex()
  {
    $this->abonoList = AbonoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->abono = AbonoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->abono);
  }

  public function executeCreate()
  {
    $this->form = new AbonoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new AbonoForm(AbonoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new AbonoForm(AbonoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('abono'));
    if ($this->form->isValid())
    {
      $abono = $this->form->save();

      $this->redirect('abono/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($abono = AbonoPeer::retrieveByPk($request->getParameter('id')));

    $abono->delete();

    $this->redirect('abono/index');
  }
}
