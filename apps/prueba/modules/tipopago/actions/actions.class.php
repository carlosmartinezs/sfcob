<?php

/**
 * tipopago actions.
 *
 * @package    sgcj
 * @subpackage tipopago
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class tipopagoActions extends sfActions
{
  public function executeIndex()
  {
    $this->tipo_pagoList = TipoPagoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->tipo_pago = TipoPagoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tipo_pago);
  }

  public function executeCreate()
  {
    $this->form = new TipoPagoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new TipoPagoForm(TipoPagoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TipoPagoForm(TipoPagoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('tipo_pago'));
    if ($this->form->isValid())
    {
      $tipo_pago = $this->form->save();

      $this->redirect('tipopago/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($tipo_pago = TipoPagoPeer::retrieveByPk($request->getParameter('id')));

    $tipo_pago->delete();

    $this->redirect('tipopago/index');
  }
}
