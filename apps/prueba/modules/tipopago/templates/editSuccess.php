<?php $tipo_pago = $form->getObject() ?>
<h1><?php echo $tipo_pago->isNew() ? 'Ingresar' : 'Editar' ?> Tipo de Pago</h1>

<form action="<?php echo url_for('tipopago/update'.(!$tipo_pago->isNew() ? '?id='.$tipo_pago->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('tipopago/index') ?>">Cancelar</a>
          <?php if (!$tipo_pago->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'tipopago/delete?id='.$tipo_pago->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el tipo de pago?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="tipo_pago_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="tipo_pago_descripcion">Descripci&oacute;n</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
