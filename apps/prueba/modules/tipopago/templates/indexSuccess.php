<h1>Lista de Tipos de Pago</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($tipo_pagoList as $tipo_pago): ?>
    <tr>
      <td><a href="<?php echo url_for('tipopago/show?id='.$tipo_pago->getId()) ?>"><?php echo $tipo_pago->getId() ?></a></td>
      <td><?php echo $tipo_pago->getNombre() ?></td>
      <td><?php echo $tipo_pago->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('tipopago/create') ?>">Crear</a>
