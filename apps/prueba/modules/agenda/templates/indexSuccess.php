<h1>Agenda List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Perfil</th>
      <th>Descripcion</th>
      <th>Fecha</th>
      <th>Realizada</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($agendaList as $agenda): ?>
    <tr>
      <td><a href="<?php echo url_for('agenda/show?id='.$agenda->getId()) ?>"><?php echo $agenda->getId() ?></a></td>
      <td><?php echo $agenda->getPerfilId() ?></td>
      <td><?php echo $agenda->getDescripcion() ?></td>
      <td><?php echo $agenda->getFecha() ?></td>
      <td><?php echo $agenda->getRealizada() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('agenda/create') ?>">Create</a>
