<?php

/**
 * diligenciaformato actions.
 *
 * @package    sgcj
 * @subpackage diligenciaformato
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class diligenciaformatoActions extends sfActions
{
  public function executeIndex()
  {
    $this->diligencia_formatoList = DiligenciaFormatoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->diligencia_formato = DiligenciaFormatoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->diligencia_formato);
  }

  public function executeCreate()
  {
    $this->form = new DiligenciaFormatoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new DiligenciaFormatoForm(DiligenciaFormatoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new DiligenciaFormatoForm(DiligenciaFormatoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('diligencia_formato'));
    if ($this->form->isValid())
    {
      $diligencia_formato = $this->form->save();

      $this->redirect('diligenciaformato/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($diligencia_formato = DiligenciaFormatoPeer::retrieveByPk($request->getParameter('id')));

    $diligencia_formato->delete();

    $this->redirect('diligenciaformato/index');
  }
}
