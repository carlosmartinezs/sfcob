<?php $diligencia_formato = $form->getObject() ?>
<h1><?php echo $diligencia_formato->isNew() ? 'Ingresar' : 'Editar' ?> Carta / Escrito en Diligencia</h1>

<form action="<?php echo url_for('diligenciaformato/update'.(!$diligencia_formato->isNew() ? '?id='.$diligencia_formato->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('diligenciaformato/index') ?>">Cancelar</a>
          <?php if (!$diligencia_formato->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'diligenciaformato/delete?id='.$diligencia_formato->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar la carta / escrito de la diligencia?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="diligencia_formato_diligencia_id">Diligencia</label></th>
        <td>
          <?php echo $form['diligencia_id']->renderError() ?>
          <?php echo $form['diligencia_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="diligencia_formato_formato_id">Carta / Escrito</label></th>
        <td>
          <?php echo $form['formato_id']->renderError() ?>
          <?php echo $form['formato_id'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
