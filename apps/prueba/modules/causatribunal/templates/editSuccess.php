<?php $causa_tribunal = $form->getObject() ?>
<h1><?php echo $causa_tribunal->isNew() ? 'Ingresar' : 'Editar' ?> Tribunal en la Causa</h1>

<form action="<?php echo url_for('causatribunal/update'.(!$causa_tribunal->isNew() ? '?id='.$causa_tribunal->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('causatribunal/index') ?>">Cancel</a>
          <?php if (!$causa_tribunal->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'causatribunal/delete?id='.$causa_tribunal->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el registro?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="causa_tribunal_causa_id">Causa</label></th>
        <td>
          <?php echo $form['causa_id']->renderError() ?>
          <?php echo $form['causa_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_tribunal_tribunal_id">Tribunal</label></th>
        <td>
          <?php echo $form['tribunal_id']->renderError() ?>
          <?php echo $form['tribunal_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_tribunal_fecha_cambio">Fecha de Cambio</label></th>
        <td>
          <?php echo $form['fecha_cambio']->renderError() ?>
          <?php echo $form['fecha_cambio'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_tribunal_motivo">Motivo</label></th>
        <td>
          <?php echo $form['motivo']->renderError() ?>
          <?php echo $form['motivo'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_tribunal_activo">Tribunal Activo</label></th>
        <td>
          <?php echo $form['activo']->renderError() ?>
          <?php echo $form['activo'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
