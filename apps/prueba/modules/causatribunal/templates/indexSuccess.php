<h1>Lista de Causas en historial de tribunales</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Causa</th>
      <th>Tribunal</th>
      <th>Fecha de Cambio</th>
      <th>Motivo</th>
      <th>Tribunal Activo</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($causa_tribunalList as $causa_tribunal): ?>
    <tr>
      <td><a href="<?php echo url_for('causatribunal/show?id='.$causa_tribunal->getId()) ?>"><?php echo $causa_tribunal->getId() ?></a></td>
      <td><?php echo $causa_tribunal->getCausaId() ?></td>
      <td><?php echo $causa_tribunal->getTribunalId() ?></td>
      <td><?php echo $causa_tribunal->getFechaCambio() ?></td>
      <td><?php echo $causa_tribunal->getMotivo() ?></td>
      <td><?php echo $causa_tribunal->getActivo() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('causatribunal/create') ?>">Crear</a>
