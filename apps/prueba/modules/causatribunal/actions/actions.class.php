<?php

/**
 * causatribunal actions.
 *
 * @package    sgcj
 * @subpackage causatribunal
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class causatribunalActions extends sfActions
{
  public function executeIndex()
  {
    $this->causa_tribunalList = CausaTribunalPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->causa_tribunal = CausaTribunalPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->causa_tribunal);
  }

  public function executeCreate()
  {
    $this->form = new CausaTribunalForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new CausaTribunalForm(CausaTribunalPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new CausaTribunalForm(CausaTribunalPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('causa_tribunal'));
    if ($this->form->isValid())
    {
      $causa_tribunal = $this->form->save();

      $this->redirect('causatribunal/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($causa_tribunal = CausaTribunalPeer::retrieveByPk($request->getParameter('id')));

    $causa_tribunal->delete();

    $this->redirect('causatribunal/index');
  }
}
