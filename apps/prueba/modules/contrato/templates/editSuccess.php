<?php $contrato = $form->getObject() ?>
<h1><?php echo $contrato->isNew() ? 'Ingresar' : 'Editar' ?> Contrato</h1>

<form action="<?php echo url_for('contrato/update'.(!$contrato->isNew() ? '?id='.$contrato->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('contrato/index') ?>">Cancelar</a>
          <?php if (!$contrato->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'contrato/delete?id='.$contrato->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el contrato?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="contrato_acreedor_id">Acreedor</label></th>
        <td>
          <?php echo $form['acreedor_id']->renderError() ?>
          <?php echo $form['acreedor_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="contrato_fecha_inicio">Fecha de Inicio</label></th>
        <td>
          <?php echo $form['fecha_inicio']->renderError() ?>
          <?php echo $form['fecha_inicio'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="contrato_fecha_termino">Fecha de T&eacute;rmino</label></th>
        <td>
          <?php echo $form['fecha_termino']->renderError() ?>
          <?php echo $form['fecha_termino'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="contrato_porcentaje_honorario">Porcentaje de Honorarios</label></th>
        <td>
          <?php echo $form['porcentaje_honorario']->renderError() ?>
          <?php echo $form['porcentaje_honorario'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="contrato_estado">Estado</label></th>
        <td>
          <?php echo $form['estado']->renderError() ?>
          <?php echo $form['estado'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
