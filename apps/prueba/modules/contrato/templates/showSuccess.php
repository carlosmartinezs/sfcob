<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $contrato->getId() ?></td>
    </tr>
    <tr>
      <th>Acreedor:</th>
      <td><?php echo $acreedor->__toString() ?></td>
    </tr>
    <tr>
      <th>Fecha de Inicio:</th>
      <td><?php echo $contrato->getFechaInicio() ?></td>
    </tr>
    <tr>
      <th>Fecha de T&eacute;rmino:</th>
      <td><?php echo $contrato->getFechaTermino() ?></td>
    </tr>
    <tr>
      <th>Porcentaje de Honorarios:</th>
      <td><?php echo $contrato->getPorcentajeHonorario() . "%" ?></td>
    </tr>
    <tr>
      <th>Estado:</th>
      <td><?php echo $contrato->getEstado() ? 'Activo' : 'Cancelado' ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('contrato/edit?id='.$contrato->getId()) ?>">Editar</a>
&nbsp;
<a href="<?php echo url_for('contrato/index') ?>">Listar</a>
