<?php

/**
 * contrato actions.
 *
 * @package    sgcj
 * @subpackage contrato
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class contratoActions extends sfActions
{
  public function executeIndex()
  {
    $this->contratoList = ContratoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->contrato = ContratoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->contrato);
    $this->acreedor = AcreedorPeer::retrieveByPK($this->contrato->getAcreedorId());
  }

  public function executeCreate()
  {
    $this->form = new ContratoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new ContratoForm(ContratoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new ContratoForm(ContratoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('contrato'));
    if ($this->form->isValid())
    {
      $contrato = $this->form->save();

      $this->redirect('contrato/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($contrato = ContratoPeer::retrieveByPk($request->getParameter('id')));

    $contrato->delete();

    $this->redirect('contrato/index');
  }
}
