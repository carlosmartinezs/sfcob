<h1>Clasegiro List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($clase_giroList as $clase_giro): ?>
    <tr>
      <td><a href="<?php echo url_for('clasegiro/show?id='.$clase_giro->getId()) ?>"><?php echo $clase_giro->getId() ?></a></td>
      <td><?php echo $clase_giro->getNombre() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('clasegiro/create') ?>">Create</a>
