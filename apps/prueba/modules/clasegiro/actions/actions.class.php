<?php

/**
 * clasegiro actions.
 *
 * @package    sgcj
 * @subpackage clasegiro
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class clasegiroActions extends sfActions
{
  public function executeIndex()
  {
    $this->clase_giroList = ClaseGiroPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->clase_giro = ClaseGiroPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->clase_giro);
  }

  public function executeCreate()
  {
    $this->form = new ClaseGiroForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new ClaseGiroForm(ClaseGiroPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new ClaseGiroForm(ClaseGiroPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('clase_giro'));
    if ($this->form->isValid())
    {
      $clase_giro = $this->form->save();

      $this->redirect('clasegiro/edit?id='.$clase_giro->getId());
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($clase_giro = ClaseGiroPeer::retrieveByPk($request->getParameter('id')));

    $clase_giro->delete();

    $this->redirect('clasegiro/index');
  }
}
