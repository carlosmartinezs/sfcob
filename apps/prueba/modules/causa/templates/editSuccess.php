<?php $causa = $form->getObject() ?>
<h1><?php echo $causa->isNew() ? 'Ingresar' : 'Editar' ?> Causa</h1>

<form action="<?php echo url_for('causa/update'.(!$causa->isNew() ? '?id='.$causa->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('causa/index') ?>">Cancelar</a>
          <?php if (!$causa->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'causa/delete?id='.$causa->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar la causa?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="causa_herencia_exhorto">Es un exhorto?</label></th>
        <td>
          <?php echo $form['herencia_exhorto']->renderError() ?>
          <?php echo $form['herencia_exhorto'] ?>
          Se debe sacar para la vista, pero hay que darle valor en el update del action, segun el tipo de casua elegido
        </td>
      </tr>
      <tr>
        <th><label for="causa_tipo_causa_id">Tipo de Causa</label></th>
        <td>
          <?php echo $form['tipo_causa_id']->renderError() ?>
          <?php echo $form['tipo_causa_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_causa_exhorto">Causa Exhorto</label></th>
        <td>
          <?php echo $form['causa_exhorto']->renderError() ?>
          <?php echo $form['causa_exhorto'] ?>
        <?php echo $form['id'] ?>
        Desactivado si no hay checkbox
        </td>
      </tr>
      <tr>
        <th><label for="causa_deudor_id">Deudor</label></th>
        <td>
          <?php echo $form['deudor_id']->renderError() ?>
          <?php echo $form['deudor_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_materia_id">Materia</label></th>
        <td>
          <?php echo $form['materia_id']->renderError() ?>
          <?php echo $form['materia_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_contrato_id">Contrato</label></th>
        <td>
          <?php echo $form['contrato_id']->renderError() ?>
          <?php echo $form['contrato_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_rol">Rol</label></th>
        <td>
          <?php echo $form['rol']->renderError() ?>
          <?php echo $form['rol'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_competencia">Competencia</label></th>
        <td>
          <?php echo $form['competencia']->renderError() ?>
          <?php echo $form['competencia'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_fecha_inicio">Fecha de Inicio</label></th>
        <td>
          <?php echo $form['fecha_inicio']->renderError() ?>
          <?php echo $form['fecha_inicio'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="causa_fecha_termino">Fecha de T&eacute;rmino</label></th>
        <td>
          <?php echo $form['fecha_termino']->renderError() ?>
          <?php echo $form['fecha_termino'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
