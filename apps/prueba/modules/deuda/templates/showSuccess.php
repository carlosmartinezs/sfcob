<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $deuda->getId() ?></td>
    </tr>
    <tr>
      <th>Causa:</th>
      <td><?php echo $deuda->getCausaId() ?></td>
    </tr>
    <tr>
      <th>Monto:</th>
      <td><?php echo $deuda->getMonto() ?></td>
    </tr>
    <tr>
      <th>Inter&eacute;s:</th>
      <td><?php echo $deuda->getInteres() ?></td>
    </tr>
    <tr>
      <th>Honorarios:</th>
      <td><?php echo $deuda->getHonorarios() ?>%</td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('deuda/edit?id='.$deuda->getId()) ?>">Editar</a>
&nbsp;
<a href="<?php echo url_for('deuda/index') ?>">Listar</a>
