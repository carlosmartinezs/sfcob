<h1>Lista de Deudas</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Causa</th>
      <th>Monto</th>
      <th>Inter&eacute;s</th>
      <th>Honorarios</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($deudaList as $deuda): ?>
    <tr>
      <td><a href="<?php echo url_for('deuda/show?id='.$deuda->getId()) ?>"><?php echo $deuda->getId() ?></a></td>
      <td><?php echo $deuda->getCausaId() ?></td>
      <td><?php echo $deuda->getMonto() ?></td>
      <td><?php echo $deuda->getInteres() ?></td>
      <td><?php echo $deuda->getHonorarios() ?>%</td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('deuda/create') ?>">Crear</a>
