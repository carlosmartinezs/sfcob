<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $documento->getId() ?></td>
    </tr>
    <tr>
      <th>Tipo de Documento:</th>
      <td><?php echo $documento->getTipoDocumentoId() ?></td>
    </tr>
    <tr>
      <th>Causa:</th>
      <td><?php echo $documento->getCausaId() ?></td>
    </tr>
    <tr>
      <th>Fecha de Vencimiento:</th>
      <td><?php echo $documento->getFechaVencimiento() ?></td>
    </tr>
    <tr>
      <th>Monto:</th>
      <td><?php echo $documento->getMonto() ?></td>
    </tr>
    <tr>
      <th>N&uacute;mero de Documento:</th>
      <td><?php echo $documento->getNroDocumento() ?></td>
    </tr>
    <tr>
      <th>Descripci&oacute;n:</th>
      <td><?php echo $documento->getDescripcion() ?></td>
    </tr>
    <tr>
      <th>Fecha de Emisi&oacute;n:</th>
      <td><?php echo $documento->getFechaEmision() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('documento/edit?id='.$documento->getId()) ?>">Editar</a>
&nbsp;
<a href="<?php echo url_for('documento/index') ?>">Listar</a>
