<h1>Lista de Documentos</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Tipo de Documento</th>
      <th>Causa</th>
      <th>Fecha de Vencimiento</th>
      <th>Monto</th>
      <th>N&uacute;mero de Documento</th>
      <th>Descripci&oacute;n</th>
      <th>Fecha de Emisi&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($documentoList as $documento): ?>
    <tr>
      <td><a href="<?php echo url_for('documento/show?id='.$documento->getId()) ?>"><?php echo $documento->getId() ?></a></td>
      <td><?php echo $documento->getTipoDocumentoId() ?></td>
      <td><?php echo $documento->getCausaId() ?></td>
      <td><?php echo $documento->getFechaVencimiento() ?></td>
      <td><?php echo $documento->getMonto() ?></td>
      <td><?php echo $documento->getNroDocumento() ?></td>
      <td><?php echo $documento->getDescripcion() ?></td>
      <td><?php echo $documento->getFechaEmision() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('documento/create') ?>">Crear</a>
