<?php $materia = $form->getObject() ?>
<h1><?php echo $materia->isNew() ? 'Ingresar' : 'Editar' ?> Materia</h1>

<form action="<?php echo url_for('materia/update'.(!$materia->isNew() ? '?id='.$materia->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('materia/index') ?>">Cancelar</a>
          <?php if (!$materia->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'materia/delete?id='.$materia->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar la materia?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="materia_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="materia_descripcion">Descripci&oacute;n</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
