<?php

/**
 * tipogarantia actions.
 *
 * @package    sgcj
 * @subpackage tipogarantia
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class tipogarantiaActions extends sfActions
{
  public function executeIndex()
  {
    $this->tipo_garantiaList = TipoGarantiaPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->tipo_garantia = TipoGarantiaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tipo_garantia);
  }

  public function executeCreate()
  {
    $this->form = new TipoGarantiaForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new TipoGarantiaForm(TipoGarantiaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TipoGarantiaForm(TipoGarantiaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('tipo_garantia'));
    if ($this->form->isValid())
    {
      $tipo_garantia = $this->form->save();

      $this->redirect('tipogarantia/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($tipo_garantia = TipoGarantiaPeer::retrieveByPk($request->getParameter('id')));

    $tipo_garantia->delete();

    $this->redirect('tipogarantia/index');
  }
}
