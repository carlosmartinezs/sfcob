<h1>Lista de Tipos de Garant&iacute;a</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($tipo_garantiaList as $tipo_garantia): ?>
    <tr>
      <td><a href="<?php echo url_for('tipogarantia/show?id='.$tipo_garantia->getId()) ?>"><?php echo $tipo_garantia->getId() ?></a></td>
      <td><?php echo $tipo_garantia->getNombre() ?></td>
      <td><?php echo $tipo_garantia->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('tipogarantia/create') ?>">Crear</a>
