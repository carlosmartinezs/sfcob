<?php $tipo_garantia = $form->getObject() ?>
<h1><?php echo $tipo_garantia->isNew() ? 'Ingresar' : 'Editar' ?> Tipo de Garant&iacute;a</h1>

<form action="<?php echo url_for('tipogarantia/update'.(!$tipo_garantia->isNew() ? '?id='.$tipo_garantia->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('tipogarantia/index') ?>">Cancelar</a>
          <?php if (!$tipo_garantia->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'tipogarantia/delete?id='.$tipo_garantia->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el tipo de garant&iacute;a?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="tipo_garantia_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="tipo_garantia_descripcion">Descripci&oacute;n</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
