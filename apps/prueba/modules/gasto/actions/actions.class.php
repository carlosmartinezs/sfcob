<?php

/**
 * gasto actions.
 *
 * @package    sgcj
 * @subpackage gasto
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class gastoActions extends sfActions
{
  public function executeIndex()
  {
    $this->gastoList = GastoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->gasto = GastoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->gasto);
  }

  public function executeCreate()
  {
    $this->form = new GastoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new GastoForm(GastoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new GastoForm(GastoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('gasto'));
    if ($this->form->isValid())
    {
      $gasto = $this->form->save();

      $this->redirect('gasto/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($gasto = GastoPeer::retrieveByPk($request->getParameter('id')));

    $gasto->delete();

    $this->redirect('gasto/index');
  }
}
