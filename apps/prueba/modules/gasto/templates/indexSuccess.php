<h1>Lista de Gastos</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Tipo de Gasto</th>
      <th>Diligencia</th>
      <th>Monto</th>
      <th>Fecha</th>
      <th>Motivo</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($gastoList as $gasto): ?>
    <tr>
      <td><a href="<?php echo url_for('gasto/show?id='.$gasto->getId()) ?>"><?php echo $gasto->getId() ?></a></td>
      <td><?php echo $gasto->getTipoGastoId() ?></td>
      <td><?php echo $gasto->getDiligenciaId() ?></td>
      <td><?php echo $gasto->getMonto() ?></td>
      <td><?php echo $gasto->getFecha() ?></td>
      <td><?php echo $gasto->getMotivo() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('gasto/create') ?>">Crear</a>
