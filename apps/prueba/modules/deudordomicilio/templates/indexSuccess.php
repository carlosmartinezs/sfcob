<h1>Deudordomicilio List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Deudor</th>
      <th>Comuna</th>
      <th>Calle</th>
      <th>Numero</th>
      <th>Depto</th>
      <th>Villa</th>
      <th>Telefono</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($deudor_domicilioList as $deudor_domicilio): ?>
    <tr>
      <td><a href="<?php echo url_for('deudordomicilio/show?id='.$deudor_domicilio->getId()) ?>"><?php echo $deudor_domicilio->getId() ?></a></td>
      <td><?php echo $deudor_domicilio->getDeudorId() ?></td>
      <td><?php echo $deudor_domicilio->getComunaId() ?></td>
      <td><?php echo $deudor_domicilio->getCalle() ?></td>
      <td><?php echo $deudor_domicilio->getNumero() ?></td>
      <td><?php echo $deudor_domicilio->getDepto() ?></td>
      <td><?php echo $deudor_domicilio->getVilla() ?></td>
      <td><?php echo $deudor_domicilio->getTelefono() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('deudordomicilio/create') ?>">Create</a>
