<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $deudor_domicilio->getId() ?></td>
    </tr>
    <tr>
      <th>Deudor:</th>
      <td><?php echo $deudor_domicilio->getDeudorId() ?></td>
    </tr>
    <tr>
      <th>Comuna:</th>
      <td><?php echo $deudor_domicilio->getComunaId() ?></td>
    </tr>
    <tr>
      <th>Calle:</th>
      <td><?php echo $deudor_domicilio->getCalle() ?></td>
    </tr>
    <tr>
      <th>Numero:</th>
      <td><?php echo $deudor_domicilio->getNumero() ?></td>
    </tr>
    <tr>
      <th>Depto:</th>
      <td><?php echo $deudor_domicilio->getDepto() ?></td>
    </tr>
    <tr>
      <th>Villa:</th>
      <td><?php echo $deudor_domicilio->getVilla() ?></td>
    </tr>
    <tr>
      <th>Telefono:</th>
      <td><?php echo $deudor_domicilio->getTelefono() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('deudordomicilio/edit?id='.$deudor_domicilio->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('deudordomicilio/index') ?>">List</a>
