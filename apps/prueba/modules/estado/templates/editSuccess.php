<?php $estado = $form->getObject() ?>
<h1><?php echo $estado->isNew() ? 'Ingresar' : 'Editar' ?> Estado</h1>

<form action="<?php echo url_for('estado/update'.(!$estado->isNew() ? '?id='.$estado->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('estado/index') ?>">Cancelar</a>
          <?php if (!$estado->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'estado/delete?id='.$estado->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el estado de causa?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="estado_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="estado_descripcion">Descripci&oacute;n</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
