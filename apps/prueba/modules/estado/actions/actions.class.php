<?php

/**
 * estado actions.
 *
 * @package    sgcj
 * @subpackage estado
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class estadoActions extends sfActions
{
  public function executeIndex()
  {
    $this->estadoList = EstadoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->estado = EstadoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->estado);
  }

  public function executeCreate()
  {
    $this->form = new EstadoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new EstadoForm(EstadoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new EstadoForm(EstadoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('estado'));
    if ($this->form->isValid())
    {
      $estado = $this->form->save();

      $this->redirect('estado/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($estado = EstadoPeer::retrieveByPk($request->getParameter('id')));

    $estado->delete();

    $this->redirect('estado/index');
  }
}
