<h1>Giro List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Clase giro</th>
      <th>Nombre</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($giroList as $giro): ?>
    <tr>
      <td><a href="<?php echo url_for('giro/show?id='.$giro->getId()) ?>"><?php echo $giro->getId() ?></a></td>
      <td><?php echo $giro->getClaseGiroId() ?></td>
      <td><?php echo $giro->getNombre() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('giro/create') ?>">Create</a>
