<?php $giro = $form->getObject() ?>
<h1><?php echo $giro->isNew() ? 'New' : 'Edit' ?> Giro</h1>

<form action="<?php echo url_for('giro/update'.(!$giro->isNew() ? '?id='.$giro->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('giro/index') ?>">Cancel</a>
          <?php if (!$giro->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'giro/delete?id='.$giro->getId(), array('post' => true, 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="giro_clase_giro_id">Clase giro id</label></th>
        <td>
          <?php echo $form['clase_giro_id']->renderError() ?>
          <?php echo $form['clase_giro_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="giro_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
