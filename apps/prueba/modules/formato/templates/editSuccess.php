<?php $formato = $form->getObject() ?>
<h1><?php echo $formato->isNew() ? 'Ingresar' : 'Editar' ?> Carta / Escrito</h1>

<form action="<?php echo url_for('formato/update'.(!$formato->isNew() ? '?id='.$formato->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('formato/index') ?>">Cancelar</a>
          <?php if (!$formato->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'formato/delete?id='.$formato->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar la carta / escrito?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="formato_tipo">Tipo</label></th>
        <td>
          <?php echo $form['tipo']->renderError() ?>
          <?php echo $form['tipo'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="formato_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="formato_cuerpo">Cuerpo</label></th>
        <td>
          <?php echo $form['cuerpo']->renderError() ?>
          <?php echo $form['cuerpo'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
