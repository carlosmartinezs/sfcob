<h1>Lista de Cartas / Escritos</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Tipo</th>
      <th>Nombre</th>
      <th>Cuerpo</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($formatoList as $formato): ?>
    <tr>
      <td><a href="<?php echo url_for('formato/show?id='.$formato->getId()) ?>"><?php echo $formato->getId() ?></a></td>
      <td><?php echo $formato->getTipo() ?></td>
      <td><?php echo $formato->getNombre() ?></td>
      <td><?php echo $formato->getCuerpo() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('formato/create') ?>">Crear</a>
