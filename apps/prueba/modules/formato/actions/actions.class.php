<?php

/**
 * formato actions.
 *
 * @package    sgcj
 * @subpackage formato
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class formatoActions extends sfActions
{
  public function executeIndex()
  {
    $this->formatoList = FormatoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->formato = FormatoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->formato);
  }

  public function executeCreate()
  {
    $this->form = new FormatoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new FormatoForm(FormatoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new FormatoForm(FormatoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('formato'));
    if ($this->form->isValid())
    {
      $formato = $this->form->save();

      $this->redirect('formato/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($formato = FormatoPeer::retrieveByPk($request->getParameter('id')));

    $formato->delete();

    $this->redirect('formato/index');
  }
}
