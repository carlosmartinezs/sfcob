<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $deudor->getId() ?></td>
    </tr>
    <tr>
      <th>Rut:</th>
      <td><?php echo $deudor->getRutDeudor() ?></td>
    </tr>
    <tr>
      <th>Nombres:</th>
      <td><?php echo $deudor->getNombres() ?></td>
    </tr>
    <tr>
      <th>Apellido Paterno:</th>
      <td><?php echo $deudor->getApellidoPaterno() ?></td>
    </tr>
    <tr>
      <th>Apellido Materno:</th>
      <td><?php echo $deudor->getApellidoMaterno() ?></td>
    </tr>
    <tr>
      <th>Tel&eacute;fono Celular:</th>
      <td><?php echo $deudor->getCelular() ?></td>
    </tr>
    <tr>
      <th>Email:</th>
      <td><?php echo $deudor->getEmail() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('deudor/edit?id='.$deudor->getId()) ?>">Editar</a>
&nbsp;
<a href="<?php echo url_for('deudor/index') ?>">Listar</a>
