<?php

/**
 * deudor actions.
 *
 * @package    sgcj
 * @subpackage deudor
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class deudorActions extends sfActions
{
  public function executeIndex()
  {
    $this->deudorList = DeudorPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->deudor = DeudorPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->deudor);
  }

  public function executeCreate()
  {
    $this->form = new DeudorForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new DeudorForm(DeudorPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new DeudorForm(DeudorPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('deudor'));
    if ($this->form->isValid())
    {
      $deudor = $this->form->save();

      $this->redirect('deudor/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($deudor = DeudorPeer::retrieveByPk($request->getParameter('id')));

    $deudor->delete();

    $this->redirect('deudor/index');
  }
}
