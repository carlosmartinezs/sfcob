<?php $estado_causa = $form->getObject() ?>
<h1><?php echo $estado_causa->isNew() ? 'Ingresar' : 'Editar' ?> Estado de Causa</h1>

<form action="<?php echo url_for('estadocausa/update'.(!$estado_causa->isNew() ? '?id='.$estado_causa->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('estadocausa/index') ?>">Cancelar</a>
          <?php if (!$estado_causa->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'estadocausa/delete?id='.$estado_causa->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el estado de la causa?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="estado_causa_causa_id">Causa</label></th>
        <td>
          <?php echo $form['causa_id']->renderError() ?>
          <?php echo $form['causa_id'] ?>
          este campo se llena automaticamente, ya que para cambiar el estado de la causa debe hacerse a traves de la vista de la causa
        </td>
      </tr>
      <tr>
        <th><label for="estado_causa_estado_id">Estado</label></th>
        <td>
          <?php echo $form['estado_id']->renderError() ?>
          <?php echo $form['estado_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="estado_causa_fecha">Fecha</label></th>
        <td>
          <?php echo $form['fecha']->renderError() ?>
          <?php echo $form['fecha'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="estado_causa_motivo">Motivo</label></th>
        <td>
          <?php echo $form['motivo']->renderError() ?>
          <?php echo $form['motivo'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="estado_causa_activo">Estado activo?</label></th>
        <td>
          <?php echo $form['activo']->renderError() ?>
          <?php echo $form['activo'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
