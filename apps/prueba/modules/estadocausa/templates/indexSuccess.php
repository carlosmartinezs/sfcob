<h1>Lista de Estados de causas</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Causa</th>
      <th>Estado</th>
      <th>Fecha</th>
      <th>Motivo</th>
      <th>Estado activo?</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($estado_causaList as $estado_causa): ?>
    <tr>
      <td><a href="<?php echo url_for('estadocausa/show?id='.$estado_causa->getId()) ?>"><?php echo $estado_causa->getId() ?></a></td>
      <td><?php echo $estado_causa->getCausaId() ?></td>
      <td><?php echo $estado_causa->getEstadoId() ?></td>
      <td><?php echo $estado_causa->getFecha() ?></td>
      <td><?php echo $estado_causa->getMotivo() ?></td>
      <td><?php echo $estado_causa->getActivo() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('estadocausa/create') ?>">Crear</a>
