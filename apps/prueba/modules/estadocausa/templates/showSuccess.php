<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $estado_causa->getId() ?></td>
    </tr>
    <tr>
      <th>Causa:</th>
      <td><?php echo $estado_causa->getCausaId() ?></td>
    </tr>
    <tr>
      <th>Estado:</th>
      <td><?php echo $estado_causa->getEstadoId() ?></td>
    </tr>
    <tr>
      <th>Fecha:</th>
      <td><?php echo $estado_causa->getFecha() ?></td>
    </tr>
    <tr>
      <th>Motivo:</th>
      <td><?php echo $estado_causa->getMotivo() ?></td>
    </tr>
    <tr>
      <th>Estado activo?:</th>
      <td><?php echo $estado_causa->getActivo() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('estadocausa/edit?id='.$estado_causa->getId()) ?>">Editar</a>
&nbsp;
<a href="<?php echo url_for('estadocausa/index') ?>">Listar</a>
