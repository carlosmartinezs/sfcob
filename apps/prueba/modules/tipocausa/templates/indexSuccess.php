<h1>Lista de Tipos de Causa</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($tipo_causaList as $tipo_causa): ?>
    <tr>
      <td><a href="<?php echo url_for('tipocausa/show?id='.$tipo_causa->getId()) ?>"><?php echo $tipo_causa->getId() ?></a></td>
      <td><?php echo $tipo_causa->getNombre() ?></td>
      <td><?php echo $tipo_causa->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('tipocausa/create') ?>">Crear</a>
