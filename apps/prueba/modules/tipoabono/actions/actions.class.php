<?php

/**
 * tipoabono actions.
 *
 * @package    sgcj
 * @subpackage tipoabono
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class tipoabonoActions extends sfActions
{
  public function executeIndex()
  {
    $this->tipo_abonoList = TipoAbonoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->tipo_abono = TipoAbonoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tipo_abono);
  }

  public function executeCreate()
  {
    $this->form = new TipoAbonoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new TipoAbonoForm(TipoAbonoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TipoAbonoForm(TipoAbonoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('tipo_abono'));
    if ($this->form->isValid())
    {
      $tipo_abono = $this->form->save();

      $this->redirect('tipoabono/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($tipo_abono = TipoAbonoPeer::retrieveByPk($request->getParameter('id')));

    $tipo_abono->delete();

    $this->redirect('tipoabono/index');
  }
}
