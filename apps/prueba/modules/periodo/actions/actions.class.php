<?php

/**
 * periodo actions.
 *
 * @package    sgcj
 * @subpackage periodo
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class periodoActions extends sfActions
{
  public function executeIndex()
  {
    $this->periodoList = PeriodoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->periodo = PeriodoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->periodo);
  }

  public function executeCreate()
  {
    $this->form = new PeriodoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new PeriodoForm(PeriodoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new PeriodoForm(PeriodoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('periodo'));
    if ($this->form->isValid())
    {
      $periodo = $this->form->save();

      $this->redirect('periodo/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($periodo = PeriodoPeer::retrieveByPk($request->getParameter('id')));

    $periodo->delete();

    $this->redirect('periodo/index');
  }
}
