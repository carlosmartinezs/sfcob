<?php

/**
 * tipogasto actions.
 *
 * @package    sgcj
 * @subpackage tipogasto
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class tipogastoActions extends sfActions
{
  public function executeIndex()
  {
    $this->tipo_gastoList = TipoGastoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->tipo_gasto = TipoGastoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tipo_gasto);
  }

  public function executeCreate()
  {
    $this->form = new TipoGastoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new TipoGastoForm(TipoGastoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TipoGastoForm(TipoGastoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('tipo_gasto'));
    if ($this->form->isValid())
    {
      $tipo_gasto = $this->form->save();

      $this->redirect('tipogasto/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($tipo_gasto = TipoGastoPeer::retrieveByPk($request->getParameter('id')));

    $tipo_gasto->delete();

    $this->redirect('tipogasto/index');
  }
}
