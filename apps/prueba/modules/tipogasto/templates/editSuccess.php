<?php $tipo_gasto = $form->getObject() ?>
<h1><?php echo $tipo_gasto->isNew() ? 'Ingresar' : 'Editar' ?> Tipo de Gasto</h1>

<form action="<?php echo url_for('tipogasto/update'.(!$tipo_gasto->isNew() ? '?id='.$tipo_gasto->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('tipogasto/index') ?>">Cancelar</a>
          <?php if (!$tipo_gasto->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'tipogasto/delete?id='.$tipo_gasto->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el tipo de gasto?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="tipo_gasto_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="tipo_gasto_descripcion">Descripci&oacute;n</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
