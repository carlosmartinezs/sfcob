<h1>Lista de Tipos de Gasto</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($tipo_gastoList as $tipo_gasto): ?>
    <tr>
      <td><a href="<?php echo url_for('tipogasto/show?id='.$tipo_gasto->getId()) ?>"><?php echo $tipo_gasto->getId() ?></a></td>
      <td><?php echo $tipo_gasto->getNombre() ?></td>
      <td><?php echo $tipo_gasto->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('tipogasto/create') ?>">Crear</a>
