<h1>Lista de Garant&iacute;as</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Causa</th>
      <th>Tipo de garant&iacute;a</th>
      <th>Valor</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($garantiaList as $garantia): ?>
    <tr>
      <td><a href="<?php echo url_for('garantia/show?id='.$garantia->getId()) ?>"><?php echo $garantia->getId() ?></a></td>
      <td><?php echo $garantia->getNombreCausa() ?></td>
      <td><?php echo $garantia->getNombreTipoGarantia() ?></td>
      <td><?php echo $garantia->getValor() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('garantia/create') ?>">Crear</a>
