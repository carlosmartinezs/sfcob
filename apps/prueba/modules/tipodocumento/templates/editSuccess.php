<?php $tipo_documento = $form->getObject() ?>
<h1><?php echo $tipo_documento->isNew() ? 'Ingresar' : 'Editar' ?> Tipo de Documento</h1>

<form action="<?php echo url_for('tipodocumento/update'.(!$tipo_documento->isNew() ? '?id='.$tipo_documento->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('tipodocumento/index') ?>">Cancelar</a>
          <?php if (!$tipo_documento->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'tipodocumento/delete?id='.$tipo_documento->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el tipo de documento?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="tipo_documento_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="tipo_documento_descripcion">Descripci&oacute;n</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
