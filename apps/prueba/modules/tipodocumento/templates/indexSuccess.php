<h1>Lista de Tipos de Documento</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($tipo_documentoList as $tipo_documento): ?>
    <tr>
      <td><a href="<?php echo url_for('tipodocumento/show?id='.$tipo_documento->getId()) ?>"><?php echo $tipo_documento->getId() ?></a></td>
      <td><?php echo $tipo_documento->getNombre() ?></td>
      <td><?php echo $tipo_documento->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('tipodocumento/create') ?>">Crear</a>
