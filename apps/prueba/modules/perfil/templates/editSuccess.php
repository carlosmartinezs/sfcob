<?php $perfil = $form->getObject() ?>
<h1><?php echo $perfil->isNew() ? 'New' : 'Edit' ?> Perfil</h1>

<form action="<?php echo url_for('perfil/update'.(!$perfil->isNew() ? '?id='.$perfil->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('perfil/index') ?>">Cancel</a>
          <?php if (!$perfil->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'perfil/delete?id='.$perfil->getId(), array('post' => true, 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['sf_guard_user_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['sf_guard_user_id']->renderError() ?>
          <?php echo $form['sf_guard_user_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['password1']->renderLabel() ?></th>
        <td>
          <?php echo $form['password1']->renderError() ?>
          <?php echo $form['password1'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['password2']->renderLabel() ?></th>
        <td>
          <?php echo $form['password2']->renderError() ?>
          <?php echo $form['password2'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['password3']->renderLabel() ?></th>
        <td>
          <?php echo $form['password3']->renderError() ?>
          <?php echo $form['password3'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['ultimo_cambio']->renderLabel() ?></th>
        <td>
          <?php echo $form['ultimo_cambio']->renderError() ?>
          <?php echo $form['ultimo_cambio'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['nombres']->renderLabel() ?></th>
        <td>
          <?php echo $form['nombres']->renderError() ?>
          <?php echo $form['nombres'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['apellido_paterno']->renderLabel() ?></th>
        <td>
          <?php echo $form['apellido_paterno']->renderError() ?>
          <?php echo $form['apellido_paterno'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['apellido_materno']->renderLabel() ?></th>
        <td>
          <?php echo $form['apellido_materno']->renderError() ?>
          <?php echo $form['apellido_materno'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['comuna_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['comuna_id']->renderError() ?>
          <?php echo $form['comuna_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['direccion']->renderLabel() ?></th>
        <td>
          <?php echo $form['direccion']->renderError() ?>
          <?php echo $form['direccion'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['telefono']->renderLabel() ?></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['fax']->renderLabel() ?></th>
        <td>
          <?php echo $form['fax']->renderError() ?>
          <?php echo $form['fax'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['email']->renderLabel() ?></th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
