<h1>Perfil List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Sf guard user</th>
      <th>Password1</th>
      <th>Password2</th>
      <th>Password3</th>
      <th>Ultimo cambio</th>
      <th>Nombres</th>
      <th>Apellido paterno</th>
      <th>Apellido materno</th>
      <th>Comuna</th>
      <th>Direccion</th>
      <th>Telefono</th>
      <th>Fax</th>
      <th>Email</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($perfilList as $perfil): ?>
    <tr>
      <td><a href="<?php echo url_for('perfil/edit?id='.$perfil->getId()) ?>"><?php echo $perfil->getId() ?></a></td>
      <td><?php echo $perfil->getSfGuardUserId() ?></td>
      <td><?php echo $perfil->getPassword1() ?></td>
      <td><?php echo $perfil->getPassword2() ?></td>
      <td><?php echo $perfil->getPassword3() ?></td>
      <td><?php echo $perfil->getUltimoCambio() ?></td>
      <td><?php echo $perfil->getNombres() ?></td>
      <td><?php echo $perfil->getApellidoPaterno() ?></td>
      <td><?php echo $perfil->getApellidoMaterno() ?></td>
      <td><?php echo $perfil->getComunaId() ?></td>
      <td><?php echo $perfil->getDireccion() ?></td>
      <td><?php echo $perfil->getTelefono() ?></td>
      <td><?php echo $perfil->getFax() ?></td>
      <td><?php echo $perfil->getEmail() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('perfil/create') ?>">Create</a>
