<h1>Acreedor List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Comuna</th>
      <th>Giro</th>
      <th>Sf guard user</th>
      <th>Rut acreedor</th>
      <th>Nombres</th>
      <th>Apellido paterno</th>
      <th>Apellido materno</th>
      <th>Direccion</th>
      <th>Telefono</th>
      <th>Fax</th>
      <th>Email</th>
      <th>Rep legal</th>
      <th>Rut rep legal</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($acreedorList as $acreedor): ?>
    <tr>
      <td><a href="<?php echo url_for('acreedor/show?id='.$acreedor->getId()) ?>"><?php echo $acreedor->getId() ?></a></td>
      <td><?php echo $acreedor->getComunaId() ?></td>
      <td><?php echo $acreedor->getGiroId() ?></td>
      <td><?php echo $acreedor->getSfGuardUserId() ?></td>
      <td><?php echo $acreedor->getRutAcreedor() ?></td>
      <td><?php echo $acreedor->getNombres() ?></td>
      <td><?php echo $acreedor->getApellidoPaterno() ?></td>
      <td><?php echo $acreedor->getApellidoMaterno() ?></td>
      <td><?php echo $acreedor->getDireccion() ?></td>
      <td><?php echo $acreedor->getTelefono() ?></td>
      <td><?php echo $acreedor->getFax() ?></td>
      <td><?php echo $acreedor->getEmail() ?></td>
      <td><?php echo $acreedor->getRepLegal() ?></td>
      <td><?php echo $acreedor->getRutRepLegal() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('acreedor/create') ?>">Create</a>
