<?php $acreedor = $form->getObject() ?>
<h1><?php echo $acreedor->isNew() ? 'New' : 'Edit' ?> Acreedor</h1>

<form action="<?php echo url_for('acreedor/update'.(!$acreedor->isNew() ? '?id='.$acreedor->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('acreedor/index') ?>">Cancel</a>
          <?php if (!$acreedor->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'acreedor/delete?id='.$acreedor->getId(), array('post' => true, 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="acreedor_rut_acreedor">Rut acreedor</label></th>
        <td>
          <?php echo $form['rut_acreedor']->renderError() ?>
          <?php echo $form['rut_acreedor'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_nombres">Nombres</label></th>
        <td>
          <?php echo $form['nombres']->renderError() ?>
          <?php echo $form['nombres'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_apellido_paterno">Apellido paterno</label></th>
        <td>
          <?php echo $form['apellido_paterno']->renderError() ?>
          <?php echo $form['apellido_paterno'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_apellido_materno">Apellido materno</label></th>
        <td>
          <?php echo $form['apellido_materno']->renderError() ?>
          <?php echo $form['apellido_materno'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_direccion">Direccion</label></th>
        <td>
          <?php echo $form['direccion']->renderError() ?>
          <?php echo $form['direccion'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_telefono">Telefono</label></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_fax">Fax</label></th>
        <td>
          <?php echo $form['fax']->renderError() ?>
          <?php echo $form['fax'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_email">Email</label></th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_rep_legal">Rep legal</label></th>
        <td>
          <?php echo $form['rep_legal']->renderError() ?>
          <?php echo $form['rep_legal'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="acreedor_rut_rep_legal">Rut rep legal</label></th>
        <td>
          <?php echo $form['rut_rep_legal']->renderError() ?>
          <?php echo $form['rut_rep_legal'] ?>

        <?php echo $form['id'] ?>
        <?php echo $form['comuna_id'] ?>
        <?php echo $form['giro_id'] ?>
        <?php echo $form['sf_guard_user_id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
