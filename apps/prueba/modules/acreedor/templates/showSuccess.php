<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $acreedor->getId() ?></td>
    </tr>
    <tr>
      <th>Comuna:</th>
      <td><?php echo $acreedor->getComunaId() ?></td>
    </tr>
    <tr>
      <th>Giro:</th>
      <td><?php echo $acreedor->getGiroId() ?></td>
    </tr>
    <tr>
      <th>Sf guard user:</th>
      <td><?php echo $acreedor->getSfGuardUserId() ?></td>
    </tr>
    <tr>
      <th>Rut acreedor:</th>
      <td><?php echo $acreedor->getRutAcreedor() ?></td>
    </tr>
    <tr>
      <th>Nombres:</th>
      <td><?php echo $acreedor->getNombres() ?></td>
    </tr>
    <tr>
      <th>Apellido paterno:</th>
      <td><?php echo $acreedor->getApellidoPaterno() ?></td>
    </tr>
    <tr>
      <th>Apellido materno:</th>
      <td><?php echo $acreedor->getApellidoMaterno() ?></td>
    </tr>
    <tr>
      <th>Direccion:</th>
      <td><?php echo $acreedor->getDireccion() ?></td>
    </tr>
    <tr>
      <th>Telefono:</th>
      <td><?php echo $acreedor->getTelefono() ?></td>
    </tr>
    <tr>
      <th>Fax:</th>
      <td><?php echo $acreedor->getFax() ?></td>
    </tr>
    <tr>
      <th>Email:</th>
      <td><?php echo $acreedor->getEmail() ?></td>
    </tr>
    <tr>
      <th>Rep legal:</th>
      <td><?php echo $acreedor->getRepLegal() ?></td>
    </tr>
    <tr>
      <th>Rut rep legal:</th>
      <td><?php echo $acreedor->getRutRepLegal() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('acreedor/edit?id='.$acreedor->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('acreedor/index') ?>">List</a>
