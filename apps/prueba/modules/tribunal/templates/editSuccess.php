<?php $tribunal = $form->getObject() ?>
<form action="<?php echo url_for('tribunal/update'.(!$tribunal->isNew() ? '?id='.$tribunal->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<h2><?php echo $tribunal->isNew() ? 'New' : 'Edit' ?> Tribunal</h2>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('tribunal/index') ?>">Cancel</a>
          <?php if (!$tribunal->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'tribunal/delete?id='.$tribunal->getId(), array('post' => true, 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="tribunal_comuna_id">Comuna id</label></th>
        <td>
          <?php echo $form['comuna_id']->renderError() ?>
          <?php echo $form['comuna_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="tribunal_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="tribunal_region_id">Region id</label></th>
        <td>
          <?php echo $form['region_id']->renderError() ?>
          <?php echo $form['region_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="tribunal_ciudad_id">Ciudad id</label></th>
        <td>
          <?php echo $form['ciudad_id']->renderError() ?>
          <?php echo $form['ciudad_id'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
