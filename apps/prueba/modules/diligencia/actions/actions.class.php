<?php

/**
 * diligencia actions.
 *
 * @package    sgcj
 * @subpackage diligencia
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class diligenciaActions extends sfActions
{
  public function executeIndex()
  {
    $this->diligenciaList = DiligenciaPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->diligencia = DiligenciaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->diligencia);
  }

  public function executeCreate()
  {
    $this->form = new DiligenciaForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new DiligenciaForm(DiligenciaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new DiligenciaForm(DiligenciaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('diligencia'));
    if ($this->form->isValid())
    {
      $diligencia = $this->form->save();

      $this->redirect('diligencia/edit?id='.$diligencia->getId());
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($diligencia = DiligenciaPeer::retrieveByPk($request->getParameter('id')));

    $diligencia->delete();

    $this->redirect('diligencia/index');
  }
}
