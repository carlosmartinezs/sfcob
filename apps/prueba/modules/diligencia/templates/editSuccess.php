<?php $diligencia = $form->getObject() ?>
<h1><?php echo $diligencia->isNew() ? 'New' : 'Edit' ?> Diligencia</h1>

<form action="<?php echo url_for('diligencia/update'.(!$diligencia->isNew() ? '?id='.$diligencia->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('diligencia/index') ?>">Cancel</a>
          <?php if (!$diligencia->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'diligencia/delete?id='.$diligencia->getId(), array('post' => true, 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="diligencia_periodo_causa_id">Periodo causa id</label></th>
        <td>
          <?php echo $form['periodo_causa_id']->renderError() ?>
          <?php echo $form['periodo_causa_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="diligencia_fecha">Fecha</label></th>
        <td>
          <?php echo $form['fecha']->renderError() ?>
          <?php echo $form['fecha'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="diligencia_descripcion">Descripcion</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
