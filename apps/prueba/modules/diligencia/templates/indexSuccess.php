<h1>Diligencia List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Periodo causa</th>
      <th>Fecha</th>
      <th>Descripcion</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($diligenciaList as $diligencia): ?>
    <tr>
      <td><a href="<?php echo url_for('diligencia/show?id='.$diligencia->getId()) ?>"><?php echo $diligencia->getId() ?></a></td>
      <td><?php echo $diligencia->getPeriodoCausaId() ?></td>
      <td><?php echo $diligencia->getFecha() ?></td>
      <td><?php echo $diligencia->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('diligencia/create') ?>">Create</a>
