<h1>Lista de Periodos de causas</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Causa</th>
      <th>Periodo</th>
      <th>Fecha de Inicio</th>
      <th>Periodo activo?</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($periodo_causaList as $periodo_causa): ?>
    <tr>
      <td><a href="<?php echo url_for('periodocausa/show?id='.$periodo_causa->getId()) ?>"><?php echo $periodo_causa->getId() ?></a></td>
      <td><?php echo $periodo_causa->getCausaId() ?></td>
      <td><?php echo $periodo_causa->getPeriodoId() ?></td>
      <td><?php echo $periodo_causa->getFechaInicio() ?></td>
      <td><?php echo $periodo_causa->getActivo() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('periodocausa/create') ?>">Crear</a>
