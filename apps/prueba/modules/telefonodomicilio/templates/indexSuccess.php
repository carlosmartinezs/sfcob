<h1>Lista de Tel&eacute;fonos en Domicilio de Deudor</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Domicilio de Deudor</th>
      <th>Tel&eacute;fono</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($telefono_domicilioList as $telefono_domicilio): ?>
    <tr>
      <td><a href="<?php echo url_for('telefonodomicilio/show?id='.$telefono_domicilio->getId()) ?>"><?php echo $telefono_domicilio->getId() ?></a></td>
      <td><?php echo $telefono_domicilio->getDeudorDomicilioId() ?></td>
      <td><?php echo $telefono_domicilio->getTelefono() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('telefonodomicilio/create') ?>">Crear</a>
