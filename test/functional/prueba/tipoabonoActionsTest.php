<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

// create a new test browser
$browser = new sfTestBrowser();

$browser->
  get('/tipoabono/index')->
  isStatusCode(200)->
  isRequestParameter('module', 'tipoabono')->
  isRequestParameter('action', 'index')->
  checkResponseElement('body', '!/This is a temporary page/');
