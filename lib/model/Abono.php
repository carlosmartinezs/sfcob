<?php

/**
 * Subclass for representing a row from the 'abono' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Abono extends BaseAbono
{
	public function __toString(){
		return $this->getMotivo();
	}
}
