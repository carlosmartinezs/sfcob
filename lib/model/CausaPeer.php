<?php

/**
 * Subclass for performing query and update operations on the 'causa' table.
 *
 * 
 *
 * @package lib.model
 */ 
class CausaPeer extends BaseCausaPeer
{
	public static function getCausasAcreedorFiltroDeEstado($acreedor,$filtroCausa)
	{
		$criteria = new Criteria();
  		$criteria->addJoin(ContratoPeer::ACREEDOR_ID, AcreedorPeer::ID);
  		$criteria->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);
  		$criteria->add(ContratoPeer::ACREEDOR_ID,$acreedor->getId());
  		$criteria->addJoin(CausaPeer::ID,EstadoCausaPeer::CAUSA_ID);
  		$criteria->add(EstadoCausaPeer::ACTIVO,1);
  	
  		$lista = null;
  		
  		for($i = 0; $i < count($filtroCausa); $i++)
  		{
  			if($filtroCausa[$i])
  			{
  				$lista[$i] = $i+1;
  			}	
  		}

  		$criteria->add(EstadoCausaPeer::ESTADO_ID,$lista,Criteria::IN);
  		$criteria->setDistinct();
  		return CausaPeer::doSelect($criteria);
	}
	
    public static function getCriteria($datos)
	{
		$criteria = new Criteria();
		
		if($datos['id'] != null && $datos['id'] != '')
		{
			$criteria->add(CausaPeer::ID,$datos['id']);
		}
		
		if($datos['tipo_causa_id'] != null && $datos['tipo_causa_id'] != '')
		{
			$criteria->add(CausaPeer::TIPO_CAUSA_ID,$datos['tipo_causa_id']);
		}
		
		if($datos['competencia'] != null && $datos['competencia'] != '')
		{
			$criteria->add(CausaPeer::COMPETENCIA_ID,$datos['competencia']);
		}
		
		if($datos['rol'] != null && $datos['rol'] != '')
		{
			$criteria->add(CausaPeer::ROL,'%'.$datos['rol'].'%',Criteria::LIKE);
		}
		
		if($datos['materia_id'] != null && $datos['materia_id'] != '')
		{
			$criteria->add(CausaPeer::MATERIA_ID,$datos['materia_id']);
		}
		
		if($datos['searchcontrato'] != null && $datos['searchcontrato'] != '')
		{
			$c = new Criteria();
			$c->addJoin(ContratoPeer::ACREEDOR_ID,AcreedorPeer::ID);
			$c->add(ContratoPeer::ID,$datos['searchcontrato']);
			
			$acreedor = AcreedorPeer::doSelectOne($c);
			
			$criteria->addJoin(CausaPeer::CONTRATO_ID,ContratoPeer::ID);
			$criteria->addJoin(ContratoPeer::ACREEDOR_ID,AcreedorPeer::ID);
			$criteria->add(AcreedorPeer::ID, $acreedor->getId());
		}
		
		if($datos['searchdeudor'] != null && $datos['searchdeudor'] != '')
		{
			$criteria->add(CausaPeer::DEUDOR_ID,$datos['searchdeudor']);
		}
		
		if($datos['desde'] != null && $datos['desde'] != '--')
		{
			$fecha = $datos['desde'];
			
			$tmp = $fecha['year'];
			$tmp .= '-'.$fecha['month'];
			$tmp .= '-'.$fecha['day'];
			
			if($tmp != '--')
			{
				$criteria->addAnd(CausaPeer::FECHA_INICIO,$tmp,Criteria::GREATER_EQUAL);
			}
		}
		
		if($datos['hasta'] != null && $datos['hasta'] != '--')
		{
			$fecha = $datos['hasta'];
			
			$tmp = $fecha['year'];
			$tmp .= '-'.$fecha['month'];
			$tmp .= '-'.$fecha['day'];
			
			if($tmp != '--')
			{
				$criteria->addAnd(CausaPeer::FECHA_INICIO,$tmp, Criteria::LESS_EQUAL);
			}
		}
		
		if($datos['herencia_exhorto'] && $datos['herencia_exhorto'] != false)
		{
			$criteria->add(CausaPeer::HERENCIA_EXHORTO,true);
		}
				
		if($datos['periodo'] != null && $datos['periodo'] != '')
		{
			$criteria->addJoin(PeriodoPeer::ID,PeriodoCausaPeer::PERIODO_ID);
			$criteria->addJoin(PeriodoCausaPeer::CAUSA_ID,CausaPeer::ID);
			$criteria->add(PeriodoPeer::ID,$datos['periodo']);
		}
		
		if($datos['estado'] != null && $datos['estado'] != '')
		{
			//echo "alert('estado: ".$datos['estado']."')";
			$criteria->addJoin(EstadoCausaPeer::CAUSA_ID,CausaPeer::ID);
			$criteria->add(EstadoCausaPeer::ACTIVO,true);
			$criteria->add(EstadoCausaPeer::ESTADO_ID,$datos['estado']);
		}
		
		return $criteria;
	}
}
