<?php

/**
 * Subclass for representing a row from the 'materia' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Materia extends BaseMateria
{
	public function __toString(){
		return $this->getNombre();
	}
}
