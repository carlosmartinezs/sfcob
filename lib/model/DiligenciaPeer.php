<?php

/**
 * Subclass for performing query and update operations on the 'diligencia' table.
 *
 * 
 *
 * @package lib.model
 */ 
class DiligenciaPeer extends BaseDiligenciaPeer
{
	public static function getDilicenciasCausaPorFechaConLimite($causa,$limite){
		
		$criteria = new Criteria();
  		$criteria->addJoin(PeriodoCausaPeer::CAUSA_ID,CausaPeer::ID);
  		$criteria->addJoin(DiligenciaPeer::PERIODO_CAUSA_ID,PeriodoCausaPeer::ID);
  		$criteria->add(CausaPeer::ID,$causa->getId());
  		$criteria->addDescendingOrderByColumn('id');
  		$criteria->setLimit($limite);
  	
  		return DiligenciaPeer::doSelect($criteria);
	}
}