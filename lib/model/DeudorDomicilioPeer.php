<?php

/**
 * Subclass for performing query and update operations on the 'deudor_domicilio' table.
 *
 * 
 *
 * @package lib.model
 */ 
class DeudorDomicilioPeer extends BaseDeudorDomicilioPeer
{
	static public function getByIdDeudor($iddeudor){
		
		$criteria = new Criteria();
		$criteria->add(DeudorDomicilioPeer::DEUDOR_ID, $iddeudor);
		return DeudorDomicilioPeer::doSelect($criteria);
	}

}
