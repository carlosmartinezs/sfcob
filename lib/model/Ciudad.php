<?php

/**
 * Subclass for representing a row from the 'ciudad' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Ciudad extends BaseCiudad
{
	public function __toString(){
		return $this->getNombre();
	}
}
