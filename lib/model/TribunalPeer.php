<?php

/**
 * Subclass for performing query and update operations on the 'tribunal' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TribunalPeer extends BaseTribunalPeer
{
	public static function getCriteria($request)
	{
		$criteria = new Criteria();
		
		if($request->hasParameter('acreedor_ciudad_id') && $request->getParameter('acreedor_ciudad_id') != ''){
			$criteria->addJoin(CiudadPeer::ID,ComunaPeer::CIUDAD_ID);
			$criteria->addJoin(ComunaPeer::ID,TribunalPeer::COMUNA_ID);
			$criteria->add(CiudadPeer::ID,$request->getParameter('acreedor_ciudad_id'));
		}
		
	    if($request->hasParameter('region_id') && $request->getParameter('region_id') != ''){
	    	$criteria->addJoin(RegionPeer::ID,CiudadPeer::REGION_ID);
	    	$criteria->addJoin(CiudadPeer::ID,ComunaPeer::CIUDAD_ID);
	    	$criteria->addJoin(ComunaPeer::ID,TribunalPeer::COMUNA_ID);
			$criteria->add(RegionPeer::ID,$request->getParameter('region_id'));
		}
		
		if($request->hasParameter('nombre') && $request->getParameter('nombre') != ''){
			$criteria->add(TribunalPeer::NOMBRE,'%'.$request->getParameter('nombre').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('comuna') && $request->getParameter('comuna') != ''){
			$criteria->add(TribunalPeer::COMUNA_ID,$request->getParameter('comuna'));
		}
		
		return $criteria;
	}
}
