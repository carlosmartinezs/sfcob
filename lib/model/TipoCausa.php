<?php

/**
 * Subclass for representing a row from the 'tipo_causa' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TipoCausa extends BaseTipoCausa
{
	public function __toString(){
		return $this->getNombre();
	}
}
