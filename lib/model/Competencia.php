<?php

/**
 * Subclass for representing a row from the 'competencia' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Competencia extends BaseCompetencia
{
	public function __toString()
	{
		return $this->getNombre();
	}
}
