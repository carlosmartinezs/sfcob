<?php

/**
 * Subclass for representing a row from the 'causa' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Causa extends BaseCausa
{
	public function __toString()
	{
		return $this->getCaratula();
	}
	
	public function getEstadoCausaActivo()
	{
		$criteria = new Criteria();
		$criteria->add(EstadoCausaPeer::ACTIVO, true);
		$criteria->add(EstadoCausaPeer::CAUSA_ID, $this->getId());
		$estadoCausa = EstadoCausaPeer::doSelectOne($criteria);
		
		return $estadoCausa;
	}
	
	public function getAcreedor(){
		return $this->getContrato()->getAcreedor();
	}
	
	public function getNombreDeudor()
	{
		return $this->getDeudor()->__toString();
	}
	
	public function getNombreMateria()
	{
		$c = new Criteria();
		$c->add(MateriaPeer::ID, $this->getMateriaId());
		$materia = MateriaPeer::doSelectOne($c);
		
		return $materia;
	}
	
	public function getCausaExhortoObj(){
		return CausaPeer::retrieveByPK($this->getCausaExhorto());
	}
	
	public function getUltimaDiligencia(){
		$criteria = new Criteria();
		$criteria->addJoin(PeriodoCausaPeer::CAUSA_ID,CausaPeer::ID);
		$criteria->addJoin(DiligenciaPeer::PERIODO_CAUSA_ID,PeriodoCausaPeer::ID);
		$criteria->add(CausaPeer::ID,$this->getId());
		$criteria->addDescendingOrderByColumn('id');
		return DiligenciaPeer::doSelectOne($criteria);
	}
	
	public function getCausaBase(){
		return $this->getCausaExhortoObj();
	}
	
	public function getTribunalActual(){
		$causaTribunalList = $this->getCausaTribunals();
		foreach($causaTribunalList as $causaTribunal){
			if($causaTribunal->getActivo()){
				return $causaTribunal->getTribunal();
			}
		}
	}
	
	public function getCausaTribunalActual(){
		$causaTribunalList = $this->getCausaTribunals();
		foreach($causaTribunalList as $causaTribunal){
			if($causaTribunal->getActivo()){
				return $causaTribunal;
			}
		}
	}
	
	public function getPeriodoCausaActual(){
		$periodoCausaList = $this->getPeriodoCausas(new Criteria());
		foreach($periodoCausaList as $periodoCausa){
			if($periodoCausa->getActivo()){
				return $periodoCausa;
			}
		}
	}
	
	public function getDeuda()
	{
		$criteria = new Criteria();
		$criteria->add(DeudaPeer::CAUSA_ID,$this->getId());
		return DeudaPeer::doSelectOne($criteria);
	}
	
	public function getTotalDeuda(){
		return $this->getDeudaMonto() + $this->getDeudaInteres() + $this->getDeudaGastosCobranza();
	}
	
	public function getAbonos(){
		return $this->getDeuda()->getAbonos();
	}
	
	public function getDeudaMonto(){
		return $this->getDeuda()->getMonto();
	}

	public function getDeudaInteres(){
		return $this->getDeuda()->getInteres();
	}
	
	public function getDeudaGastosCobranza(){
		return $this->getDeuda()->getGastosCobranza();
	}
	
	public function getTotalAbono(){
		return $this->getAbonoMonto() + $this->getAbonoInteres() + $this->getAbonoGastosCobranza();
	}
	
	public function getAbonoMonto(){
		$abonos = $this->getDeuda()->getAbonosMonto();
		$abonoMonto = 0;
		foreach($abonos as $abono){
			$abonoMonto += $abono->getValor();
		}
		return $abonoMonto;
	}
	
	public function getAbonoInteres(){
		$abonos = $this->getDeuda()->getAbonosInteres();
		$abonoInteres = 0;
		foreach($abonos as $abono){
			$abonoInteres += $abono->getValor();
		}
		return $abonoInteres;
	}
	
	public function getAbonoGastosCobranza(){
		$abonos = $this->getDeuda()->getAbonosGastosCobranza();
		$abonoGastosCobranza = 0;
		foreach($abonos as $abono){
			$abonoGastosCobranza += $abono->getValor();
		}
		return $abonoGastosCobranza;
	}
	
	public function getDeudaPendiente(){
		return $this->getDeudaPendienteMonto() + $this->getDeudaPendienteInteres() + $this->getDeudaPendienteGastosCobranza();
	}
	
	public function getDeudaPendienteMonto(){
		return $this->getDeudaMonto() - $this->getAbonoMonto();
	}
	
	public function getDeudaPendienteInteres(){
		return $this->getDeudaInteres() - $this->getAbonoInteres();
	}
	
	public function getDeudaPendienteGastosCobranza(){
		return $this->getDeudaGastosCobranza() - $this->getAbonoGastosCobranza();
	}
	
	public function getCaratula(){
		return $this->getAcreedor().' con '.$this->getDeudor();
	}
	
	public function getFechaTermino(){
		$c = new Criteria();
    	$c->add(EstadoCausaPeer::CAUSA_ID, $this->getId());
    	$c->add(EstadoCausaPeer::ACTIVO, true);
    	$estado = EstadoCausaPeer::doSelectOne($c);
    	$this->estadoId = $estado->getEstadoId();
    	if($this->estadoId == 2 || $this->estadoId == 4){
    		$this->estadoFechaCambio = $estado->getFecha('d-m-Y');
   		}else{
   			$this->estadoFechaCambio = "";
   		}
   		return $this->estadoFechaCambio;
	}
	
	public function masDeUnMesSinDiligencias()
	{
		if(count($this->getDiligencias()) > 0)
		{
			$ultimaDiligencia = $this->getUltimaDiligencia();
			$ultimaFecha = $ultimaDiligencia->getFecha('Y-m-d');
		}
		else
		{
			$ultimaFecha = $this->getFechaInicio('Y-m-d');
		}
		
		$hoy = strtotime(date('Y-m-d'), 0);
     	$ultimaFecha = strtotime($ultimaFecha, 0);
     	$cuanto = $hoy-$ultimaFecha;
     	$dif = floor($cuanto / 86400);
     	
     	if($dif > 30)
     	{
     		return true;
     	}
     	else
     	{
     		return false;
     	}
	}
	
	public function getDiligencias()
	{
		$c = new Criteria();
		$c->addJoin(CausaPeer::ID,PeriodoCausaPeer::CAUSA_ID);
		$c->addJoin(PeriodoCausaPeer::ID,DiligenciaPeer::PERIODO_CAUSA_ID);
		$c->add(CausaPeer::ID,$this->getId());
		
		return DiligenciaPeer::doSelect($c);
	}
	
	public function getCuantia()
	{
		$deuda = $this->getDeuda();
		$cuantia = $deuda->getMonto() + $deuda->getInteres() + $deuda->getGastosCobranza();
		return $cuantia;
	}
	
	public function estaTerminada()
	{
		if($this->getEstadoCausaActivo()->getEstadoId() == 2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function estaSaldada()
	{
		if($this->getDeuda()->getMonto() == 0 || $this->getDeudaPendiente() > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function honorariosDelMes()
	{
		$porcentaje = $this->getContrato()->getPorcentajeHonorario();
		
		$c = new Criteria();
		$c->add(CausaPeer::ID,$this->getId());
		$c->addJoin(CausaPeer::ID,DeudaPeer::CAUSA_ID);
		$c->addJoin(DeudaPeer::ID,AbonoPeer::DEUDA_ID);
		$c->add(AbonoPeer::FECHA,date('Y-m').'-01',Criteria::GREATER_EQUAL);
		$c->addAnd(AbonoPeer::FECHA,date('Y-m').'-'.Funciones::ultimoDiaMes(date('m'),date('Y')),Criteria::LESS_EQUAL);
		
		$abonoList = AbonoPeer::doSelect($c);
		
		$honorariosMes = 0;
		
		foreach($abonoList as $abono)
		{
			$honorariosMes += $abono->getValor() * $porcentaje / 100;
		}
		
		return floor($honorariosMes);
	}
	
	public function gastosDelMes()
	{
		$c = new Criteria();
		$c->add(CausaPeer::ID,$this->getId());
		$c->addJoin(GastoPeer::DILIGENCIA_ID,DiligenciaPeer::ID);
		$c->addJoin(DiligenciaPeer::PERIODO_CAUSA_ID,PeriodoCausaPeer::ID);
		$c->addJoin(PeriodoCausaPeer::CAUSA_ID,CausaPeer::ID);
		$c->add(GastoPeer::FECHA,date('Y-m').'-01',Criteria::GREATER_EQUAL);
		$c->addAnd(GastoPeer::FECHA,date('Y-m').'-'.Funciones::ultimoDiaMes(date('m'),date('Y')),Criteria::LESS_EQUAL);
		
		$gastoList = GastoPeer::doSelect($c);
		
		$gastosMes = 0;
		
		foreach($gastoList as $gasto)
		{
			$gastosMes += $gasto->getMonto();
		}
		
		return $gastosMes;
		
	}
	
	public function getGastos()
	{
		$c = new Criteria();
		$c->add(CausaPeer::ID,$this->getId());
		$c->addJoin(GastoPeer::DILIGENCIA_ID,DiligenciaPeer::ID);
		$c->addJoin(DiligenciaPeer::PERIODO_CAUSA_ID,PeriodoCausaPeer::ID);
		$c->addJoin(PeriodoCausaPeer::CAUSA_ID,CausaPeer::ID);
		
		$gastoList = GastoPeer::doSelect($c);
		
		$gastos = 0;
		
		foreach($gastoList as $gasto)
		{
			$gastos += $gasto->getMonto();
		}
		
		return $gastos;
	}
}
