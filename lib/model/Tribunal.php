<?php

/**
 * Subclass for representing a row from the 'tribunal' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Tribunal extends BaseTribunal
{
	public function __toString()
	{
		return $this->getNombre().' de '.$this->getComuna();
	}
}
