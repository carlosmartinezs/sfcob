<?php

/**
 * Subclass for representing a row from the 'contrato' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Contrato extends BaseContrato
{
	public function getNombreAcreedor(){
		return $this->getAcreedor()->__toString();
	}
	
	public function __toString(){
		return $this->getNombreAcreedor();
	}
}
