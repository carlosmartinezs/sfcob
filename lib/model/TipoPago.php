<?php

/**
 * Subclass for representing a row from the 'tipo_pago' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TipoPago extends BaseTipoPago
{
	public function __toString(){
		return $this->getNombre();
	}
}
