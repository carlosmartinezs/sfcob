<?php

/**
 * Subclass for representing a row from the 'diligencia' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Diligencia extends BaseDiligencia
{
	public function __toString(){
		return $this->getDescripcion();
	}
}
