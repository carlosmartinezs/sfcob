<?php

/**
 * Subclass for representing a row from the 'telefono_domicilio' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TelefonoDomicilio extends BaseTelefonoDomicilio
{
	public function getDomicilio(){
		return DeudorDomicilioPeer::retrieveByPK($this->getDeudorDomicilioId())->__toString();
	}
	
	public function __toString()
	{
		return $this->getTelefono();
	}
}
