<?php

/**
 * Subclass for representing a row from the 'formato' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Formato extends BaseFormato
{
	public function __toString()
	{
		return $this->getNombre();
	}
	
	public function getTiponombre()
	{
		if($this->getTipo() === false){
			return 'Carta';
		}else{
			return 'Escrito';
		}
	}
}
