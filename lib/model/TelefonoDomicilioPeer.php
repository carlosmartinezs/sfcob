<?php

/**
 * Subclass for performing query and update operations on the 'telefono_domicilio' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TelefonoDomicilioPeer extends BaseTelefonoDomicilioPeer
{
	static public function getByIdDeudorDomicilio($iddeudordomicilio){
		
		$criteria = new Criteria();
		$criteria->add(TelefonoDomicilioPeer::DEUDOR_DOMICILIO_ID, $iddeudordomicilio);
		return TelefonoDomicilioPeer::doSelect($criteria);
	}
}
