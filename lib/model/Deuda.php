<?php

/**
 * Subclass for representing a row from the 'deuda' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Deuda extends BaseDeuda
{
	public function __toString(){
		return $this->getCausa();
	}
	
	public function getAbonosMonto(){
		$criteria = new Criteria();
		$criteria->add(AbonoPeer::TIPO_ABONO_ID,1);
		$criteria->add(AbonoPeer::DEUDA_ID,$this->getId());
		return $this->getAbonos($criteria);
	}
	
	public function getAbonosInteres(){
		$criteria = new Criteria();
		$criteria->add(AbonoPeer::TIPO_ABONO_ID,2);
		$criteria->add(AbonoPeer::DEUDA_ID,$this->getId());
		return $this->getAbonos($criteria);
	}
	
	public function getAbonosGastosCobranza(){
		$criteria = new Criteria();
		$criteria->add(AbonoPeer::TIPO_ABONO_ID,3);
		$criteria->add(AbonoPeer::DEUDA_ID,$this->getId());
		return $this->getAbonos($criteria);
	}
	
	public function getDeudaTotal(){
		return $this->getMonto() + $this->getInteres() + $this->getGastosCobranza();
	}
	public function getTotalPagado(){
		return $this->getAbonosInteres() + $this->getAbonosMonto();
	}
	public function getDeudaPendiente(){
		return $this->getDeudaTotal() - $this->getTotalPagado();
	}
}
