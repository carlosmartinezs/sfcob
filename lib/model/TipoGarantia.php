<?php

/**
 * Subclass for representing a row from the 'tipo_garantia' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TipoGarantia extends BaseTipoGarantia
{
	public function __toString()
	{
		return $this->getNombre();
	}
}
