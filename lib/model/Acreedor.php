<?php

/**
 * Subclass for representing a row from the 'acreedor' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Acreedor extends BaseAcreedor
{
	
  public function __toString()
  {
    return $this->getNombreCompleto();
  }
  
  public function getNombreCompleto()
  {
  	return $this->getNombres().' '.$this->getApellidoPaterno().' '.$this->getApellidoMaterno();
  }
  
  public function tieneContratoActivo(){
  	
  	$criteria = new Criteria();
  	$criteria->add(ContratoPeer::ACREEDOR_ID, $this->getId());
  	$contratoList = ContratoPeer::doSelect($criteria);
  	
  	foreach($contratoList as $contrato)
  	{
	  	if($contrato instanceof Contrato)
	  	{	
	  		if($contrato->getEstado())
	  		{
	  			return true;
	  		}
	  		
	  	}
  	}
  	
  	return false;
  	
  }
  
  public function getContratoActivo(){
  	
  	$criteria = new Criteria();
  	$criteria->add(ContratoPeer::ACREEDOR_ID, $this->getId());
  	$criteria->add(ContratoPeer::ESTADO, true);
  	$contrato = ContratoPeer::doSelectOne($criteria);
  	
  	if($contrato instanceof Contrato)
  	{
  		return $contrato;
  	}
  	else
  	{
  		return null;
  	}
  }
  
  public function getCausas()
  {
  	$c = new Criteria();
  	$c->addJoin(AcreedorPeer::ID,ContratoPeer::ACREEDOR_ID);
  	$c->addJoin(ContratoPeer::ID,CausaPeer::CONTRATO_ID);
  	$c->add(AcreedorPeer::ID,$this->getId());
  	
  	return CausaPeer::doSelect($c);
  }
  
  public function getUltimoContrato()
  {
  	$c = new Criteria();
  	$c->addJoin(AcreedorPeer::ID,ContratoPeer::ACREEDOR_ID);
  	$c->add(AcreedorPeer::ID,$this->getId());
  	$c->addDescendingOrderByColumn(ContratoPeer::FECHA_TERMINO);
  	
  	$contrato = ContratoPeer::doSelectOne($c);
  	
  	return $contrato;
  }
}
