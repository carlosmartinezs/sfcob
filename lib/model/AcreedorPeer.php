<?php

/**
 * Subclass for performing query and update operations on the 'acreedor' table.
 *
 * 
 *
 * @package lib.model
 */ 
class AcreedorPeer extends BaseAcreedorPeer
{
	public static function getCriteria($request)
	{
		$criteria = new Criteria();
		
		if($request->hasParameter('rut_acreedor') && ($request->getParameter('rut_acreedor') != '')){
			$criteria->add(AcreedorPeer::RUT_ACREEDOR,$request->getParameter('rut_acreedor').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('nombres') && ($request->getParameter('nombres') != '')){
			$criteria->add(AcreedorPeer::NOMBRES,'%'.$request->getParameter('nombres').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('apellido_paterno') && ($request->getParameter('apellido_paterno') != '')){
			$criteria->add(AcreedorPeer::APELLIDO_PATERNO,'%'.$request->getParameter('apellido_paterno').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('apellido_materno') && ($request->getParameter('apellido_materno') != '')){
			$criteria->add(AcreedorPeer::APELLIDO_MATERNO,'%'.$request->getParameter('apellido_materno').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('giro') && ($request->getParameter('giro') != '')){
			$criteria->add(AcreedorPeer::GIRO_ID,$request->getParameter('giro'));
		}
		
		if($request->hasParameter('direccion') && ($request->getParameter('direccion') != '')){
			$criteria->add(AcreedorPeer::DIRECCION,'%'.$request->getParameter('direccion').'%',Criteria::LIKE);
		}
		
	    if($request->hasParameter('region_id') && ($request->getParameter('region_id') != '')){
	    	$criteria->addJoin(CiudadPeer::REGION_ID,RegionPeer::ID);
	    	$criteria->addJoin(ComunaPeer::CIUDAD_ID,CiudadPeer::ID);
	    	$criteria->addJoin(AcreedorPeer::COMUNA_ID,ComunaPeer::ID);
			$criteria->add(RegionPeer::ID,$request->getParameter('region_id'));
		}
	    if($request->hasParameter('acreedor_ciudad_id') && ($request->getParameter('acreedor_ciudad_id') != '')){
			$criteria->add(CiudadPeer::ID,$request->getParameter('acreedor_ciudad_id'));
		}
		if($request->hasParameter('comuna') && ($request->getParameter('comuna') != '')){
			$criteria->add(AcreedorPeer::COMUNA_ID,$request->getParameter('comuna'));
		}
		
		if($request->hasParameter('telefono') && ($request->getParameter('telefono') != '')){
			$criteria->add(AcreedorPeer::TELEFONO,'%'.$request->getParameter('telefono').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('fax') && ($request->getParameter('fax') != '')){
			$criteria->add(AcreedorPeer::FAX,'%'.$request->getParameter('fax').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('email') && ($request->getParameter('email') != '')){
			$criteria->add(AcreedorPeer::EMAIL,'%'.$request->getParameter('email').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('rep_legal') && ($request->getParameter('rep_legal') != '')){
			$criteria->add(AcreedorPeer::REP_LEGAL,'%'.$request->getParameter('rep_legal').'%',Criteria::LIKE);
		}

		if($request->hasParameter('rut_rep_legal') && ($request->getParameter('rut_rep_legal') != '')){
			$criteria->add(AcreedorPeer::RUT_REP_LEGAL,'%'.$request->getParameter('rut_rep_legal').'%',Criteria::LIKE);
		}
		
		return $criteria;
	}

}
