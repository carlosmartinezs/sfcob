<?php

/**
 * Subclass for performing query and update operations on the 'deudor' table.
 *
 * 
 *
 * @package lib.model
 */ 
class DeudorPeer extends BaseDeudorPeer
{
	public static function getCriteria($request)
	{
		$criteria = new Criteria();
		
		if($request->hasParameter('rut_deudor') && ($request->getParameter('rut_deudor') != '')){
			$criteria->add(DeudorPeer::RUT_DEUDOR,$request->getParameter('rut_deudor').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('nombres') && ($request->getParameter('nombres') != '')){
			$criteria->add(DeudorPeer::NOMBRES,'%'.$request->getParameter('nombres').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('apellido_paterno') && ($request->getParameter('apellido_paterno') != '')){
			$criteria->add(DeudorPeer::APELLIDO_PATERNO,$request->getParameter('apellido_paterno').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('apellido_materno') && ($request->getParameter('apellido_materno') != '')){
			$criteria->add(DeudorPeer::APELLIDO_MATERNO,$request->getParameter('apellido_materno').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('celular') && ($request->getParameter('celular') != '')){
			$criteria->add(DeudorPeer::CELULAR,$request->getParameter('celular').'%',Criteria::LIKE);
		}
		
		if($request->hasParameter('email') && ($request->getParameter('email') != '')){
			$criteria->add(DeudorPeer::EMAIL,$request->getParameter('email').'%',Criteria::LIKE);
		}
		
		return $criteria;
	}
}
