<?php

/**
 * Subclass for representing a row from the 'tipo_abono' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TipoAbono extends BaseTipoAbono
{
	public function __toString(){
		return $this->getNombre();
	}
}
