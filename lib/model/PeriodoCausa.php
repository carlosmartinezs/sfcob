<?php

/**
 * Subclass for representing a row from the 'periodo_causa' table.
 *
 * 
 *
 * @package lib.model
 */ 
class PeriodoCausa extends BasePeriodoCausa
{
	public function __toString(){
		return $this->getCausa()->__toString() . "  /  " . $this->getPeriodo()->getNombre() . "( " . $this->getFechaInicio() . " )";
	}
}
