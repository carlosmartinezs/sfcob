<?php

/**
 * Subclass for representing a row from the 'clase_giro' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClaseGiro extends BaseClaseGiro
{
	public function __toString(){
		return $this->getNombre();
	}
}
