<?php


/**
 * This class adds structure of 'deudor_domicilio' table to 'propel' DatabaseMap object.
 *
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class DeudorDomicilioMapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.DeudorDomicilioMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('deudor_domicilio');
		$tMap->setPhpName('DeudorDomicilio');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('DEUDOR_ID', 'DeudorId', 'int', CreoleTypes::INTEGER, 'deudor', 'ID', true, null);

		$tMap->addForeignKey('COMUNA_ID', 'ComunaId', 'int', CreoleTypes::INTEGER, 'comuna', 'ID', true, null);

		$tMap->addColumn('CALLE', 'Calle', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('NUMERO', 'Numero', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('DEPTO', 'Depto', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('VILLA', 'Villa', 'string', CreoleTypes::VARCHAR, false, 25);

		$tMap->addColumn('TELEFONO', 'Telefono', 'string', CreoleTypes::VARCHAR, true, 25);

	} // doBuild()

} // DeudorDomicilioMapBuilder
