<?php


/**
 * This class adds structure of 'causa' table to 'propel' DatabaseMap object.
 *
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class CausaMapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.CausaMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('causa');
		$tMap->setPhpName('Causa');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('TIPO_CAUSA_ID', 'TipoCausaId', 'int', CreoleTypes::INTEGER, 'tipo_causa', 'ID', true, null);

		$tMap->addForeignKey('DEUDOR_ID', 'DeudorId', 'int', CreoleTypes::INTEGER, 'deudor', 'ID', true, null);

		$tMap->addForeignKey('MATERIA_ID', 'MateriaId', 'int', CreoleTypes::INTEGER, 'materia', 'ID', true, null);

		$tMap->addForeignKey('CONTRATO_ID', 'ContratoId', 'int', CreoleTypes::INTEGER, 'contrato', 'ID', true, null);

		$tMap->addColumn('ROL', 'Rol', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addForeignKey('COMPETENCIA_ID', 'CompetenciaId', 'int', CreoleTypes::INTEGER, 'competencia', 'ID', true, null);

		$tMap->addColumn('FECHA_INICIO', 'FechaInicio', 'int', CreoleTypes::DATE, true, null);

		$tMap->addColumn('HERENCIA_EXHORTO', 'HerenciaExhorto', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('CAUSA_EXHORTO', 'CausaExhorto', 'int', CreoleTypes::INTEGER, false, null);

	} // doBuild()

} // CausaMapBuilder
