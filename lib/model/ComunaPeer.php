<?php

/**
 * Subclass for performing query and update operations on the 'comuna' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ComunaPeer extends BaseComunaPeer
{
  static public function doSelectByCiudad($ciudad)
  {
    $c= new Criteria();
    $c->add(ComunaPeer::CIUDAD_ID ,$ciudad);
    return  ComunaPeer::doSelect($c);
  }
}
