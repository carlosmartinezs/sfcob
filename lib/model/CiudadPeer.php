<?php

/**
 * Subclass for performing query and update operations on the 'ciudad' table.
 *
 * 
 *
 * @package lib.model
 */ 
class CiudadPeer extends BaseCiudadPeer
{
  static public function doSelectByRegion($region)
  {
    $c= new Criteria();
    $c->add(CiudadPeer::REGION_ID ,$region);
    return  CiudadPeer::doSelect($c);
  }
}
