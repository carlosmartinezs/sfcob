<?php

/**
 * Subclass for representing a row from the 'deudor' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Deudor extends BaseDeudor
{
	
	public function __toString()
  	{
    	return $this->getNombres().' '.$this->getApellidoPaterno().' '.$this->getApellidoMaterno();
  	}
  	
  	public function getTotalDeuda(){
  		return $this->getTotalDeudaMonto() + $this->getTotalDeudaInteres() + $this->getTotalDeudaGastosCobranza();
  	}
  	
  	public function getTotalAbono(){		
  		return $this->getTotalAbonoMonto() + $this->getTotalAbonoInteres() + $this->getTotalAbonoGastosCobranza();
  	}
  	
  	public function getDeudaPendiente()
  	{
  		return $this->getTotalDeuda() - $this->getTotalAbono();
  	}
  	
  	public function getDeudaPendienteMonto()
  	{
  		return $this->getTotalDeudaMonto() - $this->getTotalAbonoMonto();
  	}

  	public function getDeudaPendienteInteres()
  	{
  		return $this->getTotalDeudaInteres() - $this->getTotalAbonoInteres();
  	}
  	
	public function getDeudaPendienteGastosCobranza()
  	{
  		return $this->getTotalDeudaGastosCobranza() - $this->getTotalAbonoGastosCobranza();
  	}
  	
	public function getTotalDeudaMonto()
	{
  		$causasList = $this->getCausas();
  		$deudaTotalMonto = 0;
  		foreach($causasList as $causa)
  		{
  			if(($deuda = $causa->getDeuda()) != null)
  			{
  				$deudaTotalMonto += $deuda->getMonto();
  			}
  		}
  		return $deudaTotalMonto;
	}
  	
	public function getTotalDeudaInteres()
	{
  		$causasList = $this->getCausas();
  		$deudaTotalInteres = 0;
  		foreach($causasList as $causa)
  		{
  			if(($deuda = $causa->getDeuda()))
  			{
  				$deudaTotalInteres += $deuda->getInteres();
  			}
  		}
  		return $deudaTotalInteres;
  	}
  	
	public function getTotalDeudaGastosCobranza()
	{
  		$causasList = $this->getCausas();
  		$deudaTotalGastosCobranza = 0;
  		foreach($causasList as $causa)
  		{
  			if(($deuda = $causa->getDeuda()))
  			{
  				$deudaTotalGastosCobranza += $deuda->getGastosCobranza();
  			}
  		}
  		return $deudaTotalGastosCobranza;
  	}
  	
  	public function getTotalAbonoMonto()
  	{
  		$causasList = $this->getCausas();
  		$abonoTotal = 0;
  		foreach($causasList as $causa)
  		{
  			$abonoTotal += $causa->getAbonoMonto();
  		}
  		return $abonoTotal;
  	}
  	
	public function getTotalAbonoInteres()
	{
  		$causasList = $this->getCausas();
  		$interesTotal = 0;
  		foreach($causasList as $causa)
  		{
  			$interesTotal += $causa->getAbonoInteres();
  		}
  		return $interesTotal;
  	}
  	
  	
  	
	public function getTotalAbonoGastosCobranza()
	{
  		$causasList = $this->getCausas();
  		$interesTotal = 0;
  		foreach($causasList as $causa)
  		{
  			$interesTotal += $causa->getAbonoGastosCobranza();
  		}
  		return $interesTotal;
  	}
  	
  	public function getDomicilio($numero)
  	{
  		$domicilios = $this->getDeudorDomicilios(new Criteria);
  		$cont = count($domicilios);
  		if($cont <= $numero)
  		{
  			return $domicilios[$numero-1];
  		}
  		
  		return null;
  	}
}
