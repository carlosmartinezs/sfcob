<?php

/**
 * Subclass for performing query and update operations on the 'giro' table.
 *
 * 
 *
 * @package lib.model
 */ 
class GiroPeer extends BaseGiroPeer
{
	static public function doSelectByClaseGiro($clasegiro)
  {
    $c= new Criteria();
    $c->add(GiroPeer::CLASE_GIRO_ID ,$clasegiro);
    return  GiroPeer::doSelect($c);
  }
	
}
