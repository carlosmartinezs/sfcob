<?php

/**
 * Subclass for representing a row from the 'estado' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Estado extends BaseEstado
{
	public function __toString()
	{
		return $this->getNombre();
	}
}
