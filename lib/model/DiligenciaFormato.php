<?php

/**
 * Subclass for representing a row from the 'diligencia_formato' table.
 *
 * 
 *
 * @package lib.model
 */ 
class DiligenciaFormato extends BaseDiligenciaFormato
{
	public function getFecha($format = 'd-m-Y'){
		return $this->getDiligencia()->getFecha($format);
	}
}
