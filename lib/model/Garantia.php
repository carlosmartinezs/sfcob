<?php

/**
 * Subclass for representing a row from the 'garantia' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Garantia extends BaseGarantia
{
	public function getNombreCausa(){
		return $this->getCausa()->__toString();
	}
	public function __toString(){
		return $this->getNombreCausa()." ".$this->getNombreTipoGarantia();
	}
	public function getNombreTipoGarantia(){
		return $this->getTipoGarantia()->__toString();
	}
}
