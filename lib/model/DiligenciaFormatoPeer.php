<?php

/**
 * Subclass for performing query and update operations on the 'diligencia_formato' table.
 *
 * 
 *
 * @package lib.model
 */ 
class DiligenciaFormatoPeer extends BaseDiligenciaFormatoPeer
{
	public static function getCriteria($request){
		
		$c = new Criteria();
		
		
		
		if($request->hasParameter('id') && $request->getParameter('id') != null){
			
			$c->add(FormatoPeer::ID,$request->getParameter('id'));
			
		}
		
		if($request->hasParameter('periodo') && $request->getParameter('periodo') != null){
			
			$c->addJoin(PeriodoPeer::ID,PeriodoCausaPeer::PERIODO_ID);
			$c->addJoin(PeriodoCausaPeer::ID,DiligenciaPeer::PERIODO_CAUSA_ID);
			$c->addJoin(DiligenciaPeer::ID,DiligenciaFormatoPeer::DILIGENCIA_ID);
			$c->addJoin(DiligenciaFormatoPeer::FORMATO_ID, FormatoPeer::ID);
			$c->add(PeriodoPeer::ID, $request->getParameter('periodo'));
			
		}
		
		if($request->hasParameter('id_causa') && $request->getParameter('id_causa') != null){
			
			$c->addJoin(PeriodoCausaPeer::CAUSA_ID,CausaPeer::ID);
			$c->add(CausaPeer::ID,$request->getParameter('id_causa'));
			$causa = true;
			
		}
		
		if($request->hasParameter('id_deudor') && $request->getParameter('id_deudor') != null){
			
			if(!$causa){
				$c->addJoin(PeriodoCausaPeer::CAUSA_ID,CausaPeer::ID);
			}
			
			$c->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);
			$c->add(DeudorPeer::ID, $request->getParameter('id_deudor'));
		}
		
		if($request->hasParameter('desde') != null && $request->getParameter('desde') != '--')
		{
			$fecha = $request->getParameter('desde');
			
			$tmp = $fecha['year'];
			$tmp .= '-'.$fecha['month'];
			$tmp .= '-'.$fecha['day'];
			
			if($tmp != '--')
			{
				$c->addJoin(DiligenciaPeer::ID,DiligenciaFormatoPeer::DILIGENCIA_ID);
				$c->addAnd(DiligenciaPeer::FECHA,$tmp,Criteria::GREATER_EQUAL);
			}
		}
		
		if($request->hasParameter('hasta') != null && $request->getParameter('hasta') != '--')
		{
			$fecha = $request->getParameter('hasta');
			
			$tmp = $fecha['year'];
			$tmp .= '-'.$fecha['month'];
			$tmp .= '-'.$fecha['day'];
			
			if($tmp != '--')
			{
				$c->addJoin(DiligenciaPeer::ID,DiligenciaFormatoPeer::DILIGENCIA_ID);
				$c->addAnd(DiligenciaPeer::FECHA,$tmp, Criteria::LESS_EQUAL);
			}
		}
		
		if($request->hasParameter('tipo_formato') && $request->getParameter('tipo_formato') != null){
			$accion = $request->getParameter('tipo_formato');
			
			$c->addJoin(DiligenciaPeer::ID,DiligenciaFormatoPeer::DILIGENCIA_ID);
			$c->addJoin(DiligenciaFormatoPeer::FORMATO_ID, FormatoPeer::ID);
			
			if('cartaenviada' == $accion)
			{
				$c->add(FormatoPeer::TIPO,0);
			}
			else
			{
				$c->add(FormatoPeer::TIPO,1);
			}
		}
		return $c;
	}
}
