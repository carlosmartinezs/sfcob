<?php

/**
 * Base class that represents a row from the 'abono' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseAbono extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        AbonoPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the tipo_abono_id field.
	 * @var        int
	 */
	protected $tipo_abono_id;


	/**
	 * The value for the tipo_pago_id field.
	 * @var        int
	 */
	protected $tipo_pago_id;


	/**
	 * The value for the deuda_id field.
	 * @var        int
	 */
	protected $deuda_id;


	/**
	 * The value for the valor field.
	 * @var        int
	 */
	protected $valor;


	/**
	 * The value for the fecha field.
	 * @var        int
	 */
	protected $fecha;


	/**
	 * The value for the motivo field.
	 * @var        string
	 */
	protected $motivo;


	/**
	 * The value for the nro_recibo field.
	 * @var        int
	 */
	protected $nro_recibo;

	/**
	 * @var        TipoAbono
	 */
	protected $aTipoAbono;

	/**
	 * @var        TipoPago
	 */
	protected $aTipoPago;

	/**
	 * @var        Deuda
	 */
	protected $aDeuda;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [tipo_abono_id] column value.
	 * 
	 * @return     int
	 */
	public function getTipoAbonoId()
	{

		return $this->tipo_abono_id;
	}

	/**
	 * Get the [tipo_pago_id] column value.
	 * 
	 * @return     int
	 */
	public function getTipoPagoId()
	{

		return $this->tipo_pago_id;
	}

	/**
	 * Get the [deuda_id] column value.
	 * 
	 * @return     int
	 */
	public function getDeudaId()
	{

		return $this->deuda_id;
	}

	/**
	 * Get the [valor] column value.
	 * 
	 * @return     int
	 */
	public function getValor()
	{

		return $this->valor;
	}

	/**
	 * Get the [optionally formatted] [fecha] column value.
	 * 
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the integer unix timestamp will be returned.
	 * @return     mixed Formatted date/time value as string or integer unix timestamp (if format is NULL).
	 * @throws     PropelException - if unable to convert the date/time to timestamp.
	 */
	public function getFecha($format = 'Y-m-d')
	{

		if ($this->fecha === null || $this->fecha === '') {
			return null;
		} elseif (!is_int($this->fecha)) {
			// a non-timestamp value was set externally, so we convert it
			$ts = strtotime($this->fecha);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse value of [fecha] as date/time value: " . var_export($this->fecha, true));
			}
		} else {
			$ts = $this->fecha;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	/**
	 * Get the [motivo] column value.
	 * 
	 * @return     string
	 */
	public function getMotivo()
	{

		return $this->motivo;
	}

	/**
	 * Get the [nro_recibo] column value.
	 * 
	 * @return     int
	 */
	public function getNroRecibo()
	{

		return $this->nro_recibo;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = AbonoPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [tipo_abono_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setTipoAbonoId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipo_abono_id !== $v) {
			$this->tipo_abono_id = $v;
			$this->modifiedColumns[] = AbonoPeer::TIPO_ABONO_ID;
		}

		if ($this->aTipoAbono !== null && $this->aTipoAbono->getId() !== $v) {
			$this->aTipoAbono = null;
		}

	} // setTipoAbonoId()

	/**
	 * Set the value of [tipo_pago_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setTipoPagoId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipo_pago_id !== $v) {
			$this->tipo_pago_id = $v;
			$this->modifiedColumns[] = AbonoPeer::TIPO_PAGO_ID;
		}

		if ($this->aTipoPago !== null && $this->aTipoPago->getId() !== $v) {
			$this->aTipoPago = null;
		}

	} // setTipoPagoId()

	/**
	 * Set the value of [deuda_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setDeudaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->deuda_id !== $v) {
			$this->deuda_id = $v;
			$this->modifiedColumns[] = AbonoPeer::DEUDA_ID;
		}

		if ($this->aDeuda !== null && $this->aDeuda->getId() !== $v) {
			$this->aDeuda = null;
		}

	} // setDeudaId()

	/**
	 * Set the value of [valor] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setValor($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->valor !== $v) {
			$this->valor = $v;
			$this->modifiedColumns[] = AbonoPeer::VALOR;
		}

	} // setValor()

	/**
	 * Set the value of [fecha] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setFecha($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse date/time value for [fecha] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->fecha !== $ts) {
			$this->fecha = $ts;
			$this->modifiedColumns[] = AbonoPeer::FECHA;
		}

	} // setFecha()

	/**
	 * Set the value of [motivo] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setMotivo($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->motivo !== $v) {
			$this->motivo = $v;
			$this->modifiedColumns[] = AbonoPeer::MOTIVO;
		}

	} // setMotivo()

	/**
	 * Set the value of [nro_recibo] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setNroRecibo($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nro_recibo !== $v) {
			$this->nro_recibo = $v;
			$this->modifiedColumns[] = AbonoPeer::NRO_RECIBO;
		}

	} // setNroRecibo()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->tipo_abono_id = $rs->getInt($startcol + 1);

			$this->tipo_pago_id = $rs->getInt($startcol + 2);

			$this->deuda_id = $rs->getInt($startcol + 3);

			$this->valor = $rs->getInt($startcol + 4);

			$this->fecha = $rs->getDate($startcol + 5, null);

			$this->motivo = $rs->getString($startcol + 6);

			$this->nro_recibo = $rs->getInt($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 8; // 8 = AbonoPeer::NUM_COLUMNS - AbonoPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Abono object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AbonoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			AbonoPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AbonoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aTipoAbono !== null) {
				if ($this->aTipoAbono->isModified()) {
					$affectedRows += $this->aTipoAbono->save($con);
				}
				$this->setTipoAbono($this->aTipoAbono);
			}

			if ($this->aTipoPago !== null) {
				if ($this->aTipoPago->isModified()) {
					$affectedRows += $this->aTipoPago->save($con);
				}
				$this->setTipoPago($this->aTipoPago);
			}

			if ($this->aDeuda !== null) {
				if ($this->aDeuda->isModified()) {
					$affectedRows += $this->aDeuda->save($con);
				}
				$this->setDeuda($this->aDeuda);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = AbonoPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += AbonoPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aTipoAbono !== null) {
				if (!$this->aTipoAbono->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTipoAbono->getValidationFailures());
				}
			}

			if ($this->aTipoPago !== null) {
				if (!$this->aTipoPago->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTipoPago->getValidationFailures());
				}
			}

			if ($this->aDeuda !== null) {
				if (!$this->aDeuda->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aDeuda->getValidationFailures());
				}
			}


			if (($retval = AbonoPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AbonoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTipoAbonoId();
				break;
			case 2:
				return $this->getTipoPagoId();
				break;
			case 3:
				return $this->getDeudaId();
				break;
			case 4:
				return $this->getValor();
				break;
			case 5:
				return $this->getFecha();
				break;
			case 6:
				return $this->getMotivo();
				break;
			case 7:
				return $this->getNroRecibo();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AbonoPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTipoAbonoId(),
			$keys[2] => $this->getTipoPagoId(),
			$keys[3] => $this->getDeudaId(),
			$keys[4] => $this->getValor(),
			$keys[5] => $this->getFecha(),
			$keys[6] => $this->getMotivo(),
			$keys[7] => $this->getNroRecibo(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AbonoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTipoAbonoId($value);
				break;
			case 2:
				$this->setTipoPagoId($value);
				break;
			case 3:
				$this->setDeudaId($value);
				break;
			case 4:
				$this->setValor($value);
				break;
			case 5:
				$this->setFecha($value);
				break;
			case 6:
				$this->setMotivo($value);
				break;
			case 7:
				$this->setNroRecibo($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AbonoPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTipoAbonoId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTipoPagoId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setDeudaId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setValor($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setFecha($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setMotivo($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setNroRecibo($arr[$keys[7]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(AbonoPeer::DATABASE_NAME);

		if ($this->isColumnModified(AbonoPeer::ID)) $criteria->add(AbonoPeer::ID, $this->id);
		if ($this->isColumnModified(AbonoPeer::TIPO_ABONO_ID)) $criteria->add(AbonoPeer::TIPO_ABONO_ID, $this->tipo_abono_id);
		if ($this->isColumnModified(AbonoPeer::TIPO_PAGO_ID)) $criteria->add(AbonoPeer::TIPO_PAGO_ID, $this->tipo_pago_id);
		if ($this->isColumnModified(AbonoPeer::DEUDA_ID)) $criteria->add(AbonoPeer::DEUDA_ID, $this->deuda_id);
		if ($this->isColumnModified(AbonoPeer::VALOR)) $criteria->add(AbonoPeer::VALOR, $this->valor);
		if ($this->isColumnModified(AbonoPeer::FECHA)) $criteria->add(AbonoPeer::FECHA, $this->fecha);
		if ($this->isColumnModified(AbonoPeer::MOTIVO)) $criteria->add(AbonoPeer::MOTIVO, $this->motivo);
		if ($this->isColumnModified(AbonoPeer::NRO_RECIBO)) $criteria->add(AbonoPeer::NRO_RECIBO, $this->nro_recibo);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(AbonoPeer::DATABASE_NAME);

		$criteria->add(AbonoPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Abono (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTipoAbonoId($this->tipo_abono_id);

		$copyObj->setTipoPagoId($this->tipo_pago_id);

		$copyObj->setDeudaId($this->deuda_id);

		$copyObj->setValor($this->valor);

		$copyObj->setFecha($this->fecha);

		$copyObj->setMotivo($this->motivo);

		$copyObj->setNroRecibo($this->nro_recibo);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Abono Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     AbonoPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new AbonoPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a TipoAbono object.
	 *
	 * @param      TipoAbono $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setTipoAbono($v)
	{


		if ($v === null) {
			$this->setTipoAbonoId(NULL);
		} else {
			$this->setTipoAbonoId($v->getId());
		}


		$this->aTipoAbono = $v;
	}


	/**
	 * Get the associated TipoAbono object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     TipoAbono The associated TipoAbono object.
	 * @throws     PropelException
	 */
	public function getTipoAbono($con = null)
	{
		if ($this->aTipoAbono === null && ($this->tipo_abono_id !== null)) {
			// include the related Peer class
			$this->aTipoAbono = TipoAbonoPeer::retrieveByPK($this->tipo_abono_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = TipoAbonoPeer::retrieveByPK($this->tipo_abono_id, $con);
			   $obj->addTipoAbonos($this);
			 */
		}
		return $this->aTipoAbono;
	}

	/**
	 * Declares an association between this object and a TipoPago object.
	 *
	 * @param      TipoPago $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setTipoPago($v)
	{


		if ($v === null) {
			$this->setTipoPagoId(NULL);
		} else {
			$this->setTipoPagoId($v->getId());
		}


		$this->aTipoPago = $v;
	}


	/**
	 * Get the associated TipoPago object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     TipoPago The associated TipoPago object.
	 * @throws     PropelException
	 */
	public function getTipoPago($con = null)
	{
		if ($this->aTipoPago === null && ($this->tipo_pago_id !== null)) {
			// include the related Peer class
			$this->aTipoPago = TipoPagoPeer::retrieveByPK($this->tipo_pago_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = TipoPagoPeer::retrieveByPK($this->tipo_pago_id, $con);
			   $obj->addTipoPagos($this);
			 */
		}
		return $this->aTipoPago;
	}

	/**
	 * Declares an association between this object and a Deuda object.
	 *
	 * @param      Deuda $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setDeuda($v)
	{


		if ($v === null) {
			$this->setDeudaId(NULL);
		} else {
			$this->setDeudaId($v->getId());
		}


		$this->aDeuda = $v;
	}


	/**
	 * Get the associated Deuda object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Deuda The associated Deuda object.
	 * @throws     PropelException
	 */
	public function getDeuda($con = null)
	{
		if ($this->aDeuda === null && ($this->deuda_id !== null)) {
			// include the related Peer class
			$this->aDeuda = DeudaPeer::retrieveByPK($this->deuda_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = DeudaPeer::retrieveByPK($this->deuda_id, $con);
			   $obj->addDeudas($this);
			 */
		}
		return $this->aDeuda;
	}

} // BaseAbono
