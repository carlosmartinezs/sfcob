<?php

/**
 * Base class that represents a row from the 'deudor_domicilio' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseDeudorDomicilio extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        DeudorDomicilioPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the deudor_id field.
	 * @var        int
	 */
	protected $deudor_id;


	/**
	 * The value for the comuna_id field.
	 * @var        int
	 */
	protected $comuna_id;


	/**
	 * The value for the calle field.
	 * @var        string
	 */
	protected $calle;


	/**
	 * The value for the numero field.
	 * @var        int
	 */
	protected $numero;


	/**
	 * The value for the depto field.
	 * @var        string
	 */
	protected $depto;


	/**
	 * The value for the villa field.
	 * @var        string
	 */
	protected $villa;


	/**
	 * The value for the telefono field.
	 * @var        string
	 */
	protected $telefono;

	/**
	 * @var        Deudor
	 */
	protected $aDeudor;

	/**
	 * @var        Comuna
	 */
	protected $aComuna;

	/**
	 * Collection to store aggregation of collTelefonoDomicilios.
	 * @var        array
	 */
	protected $collTelefonoDomicilios;

	/**
	 * The criteria used to select the current contents of collTelefonoDomicilios.
	 * @var        Criteria
	 */
	protected $lastTelefonoDomicilioCriteria = null;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [deudor_id] column value.
	 * 
	 * @return     int
	 */
	public function getDeudorId()
	{

		return $this->deudor_id;
	}

	/**
	 * Get the [comuna_id] column value.
	 * 
	 * @return     int
	 */
	public function getComunaId()
	{

		return $this->comuna_id;
	}

	/**
	 * Get the [calle] column value.
	 * 
	 * @return     string
	 */
	public function getCalle()
	{

		return $this->calle;
	}

	/**
	 * Get the [numero] column value.
	 * 
	 * @return     int
	 */
	public function getNumero()
	{

		return $this->numero;
	}

	/**
	 * Get the [depto] column value.
	 * 
	 * @return     string
	 */
	public function getDepto()
	{

		return $this->depto;
	}

	/**
	 * Get the [villa] column value.
	 * 
	 * @return     string
	 */
	public function getVilla()
	{

		return $this->villa;
	}

	/**
	 * Get the [telefono] column value.
	 * 
	 * @return     string
	 */
	public function getTelefono()
	{

		return $this->telefono;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = DeudorDomicilioPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [deudor_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setDeudorId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->deudor_id !== $v) {
			$this->deudor_id = $v;
			$this->modifiedColumns[] = DeudorDomicilioPeer::DEUDOR_ID;
		}

		if ($this->aDeudor !== null && $this->aDeudor->getId() !== $v) {
			$this->aDeudor = null;
		}

	} // setDeudorId()

	/**
	 * Set the value of [comuna_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setComunaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->comuna_id !== $v) {
			$this->comuna_id = $v;
			$this->modifiedColumns[] = DeudorDomicilioPeer::COMUNA_ID;
		}

		if ($this->aComuna !== null && $this->aComuna->getId() !== $v) {
			$this->aComuna = null;
		}

	} // setComunaId()

	/**
	 * Set the value of [calle] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setCalle($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->calle !== $v) {
			$this->calle = $v;
			$this->modifiedColumns[] = DeudorDomicilioPeer::CALLE;
		}

	} // setCalle()

	/**
	 * Set the value of [numero] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setNumero($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->numero !== $v) {
			$this->numero = $v;
			$this->modifiedColumns[] = DeudorDomicilioPeer::NUMERO;
		}

	} // setNumero()

	/**
	 * Set the value of [depto] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setDepto($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->depto !== $v) {
			$this->depto = $v;
			$this->modifiedColumns[] = DeudorDomicilioPeer::DEPTO;
		}

	} // setDepto()

	/**
	 * Set the value of [villa] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setVilla($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->villa !== $v) {
			$this->villa = $v;
			$this->modifiedColumns[] = DeudorDomicilioPeer::VILLA;
		}

	} // setVilla()

	/**
	 * Set the value of [telefono] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setTelefono($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->telefono !== $v) {
			$this->telefono = $v;
			$this->modifiedColumns[] = DeudorDomicilioPeer::TELEFONO;
		}

	} // setTelefono()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->deudor_id = $rs->getInt($startcol + 1);

			$this->comuna_id = $rs->getInt($startcol + 2);

			$this->calle = $rs->getString($startcol + 3);

			$this->numero = $rs->getInt($startcol + 4);

			$this->depto = $rs->getString($startcol + 5);

			$this->villa = $rs->getString($startcol + 6);

			$this->telefono = $rs->getString($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 8; // 8 = DeudorDomicilioPeer::NUM_COLUMNS - DeudorDomicilioPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating DeudorDomicilio object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DeudorDomicilioPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			DeudorDomicilioPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DeudorDomicilioPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aDeudor !== null) {
				if ($this->aDeudor->isModified()) {
					$affectedRows += $this->aDeudor->save($con);
				}
				$this->setDeudor($this->aDeudor);
			}

			if ($this->aComuna !== null) {
				if ($this->aComuna->isModified()) {
					$affectedRows += $this->aComuna->save($con);
				}
				$this->setComuna($this->aComuna);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = DeudorDomicilioPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += DeudorDomicilioPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collTelefonoDomicilios !== null) {
				foreach($this->collTelefonoDomicilios as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aDeudor !== null) {
				if (!$this->aDeudor->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aDeudor->getValidationFailures());
				}
			}

			if ($this->aComuna !== null) {
				if (!$this->aComuna->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aComuna->getValidationFailures());
				}
			}


			if (($retval = DeudorDomicilioPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collTelefonoDomicilios !== null) {
					foreach($this->collTelefonoDomicilios as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DeudorDomicilioPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getDeudorId();
				break;
			case 2:
				return $this->getComunaId();
				break;
			case 3:
				return $this->getCalle();
				break;
			case 4:
				return $this->getNumero();
				break;
			case 5:
				return $this->getDepto();
				break;
			case 6:
				return $this->getVilla();
				break;
			case 7:
				return $this->getTelefono();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DeudorDomicilioPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getDeudorId(),
			$keys[2] => $this->getComunaId(),
			$keys[3] => $this->getCalle(),
			$keys[4] => $this->getNumero(),
			$keys[5] => $this->getDepto(),
			$keys[6] => $this->getVilla(),
			$keys[7] => $this->getTelefono(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DeudorDomicilioPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setDeudorId($value);
				break;
			case 2:
				$this->setComunaId($value);
				break;
			case 3:
				$this->setCalle($value);
				break;
			case 4:
				$this->setNumero($value);
				break;
			case 5:
				$this->setDepto($value);
				break;
			case 6:
				$this->setVilla($value);
				break;
			case 7:
				$this->setTelefono($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DeudorDomicilioPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDeudorId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setComunaId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setCalle($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNumero($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setDepto($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setVilla($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTelefono($arr[$keys[7]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(DeudorDomicilioPeer::DATABASE_NAME);

		if ($this->isColumnModified(DeudorDomicilioPeer::ID)) $criteria->add(DeudorDomicilioPeer::ID, $this->id);
		if ($this->isColumnModified(DeudorDomicilioPeer::DEUDOR_ID)) $criteria->add(DeudorDomicilioPeer::DEUDOR_ID, $this->deudor_id);
		if ($this->isColumnModified(DeudorDomicilioPeer::COMUNA_ID)) $criteria->add(DeudorDomicilioPeer::COMUNA_ID, $this->comuna_id);
		if ($this->isColumnModified(DeudorDomicilioPeer::CALLE)) $criteria->add(DeudorDomicilioPeer::CALLE, $this->calle);
		if ($this->isColumnModified(DeudorDomicilioPeer::NUMERO)) $criteria->add(DeudorDomicilioPeer::NUMERO, $this->numero);
		if ($this->isColumnModified(DeudorDomicilioPeer::DEPTO)) $criteria->add(DeudorDomicilioPeer::DEPTO, $this->depto);
		if ($this->isColumnModified(DeudorDomicilioPeer::VILLA)) $criteria->add(DeudorDomicilioPeer::VILLA, $this->villa);
		if ($this->isColumnModified(DeudorDomicilioPeer::TELEFONO)) $criteria->add(DeudorDomicilioPeer::TELEFONO, $this->telefono);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(DeudorDomicilioPeer::DATABASE_NAME);

		$criteria->add(DeudorDomicilioPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of DeudorDomicilio (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setDeudorId($this->deudor_id);

		$copyObj->setComunaId($this->comuna_id);

		$copyObj->setCalle($this->calle);

		$copyObj->setNumero($this->numero);

		$copyObj->setDepto($this->depto);

		$copyObj->setVilla($this->villa);

		$copyObj->setTelefono($this->telefono);


		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach($this->getTelefonoDomicilios() as $relObj) {
				$copyObj->addTelefonoDomicilio($relObj->copy($deepCopy));
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     DeudorDomicilio Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     DeudorDomicilioPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new DeudorDomicilioPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Deudor object.
	 *
	 * @param      Deudor $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setDeudor($v)
	{


		if ($v === null) {
			$this->setDeudorId(NULL);
		} else {
			$this->setDeudorId($v->getId());
		}


		$this->aDeudor = $v;
	}


	/**
	 * Get the associated Deudor object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Deudor The associated Deudor object.
	 * @throws     PropelException
	 */
	public function getDeudor($con = null)
	{
		if ($this->aDeudor === null && ($this->deudor_id !== null)) {
			// include the related Peer class
			$this->aDeudor = DeudorPeer::retrieveByPK($this->deudor_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = DeudorPeer::retrieveByPK($this->deudor_id, $con);
			   $obj->addDeudors($this);
			 */
		}
		return $this->aDeudor;
	}

	/**
	 * Declares an association between this object and a Comuna object.
	 *
	 * @param      Comuna $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setComuna($v)
	{


		if ($v === null) {
			$this->setComunaId(NULL);
		} else {
			$this->setComunaId($v->getId());
		}


		$this->aComuna = $v;
	}


	/**
	 * Get the associated Comuna object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Comuna The associated Comuna object.
	 * @throws     PropelException
	 */
	public function getComuna($con = null)
	{
		if ($this->aComuna === null && ($this->comuna_id !== null)) {
			// include the related Peer class
			$this->aComuna = ComunaPeer::retrieveByPK($this->comuna_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = ComunaPeer::retrieveByPK($this->comuna_id, $con);
			   $obj->addComunas($this);
			 */
		}
		return $this->aComuna;
	}

	/**
	 * Temporary storage of collTelefonoDomicilios to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initTelefonoDomicilios()
	{
		if ($this->collTelefonoDomicilios === null) {
			$this->collTelefonoDomicilios = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this DeudorDomicilio has previously
	 * been saved, it will retrieve related TelefonoDomicilios from storage.
	 * If this DeudorDomicilio is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getTelefonoDomicilios($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTelefonoDomicilios === null) {
			if ($this->isNew()) {
			   $this->collTelefonoDomicilios = array();
			} else {

				$criteria->add(TelefonoDomicilioPeer::DEUDOR_DOMICILIO_ID, $this->getId());

				TelefonoDomicilioPeer::addSelectColumns($criteria);
				$this->collTelefonoDomicilios = TelefonoDomicilioPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(TelefonoDomicilioPeer::DEUDOR_DOMICILIO_ID, $this->getId());

				TelefonoDomicilioPeer::addSelectColumns($criteria);
				if (!isset($this->lastTelefonoDomicilioCriteria) || !$this->lastTelefonoDomicilioCriteria->equals($criteria)) {
					$this->collTelefonoDomicilios = TelefonoDomicilioPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTelefonoDomicilioCriteria = $criteria;
		return $this->collTelefonoDomicilios;
	}

	/**
	 * Returns the number of related TelefonoDomicilios.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countTelefonoDomicilios($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TelefonoDomicilioPeer::DEUDOR_DOMICILIO_ID, $this->getId());

		return TelefonoDomicilioPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a TelefonoDomicilio object to this object
	 * through the TelefonoDomicilio foreign key attribute
	 *
	 * @param      TelefonoDomicilio $l TelefonoDomicilio
	 * @return     void
	 * @throws     PropelException
	 */
	public function addTelefonoDomicilio(TelefonoDomicilio $l)
	{
		$this->collTelefonoDomicilios[] = $l;
		$l->setDeudorDomicilio($this);
	}

} // BaseDeudorDomicilio
