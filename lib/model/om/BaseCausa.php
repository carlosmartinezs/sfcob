<?php

/**
 * Base class that represents a row from the 'causa' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseCausa extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        CausaPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the tipo_causa_id field.
	 * @var        int
	 */
	protected $tipo_causa_id;


	/**
	 * The value for the deudor_id field.
	 * @var        int
	 */
	protected $deudor_id;


	/**
	 * The value for the materia_id field.
	 * @var        int
	 */
	protected $materia_id;


	/**
	 * The value for the contrato_id field.
	 * @var        int
	 */
	protected $contrato_id;


	/**
	 * The value for the rol field.
	 * @var        string
	 */
	protected $rol;


	/**
	 * The value for the competencia_id field.
	 * @var        int
	 */
	protected $competencia_id;


	/**
	 * The value for the fecha_inicio field.
	 * @var        int
	 */
	protected $fecha_inicio;


	/**
	 * The value for the herencia_exhorto field.
	 * @var        boolean
	 */
	protected $herencia_exhorto;


	/**
	 * The value for the causa_exhorto field.
	 * @var        int
	 */
	protected $causa_exhorto;

	/**
	 * @var        TipoCausa
	 */
	protected $aTipoCausa;

	/**
	 * @var        Deudor
	 */
	protected $aDeudor;

	/**
	 * @var        Materia
	 */
	protected $aMateria;

	/**
	 * @var        Contrato
	 */
	protected $aContrato;

	/**
	 * @var        Competencia
	 */
	protected $aCompetencia;

	/**
	 * Collection to store aggregation of collDocumentos.
	 * @var        array
	 */
	protected $collDocumentos;

	/**
	 * The criteria used to select the current contents of collDocumentos.
	 * @var        Criteria
	 */
	protected $lastDocumentoCriteria = null;

	/**
	 * Collection to store aggregation of collDeudas.
	 * @var        array
	 */
	protected $collDeudas;

	/**
	 * The criteria used to select the current contents of collDeudas.
	 * @var        Criteria
	 */
	protected $lastDeudaCriteria = null;

	/**
	 * Collection to store aggregation of collEstadoCausas.
	 * @var        array
	 */
	protected $collEstadoCausas;

	/**
	 * The criteria used to select the current contents of collEstadoCausas.
	 * @var        Criteria
	 */
	protected $lastEstadoCausaCriteria = null;

	/**
	 * Collection to store aggregation of collPeriodoCausas.
	 * @var        array
	 */
	protected $collPeriodoCausas;

	/**
	 * The criteria used to select the current contents of collPeriodoCausas.
	 * @var        Criteria
	 */
	protected $lastPeriodoCausaCriteria = null;

	/**
	 * Collection to store aggregation of collCausaTribunals.
	 * @var        array
	 */
	protected $collCausaTribunals;

	/**
	 * The criteria used to select the current contents of collCausaTribunals.
	 * @var        Criteria
	 */
	protected $lastCausaTribunalCriteria = null;

	/**
	 * Collection to store aggregation of collGarantias.
	 * @var        array
	 */
	protected $collGarantias;

	/**
	 * The criteria used to select the current contents of collGarantias.
	 * @var        Criteria
	 */
	protected $lastGarantiaCriteria = null;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [tipo_causa_id] column value.
	 * 
	 * @return     int
	 */
	public function getTipoCausaId()
	{

		return $this->tipo_causa_id;
	}

	/**
	 * Get the [deudor_id] column value.
	 * 
	 * @return     int
	 */
	public function getDeudorId()
	{

		return $this->deudor_id;
	}

	/**
	 * Get the [materia_id] column value.
	 * 
	 * @return     int
	 */
	public function getMateriaId()
	{

		return $this->materia_id;
	}

	/**
	 * Get the [contrato_id] column value.
	 * 
	 * @return     int
	 */
	public function getContratoId()
	{

		return $this->contrato_id;
	}

	/**
	 * Get the [rol] column value.
	 * 
	 * @return     string
	 */
	public function getRol()
	{

		return $this->rol;
	}

	/**
	 * Get the [competencia_id] column value.
	 * 
	 * @return     int
	 */
	public function getCompetenciaId()
	{

		return $this->competencia_id;
	}

	/**
	 * Get the [optionally formatted] [fecha_inicio] column value.
	 * 
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the integer unix timestamp will be returned.
	 * @return     mixed Formatted date/time value as string or integer unix timestamp (if format is NULL).
	 * @throws     PropelException - if unable to convert the date/time to timestamp.
	 */
	public function getFechaInicio($format = 'Y-m-d')
	{

		if ($this->fecha_inicio === null || $this->fecha_inicio === '') {
			return null;
		} elseif (!is_int($this->fecha_inicio)) {
			// a non-timestamp value was set externally, so we convert it
			$ts = strtotime($this->fecha_inicio);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse value of [fecha_inicio] as date/time value: " . var_export($this->fecha_inicio, true));
			}
		} else {
			$ts = $this->fecha_inicio;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	/**
	 * Get the [herencia_exhorto] column value.
	 * 
	 * @return     boolean
	 */
	public function getHerenciaExhorto()
	{

		return $this->herencia_exhorto;
	}

	/**
	 * Get the [causa_exhorto] column value.
	 * 
	 * @return     int
	 */
	public function getCausaExhorto()
	{

		return $this->causa_exhorto;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = CausaPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [tipo_causa_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setTipoCausaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipo_causa_id !== $v) {
			$this->tipo_causa_id = $v;
			$this->modifiedColumns[] = CausaPeer::TIPO_CAUSA_ID;
		}

		if ($this->aTipoCausa !== null && $this->aTipoCausa->getId() !== $v) {
			$this->aTipoCausa = null;
		}

	} // setTipoCausaId()

	/**
	 * Set the value of [deudor_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setDeudorId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->deudor_id !== $v) {
			$this->deudor_id = $v;
			$this->modifiedColumns[] = CausaPeer::DEUDOR_ID;
		}

		if ($this->aDeudor !== null && $this->aDeudor->getId() !== $v) {
			$this->aDeudor = null;
		}

	} // setDeudorId()

	/**
	 * Set the value of [materia_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setMateriaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->materia_id !== $v) {
			$this->materia_id = $v;
			$this->modifiedColumns[] = CausaPeer::MATERIA_ID;
		}

		if ($this->aMateria !== null && $this->aMateria->getId() !== $v) {
			$this->aMateria = null;
		}

	} // setMateriaId()

	/**
	 * Set the value of [contrato_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setContratoId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->contrato_id !== $v) {
			$this->contrato_id = $v;
			$this->modifiedColumns[] = CausaPeer::CONTRATO_ID;
		}

		if ($this->aContrato !== null && $this->aContrato->getId() !== $v) {
			$this->aContrato = null;
		}

	} // setContratoId()

	/**
	 * Set the value of [rol] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setRol($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rol !== $v) {
			$this->rol = $v;
			$this->modifiedColumns[] = CausaPeer::ROL;
		}

	} // setRol()

	/**
	 * Set the value of [competencia_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setCompetenciaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->competencia_id !== $v) {
			$this->competencia_id = $v;
			$this->modifiedColumns[] = CausaPeer::COMPETENCIA_ID;
		}

		if ($this->aCompetencia !== null && $this->aCompetencia->getId() !== $v) {
			$this->aCompetencia = null;
		}

	} // setCompetenciaId()

	/**
	 * Set the value of [fecha_inicio] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setFechaInicio($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse date/time value for [fecha_inicio] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->fecha_inicio !== $ts) {
			$this->fecha_inicio = $ts;
			$this->modifiedColumns[] = CausaPeer::FECHA_INICIO;
		}

	} // setFechaInicio()

	/**
	 * Set the value of [herencia_exhorto] column.
	 * 
	 * @param      boolean $v new value
	 * @return     void
	 */
	public function setHerenciaExhorto($v)
	{

		if ($this->herencia_exhorto !== $v) {
			$this->herencia_exhorto = $v;
			$this->modifiedColumns[] = CausaPeer::HERENCIA_EXHORTO;
		}

	} // setHerenciaExhorto()

	/**
	 * Set the value of [causa_exhorto] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setCausaExhorto($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->causa_exhorto !== $v) {
			$this->causa_exhorto = $v;
			$this->modifiedColumns[] = CausaPeer::CAUSA_EXHORTO;
		}

	} // setCausaExhorto()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->tipo_causa_id = $rs->getInt($startcol + 1);

			$this->deudor_id = $rs->getInt($startcol + 2);

			$this->materia_id = $rs->getInt($startcol + 3);

			$this->contrato_id = $rs->getInt($startcol + 4);

			$this->rol = $rs->getString($startcol + 5);

			$this->competencia_id = $rs->getInt($startcol + 6);

			$this->fecha_inicio = $rs->getDate($startcol + 7, null);

			$this->herencia_exhorto = $rs->getBoolean($startcol + 8);

			$this->causa_exhorto = $rs->getInt($startcol + 9);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 10; // 10 = CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Causa object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CausaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			CausaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CausaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aTipoCausa !== null) {
				if ($this->aTipoCausa->isModified()) {
					$affectedRows += $this->aTipoCausa->save($con);
				}
				$this->setTipoCausa($this->aTipoCausa);
			}

			if ($this->aDeudor !== null) {
				if ($this->aDeudor->isModified()) {
					$affectedRows += $this->aDeudor->save($con);
				}
				$this->setDeudor($this->aDeudor);
			}

			if ($this->aMateria !== null) {
				if ($this->aMateria->isModified()) {
					$affectedRows += $this->aMateria->save($con);
				}
				$this->setMateria($this->aMateria);
			}

			if ($this->aContrato !== null) {
				if ($this->aContrato->isModified()) {
					$affectedRows += $this->aContrato->save($con);
				}
				$this->setContrato($this->aContrato);
			}

			if ($this->aCompetencia !== null) {
				if ($this->aCompetencia->isModified()) {
					$affectedRows += $this->aCompetencia->save($con);
				}
				$this->setCompetencia($this->aCompetencia);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = CausaPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += CausaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collDocumentos !== null) {
				foreach($this->collDocumentos as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collDeudas !== null) {
				foreach($this->collDeudas as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collEstadoCausas !== null) {
				foreach($this->collEstadoCausas as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collPeriodoCausas !== null) {
				foreach($this->collPeriodoCausas as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collCausaTribunals !== null) {
				foreach($this->collCausaTribunals as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collGarantias !== null) {
				foreach($this->collGarantias as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aTipoCausa !== null) {
				if (!$this->aTipoCausa->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTipoCausa->getValidationFailures());
				}
			}

			if ($this->aDeudor !== null) {
				if (!$this->aDeudor->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aDeudor->getValidationFailures());
				}
			}

			if ($this->aMateria !== null) {
				if (!$this->aMateria->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aMateria->getValidationFailures());
				}
			}

			if ($this->aContrato !== null) {
				if (!$this->aContrato->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aContrato->getValidationFailures());
				}
			}

			if ($this->aCompetencia !== null) {
				if (!$this->aCompetencia->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCompetencia->getValidationFailures());
				}
			}


			if (($retval = CausaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collDocumentos !== null) {
					foreach($this->collDocumentos as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collDeudas !== null) {
					foreach($this->collDeudas as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collEstadoCausas !== null) {
					foreach($this->collEstadoCausas as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collPeriodoCausas !== null) {
					foreach($this->collPeriodoCausas as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collCausaTribunals !== null) {
					foreach($this->collCausaTribunals as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collGarantias !== null) {
					foreach($this->collGarantias as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CausaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTipoCausaId();
				break;
			case 2:
				return $this->getDeudorId();
				break;
			case 3:
				return $this->getMateriaId();
				break;
			case 4:
				return $this->getContratoId();
				break;
			case 5:
				return $this->getRol();
				break;
			case 6:
				return $this->getCompetenciaId();
				break;
			case 7:
				return $this->getFechaInicio();
				break;
			case 8:
				return $this->getHerenciaExhorto();
				break;
			case 9:
				return $this->getCausaExhorto();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CausaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTipoCausaId(),
			$keys[2] => $this->getDeudorId(),
			$keys[3] => $this->getMateriaId(),
			$keys[4] => $this->getContratoId(),
			$keys[5] => $this->getRol(),
			$keys[6] => $this->getCompetenciaId(),
			$keys[7] => $this->getFechaInicio(),
			$keys[8] => $this->getHerenciaExhorto(),
			$keys[9] => $this->getCausaExhorto(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CausaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTipoCausaId($value);
				break;
			case 2:
				$this->setDeudorId($value);
				break;
			case 3:
				$this->setMateriaId($value);
				break;
			case 4:
				$this->setContratoId($value);
				break;
			case 5:
				$this->setRol($value);
				break;
			case 6:
				$this->setCompetenciaId($value);
				break;
			case 7:
				$this->setFechaInicio($value);
				break;
			case 8:
				$this->setHerenciaExhorto($value);
				break;
			case 9:
				$this->setCausaExhorto($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CausaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTipoCausaId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setDeudorId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setMateriaId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setContratoId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setRol($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setCompetenciaId($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setFechaInicio($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setHerenciaExhorto($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setCausaExhorto($arr[$keys[9]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(CausaPeer::DATABASE_NAME);

		if ($this->isColumnModified(CausaPeer::ID)) $criteria->add(CausaPeer::ID, $this->id);
		if ($this->isColumnModified(CausaPeer::TIPO_CAUSA_ID)) $criteria->add(CausaPeer::TIPO_CAUSA_ID, $this->tipo_causa_id);
		if ($this->isColumnModified(CausaPeer::DEUDOR_ID)) $criteria->add(CausaPeer::DEUDOR_ID, $this->deudor_id);
		if ($this->isColumnModified(CausaPeer::MATERIA_ID)) $criteria->add(CausaPeer::MATERIA_ID, $this->materia_id);
		if ($this->isColumnModified(CausaPeer::CONTRATO_ID)) $criteria->add(CausaPeer::CONTRATO_ID, $this->contrato_id);
		if ($this->isColumnModified(CausaPeer::ROL)) $criteria->add(CausaPeer::ROL, $this->rol);
		if ($this->isColumnModified(CausaPeer::COMPETENCIA_ID)) $criteria->add(CausaPeer::COMPETENCIA_ID, $this->competencia_id);
		if ($this->isColumnModified(CausaPeer::FECHA_INICIO)) $criteria->add(CausaPeer::FECHA_INICIO, $this->fecha_inicio);
		if ($this->isColumnModified(CausaPeer::HERENCIA_EXHORTO)) $criteria->add(CausaPeer::HERENCIA_EXHORTO, $this->herencia_exhorto);
		if ($this->isColumnModified(CausaPeer::CAUSA_EXHORTO)) $criteria->add(CausaPeer::CAUSA_EXHORTO, $this->causa_exhorto);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(CausaPeer::DATABASE_NAME);

		$criteria->add(CausaPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Causa (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTipoCausaId($this->tipo_causa_id);

		$copyObj->setDeudorId($this->deudor_id);

		$copyObj->setMateriaId($this->materia_id);

		$copyObj->setContratoId($this->contrato_id);

		$copyObj->setRol($this->rol);

		$copyObj->setCompetenciaId($this->competencia_id);

		$copyObj->setFechaInicio($this->fecha_inicio);

		$copyObj->setHerenciaExhorto($this->herencia_exhorto);

		$copyObj->setCausaExhorto($this->causa_exhorto);


		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach($this->getDocumentos() as $relObj) {
				$copyObj->addDocumento($relObj->copy($deepCopy));
			}

			foreach($this->getDeudas() as $relObj) {
				$copyObj->addDeuda($relObj->copy($deepCopy));
			}

			foreach($this->getEstadoCausas() as $relObj) {
				$copyObj->addEstadoCausa($relObj->copy($deepCopy));
			}

			foreach($this->getPeriodoCausas() as $relObj) {
				$copyObj->addPeriodoCausa($relObj->copy($deepCopy));
			}

			foreach($this->getCausaTribunals() as $relObj) {
				$copyObj->addCausaTribunal($relObj->copy($deepCopy));
			}

			foreach($this->getGarantias() as $relObj) {
				$copyObj->addGarantia($relObj->copy($deepCopy));
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Causa Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     CausaPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new CausaPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a TipoCausa object.
	 *
	 * @param      TipoCausa $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setTipoCausa($v)
	{


		if ($v === null) {
			$this->setTipoCausaId(NULL);
		} else {
			$this->setTipoCausaId($v->getId());
		}


		$this->aTipoCausa = $v;
	}


	/**
	 * Get the associated TipoCausa object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     TipoCausa The associated TipoCausa object.
	 * @throws     PropelException
	 */
	public function getTipoCausa($con = null)
	{
		if ($this->aTipoCausa === null && ($this->tipo_causa_id !== null)) {
			// include the related Peer class
			$this->aTipoCausa = TipoCausaPeer::retrieveByPK($this->tipo_causa_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = TipoCausaPeer::retrieveByPK($this->tipo_causa_id, $con);
			   $obj->addTipoCausas($this);
			 */
		}
		return $this->aTipoCausa;
	}

	/**
	 * Declares an association between this object and a Deudor object.
	 *
	 * @param      Deudor $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setDeudor($v)
	{


		if ($v === null) {
			$this->setDeudorId(NULL);
		} else {
			$this->setDeudorId($v->getId());
		}


		$this->aDeudor = $v;
	}


	/**
	 * Get the associated Deudor object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Deudor The associated Deudor object.
	 * @throws     PropelException
	 */
	public function getDeudor($con = null)
	{
		if ($this->aDeudor === null && ($this->deudor_id !== null)) {
			// include the related Peer class
			$this->aDeudor = DeudorPeer::retrieveByPK($this->deudor_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = DeudorPeer::retrieveByPK($this->deudor_id, $con);
			   $obj->addDeudors($this);
			 */
		}
		return $this->aDeudor;
	}

	/**
	 * Declares an association between this object and a Materia object.
	 *
	 * @param      Materia $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setMateria($v)
	{


		if ($v === null) {
			$this->setMateriaId(NULL);
		} else {
			$this->setMateriaId($v->getId());
		}


		$this->aMateria = $v;
	}


	/**
	 * Get the associated Materia object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Materia The associated Materia object.
	 * @throws     PropelException
	 */
	public function getMateria($con = null)
	{
		if ($this->aMateria === null && ($this->materia_id !== null)) {
			// include the related Peer class
			$this->aMateria = MateriaPeer::retrieveByPK($this->materia_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = MateriaPeer::retrieveByPK($this->materia_id, $con);
			   $obj->addMaterias($this);
			 */
		}
		return $this->aMateria;
	}

	/**
	 * Declares an association between this object and a Contrato object.
	 *
	 * @param      Contrato $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setContrato($v)
	{


		if ($v === null) {
			$this->setContratoId(NULL);
		} else {
			$this->setContratoId($v->getId());
		}


		$this->aContrato = $v;
	}


	/**
	 * Get the associated Contrato object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Contrato The associated Contrato object.
	 * @throws     PropelException
	 */
	public function getContrato($con = null)
	{
		if ($this->aContrato === null && ($this->contrato_id !== null)) {
			// include the related Peer class
			$this->aContrato = ContratoPeer::retrieveByPK($this->contrato_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = ContratoPeer::retrieveByPK($this->contrato_id, $con);
			   $obj->addContratos($this);
			 */
		}
		return $this->aContrato;
	}

	/**
	 * Declares an association between this object and a Competencia object.
	 *
	 * @param      Competencia $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setCompetencia($v)
	{


		if ($v === null) {
			$this->setCompetenciaId(NULL);
		} else {
			$this->setCompetenciaId($v->getId());
		}


		$this->aCompetencia = $v;
	}


	/**
	 * Get the associated Competencia object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Competencia The associated Competencia object.
	 * @throws     PropelException
	 */
	public function getCompetencia($con = null)
	{
		if ($this->aCompetencia === null && ($this->competencia_id !== null)) {
			// include the related Peer class
			$this->aCompetencia = CompetenciaPeer::retrieveByPK($this->competencia_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = CompetenciaPeer::retrieveByPK($this->competencia_id, $con);
			   $obj->addCompetencias($this);
			 */
		}
		return $this->aCompetencia;
	}

	/**
	 * Temporary storage of collDocumentos to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initDocumentos()
	{
		if ($this->collDocumentos === null) {
			$this->collDocumentos = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa has previously
	 * been saved, it will retrieve related Documentos from storage.
	 * If this Causa is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getDocumentos($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collDocumentos === null) {
			if ($this->isNew()) {
			   $this->collDocumentos = array();
			} else {

				$criteria->add(DocumentoPeer::CAUSA_ID, $this->getId());

				DocumentoPeer::addSelectColumns($criteria);
				$this->collDocumentos = DocumentoPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(DocumentoPeer::CAUSA_ID, $this->getId());

				DocumentoPeer::addSelectColumns($criteria);
				if (!isset($this->lastDocumentoCriteria) || !$this->lastDocumentoCriteria->equals($criteria)) {
					$this->collDocumentos = DocumentoPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastDocumentoCriteria = $criteria;
		return $this->collDocumentos;
	}

	/**
	 * Returns the number of related Documentos.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countDocumentos($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(DocumentoPeer::CAUSA_ID, $this->getId());

		return DocumentoPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a Documento object to this object
	 * through the Documento foreign key attribute
	 *
	 * @param      Documento $l Documento
	 * @return     void
	 * @throws     PropelException
	 */
	public function addDocumento(Documento $l)
	{
		$this->collDocumentos[] = $l;
		$l->setCausa($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa is new, it will return
	 * an empty collection; or if this Causa has previously
	 * been saved, it will retrieve related Documentos from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Causa.
	 */
	public function getDocumentosJoinTipoDocumento($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collDocumentos === null) {
			if ($this->isNew()) {
				$this->collDocumentos = array();
			} else {

				$criteria->add(DocumentoPeer::CAUSA_ID, $this->getId());

				$this->collDocumentos = DocumentoPeer::doSelectJoinTipoDocumento($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(DocumentoPeer::CAUSA_ID, $this->getId());

			if (!isset($this->lastDocumentoCriteria) || !$this->lastDocumentoCriteria->equals($criteria)) {
				$this->collDocumentos = DocumentoPeer::doSelectJoinTipoDocumento($criteria, $con);
			}
		}
		$this->lastDocumentoCriteria = $criteria;

		return $this->collDocumentos;
	}

	/**
	 * Temporary storage of collDeudas to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initDeudas()
	{
		if ($this->collDeudas === null) {
			$this->collDeudas = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa has previously
	 * been saved, it will retrieve related Deudas from storage.
	 * If this Causa is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getDeudas($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collDeudas === null) {
			if ($this->isNew()) {
			   $this->collDeudas = array();
			} else {

				$criteria->add(DeudaPeer::CAUSA_ID, $this->getId());

				DeudaPeer::addSelectColumns($criteria);
				$this->collDeudas = DeudaPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(DeudaPeer::CAUSA_ID, $this->getId());

				DeudaPeer::addSelectColumns($criteria);
				if (!isset($this->lastDeudaCriteria) || !$this->lastDeudaCriteria->equals($criteria)) {
					$this->collDeudas = DeudaPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastDeudaCriteria = $criteria;
		return $this->collDeudas;
	}

	/**
	 * Returns the number of related Deudas.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countDeudas($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(DeudaPeer::CAUSA_ID, $this->getId());

		return DeudaPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a Deuda object to this object
	 * through the Deuda foreign key attribute
	 *
	 * @param      Deuda $l Deuda
	 * @return     void
	 * @throws     PropelException
	 */
	public function addDeuda(Deuda $l)
	{
		$this->collDeudas[] = $l;
		$l->setCausa($this);
	}

	/**
	 * Temporary storage of collEstadoCausas to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initEstadoCausas()
	{
		if ($this->collEstadoCausas === null) {
			$this->collEstadoCausas = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa has previously
	 * been saved, it will retrieve related EstadoCausas from storage.
	 * If this Causa is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getEstadoCausas($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collEstadoCausas === null) {
			if ($this->isNew()) {
			   $this->collEstadoCausas = array();
			} else {

				$criteria->add(EstadoCausaPeer::CAUSA_ID, $this->getId());

				EstadoCausaPeer::addSelectColumns($criteria);
				$this->collEstadoCausas = EstadoCausaPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(EstadoCausaPeer::CAUSA_ID, $this->getId());

				EstadoCausaPeer::addSelectColumns($criteria);
				if (!isset($this->lastEstadoCausaCriteria) || !$this->lastEstadoCausaCriteria->equals($criteria)) {
					$this->collEstadoCausas = EstadoCausaPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastEstadoCausaCriteria = $criteria;
		return $this->collEstadoCausas;
	}

	/**
	 * Returns the number of related EstadoCausas.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countEstadoCausas($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(EstadoCausaPeer::CAUSA_ID, $this->getId());

		return EstadoCausaPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a EstadoCausa object to this object
	 * through the EstadoCausa foreign key attribute
	 *
	 * @param      EstadoCausa $l EstadoCausa
	 * @return     void
	 * @throws     PropelException
	 */
	public function addEstadoCausa(EstadoCausa $l)
	{
		$this->collEstadoCausas[] = $l;
		$l->setCausa($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa is new, it will return
	 * an empty collection; or if this Causa has previously
	 * been saved, it will retrieve related EstadoCausas from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Causa.
	 */
	public function getEstadoCausasJoinEstado($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collEstadoCausas === null) {
			if ($this->isNew()) {
				$this->collEstadoCausas = array();
			} else {

				$criteria->add(EstadoCausaPeer::CAUSA_ID, $this->getId());

				$this->collEstadoCausas = EstadoCausaPeer::doSelectJoinEstado($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(EstadoCausaPeer::CAUSA_ID, $this->getId());

			if (!isset($this->lastEstadoCausaCriteria) || !$this->lastEstadoCausaCriteria->equals($criteria)) {
				$this->collEstadoCausas = EstadoCausaPeer::doSelectJoinEstado($criteria, $con);
			}
		}
		$this->lastEstadoCausaCriteria = $criteria;

		return $this->collEstadoCausas;
	}

	/**
	 * Temporary storage of collPeriodoCausas to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initPeriodoCausas()
	{
		if ($this->collPeriodoCausas === null) {
			$this->collPeriodoCausas = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa has previously
	 * been saved, it will retrieve related PeriodoCausas from storage.
	 * If this Causa is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getPeriodoCausas($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPeriodoCausas === null) {
			if ($this->isNew()) {
			   $this->collPeriodoCausas = array();
			} else {

				$criteria->add(PeriodoCausaPeer::CAUSA_ID, $this->getId());

				PeriodoCausaPeer::addSelectColumns($criteria);
				$this->collPeriodoCausas = PeriodoCausaPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(PeriodoCausaPeer::CAUSA_ID, $this->getId());

				PeriodoCausaPeer::addSelectColumns($criteria);
				if (!isset($this->lastPeriodoCausaCriteria) || !$this->lastPeriodoCausaCriteria->equals($criteria)) {
					$this->collPeriodoCausas = PeriodoCausaPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastPeriodoCausaCriteria = $criteria;
		return $this->collPeriodoCausas;
	}

	/**
	 * Returns the number of related PeriodoCausas.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countPeriodoCausas($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(PeriodoCausaPeer::CAUSA_ID, $this->getId());

		return PeriodoCausaPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a PeriodoCausa object to this object
	 * through the PeriodoCausa foreign key attribute
	 *
	 * @param      PeriodoCausa $l PeriodoCausa
	 * @return     void
	 * @throws     PropelException
	 */
	public function addPeriodoCausa(PeriodoCausa $l)
	{
		$this->collPeriodoCausas[] = $l;
		$l->setCausa($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa is new, it will return
	 * an empty collection; or if this Causa has previously
	 * been saved, it will retrieve related PeriodoCausas from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Causa.
	 */
	public function getPeriodoCausasJoinPeriodo($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPeriodoCausas === null) {
			if ($this->isNew()) {
				$this->collPeriodoCausas = array();
			} else {

				$criteria->add(PeriodoCausaPeer::CAUSA_ID, $this->getId());

				$this->collPeriodoCausas = PeriodoCausaPeer::doSelectJoinPeriodo($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(PeriodoCausaPeer::CAUSA_ID, $this->getId());

			if (!isset($this->lastPeriodoCausaCriteria) || !$this->lastPeriodoCausaCriteria->equals($criteria)) {
				$this->collPeriodoCausas = PeriodoCausaPeer::doSelectJoinPeriodo($criteria, $con);
			}
		}
		$this->lastPeriodoCausaCriteria = $criteria;

		return $this->collPeriodoCausas;
	}

	/**
	 * Temporary storage of collCausaTribunals to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initCausaTribunals()
	{
		if ($this->collCausaTribunals === null) {
			$this->collCausaTribunals = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa has previously
	 * been saved, it will retrieve related CausaTribunals from storage.
	 * If this Causa is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getCausaTribunals($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCausaTribunals === null) {
			if ($this->isNew()) {
			   $this->collCausaTribunals = array();
			} else {

				$criteria->add(CausaTribunalPeer::CAUSA_ID, $this->getId());

				CausaTribunalPeer::addSelectColumns($criteria);
				$this->collCausaTribunals = CausaTribunalPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(CausaTribunalPeer::CAUSA_ID, $this->getId());

				CausaTribunalPeer::addSelectColumns($criteria);
				if (!isset($this->lastCausaTribunalCriteria) || !$this->lastCausaTribunalCriteria->equals($criteria)) {
					$this->collCausaTribunals = CausaTribunalPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastCausaTribunalCriteria = $criteria;
		return $this->collCausaTribunals;
	}

	/**
	 * Returns the number of related CausaTribunals.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countCausaTribunals($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CausaTribunalPeer::CAUSA_ID, $this->getId());

		return CausaTribunalPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a CausaTribunal object to this object
	 * through the CausaTribunal foreign key attribute
	 *
	 * @param      CausaTribunal $l CausaTribunal
	 * @return     void
	 * @throws     PropelException
	 */
	public function addCausaTribunal(CausaTribunal $l)
	{
		$this->collCausaTribunals[] = $l;
		$l->setCausa($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa is new, it will return
	 * an empty collection; or if this Causa has previously
	 * been saved, it will retrieve related CausaTribunals from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Causa.
	 */
	public function getCausaTribunalsJoinTribunal($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCausaTribunals === null) {
			if ($this->isNew()) {
				$this->collCausaTribunals = array();
			} else {

				$criteria->add(CausaTribunalPeer::CAUSA_ID, $this->getId());

				$this->collCausaTribunals = CausaTribunalPeer::doSelectJoinTribunal($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(CausaTribunalPeer::CAUSA_ID, $this->getId());

			if (!isset($this->lastCausaTribunalCriteria) || !$this->lastCausaTribunalCriteria->equals($criteria)) {
				$this->collCausaTribunals = CausaTribunalPeer::doSelectJoinTribunal($criteria, $con);
			}
		}
		$this->lastCausaTribunalCriteria = $criteria;

		return $this->collCausaTribunals;
	}

	/**
	 * Temporary storage of collGarantias to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initGarantias()
	{
		if ($this->collGarantias === null) {
			$this->collGarantias = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa has previously
	 * been saved, it will retrieve related Garantias from storage.
	 * If this Causa is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getGarantias($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collGarantias === null) {
			if ($this->isNew()) {
			   $this->collGarantias = array();
			} else {

				$criteria->add(GarantiaPeer::CAUSA_ID, $this->getId());

				GarantiaPeer::addSelectColumns($criteria);
				$this->collGarantias = GarantiaPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(GarantiaPeer::CAUSA_ID, $this->getId());

				GarantiaPeer::addSelectColumns($criteria);
				if (!isset($this->lastGarantiaCriteria) || !$this->lastGarantiaCriteria->equals($criteria)) {
					$this->collGarantias = GarantiaPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastGarantiaCriteria = $criteria;
		return $this->collGarantias;
	}

	/**
	 * Returns the number of related Garantias.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countGarantias($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(GarantiaPeer::CAUSA_ID, $this->getId());

		return GarantiaPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a Garantia object to this object
	 * through the Garantia foreign key attribute
	 *
	 * @param      Garantia $l Garantia
	 * @return     void
	 * @throws     PropelException
	 */
	public function addGarantia(Garantia $l)
	{
		$this->collGarantias[] = $l;
		$l->setCausa($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Causa is new, it will return
	 * an empty collection; or if this Causa has previously
	 * been saved, it will retrieve related Garantias from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Causa.
	 */
	public function getGarantiasJoinTipoGarantia($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collGarantias === null) {
			if ($this->isNew()) {
				$this->collGarantias = array();
			} else {

				$criteria->add(GarantiaPeer::CAUSA_ID, $this->getId());

				$this->collGarantias = GarantiaPeer::doSelectJoinTipoGarantia($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(GarantiaPeer::CAUSA_ID, $this->getId());

			if (!isset($this->lastGarantiaCriteria) || !$this->lastGarantiaCriteria->equals($criteria)) {
				$this->collGarantias = GarantiaPeer::doSelectJoinTipoGarantia($criteria, $con);
			}
		}
		$this->lastGarantiaCriteria = $criteria;

		return $this->collGarantias;
	}

} // BaseCausa
