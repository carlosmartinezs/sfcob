<?php

/**
 * Base class that represents a row from the 'acreedor' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseAcreedor extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        AcreedorPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the comuna_id field.
	 * @var        int
	 */
	protected $comuna_id;


	/**
	 * The value for the giro_id field.
	 * @var        int
	 */
	protected $giro_id;


	/**
	 * The value for the sf_guard_user_id field.
	 * @var        int
	 */
	protected $sf_guard_user_id;


	/**
	 * The value for the rut_acreedor field.
	 * @var        string
	 */
	protected $rut_acreedor;


	/**
	 * The value for the nombres field.
	 * @var        string
	 */
	protected $nombres;


	/**
	 * The value for the apellido_paterno field.
	 * @var        string
	 */
	protected $apellido_paterno;


	/**
	 * The value for the apellido_materno field.
	 * @var        string
	 */
	protected $apellido_materno;


	/**
	 * The value for the direccion field.
	 * @var        string
	 */
	protected $direccion;


	/**
	 * The value for the telefono field.
	 * @var        string
	 */
	protected $telefono;


	/**
	 * The value for the fax field.
	 * @var        string
	 */
	protected $fax;


	/**
	 * The value for the email field.
	 * @var        string
	 */
	protected $email;


	/**
	 * The value for the rep_legal field.
	 * @var        string
	 */
	protected $rep_legal;


	/**
	 * The value for the rut_rep_legal field.
	 * @var        string
	 */
	protected $rut_rep_legal;

	/**
	 * @var        Comuna
	 */
	protected $aComuna;

	/**
	 * @var        Giro
	 */
	protected $aGiro;

	/**
	 * @var        sfGuardUser
	 */
	protected $asfGuardUser;

	/**
	 * Collection to store aggregation of collContratos.
	 * @var        array
	 */
	protected $collContratos;

	/**
	 * The criteria used to select the current contents of collContratos.
	 * @var        Criteria
	 */
	protected $lastContratoCriteria = null;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [comuna_id] column value.
	 * 
	 * @return     int
	 */
	public function getComunaId()
	{

		return $this->comuna_id;
	}

	/**
	 * Get the [giro_id] column value.
	 * 
	 * @return     int
	 */
	public function getGiroId()
	{

		return $this->giro_id;
	}

	/**
	 * Get the [sf_guard_user_id] column value.
	 * 
	 * @return     int
	 */
	public function getSfGuardUserId()
	{

		return $this->sf_guard_user_id;
	}

	/**
	 * Get the [rut_acreedor] column value.
	 * 
	 * @return     string
	 */
	public function getRutAcreedor()
	{

		return $this->rut_acreedor;
	}

	/**
	 * Get the [nombres] column value.
	 * 
	 * @return     string
	 */
	public function getNombres()
	{

		return $this->nombres;
	}

	/**
	 * Get the [apellido_paterno] column value.
	 * 
	 * @return     string
	 */
	public function getApellidoPaterno()
	{

		return $this->apellido_paterno;
	}

	/**
	 * Get the [apellido_materno] column value.
	 * 
	 * @return     string
	 */
	public function getApellidoMaterno()
	{

		return $this->apellido_materno;
	}

	/**
	 * Get the [direccion] column value.
	 * 
	 * @return     string
	 */
	public function getDireccion()
	{

		return $this->direccion;
	}

	/**
	 * Get the [telefono] column value.
	 * 
	 * @return     string
	 */
	public function getTelefono()
	{

		return $this->telefono;
	}

	/**
	 * Get the [fax] column value.
	 * 
	 * @return     string
	 */
	public function getFax()
	{

		return $this->fax;
	}

	/**
	 * Get the [email] column value.
	 * 
	 * @return     string
	 */
	public function getEmail()
	{

		return $this->email;
	}

	/**
	 * Get the [rep_legal] column value.
	 * 
	 * @return     string
	 */
	public function getRepLegal()
	{

		return $this->rep_legal;
	}

	/**
	 * Get the [rut_rep_legal] column value.
	 * 
	 * @return     string
	 */
	public function getRutRepLegal()
	{

		return $this->rut_rep_legal;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = AcreedorPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [comuna_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setComunaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->comuna_id !== $v) {
			$this->comuna_id = $v;
			$this->modifiedColumns[] = AcreedorPeer::COMUNA_ID;
		}

		if ($this->aComuna !== null && $this->aComuna->getId() !== $v) {
			$this->aComuna = null;
		}

	} // setComunaId()

	/**
	 * Set the value of [giro_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setGiroId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->giro_id !== $v) {
			$this->giro_id = $v;
			$this->modifiedColumns[] = AcreedorPeer::GIRO_ID;
		}

		if ($this->aGiro !== null && $this->aGiro->getId() !== $v) {
			$this->aGiro = null;
		}

	} // setGiroId()

	/**
	 * Set the value of [sf_guard_user_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setSfGuardUserId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->sf_guard_user_id !== $v) {
			$this->sf_guard_user_id = $v;
			$this->modifiedColumns[] = AcreedorPeer::SF_GUARD_USER_ID;
		}

		if ($this->asfGuardUser !== null && $this->asfGuardUser->getId() !== $v) {
			$this->asfGuardUser = null;
		}

	} // setSfGuardUserId()

	/**
	 * Set the value of [rut_acreedor] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setRutAcreedor($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rut_acreedor !== $v) {
			$this->rut_acreedor = $v;
			$this->modifiedColumns[] = AcreedorPeer::RUT_ACREEDOR;
		}

	} // setRutAcreedor()

	/**
	 * Set the value of [nombres] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setNombres($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombres !== $v) {
			$this->nombres = $v;
			$this->modifiedColumns[] = AcreedorPeer::NOMBRES;
		}

	} // setNombres()

	/**
	 * Set the value of [apellido_paterno] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setApellidoPaterno($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->apellido_paterno !== $v) {
			$this->apellido_paterno = $v;
			$this->modifiedColumns[] = AcreedorPeer::APELLIDO_PATERNO;
		}

	} // setApellidoPaterno()

	/**
	 * Set the value of [apellido_materno] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setApellidoMaterno($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->apellido_materno !== $v) {
			$this->apellido_materno = $v;
			$this->modifiedColumns[] = AcreedorPeer::APELLIDO_MATERNO;
		}

	} // setApellidoMaterno()

	/**
	 * Set the value of [direccion] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setDireccion($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->direccion !== $v) {
			$this->direccion = $v;
			$this->modifiedColumns[] = AcreedorPeer::DIRECCION;
		}

	} // setDireccion()

	/**
	 * Set the value of [telefono] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setTelefono($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->telefono !== $v) {
			$this->telefono = $v;
			$this->modifiedColumns[] = AcreedorPeer::TELEFONO;
		}

	} // setTelefono()

	/**
	 * Set the value of [fax] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setFax($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->fax !== $v) {
			$this->fax = $v;
			$this->modifiedColumns[] = AcreedorPeer::FAX;
		}

	} // setFax()

	/**
	 * Set the value of [email] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setEmail($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->email !== $v) {
			$this->email = $v;
			$this->modifiedColumns[] = AcreedorPeer::EMAIL;
		}

	} // setEmail()

	/**
	 * Set the value of [rep_legal] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setRepLegal($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rep_legal !== $v) {
			$this->rep_legal = $v;
			$this->modifiedColumns[] = AcreedorPeer::REP_LEGAL;
		}

	} // setRepLegal()

	/**
	 * Set the value of [rut_rep_legal] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setRutRepLegal($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rut_rep_legal !== $v) {
			$this->rut_rep_legal = $v;
			$this->modifiedColumns[] = AcreedorPeer::RUT_REP_LEGAL;
		}

	} // setRutRepLegal()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->comuna_id = $rs->getInt($startcol + 1);

			$this->giro_id = $rs->getInt($startcol + 2);

			$this->sf_guard_user_id = $rs->getInt($startcol + 3);

			$this->rut_acreedor = $rs->getString($startcol + 4);

			$this->nombres = $rs->getString($startcol + 5);

			$this->apellido_paterno = $rs->getString($startcol + 6);

			$this->apellido_materno = $rs->getString($startcol + 7);

			$this->direccion = $rs->getString($startcol + 8);

			$this->telefono = $rs->getString($startcol + 9);

			$this->fax = $rs->getString($startcol + 10);

			$this->email = $rs->getString($startcol + 11);

			$this->rep_legal = $rs->getString($startcol + 12);

			$this->rut_rep_legal = $rs->getString($startcol + 13);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 14; // 14 = AcreedorPeer::NUM_COLUMNS - AcreedorPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Acreedor object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AcreedorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			AcreedorPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AcreedorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aComuna !== null) {
				if ($this->aComuna->isModified()) {
					$affectedRows += $this->aComuna->save($con);
				}
				$this->setComuna($this->aComuna);
			}

			if ($this->aGiro !== null) {
				if ($this->aGiro->isModified()) {
					$affectedRows += $this->aGiro->save($con);
				}
				$this->setGiro($this->aGiro);
			}

			if ($this->asfGuardUser !== null) {
				if ($this->asfGuardUser->isModified()) {
					$affectedRows += $this->asfGuardUser->save($con);
				}
				$this->setsfGuardUser($this->asfGuardUser);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = AcreedorPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += AcreedorPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collContratos !== null) {
				foreach($this->collContratos as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aComuna !== null) {
				if (!$this->aComuna->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aComuna->getValidationFailures());
				}
			}

			if ($this->aGiro !== null) {
				if (!$this->aGiro->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aGiro->getValidationFailures());
				}
			}

			if ($this->asfGuardUser !== null) {
				if (!$this->asfGuardUser->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->asfGuardUser->getValidationFailures());
				}
			}


			if (($retval = AcreedorPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collContratos !== null) {
					foreach($this->collContratos as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AcreedorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getComunaId();
				break;
			case 2:
				return $this->getGiroId();
				break;
			case 3:
				return $this->getSfGuardUserId();
				break;
			case 4:
				return $this->getRutAcreedor();
				break;
			case 5:
				return $this->getNombres();
				break;
			case 6:
				return $this->getApellidoPaterno();
				break;
			case 7:
				return $this->getApellidoMaterno();
				break;
			case 8:
				return $this->getDireccion();
				break;
			case 9:
				return $this->getTelefono();
				break;
			case 10:
				return $this->getFax();
				break;
			case 11:
				return $this->getEmail();
				break;
			case 12:
				return $this->getRepLegal();
				break;
			case 13:
				return $this->getRutRepLegal();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AcreedorPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getComunaId(),
			$keys[2] => $this->getGiroId(),
			$keys[3] => $this->getSfGuardUserId(),
			$keys[4] => $this->getRutAcreedor(),
			$keys[5] => $this->getNombres(),
			$keys[6] => $this->getApellidoPaterno(),
			$keys[7] => $this->getApellidoMaterno(),
			$keys[8] => $this->getDireccion(),
			$keys[9] => $this->getTelefono(),
			$keys[10] => $this->getFax(),
			$keys[11] => $this->getEmail(),
			$keys[12] => $this->getRepLegal(),
			$keys[13] => $this->getRutRepLegal(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AcreedorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setComunaId($value);
				break;
			case 2:
				$this->setGiroId($value);
				break;
			case 3:
				$this->setSfGuardUserId($value);
				break;
			case 4:
				$this->setRutAcreedor($value);
				break;
			case 5:
				$this->setNombres($value);
				break;
			case 6:
				$this->setApellidoPaterno($value);
				break;
			case 7:
				$this->setApellidoMaterno($value);
				break;
			case 8:
				$this->setDireccion($value);
				break;
			case 9:
				$this->setTelefono($value);
				break;
			case 10:
				$this->setFax($value);
				break;
			case 11:
				$this->setEmail($value);
				break;
			case 12:
				$this->setRepLegal($value);
				break;
			case 13:
				$this->setRutRepLegal($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AcreedorPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setComunaId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setGiroId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSfGuardUserId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setRutAcreedor($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNombres($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setApellidoPaterno($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setApellidoMaterno($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setDireccion($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setTelefono($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setFax($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setEmail($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setRepLegal($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setRutRepLegal($arr[$keys[13]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(AcreedorPeer::DATABASE_NAME);

		if ($this->isColumnModified(AcreedorPeer::ID)) $criteria->add(AcreedorPeer::ID, $this->id);
		if ($this->isColumnModified(AcreedorPeer::COMUNA_ID)) $criteria->add(AcreedorPeer::COMUNA_ID, $this->comuna_id);
		if ($this->isColumnModified(AcreedorPeer::GIRO_ID)) $criteria->add(AcreedorPeer::GIRO_ID, $this->giro_id);
		if ($this->isColumnModified(AcreedorPeer::SF_GUARD_USER_ID)) $criteria->add(AcreedorPeer::SF_GUARD_USER_ID, $this->sf_guard_user_id);
		if ($this->isColumnModified(AcreedorPeer::RUT_ACREEDOR)) $criteria->add(AcreedorPeer::RUT_ACREEDOR, $this->rut_acreedor);
		if ($this->isColumnModified(AcreedorPeer::NOMBRES)) $criteria->add(AcreedorPeer::NOMBRES, $this->nombres);
		if ($this->isColumnModified(AcreedorPeer::APELLIDO_PATERNO)) $criteria->add(AcreedorPeer::APELLIDO_PATERNO, $this->apellido_paterno);
		if ($this->isColumnModified(AcreedorPeer::APELLIDO_MATERNO)) $criteria->add(AcreedorPeer::APELLIDO_MATERNO, $this->apellido_materno);
		if ($this->isColumnModified(AcreedorPeer::DIRECCION)) $criteria->add(AcreedorPeer::DIRECCION, $this->direccion);
		if ($this->isColumnModified(AcreedorPeer::TELEFONO)) $criteria->add(AcreedorPeer::TELEFONO, $this->telefono);
		if ($this->isColumnModified(AcreedorPeer::FAX)) $criteria->add(AcreedorPeer::FAX, $this->fax);
		if ($this->isColumnModified(AcreedorPeer::EMAIL)) $criteria->add(AcreedorPeer::EMAIL, $this->email);
		if ($this->isColumnModified(AcreedorPeer::REP_LEGAL)) $criteria->add(AcreedorPeer::REP_LEGAL, $this->rep_legal);
		if ($this->isColumnModified(AcreedorPeer::RUT_REP_LEGAL)) $criteria->add(AcreedorPeer::RUT_REP_LEGAL, $this->rut_rep_legal);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(AcreedorPeer::DATABASE_NAME);

		$criteria->add(AcreedorPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Acreedor (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setComunaId($this->comuna_id);

		$copyObj->setGiroId($this->giro_id);

		$copyObj->setSfGuardUserId($this->sf_guard_user_id);

		$copyObj->setRutAcreedor($this->rut_acreedor);

		$copyObj->setNombres($this->nombres);

		$copyObj->setApellidoPaterno($this->apellido_paterno);

		$copyObj->setApellidoMaterno($this->apellido_materno);

		$copyObj->setDireccion($this->direccion);

		$copyObj->setTelefono($this->telefono);

		$copyObj->setFax($this->fax);

		$copyObj->setEmail($this->email);

		$copyObj->setRepLegal($this->rep_legal);

		$copyObj->setRutRepLegal($this->rut_rep_legal);


		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach($this->getContratos() as $relObj) {
				$copyObj->addContrato($relObj->copy($deepCopy));
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Acreedor Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     AcreedorPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new AcreedorPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Comuna object.
	 *
	 * @param      Comuna $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setComuna($v)
	{


		if ($v === null) {
			$this->setComunaId(NULL);
		} else {
			$this->setComunaId($v->getId());
		}


		$this->aComuna = $v;
	}


	/**
	 * Get the associated Comuna object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Comuna The associated Comuna object.
	 * @throws     PropelException
	 */
	public function getComuna($con = null)
	{
		if ($this->aComuna === null && ($this->comuna_id !== null)) {
			// include the related Peer class
			$this->aComuna = ComunaPeer::retrieveByPK($this->comuna_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = ComunaPeer::retrieveByPK($this->comuna_id, $con);
			   $obj->addComunas($this);
			 */
		}
		return $this->aComuna;
	}

	/**
	 * Declares an association between this object and a Giro object.
	 *
	 * @param      Giro $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setGiro($v)
	{


		if ($v === null) {
			$this->setGiroId(NULL);
		} else {
			$this->setGiroId($v->getId());
		}


		$this->aGiro = $v;
	}


	/**
	 * Get the associated Giro object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Giro The associated Giro object.
	 * @throws     PropelException
	 */
	public function getGiro($con = null)
	{
		if ($this->aGiro === null && ($this->giro_id !== null)) {
			// include the related Peer class
			$this->aGiro = GiroPeer::retrieveByPK($this->giro_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = GiroPeer::retrieveByPK($this->giro_id, $con);
			   $obj->addGiros($this);
			 */
		}
		return $this->aGiro;
	}

	/**
	 * Declares an association between this object and a sfGuardUser object.
	 *
	 * @param      sfGuardUser $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setsfGuardUser($v)
	{


		if ($v === null) {
			$this->setSfGuardUserId(NULL);
		} else {
			$this->setSfGuardUserId($v->getId());
		}


		$this->asfGuardUser = $v;
	}


	/**
	 * Get the associated sfGuardUser object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     sfGuardUser The associated sfGuardUser object.
	 * @throws     PropelException
	 */
	public function getsfGuardUser($con = null)
	{
		if ($this->asfGuardUser === null && ($this->sf_guard_user_id !== null)) {
			// include the related Peer class
			$this->asfGuardUser = sfGuardUserPeer::retrieveByPK($this->sf_guard_user_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = sfGuardUserPeer::retrieveByPK($this->sf_guard_user_id, $con);
			   $obj->addsfGuardUsers($this);
			 */
		}
		return $this->asfGuardUser;
	}

	/**
	 * Temporary storage of collContratos to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initContratos()
	{
		if ($this->collContratos === null) {
			$this->collContratos = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Acreedor has previously
	 * been saved, it will retrieve related Contratos from storage.
	 * If this Acreedor is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getContratos($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collContratos === null) {
			if ($this->isNew()) {
			   $this->collContratos = array();
			} else {

				$criteria->add(ContratoPeer::ACREEDOR_ID, $this->getId());

				ContratoPeer::addSelectColumns($criteria);
				$this->collContratos = ContratoPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(ContratoPeer::ACREEDOR_ID, $this->getId());

				ContratoPeer::addSelectColumns($criteria);
				if (!isset($this->lastContratoCriteria) || !$this->lastContratoCriteria->equals($criteria)) {
					$this->collContratos = ContratoPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastContratoCriteria = $criteria;
		return $this->collContratos;
	}

	/**
	 * Returns the number of related Contratos.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countContratos($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ContratoPeer::ACREEDOR_ID, $this->getId());

		return ContratoPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a Contrato object to this object
	 * through the Contrato foreign key attribute
	 *
	 * @param      Contrato $l Contrato
	 * @return     void
	 * @throws     PropelException
	 */
	public function addContrato(Contrato $l)
	{
		$this->collContratos[] = $l;
		$l->setAcreedor($this);
	}

} // BaseAcreedor
