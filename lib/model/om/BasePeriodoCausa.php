<?php

/**
 * Base class that represents a row from the 'periodo_causa' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BasePeriodoCausa extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        PeriodoCausaPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the causa_id field.
	 * @var        int
	 */
	protected $causa_id;


	/**
	 * The value for the periodo_id field.
	 * @var        int
	 */
	protected $periodo_id;


	/**
	 * The value for the fecha_inicio field.
	 * @var        int
	 */
	protected $fecha_inicio;


	/**
	 * The value for the activo field.
	 * @var        boolean
	 */
	protected $activo;

	/**
	 * @var        Causa
	 */
	protected $aCausa;

	/**
	 * @var        Periodo
	 */
	protected $aPeriodo;

	/**
	 * Collection to store aggregation of collDiligencias.
	 * @var        array
	 */
	protected $collDiligencias;

	/**
	 * The criteria used to select the current contents of collDiligencias.
	 * @var        Criteria
	 */
	protected $lastDiligenciaCriteria = null;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [causa_id] column value.
	 * 
	 * @return     int
	 */
	public function getCausaId()
	{

		return $this->causa_id;
	}

	/**
	 * Get the [periodo_id] column value.
	 * 
	 * @return     int
	 */
	public function getPeriodoId()
	{

		return $this->periodo_id;
	}

	/**
	 * Get the [optionally formatted] [fecha_inicio] column value.
	 * 
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the integer unix timestamp will be returned.
	 * @return     mixed Formatted date/time value as string or integer unix timestamp (if format is NULL).
	 * @throws     PropelException - if unable to convert the date/time to timestamp.
	 */
	public function getFechaInicio($format = 'Y-m-d')
	{

		if ($this->fecha_inicio === null || $this->fecha_inicio === '') {
			return null;
		} elseif (!is_int($this->fecha_inicio)) {
			// a non-timestamp value was set externally, so we convert it
			$ts = strtotime($this->fecha_inicio);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse value of [fecha_inicio] as date/time value: " . var_export($this->fecha_inicio, true));
			}
		} else {
			$ts = $this->fecha_inicio;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	/**
	 * Get the [activo] column value.
	 * 
	 * @return     boolean
	 */
	public function getActivo()
	{

		return $this->activo;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = PeriodoCausaPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [causa_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setCausaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->causa_id !== $v) {
			$this->causa_id = $v;
			$this->modifiedColumns[] = PeriodoCausaPeer::CAUSA_ID;
		}

		if ($this->aCausa !== null && $this->aCausa->getId() !== $v) {
			$this->aCausa = null;
		}

	} // setCausaId()

	/**
	 * Set the value of [periodo_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setPeriodoId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->periodo_id !== $v) {
			$this->periodo_id = $v;
			$this->modifiedColumns[] = PeriodoCausaPeer::PERIODO_ID;
		}

		if ($this->aPeriodo !== null && $this->aPeriodo->getId() !== $v) {
			$this->aPeriodo = null;
		}

	} // setPeriodoId()

	/**
	 * Set the value of [fecha_inicio] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setFechaInicio($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse date/time value for [fecha_inicio] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->fecha_inicio !== $ts) {
			$this->fecha_inicio = $ts;
			$this->modifiedColumns[] = PeriodoCausaPeer::FECHA_INICIO;
		}

	} // setFechaInicio()

	/**
	 * Set the value of [activo] column.
	 * 
	 * @param      boolean $v new value
	 * @return     void
	 */
	public function setActivo($v)
	{

		if ($this->activo !== $v) {
			$this->activo = $v;
			$this->modifiedColumns[] = PeriodoCausaPeer::ACTIVO;
		}

	} // setActivo()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->causa_id = $rs->getInt($startcol + 1);

			$this->periodo_id = $rs->getInt($startcol + 2);

			$this->fecha_inicio = $rs->getDate($startcol + 3, null);

			$this->activo = $rs->getBoolean($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 5; // 5 = PeriodoCausaPeer::NUM_COLUMNS - PeriodoCausaPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating PeriodoCausa object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PeriodoCausaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PeriodoCausaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PeriodoCausaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCausa !== null) {
				if ($this->aCausa->isModified()) {
					$affectedRows += $this->aCausa->save($con);
				}
				$this->setCausa($this->aCausa);
			}

			if ($this->aPeriodo !== null) {
				if ($this->aPeriodo->isModified()) {
					$affectedRows += $this->aPeriodo->save($con);
				}
				$this->setPeriodo($this->aPeriodo);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PeriodoCausaPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += PeriodoCausaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collDiligencias !== null) {
				foreach($this->collDiligencias as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCausa !== null) {
				if (!$this->aCausa->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCausa->getValidationFailures());
				}
			}

			if ($this->aPeriodo !== null) {
				if (!$this->aPeriodo->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aPeriodo->getValidationFailures());
				}
			}


			if (($retval = PeriodoCausaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collDiligencias !== null) {
					foreach($this->collDiligencias as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PeriodoCausaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getCausaId();
				break;
			case 2:
				return $this->getPeriodoId();
				break;
			case 3:
				return $this->getFechaInicio();
				break;
			case 4:
				return $this->getActivo();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PeriodoCausaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getCausaId(),
			$keys[2] => $this->getPeriodoId(),
			$keys[3] => $this->getFechaInicio(),
			$keys[4] => $this->getActivo(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PeriodoCausaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setCausaId($value);
				break;
			case 2:
				$this->setPeriodoId($value);
				break;
			case 3:
				$this->setFechaInicio($value);
				break;
			case 4:
				$this->setActivo($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PeriodoCausaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCausaId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPeriodoId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setFechaInicio($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setActivo($arr[$keys[4]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(PeriodoCausaPeer::DATABASE_NAME);

		if ($this->isColumnModified(PeriodoCausaPeer::ID)) $criteria->add(PeriodoCausaPeer::ID, $this->id);
		if ($this->isColumnModified(PeriodoCausaPeer::CAUSA_ID)) $criteria->add(PeriodoCausaPeer::CAUSA_ID, $this->causa_id);
		if ($this->isColumnModified(PeriodoCausaPeer::PERIODO_ID)) $criteria->add(PeriodoCausaPeer::PERIODO_ID, $this->periodo_id);
		if ($this->isColumnModified(PeriodoCausaPeer::FECHA_INICIO)) $criteria->add(PeriodoCausaPeer::FECHA_INICIO, $this->fecha_inicio);
		if ($this->isColumnModified(PeriodoCausaPeer::ACTIVO)) $criteria->add(PeriodoCausaPeer::ACTIVO, $this->activo);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PeriodoCausaPeer::DATABASE_NAME);

		$criteria->add(PeriodoCausaPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of PeriodoCausa (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCausaId($this->causa_id);

		$copyObj->setPeriodoId($this->periodo_id);

		$copyObj->setFechaInicio($this->fecha_inicio);

		$copyObj->setActivo($this->activo);


		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach($this->getDiligencias() as $relObj) {
				$copyObj->addDiligencia($relObj->copy($deepCopy));
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     PeriodoCausa Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     PeriodoCausaPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PeriodoCausaPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Causa object.
	 *
	 * @param      Causa $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setCausa($v)
	{


		if ($v === null) {
			$this->setCausaId(NULL);
		} else {
			$this->setCausaId($v->getId());
		}


		$this->aCausa = $v;
	}


	/**
	 * Get the associated Causa object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Causa The associated Causa object.
	 * @throws     PropelException
	 */
	public function getCausa($con = null)
	{
		if ($this->aCausa === null && ($this->causa_id !== null)) {
			// include the related Peer class
			$this->aCausa = CausaPeer::retrieveByPK($this->causa_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = CausaPeer::retrieveByPK($this->causa_id, $con);
			   $obj->addCausas($this);
			 */
		}
		return $this->aCausa;
	}

	/**
	 * Declares an association between this object and a Periodo object.
	 *
	 * @param      Periodo $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setPeriodo($v)
	{


		if ($v === null) {
			$this->setPeriodoId(NULL);
		} else {
			$this->setPeriodoId($v->getId());
		}


		$this->aPeriodo = $v;
	}


	/**
	 * Get the associated Periodo object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Periodo The associated Periodo object.
	 * @throws     PropelException
	 */
	public function getPeriodo($con = null)
	{
		if ($this->aPeriodo === null && ($this->periodo_id !== null)) {
			// include the related Peer class
			$this->aPeriodo = PeriodoPeer::retrieveByPK($this->periodo_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = PeriodoPeer::retrieveByPK($this->periodo_id, $con);
			   $obj->addPeriodos($this);
			 */
		}
		return $this->aPeriodo;
	}

	/**
	 * Temporary storage of collDiligencias to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initDiligencias()
	{
		if ($this->collDiligencias === null) {
			$this->collDiligencias = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this PeriodoCausa has previously
	 * been saved, it will retrieve related Diligencias from storage.
	 * If this PeriodoCausa is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getDiligencias($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collDiligencias === null) {
			if ($this->isNew()) {
			   $this->collDiligencias = array();
			} else {

				$criteria->add(DiligenciaPeer::PERIODO_CAUSA_ID, $this->getId());

				DiligenciaPeer::addSelectColumns($criteria);
				$this->collDiligencias = DiligenciaPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(DiligenciaPeer::PERIODO_CAUSA_ID, $this->getId());

				DiligenciaPeer::addSelectColumns($criteria);
				if (!isset($this->lastDiligenciaCriteria) || !$this->lastDiligenciaCriteria->equals($criteria)) {
					$this->collDiligencias = DiligenciaPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastDiligenciaCriteria = $criteria;
		return $this->collDiligencias;
	}

	/**
	 * Returns the number of related Diligencias.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countDiligencias($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(DiligenciaPeer::PERIODO_CAUSA_ID, $this->getId());

		return DiligenciaPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a Diligencia object to this object
	 * through the Diligencia foreign key attribute
	 *
	 * @param      Diligencia $l Diligencia
	 * @return     void
	 * @throws     PropelException
	 */
	public function addDiligencia(Diligencia $l)
	{
		$this->collDiligencias[] = $l;
		$l->setPeriodoCausa($this);
	}

} // BasePeriodoCausa
