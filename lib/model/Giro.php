<?php

/**
 * Subclass for representing a row from the 'giro' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Giro extends BaseGiro
{
	public function __toString(){
		return $this->getNombre();
	}
}
