<?php

/**
 * Subclass for performing query and update operations on the 'formato' table.
 *
 * 
 *
 * @package lib.model
 */ 
class FormatoPeer extends BaseFormatoPeer
{
	public static function doSelectOneByTipoCarta($tipo_carta){
		$c = new Criteria();
		$c->add(FormatoPeer::NOMBRE,$tipo_carta);
		$c->add(FormatoPeer::TIPO,0);
		
		return FormatoPeer::doSelectOne($c);
	}
	
	public static function doSelectOneByTipoEscrito($tipo_escrito){
		$c = new Criteria();
		$c->add(FormatoPeer::NOMBRE,$tipo_escrito);
		$c->add(FormatoPeer::TIPO,1);
		
		return FormatoPeer::doSelectOne($c);
	}
}
