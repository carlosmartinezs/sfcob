<?php

/**
 * Subclass for representing a row from the 'comuna' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Comuna extends BaseComuna
{
	public function __toString(){
		return $this->getNombre();
	}
}
