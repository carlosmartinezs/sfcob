<?php

/**
 * Subclass for representing a row from the 'deudor_domicilio' table.
 *
 * 
 *
 * @package lib.model
 */ 
class DeudorDomicilio extends BaseDeudorDomicilio
{
	public function getNombreDeudor(){
		return $this->getDeudor();
	}
	
	public function __toString(){
		return /*$this->getDeudor()->__toString() . "  /  " . */$this->getCalle() . " " . $this->getNumero() . ". " . $this->getComuna();
	}
	
	public function getDireccionParaReporte(){
		return $this->getCalle()." ".$this->getNumero()."<br />".$this->getComuna();
	}
}
