<?php

include("class.phpmailer.php");
include("class.smtp.php");

class GMail {

	private $mail;
	
	function __construct()
	{
		$this->mail = new PHPMailer();
		$this->mail->IsSMTP();
		$this->mail->SMTPAuth = true;
		$this->mail->SMTPSecure = "ssl";
		$this->mail->Host = "smtp.gmail.com";
		$this->mail->Port = 465;
		$this->mail->Username = "carlos.f.martinez.s@gmail.com";
		$this->mail->Password = "1412198strato";
	}
	
	public function setUsername($username)
	{
		$this->mail->Username = $username;
	}
	
	public function setPassword($pwd)
	{
		$this->mail->Password = $pwd;
	}

	public function setFrom($from)
	{
		$this->mail->From = $from;
	}
	
	public function setFromName($fromName)
	{
		$this->mail->FromName = $fromName;
	}

	public function subject($subject)
	{
		$this->mail->Subject = $subject;
	}

	public function altBody($altBody)
	{
		$this->mail->AltBody = $altBody;
	}
	

	public function msgHTML($msgHTML)
	{
		$this->mail->MsgHTML($msgHTML);
	}
	
	public function AddAttachment($pathFile)
	{
		$this->mail->AddAttachment($pathFile);
	}
	
	
	public function AddAddress($emailTo,$name)
	{
		$this->mail->AddAddress($emailTo,$name);
	}

	public function IsHTML($isHtml)
	{
		$this->mail->IsHTML($isHtml);
	}
	
	public function Send()
	{
		return $this->mail->Send();
	}
	
	public function getGMail()
	{
		return $this->mail;
	}
}
