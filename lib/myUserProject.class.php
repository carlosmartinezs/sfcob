<?php
class myUserProject extends sfGuardSecurityUser
{
    public function getAcreedor()
    {
		$id = $this->getAttribute('user_id', null, 'sfGuardSecurityUser');
		$criteria = new Criteria();
		$criteria->add(AcreedorPeer::SF_GUARD_USER_ID,$id);
		return AcreedorPeer::doSelectOne($criteria);
	}
	
	public function getPerfil(){
		$id = $this->getAttribute('user_id', null, 'sfGuardSecurityUser');
		$criteria = new Criteria();
		$criteria->add(PerfilPeer::SF_GUARD_USER_ID,$id);
		return PerfilPeer::doSelectOne($criteria);
	}
	
	public function isAcreedor(){
		
		$acreedor = $this->getAcreedor();
		
		if($acreedor instanceof Acreedor)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function getPassword(){
		//$id = $this->getAttribute('user_id', null, 'sfGuardSecurityUser');
		$userBd = sfGuardUserPeer::retrieveByPK($this->getId());
		return $userBd->getPassword();
	}
	
	public function getId(){
		return $this->getAttribute('user_id', null, 'sfGuardSecurityUser');
	}
}
