<?php

/**
 * TelefonoDomicilio form.
 *
 * @package    form
 * @subpackage telefono_domicilio
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class TelefonoDomicilioForm extends BaseTelefonoDomicilioForm
{
  public function configure()
  {
  	$this->setWidgets(array(
  	  'id'                  => new sfWidgetFormInputHidden(),
  	  'deudor_domicilio_id' => new sfWidgetFormInputHidden(),
  	  'telefono'            => new sfWidgetFormInput(),
  	));
  	
  	$this->setValidators(array(
  	  'id'       			  => new sfValidatorPropelChoice(array('model' => 'TelefonoDomicilio', 'column' => 'id', 'required' => false)),
  	  'deudor_domicilio_id' => new sfValidatorPropelChoice(array('model' => 'DeudorDomicilio', 'column' => 'id')),
  	  'telefono'  		  => new sfValidatorString(array('max_length' => 25),array('required'=>'Ingrese n&uacute;mero de tel&eacute;fono.','max_length'=>'Debe tener como m&aacute;ximo 25 caracteres.')),
  	));
  	
  	$this->widgetSchema->setNameFormat('telefono_domicilio[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}
