<?php

class FiltroTipoCartaForm extends sfForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'tipo_carta'	=> new sfWidgetFormSelectRadio(array(
    					'choices' => array(
    								'1' => 'Cobranza Aval',
    								'2' => 'Cobranza Deudor',
    								)
    					),array('name' => 'tipo_carta')),
    ));
  }
}