<?php
class SearchDeudorForm extends sfForm
{
	public function configure()
	{
		$this->setWidgets(array(
			'accion'		   => new sfWidgetFormInputHidden(),
      		'rut_deudor'       => new sfWidgetFormInput(),
      		'nombres'          => new sfWidgetFormInput(),
      		'apellido_paterno' => new sfWidgetFormInput(),
      		'apellido_materno' => new sfWidgetFormInput(),
      		'celular'          => new sfWidgetFormInput(),
      		'email'            => new sfWidgetFormInput(),
    	));
	}
}
