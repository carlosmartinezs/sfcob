<?php

class FiltroEstadoCausaForm extends sfForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'activa'   	 => new sfWidgetFormInputCheckbox(),
      'terminada'    => new sfWidgetFormInputCheckbox(),
      'castigada'    => new sfWidgetFormInputCheckbox(),
      'incobrable'   => new sfWidgetFormInputCheckbox(),
      'suspendida'   => new sfWidgetFormInputCheckbox(),
    ));
  }
}