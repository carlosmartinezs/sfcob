<?php
class SearchTribunalForm extends sfForm
{
	public function configure()
	{
		$this->setWidgets(array(
     	'accion' => new sfWidgetFormInputHidden(),
      	'ciudad' => new sfWidgetFormInput(),
     	'nombre' => new sfWidgetFormInput(),
     	'comuna' => new sfWidgetFormInput(),
   		));
	}
}