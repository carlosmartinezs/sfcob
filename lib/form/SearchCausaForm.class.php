<?php
class SearchCausaForm extends sfForm
{
	public function configure()
	{
		$dias = Funciones::getArrayDias();
  		$meses = Funciones::getArrayMeses();
  		$anios = Funciones::getArrayAnios(2,2);
  	
		$this->setWidgets(array(
			'estado' =>	new sfWidgetFormPropelSelect(array('model' => 'Estado', 'add_empty' => "-- Seleccione --")),
			'id'			   => new sfWidgetFormInput(),
			'accion'		   => new sfWidgetFormInputHidden(),
			'tipo_causa_id'    => new sfWidgetFormPropelSelect(array('model' => 'TipoCausa', 'add_empty' => "-- Seleccione --")),
      		'searchdeudor'           => new sfWidgetFormInputHidden(),
      		'materia_id'       => new sfWidgetFormPropelSelect(array('model' => 'Materia', 'add_empty' => "-- Seleccione --")),
      		'searchcontrato'         => new sfWidgetFormInputHidden(),
      		'rol'              => new sfWidgetFormInput(),
      		'competencia'      => new sfWidgetFormPropelSelect(array('model' => 'Competencia', 'add_empty' => "-- Seleccione --")),
      		'desde'     => new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a'))),
      		'hasta'    => new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a'))),
      		'herencia_exhorto' => new sfWidgetFormInputCheckbox(),
			'periodo'          => new sfWidgetFormPropelSelect(array('model' => 'Periodo', 'add_empty' => "-- Seleccione --")),
    	));
    	
    	$this->setValidators(array(
    	    'estado'           => new sfValidatorPropelChoice(array('model' => 'Estado', 'column' => 'id','required' => false)),
    		'id'			   => new sfValidatorInteger(array('required' => false)),
			'accion'		   => new sfValidatorString(array('required' => false)),
			'tipo_causa_id'    => new sfValidatorPropelChoice(array('model' => 'TipoCausa', 'column' => 'id','required' => false)),
      		'searchdeudor'     => new sfValidatorInteger(array('required' => false)),
      		'materia_id'       => new sfValidatorPropelChoice(array('model' => 'Materia', 'column' => 'id','required' => false)),
      		'searchcontrato'   => new sfValidatorInteger(array('required' => false)),
      		'rol'              => new sfValidatorString(array('required' => false)),
      		'competencia'      => new sfValidatorPropelChoice(array('model' => 'Competencia', 'column' => 'id','required' => false)),
      		'desde'            => new sfValidatorDate(array('required' => false)),
    	    'hasta'            => new sfValidatorDate(array('required' => false)),
    	    'herencia_exhorto' => new sfValidatorBoolean(array('required' => false)),
    	    'periodo'          => new sfValidatorPropelChoice(array('model' => 'Periodo', 'column' => 'id','required' => false)),
    	));
    	
    	$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
    	
    	$this->validatorSchema->setPostValidator(
    						new sfValidatorSchemaCompare(
    								'desde',
    								sfValidatorSchemaCompare::LESS_THAN,
    								'hasta',
							    	array('required'=> false,'throw_global_error' => true),
							    	array('invalid' => 'La fecha "desde" ("%left_field%") debe ser anterior a la fecha "hasta" ("%right_field%")')
							    	)
							);
	}
}