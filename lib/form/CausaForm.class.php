<?php

/**
 * Causa form.
 *
 * @package    form
 * @subpackage causa
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class CausaForm extends BaseCausaForm
{
  public function configure()
  {
  	$dias = Funciones::getArrayDias();
  	$meses = Funciones::getArrayMeses();
  	$anios = Funciones::getArrayAnios(9,2);
  	
  	$this->widgetSchema['herencia_exhorto'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['contrato_id'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['deudor_id'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['causa_exhorto'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['tipo_causa_id'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['tribunal'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['materia_id'] = new sfWidgetFormPropelSelect(array('model' => 'Materia', 'add_empty' => "-- Seleccione --"));
  	$this->widgetSchema['competencia_id'] = new sfWidgetFormPropelSelect(array('model' => 'Competencia', 'add_empty' => "-- Seleccione --"));
  	$this->widgetSchema['fecha_inicio'] = new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a'))
  											);
  	
    $this->validatorSchema['tipo_causa_id'] = new sfValidatorPropelChoice(array('model' => 'TipoCausa', 'column' => 'id'),array('required'=>'Selecione tipo de causa.'));
    $this->validatorSchema['deudor_id'] = new sfValidatorPropelChoice(array('model' => 'Deudor', 'column' => 'id'),array('required'=>'Busque un deudor.'));
    $this->validatorSchema['materia_id'] = new sfValidatorPropelChoice(array('model' => 'Materia', 'column' => 'id'),array('required'=>'Selecione la materia.'));
    $this->validatorSchema['contrato_id'] = new sfValidatorPropelChoice(array('model' => 'Contrato', 'column' => 'id'),array('required'=>'Busque un acreedor.'));
    $this->validatorSchema['rol'] = new sfValidatorString(array('max_length' => 50,'min_length'=>4),array('required'=>'Ingrese rol de la causa.','max_length' => 'Debe tener como m&aacute;ximo 50 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 4 carateres.'));
    $this->validatorSchema['competencia_id'] = new sfValidatorPropelChoice(array('model' => 'Competencia', 'column' => 'id'),array('required'=>'Seleccione una competencia.'));
    $this->validatorSchema['fecha_inicio'] = new sfValidatorDate(array(),array('invalid'=>'Fecha incorrecta.','required'=>'Ingrese fecha de inicio.'));
    $this->validatorSchema['herencia_exhorto'] = new sfValidatorBoolean(array('required' => false));
    $this->validatorSchema['causa_exhorto'] = new sfValidatorInteger(array('required' => false));
  	$this->validatorSchema['tribunal'] = new sfValidatorPropelChoice(array('model' => 'Tribunal', 'column' => 'id'),array('required'=>'Busque un tribunal.'));
	
	        $this->validatorSchema->setPostValidator(
                        new sfValidatorPropelUnique(array('model' => 'Causa', 'column' => array('rol')),array('invalid' => 'El rol ya existe.'))
                        );
  }
}
