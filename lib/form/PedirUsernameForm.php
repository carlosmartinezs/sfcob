<?php

class PedirUsernameForm extends sfForm
{
	public function setup()
	{
		$this->setWidgets(array(
     		'username' 	=> new sfWidgetFormInput()
   		));
   		
   		$this->setValidators(array(
     		'username' 	=> new sfValidatorString(array('required' => true),array('required' => 'Requerido.')),
    	));
    	
    	$this->widgetSchema->setLabels(array(
			'username'	=> 'Ingrese su nombre de usuario *',
		));

		$this->widgetSchema->setNameFormat('pedirusername[%s]');

	    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
	
	    parent::setup();
	}
}