<?php

/**
 * Gasto form.
 *
 * @package    form
 * @subpackage gasto
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class GastoForm extends BaseGastoForm
{
  public function configure()
  {
  	$dias = Funciones::getArrayDias();
  	$meses = Funciones::getArrayMeses();
  	$anios = Funciones::getArrayAnios(2,2);
  	
  	$this->widgetSchema['tipo_gasto_id'] = new sfWidgetFormPropelSelect(array('model' => 'TipoGasto', 'add_empty' => '--Seleccione--'));
  	$this->widgetSchema['fecha'] = new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a')));
  	
  	$this->validatorSchema['tipo_gasto_id'] = new sfValidatorPropelChoice(array('model' => 'TipoGasto', 'column' => 'id'),array('required'=>'Seleccione tipo de gasto.'));
    $this->validatorSchema['monto'] = new sfValidatorInteger(
													array(
														'min' => 1,
														'max' => 2147483646
													),
													array(
														'min' => '"%value%", debe ser mayor a 0.',
														'max' => '"%value%", debe ser menor o igual a 2147483646.',
														'invalid' => '"%value%", no es un n&uacute;mero o no es entero.',
														'required'=>'Ingrese un monto.'
													)
											);
    $this->validatorSchema['motivo'] = new sfValidatorString(array('min_length'=>10),array('min_length'=>'Debe ingresar un m&iacute;nimo de 10 caracteres.','required'=>'Ingrese un motivo.'));
    
  	
  }
}
