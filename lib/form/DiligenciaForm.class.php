<?php

/**
 * Diligencia form.
 *
 * @package    form
 * @subpackage diligencia
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class DiligenciaForm extends BaseDiligenciaForm
{
  public function configure()
  {
  	$dias = Funciones::getArrayDias();
  	$meses = Funciones::getArrayMeses();
  	$anios = Funciones::getArrayAnios(2,2);
  	
  	$this->widgetSchema['fecha'] = new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a')));
  	$this->widgetSchema['causa_id'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['periodo_id'] = new sfWidgetFormInputHidden();
  	
  	$this->validatorSchema['fecha'] = new sfValidatorDate(array(),array('invalid'=>'Fecha incorrecta.','required'=>'Ingrese fecha.'));
  	$this->validatorSchema['descripcion'] =  new sfValidatorString(array('min_length'=>10),array('min_length'=>'Debe ingresar un m&iacute;nimo de 10 caracteres.','required'=>'Ingrese una descripci&oacute;n.'));
    
  	$this->validatorSchema['causa_id'] = new sfValidatorInteger();
  	$this->validatorSchema['periodo_id'] = new sfValidatorInteger();
  }
}
