<?php

class FiltroTipoEscritoForm extends sfForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'tipo_escrito'	=> new sfWidgetFormSelectRadio(array(
    					'choices' => array(
    								'5' => 'Fuerza P&uacute;blica Embargo',
    								'6' => 'Fuerza P&uacute;blica Retiro Especies',
    								'7' => 'Notif. art. 44',
    								'8' => 'Notif. art. 52',
    								'9' => 'Se Exhorte',
    								)
    					),array('name' => 'tipo_escrito')),
    ));
  }
}