<?php

/**
 * TipoPago form.
 *
 * @package    form
 * @subpackage tipo_pago
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class TipoPagoForm extends BaseTipoPagoForm
{
  public function configure()
  {
  	$this->validatorSchema['nombre'] = new sfValidatorString(array('min_length'=>5,'max_length' => 50),array('min_length'=>'Debe ingresar un m&iacute;nimo de 5 caracteres.','max_length'=>'Debe ingresar un m&aacute;ximo de 50 caracteres.','required'=>'Ingrese un nombre.'));
    $this->validatorSchema['descripcion'] = new sfValidatorString(array('min_length'=>10),array('min_length'=>'Debe ingresar un m&iacute;nimo de 10 caracteres.','required'=>'Ingrese una descripci&oacute;n.'));
  }
}
