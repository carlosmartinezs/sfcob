<?php

class RecuperarContrasenaForm extends sfForm
{
	public function setup()
	{
		$this->setWidgets(array(
     		'respuesta' 	=> new sfWidgetFormInput()
   		));
   		
   		$this->setValidators(array(
     		'respuesta' 	=> new sfValidatorString(array('required' => true,'min_length'=>4),array('required' => 'Requerido.','min_length'=>'Debe tener minimo 4 caracteres.')),
    	));
    	
    	$this->widgetSchema->setLabels(array(
			'respuesta'	=> 'Ingrese su respuesta *',
		));

		$this->widgetSchema->setNameFormat('recuperarcontrasena[%s]');

	    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
	
	    parent::setup();
	}
}