<?php

/**
 * Abono form.
 *
 * @package    form
 * @subpackage abono
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class AbonoForm extends BaseAbonoForm
{
  public function configure()
  {
  	$dias = Funciones::getArrayDias();
  	$meses = Funciones::getArrayMeses();
  	$anios = Funciones::getArrayAnios(1,1);
  	
  	$this->widgetSchema['tipo_abono_id'] = new sfWidgetFormPropelSelect(array('model' => 'TipoAbono', 'add_empty' => '--Seleccione--'));
    $this->widgetSchema['tipo_pago_id'] = new sfWidgetFormPropelSelect(array('model' => 'TipoPago', 'add_empty' => '--Seleccione--'));
  	$this->widgetSchema['pendiente'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['fecha_inicio_causa'] = new sfWidgetFormDate(array(
											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
											'empty_values' => array('year' => 'A&ntilde;o', 'month' => 'Mes','day' => 'D&iacute;a')));
  	$this->widgetSchema['fecha_actual'] = new sfWidgetFormDate(array(
											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
											'empty_values' => array('year' => 'A&ntilde;o', 'month' => 'Mes','day' => 'D&iacute;a')));
  	$this->widgetSchema['fecha'] = new sfWidgetFormDate(array(
											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
											'empty_values' => array('year' => 'A&ntilde;o', 'month' => 'Mes','day' => 'D&iacute;a')));
  	
  	$this->validatorSchema['tipo_abono_id'] = new sfValidatorPropelChoice(array('model' => 'TipoAbono', 'column' => 'id'),array('required'=>'Seleccione tipo de abono'));
    $this->validatorSchema['tipo_pago_id'] = new sfValidatorPropelChoice(array('model' => 'TipoPago', 'column' => 'id'),array('required'=>'Seleccione tipo de pago'));
  	$this->validatorSchema['nro_recibo'] = new sfValidatorInteger(array(), array('invalid' => '"%value%", no es un n&uacute;mero.'));
  	$this->widgetSchema['causa_id'] = new sfWidgetFormInputHidden();
  	$this->validatorSchema['fecha_inicio_causa'] = new sfValidatorDate(array('required'=>true),array('required'=>'Debe ingresar una fecha.','invalid'=>'Fecha inv&aacute;lida.'));
  	$this->validatorSchema['fecha_actual'] = new sfValidatorDate(array('required'=>true),array('required'=>'Debe ingresar una fecha.','invalid'=>'Fecha inv&aacute;lida.'));
  	$this->validatorSchema['fecha'] = new sfValidatorDate(array('required'=>true),array('required'=>'Debe ingresar una fecha.','invalid'=>'Fecha inv&aacute;lida.'));
  	$this->validatorSchema['causa_id'] = new sfValidatorInteger();
  	$this->validatorSchema['pendiente'] = new sfValidatorInteger();
  	$this->validatorSchema['motivo'] = new sfValidatorString(array('min_length'=>10),array('min_length'=>'Debe ingresar un m&iacute;nimo de 10 caracteres.','required'=>'Ingrese un motivo.'));
    $this->validatorSchema['nro_recibo'] = new sfValidatorInteger(
													array(
														'min' => 1,
														'max' => 2147483646
													), 
													array(
														'min' => '"%value%", debe ser mayor a 0.',
														'max' => '"%value%", debe ser menor o igual a 2147483646.',
														'required'=>'Ingrese n&uacute;mero de recibo.',
														'invalid' => '"%value%", no es un n&uacute;mero o no es entero.'
													)
												);
  	$this->validatorSchema['valor'] = new sfValidatorInteger(
													array(
														'min' => 1,
														'max' => 2147483646
													),
													array(
														'min' => '"%value%", debe ser mayor a 0.',
  														'invalid' => '"%value%", no es un n&uacute;mero o no es entero.',
														'required'=>'Ingrese un monto.',
														'max' => '"%value%", debe ser menor o igual a 2147483646.'
													)
											);
  	$this->validatorSchema->setPostValidator(
  		new sfValidatorAnd(array(new sfValidatorSchemaCompare(
  							'valor',
  							sfValidatorSchemaCompare::LESS_THAN_EQUAL,
  							'pendiente',
							array('required'=> true,'throw_global_error' => false),
							array('invalid' => 'El monto a abonar ($%left_field%) debe ser menor o igual lo adeudado ($%right_field%)'))
                        ,new sfValidatorSchemaCompare(
  							'fecha',
  							sfValidatorSchemaCompare::LESS_THAN_EQUAL,
  							'fecha_actual',
							array('required'=> true,'throw_global_error' => false),
							array('invalid' => 'La fecha de abono (%left_field%) debe ser anterior o igual a hoy (%right_field%)'))
                        ,new sfValidatorSchemaCompare(
  							'fecha',
  							sfValidatorSchemaCompare::GREATER_THAN_EQUAL,
  							'fecha_inicio_causa',
							array('required'=> true,'throw_global_error' => false),
							array('invalid' => 'La fecha de abono (%left_field%) debe ser posterior o igual a la de inicio de la causa (%right_field%)'))
							)));
  }
}
