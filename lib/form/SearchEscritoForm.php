<?php
class SearchEscritoForm extends sfForm
{
	protected static $escritos = array(''=>'--Seleccione--',
		'3' => 'Fuerza P&uacute;blica Embargo',
		'4' => 'Fuerza P&uacute;blica Retiro Especies',
		'5' => 'Notif. art. 44',
		'6' => 'Notif. art. 52',
		'7' => 'Se Exhorte');
	
	public function configure()
	{
		$dias = Funciones::getArrayDias();
  		$meses = Funciones::getArrayMeses();
  		$anios = Funciones::getArrayAnios(2,2);
  		
		$this->setWidgets(array(
     		'id' => new sfWidgetFormSelect(array('choices' => self::$escritos)),
			'periodo' => new sfWidgetFormPropelSelect(array('model' => 'Periodo', 'add_empty' => '--Seleccione--')),
			'desde' => new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a'))),
			'hasta' => new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a'))),

			'id_deudor' => new sfWidgetFormInputHidden(),
   		));
   		
   		$this->setValidators(array(
      		'id'  => new sfValidatorChoice(array('choices' => array_keys(self::$escritos))),
    	));
    	
    	$this->validatorSchema->setPostValidator(
    						new sfValidatorSchemaCompare(
    								'desde',
    								sfValidatorSchemaCompare::LESS_THAN,
    								'hasta',
							    	array('required'=> false,'throw_global_error' => true),
							    	array('invalid' => 'La fecha "desde" ("%left_field%") debe ser anterior a la fecha "hasta" ("%right_field%")')
							    	)
							);
	}
}