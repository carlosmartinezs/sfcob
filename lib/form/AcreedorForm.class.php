<?php

/**
 * Acreedor form.
 *
 * @package    form
 * @subpackage acreedor
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class AcreedorForm extends BaseAcreedorForm
{
  public function configure()
  {
        $this->widgetSchema['comuna_id'] = new sfWidgetFormInputHidden();#array('model' => 'Comuna', 'add_empty' => "Seleccione Comuna"));
    	$this->widgetSchema['giro_id'] = new sfWidgetFormInputHidden();#(array('model' => 'Giro', 'add_empty' => "Seleccione Giro"));
        $this->widgetSchema['sf_guard_user_id'] = new sfWidgetFormInputHidden();
        $this->widgetSchema['clase_giro'] = new sfWidgetFormInputHidden();
        $this->widgetSchema['region'] = new sfWidgetFormInputHidden();
        $this->widgetSchema['ciudad'] = new sfWidgetFormInputHidden();
        
        $this->validatorSchema['clase_giro'] = new sfValidatorString(array('required'=>true),array('required'=>'Seleccione una clase de giro.'));
        $this->validatorSchema['giro_id'] = new sfValidatorString(array('required'=>true),array('required'=>'Seleccione un giro.'));
        $this->validatorSchema['region'] = new sfValidatorString(array('required'=>true),array('required'=>'Seleccione una regi&oacute;n.'));
        $this->validatorSchema['ciudad'] = new sfValidatorString(array('required'=>true),array('required'=>'Seleccione una provincia.'));
        $this->validatorSchema['comuna_id'] = new sfValidatorString(array('required'=>true),array('required'=>'Seleccione una comuna.'));
        $this->validatorSchema['email'] = new sfValidatorEmail(array('required'=>true),array('invalid'=>'Email inv&aacute;lido.','required'=>'Ingrese email.'));
        
      
      $this->validatorSchema['rut_acreedor'] = new sfValidatorString(array('max_length' => 12),array('required'=>'Ingrese rut.','max_length' => 'Debe tener como m&aacute;ximo 12 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 4 carateres.'));
      $this->validatorSchema['nombres'] = new sfValidatorString(array('max_length' => 50,'min_length'=>4),array('required'=>'Ingrese nombres.','max_length' => 'Debe tener como m&aacute;ximo 50 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 4 carateres.'));
      $this->validatorSchema['apellido_paterno'] = new sfValidatorString(array('max_length' => 25,'min_length'=>4),array('required'=>'Ingrese apellido paterno.','max_length' => 'Debe tener como m&aacute;ximo 25 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 4 carateres.'));
      $this->validatorSchema['apellido_materno'] = new sfValidatorString(array('max_length' => 25,'min_length'=>4),array('required'=>'Ingrese apellido materno.','max_length' => 'Debe tener como m&aacute;ximo 25 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 4 carateres.'));
      $this->validatorSchema['direccion'] = new sfValidatorString(array('min_length'=>5),array('required'=>'Ingrese una direcci&oacute;n.','min_length'=>'Debe tener como m&iacute;nimo 5 carateres.'));
      $this->validatorSchema['telefono'] = new sfValidatorString(array('max_length' => 25,'min_length'=>5),array('required'=>'Ingrese tel&eacute;fono.','max_length' => '"Debe tener como m&aacute;ximo 25 caracteres.'));
      $this->validatorSchema['fax'] = new sfValidatorString(array('max_length' => 25, 'required' => false),array('max_length' => 'Debe tener como m&aacute;ximo 25 caracteres.'));
      $this->validatorSchema['rep_legal'] = new sfValidatorString(array('max_length' => 70,'min_length'=>15),array('required'=>'Ingrese nombre completo rep. legal.','max_length' => 'Debe tener como m&aacute;ximo 70 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 15 carateres.'));
      $this->validatorSchema['rut_rep_legal'] = new sfValidatorString(array('max_length' => 12),array('required'=>'Ingrese rut.','max_length' => 'Debe tener como m&aacute;ximo 12 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 4 carateres.'));
        
        $this->validatorSchema->setPostValidator(
                new sfValidatorAnd(array(
                        new sfValidatorPropelUnique(array('model' => 'Acreedor', 'column' => array('rut_acreedor')),array('invalid' => 'Rut existente.')),
                        new sfValidatorPropelUnique(array('model' => 'Acreedor', 'column' => array('email')),array('invalid' => 'Email existente.')),
                        new sfValidatorPropelUnique(array('model' => 'Acreedor', 'column' => array('rut_rep_legal')),array('invalid' => 'Rut existente.')),
                        new sfValidatorSchemaCompare('rut_rep_legal',sfValidatorSchemaCompare::NOT_EQUAL,'rut_acreedor',
							array('required'=> true,'throw_global_error' => false),
							array('invalid' => 'Rut Acreedor ("%left_field%") <br>y Rut Rep. Legal ("%right_field%") <br>deben ser distintos'))
                        )));
  }
}


