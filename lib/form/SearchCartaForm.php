<?php
class SearchCartaForm extends sfForm
{
	protected static $cartas = array(''=>'--Seleccione--','1' => 'Cobranza Aval', '2' => 'Cobranza Deudor');
	
	public function configure()
	{
		$dias = Funciones::getArrayDias();
  		$meses = Funciones::getArrayMeses();
  		$anios = Funciones::getArrayAnios(2,2);
  		
		$this->setWidgets(array(
     		'id' => new sfWidgetFormSelect(array('choices' => self::$cartas)),
			'periodo' => new sfWidgetFormPropelSelect(array('model' => 'Periodo', 'add_empty' => '--Seleccione--')),
			'desde' => new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a'))),
			'hasta' => new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a'))),
			
			'id_deudor' => new sfWidgetFormInputHidden(),
   		));
   		
   		$this->setValidators(array(
      		'id'  => new sfValidatorChoice(array('choices' => array_keys(self::$cartas))),
    	));
    	
    	$this->validatorSchema->setPostValidator(
    						new sfValidatorSchemaCompare(
    								'desde',
    								sfValidatorSchemaCompare::LESS_THAN,
    								'hasta',
							    	array('required'=> false,'throw_global_error' => true),
							    	array('invalid' => 'La fecha "desde" ("%left_field%") debe ser anterior a la fecha "hasta" ("%right_field%")')
							    	)
							);
	}
}