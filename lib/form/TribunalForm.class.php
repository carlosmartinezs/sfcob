<?php

/**
 * Tribunal form.
 *
 * @package    form
 * @subpackage tribunal
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class TribunalForm extends BaseTribunalForm
{
  public function configure()
  {
  	$this->widgetSchema['comuna_id'] = new sfWidgetFormPropelSelect(array('model' => 'Comuna', 'add_empty' => "Seleccione Comuna"));
  	$this->widgetSchema['region'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['ciudad'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['accion'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['direccion'] = new sfWidgetFormTextarea();

  	$this->validatorSchema['region'] = new sfValidatorString(array('required'=>true),array('required'=>'Seleccione una regi&oacute;n.'));
    $this->validatorSchema['ciudad'] = new sfValidatorString(array('required'=>true),array('required'=>'Seleccione una provincia.'));
    $this->validatorSchema['comuna_id'] = new sfValidatorString(array('required'=>true),array('required'=>'Seleccione una comuna.'));
  	$this->validatorSchema['direccion'] = new sfValidatorString(array('required'=>'Debe ingresar una direcci&oacute:n'));
    $this->validatorSchema['nombre'] = new sfValidatorString(array('max_length' => 80,'min_length'=>4),array('required'=>'Ingrese nombre del tribunal.','max_length' => 'Debe tener como m&aacute;ximo 80 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 4 carateres.'));
    $this->validatorSchema['direccion'] = new sfValidatorString(array('min_length'=>5),array('required'=>'Ingrese una direcci&oacute;n.','min_length'=>'Debe tener como m&iacute;nimo 5 carateres.'));
  }
}
