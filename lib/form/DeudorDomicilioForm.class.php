<?php

/**
 * DeudorDomicilio form.
 *
 * @package    form
 * @subpackage deudor_domicilio
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class DeudorDomicilioForm extends BaseDeudorDomicilioForm
{
  public function configure()
  {
  	$this->widgetSchema['id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['deudor_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['region'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['ciudad'] = new sfWidgetFormInputHidden();
  	
  	$this->validatorSchema['region'] = new sfValidatorString(array(),array('required'=>'Seleccione una regi&oacute;n.'));
  	$this->validatorSchema['ciudad'] = new sfValidatorString(array(),array('required'=>'Seleccione una provincia.'));
  	$this->validatorSchema['comuna_id'] = new sfValidatorString(array(),array('required'=>'Seleccione una comuna.'));
    $this->validatorSchema['calle'] = new sfValidatorString(array('max_length' => 50,'min_length'=>5),array('required'=>'Ingrese el nombre de la calle.','max_length' => 'Debe tener como m&aacute;ximo 50 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 5 carateres.'));
    $this->validatorSchema['numero'] =  new sfValidatorInteger(
												array(
													'max' => 2147483646,
													'min' => 0
												),
												array(
													'max' => '"%value%", debe ser menor o igual a 2147483646.',
													'min' => '"%value%", debe ser mayor a 0.',
													'required'=>'Ingrese n&uacute;mero del domicilio.',
													'invalid' => '"%value%", no es un n&uacute;mero o no es entero.'
												)
											);
    $this->validatorSchema['depto'] = new sfValidatorString(array('max_length' => 10, 'required' => false),array('max_length'=>'Debe tener como m&aacute;ximo 10 caracteres.'));
    $this->validatorSchema['villa'] = new sfValidatorString(array('max_length' => 25, 'required' => false),array('max_length'=>'Debe tener como m&aacute;ximo 25 caracteres.'));
    $this->validatorSchema['telefono'] =  new sfValidatorString(array('max_length' => 25),array('required'=>'Ingrese n&uacute;mero de tel&eacute;fono.','max_length'=>'Debe tener como m&aacute;ximo 25 caracteres.'));
  }
}