<?php

/**
 * Formato form base class.
 *
 * @package    form
 * @subpackage formato
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseFormatoForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'     => new sfWidgetFormInputHidden(),
      'tipo'   => new sfWidgetFormInputCheckbox(),
      'nombre' => new sfWidgetFormInput(),
      'cuerpo' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'     => new sfValidatorPropelChoice(array('model' => 'Formato', 'column' => 'id', 'required' => false)),
      'tipo'   => new sfValidatorBoolean(),
      'nombre' => new sfValidatorString(array('max_length' => 50)),
      'cuerpo' => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('formato[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Formato';
  }


}
