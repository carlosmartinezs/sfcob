<?php

/**
 * Documento form base class.
 *
 * @package    form
 * @subpackage documento
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseDocumentoForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'tipo_documento_id' => new sfWidgetFormPropelSelect(array('model' => 'TipoDocumento', 'add_empty' => false)),
      'causa_id'          => new sfWidgetFormPropelSelect(array('model' => 'Causa', 'add_empty' => false)),
      'fecha_vencimiento' => new sfWidgetFormDate(),
      'monto'             => new sfWidgetFormInput(),
      'interes'           => new sfWidgetFormInput(),
      'nro_documento'     => new sfWidgetFormInput(),
      'descripcion'       => new sfWidgetFormTextarea(),
      'fecha_emision'     => new sfWidgetFormDate(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorPropelChoice(array('model' => 'Documento', 'column' => 'id', 'required' => false)),
      'tipo_documento_id' => new sfValidatorPropelChoice(array('model' => 'TipoDocumento', 'column' => 'id')),
      'causa_id'          => new sfValidatorPropelChoice(array('model' => 'Causa', 'column' => 'id')),
      'fecha_vencimiento' => new sfValidatorDate(),
      'monto'             => new sfValidatorInteger(),
      'interes'           => new sfValidatorNumber(),
      'nro_documento'     => new sfValidatorString(array('max_length' => 50)),
      'descripcion'       => new sfValidatorString(),
      'fecha_emision'     => new sfValidatorDate(),
    ));

    $this->widgetSchema->setNameFormat('documento[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Documento';
  }


}
