<?php

/**
 * Deudor form base class.
 *
 * @package    form
 * @subpackage deudor
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseDeudorForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'rut_deudor'       => new sfWidgetFormInput(),
      'nombres'          => new sfWidgetFormInput(),
      'apellido_paterno' => new sfWidgetFormInput(),
      'apellido_materno' => new sfWidgetFormInput(),
      'celular'          => new sfWidgetFormInput(),
      'email'            => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorPropelChoice(array('model' => 'Deudor', 'column' => 'id', 'required' => false)),
      'rut_deudor'       => new sfValidatorString(array('max_length' => 12)),
      'nombres'          => new sfValidatorString(array('max_length' => 50)),
      'apellido_paterno' => new sfValidatorString(array('max_length' => 25)),
      'apellido_materno' => new sfValidatorString(array('max_length' => 25)),
      'celular'          => new sfValidatorString(array('max_length' => 25)),
      'email'            => new sfValidatorString(array('max_length' => 50, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('deudor[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Deudor';
  }


}
