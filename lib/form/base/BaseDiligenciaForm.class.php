<?php

/**
 * Diligencia form base class.
 *
 * @package    form
 * @subpackage diligencia
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseDiligenciaForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'periodo_causa_id' => new sfWidgetFormPropelSelect(array('model' => 'PeriodoCausa', 'add_empty' => false)),
      'fecha'            => new sfWidgetFormDate(),
      'descripcion'      => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorPropelChoice(array('model' => 'Diligencia', 'column' => 'id', 'required' => false)),
      'periodo_causa_id' => new sfValidatorPropelChoice(array('model' => 'PeriodoCausa', 'column' => 'id')),
      'fecha'            => new sfValidatorDate(),
      'descripcion'      => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('diligencia[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Diligencia';
  }


}
