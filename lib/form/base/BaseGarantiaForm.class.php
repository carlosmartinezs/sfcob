<?php

/**
 * Garantia form base class.
 *
 * @package    form
 * @subpackage garantia
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseGarantiaForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'causa_id'         => new sfWidgetFormPropelSelect(array('model' => 'Causa', 'add_empty' => false)),
      'tipo_garantia_id' => new sfWidgetFormPropelSelect(array('model' => 'TipoGarantia', 'add_empty' => false)),
      'valor'            => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorPropelChoice(array('model' => 'Garantia', 'column' => 'id', 'required' => false)),
      'causa_id'         => new sfValidatorPropelChoice(array('model' => 'Causa', 'column' => 'id')),
      'tipo_garantia_id' => new sfValidatorPropelChoice(array('model' => 'TipoGarantia', 'column' => 'id')),
      'valor'            => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('garantia[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Garantia';
  }


}
