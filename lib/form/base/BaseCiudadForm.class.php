<?php

/**
 * Ciudad form base class.
 *
 * @package    form
 * @subpackage ciudad
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseCiudadForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'region_id' => new sfWidgetFormPropelSelect(array('model' => 'Region', 'add_empty' => false)),
      'nombre'    => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorPropelChoice(array('model' => 'Ciudad', 'column' => 'id', 'required' => false)),
      'region_id' => new sfValidatorPropelChoice(array('model' => 'Region', 'column' => 'id')),
      'nombre'    => new sfValidatorString(array('max_length' => 25)),
    ));

    $this->widgetSchema->setNameFormat('ciudad[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Ciudad';
  }


}
