<?php

/**
 * Giro form base class.
 *
 * @package    form
 * @subpackage giro
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseGiroForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'clase_giro_id' => new sfWidgetFormPropelSelect(array('model' => 'ClaseGiro', 'add_empty' => false)),
      'nombre'        => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorPropelChoice(array('model' => 'Giro', 'column' => 'id', 'required' => false)),
      'clase_giro_id' => new sfValidatorPropelChoice(array('model' => 'ClaseGiro', 'column' => 'id')),
      'nombre'        => new sfValidatorString(array('max_length' => 80)),
    ));

    $this->widgetSchema->setNameFormat('giro[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Giro';
  }


}
