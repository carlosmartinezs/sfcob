<?php

/**
 * Agenda form base class.
 *
 * @package    form
 * @subpackage agenda
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseAgendaForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'perfil_id'   => new sfWidgetFormPropelSelect(array('model' => 'sfGuardUser', 'add_empty' => false)),
      'descripcion' => new sfWidgetFormTextarea(),
      'fecha'       => new sfWidgetFormDateTime(),
      'realizada'   => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorPropelChoice(array('model' => 'Agenda', 'column' => 'id', 'required' => false)),
      'perfil_id'   => new sfValidatorPropelChoice(array('model' => 'sfGuardUser', 'column' => 'id')),
      'descripcion' => new sfValidatorString(),
      'fecha'       => new sfValidatorDateTime(),
      'realizada'   => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('agenda[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Agenda';
  }


}
