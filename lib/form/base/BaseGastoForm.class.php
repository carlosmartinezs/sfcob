<?php

/**
 * Gasto form base class.
 *
 * @package    form
 * @subpackage gasto
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseGastoForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'tipo_gasto_id' => new sfWidgetFormPropelSelect(array('model' => 'TipoGasto', 'add_empty' => false)),
      'diligencia_id' => new sfWidgetFormPropelSelect(array('model' => 'Diligencia', 'add_empty' => false)),
      'monto'         => new sfWidgetFormInput(),
      'fecha'         => new sfWidgetFormDate(),
      'motivo'        => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorPropelChoice(array('model' => 'Gasto', 'column' => 'id', 'required' => false)),
      'tipo_gasto_id' => new sfValidatorPropelChoice(array('model' => 'TipoGasto', 'column' => 'id')),
      'diligencia_id' => new sfValidatorPropelChoice(array('model' => 'Diligencia', 'column' => 'id')),
      'monto'         => new sfValidatorInteger(),
      'fecha'         => new sfValidatorDate(),
      'motivo'        => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('gasto[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Gasto';
  }


}
