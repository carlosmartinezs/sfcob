<?php

/**
 * EstadoCausa form base class.
 *
 * @package    form
 * @subpackage estado_causa
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseEstadoCausaForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'causa_id'  => new sfWidgetFormPropelSelect(array('model' => 'Causa', 'add_empty' => false)),
      'estado_id' => new sfWidgetFormPropelSelect(array('model' => 'Estado', 'add_empty' => false)),
      'fecha'     => new sfWidgetFormDate(),
      'motivo'    => new sfWidgetFormTextarea(),
      'activo'    => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorPropelChoice(array('model' => 'EstadoCausa', 'column' => 'id', 'required' => false)),
      'causa_id'  => new sfValidatorPropelChoice(array('model' => 'Causa', 'column' => 'id')),
      'estado_id' => new sfValidatorPropelChoice(array('model' => 'Estado', 'column' => 'id')),
      'fecha'     => new sfValidatorDate(),
      'motivo'    => new sfValidatorString(),
      'activo'    => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('estado_causa[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EstadoCausa';
  }


}
