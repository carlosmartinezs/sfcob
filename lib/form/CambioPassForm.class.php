<?php

class CambioPassForm extends sfForm
{
	public function setup()
	{
		$this->setWidgets(array(
     		'pass_actual' 	=> new sfWidgetFormInputPassword(),
			'pass_nva1' 	=> new sfWidgetFormInputPassword(),
			'pass_nva2' 	=> new sfWidgetFormInputPassword(),
   		));
   		
   		$this->setValidators(array(
     		'pass_actual' 	=> new sfValidatorString(array('required' => true),array('required' => 'Requerido.')),
			'pass_nva1' 	=> new sfValidatorString(array('required' => true),array('required' => 'Requerido.')),
			'pass_nva2' 	=> new sfValidatorString(array('required' => true),array('required' => 'Requerido.')),
    	));
    	
    	$this->widgetSchema->setLabels(array(
		  'pass_actual'	=> 'Contrase&ntilde;a actual *',
		  'pass_nva1'   => 'Nueva contrase&ntilde;a *',
		  'pass_nva2' 	=> 'Repita Nva. contrase&ntilde;a *',
		));
		
		$this->validatorSchema->setPostValidator(
    						new sfValidatorSchemaCompare(
    								'pass_nva1',
    								sfValidatorSchemaCompare::EQUAL,
    								'pass_nva2',
							    	array('required'=> true,'throw_global_error' => true),
							    	array('required'=>'Requerido','invalid' => 'La nueva contrase&ntilde;a no coincide')
							    	)
							);
		$this->widgetSchema->setNameFormat('cambiocontrasena[%s]');

	    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
	
	    parent::setup();
	}
}