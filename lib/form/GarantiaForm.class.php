<?php

/**
 * Garantia form.
 *
 * @package    form
 * @subpackage garantia
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class GarantiaForm extends BaseGarantiaForm
{
  public function configure()
  {
  	$this->widgetSchema['tipo_garantia_id'] = new sfWidgetFormPropelSelect(array('model' => 'TipoGarantia', 'add_empty' => '--Seleccione--'));
  	$this->validatorSchema['tipo_garantia_id'] = new sfValidatorPropelChoice(array('model' => 'TipoGarantia', 'column' => 'id'),array('required'=>'Seleccione tipo de garant&iacute;a.'));
  	$this->validatorSchema['valor'] = new sfValidatorInteger(
													array(
														'min' => 1,
														'max' => 2147483646
													),
													array(
														'min' => '"%value%", debe ser mayor a 0.',
  														'invalid' => '"%value%", no es un n&uacute;mero o no es entero.',
														'required'=>'Ingrese un valor.',
														'max' => '"%value%", debe ser menor o igual a 2147483646.'
													)
											);
  }
}
