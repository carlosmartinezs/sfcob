<?php
class SearchAcreedorForm extends sfForm
{
	public function configure()
	{
		$this->setWidgets(array(
			'accion'		   => new sfWidgetFormInputHidden(),
      		'rut_acreedor'     => new sfWidgetFormInput(),
      		'nombres'          => new sfWidgetFormInput(),
      		'apellido_paterno' => new sfWidgetFormInput(),
      		'apellido_materno' => new sfWidgetFormInput(),
      		'giro'             => new sfWidgetFormInput(),
	      	'direccion'        => new sfWidgetFormTextarea(),
	      	'comuna'           => new sfWidgetFormInput(),
	      	'ciudad'           => new sfWidgetFormInput(),
	      	'region'           => new sfWidgetFormInput(),
	      	'telefono'         => new sfWidgetFormInput(),
	      	'fax'              => new sfWidgetFormInput(),
	      	'email'            => new sfWidgetFormInput(),
	      	'rep_legal'        => new sfWidgetFormInput(),
	      	'rut_rep_legal'    => new sfWidgetFormInput(),
    	));
	}
}
