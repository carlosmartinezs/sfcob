<?php

/**
 * Deudor form.
 *
 * @package    form
 * @subpackage deudor
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class DeudorForm extends BaseDeudorForm
{
  public function configure()
  {
  	$this->validatorSchema['rut_deudor'] = new sfValidatorString(array('max_length' => 12),array('required'=>'Ingrese rut.'));
    $this->validatorSchema['nombres'] = new sfValidatorString(array('max_length' => 50,'min_length'=>4),array('required'=>'Ingrese nombres.','max_length' => '"Debe tener como m&aacute;ximo 50 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 4 carateres.'));
    $this->validatorSchema['apellido_paterno'] = new sfValidatorString(array('max_length' => 25,'min_length'=>5),array('required'=>'Ingrese apellido paterno.','max_length' => '"Debe tener como m&aacute;ximo 25 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 5 carateres.'));
    $this->validatorSchema['apellido_materno'] = new sfValidatorString(array('max_length' => 25,'min_length'=>5),array('required'=>'Ingrese apellido materno.','max_length' => '"Debe tener como m&aacute;ximo 25 caracteres.','min_length'=>'Debe tener como m&iacute;nimo 5 carateres.'));
    $this->validatorSchema['celular'] = new sfValidatorString(array('max_length' => 25,'min_length'=>5),array('required'=>'Ingrese tel&eacute;fono.','max_length' => '"Debe tener como m&aacute;ximo 25 caracteres.'));
  	$this->validatorSchema['email'] = new sfValidatorEmail(array('required'=>false),array('invalid'=>'Email inv&aacute;lido.'));
  }
}
