<?php

class PreguntaSecretaForm extends sfForm
{
	protected static $preguntas = array(
									'Nombre de mi mascota'=>'Nombre de mi mascota',
									'Apellido de soltera de mi madre'=>'Apellido de soltera de mi madre',
									'Artista favorito'=>'Artista favorito',
									'Mi primer profesor de matem&aacute;ticas'=>'Mi primer profesor de matem&aacute;ticas');
	
	public function setup()
	{
		
		$this->setWidgets(array(
     		'pregunta' 	=> new sfWidgetFormSelect(array('choices'=>self::$preguntas)),
			'respuesta' 	=> new sfWidgetFormInput(),
   		));
   		
   		$this->setValidators(array(
   		'pregunta' 	=> new sfValidatorString(
   						array('required' => false,'max_length'=>128,'min_length'=>10)),
   		'respuesta' => new sfValidatorString(
   						array(
   								'required' => true,
   								'max_length'=>128,
   								'min_length'=>4
   						),
   						array(
   								'required' => 'Ingrese una respuesta.',
   								'max_length'=>'Debe tener un m&aacute;ximo de 128 caracteres',
   								'min_length'=>'Debe tener m&iacute;nimo 4 caracteres')
   						)
   		));
    	
    	$this->widgetSchema->setLabels(array(
		  'pregunta'	=> 'Pregunta *',
		  'respuesta'   => 'Respuesta *',
		));
		
		$this->widgetSchema->setNameFormat('preguntasecreta[%s]');

	    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
	
	    parent::setup();
	}
}