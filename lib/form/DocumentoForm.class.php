<?php

/**
 * Documento form.
 *
 * @package    form
 * @subpackage documento
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class DocumentoForm extends BaseDocumentoForm
{
  public function configure()
  {
  	$dias = Funciones::getArrayDias();
  	$meses = Funciones::getArrayMeses();
  	$anios = Funciones::getArrayAnios(2,2);
  	
  	$this->widgetSchema['fecha_vencimiento'] = new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a')));
  	$this->widgetSchema['fecha_emision'] = new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a')));
  	$this->widgetSchema['tipo_documento_id'] = new sfWidgetFormPropelSelect(array('model' => 'TipoDocumento', 'add_empty' => "-- Seleccione --"));
  	
  	$this->validatorSchema['monto'] = new sfValidatorInteger(
													array(
														'min' => 1,
														'max' => 2147483646
													), 
													array(
														'min' => '"%value%", debe ser mayor a 0.',
														'max' => '"%value%", debe ser menor o igual a 2147483646.',
  														'invalid' => '"%value%", no es un n&uacute;mero o no es un entero positivo.',
														'required'=>'Ingrese monto.'
													)
											);
  	$this->validatorSchema['interes']  = new sfValidatorNumber(
													array(
	  													'max' => 100,
	  													'min' => 0
													), 
													array(
	  													'max' => '"%value%", debe ser menor o igual a 100.',
	  													'min' => '"%value%", debe ser mayor o igual a 0.',
	  													'invalid' => '"%value%", no es un n&uacute;mero.',
														'required'=>'Ingrese porcentaje.'
													)
											);
  	
  	$this->validatorSchema['tipo_documento_id'] = new sfValidatorPropelChoice(array('model' => 'TipoDocumento', 'column' => 'id'),array('required'=>'Seleccione tipo de documento'));
    $this->validatorSchema['fecha_vencimiento'] = new sfValidatorDate(array(),array('invalid'=>'Fecha incorrecta.','required'=>'Ingrese fecha de vencimiento.'));

    $this->validatorSchema['nro_documento'] = new sfValidatorString(array('min_length'=>4,'max_length' => 50),array('min_length'=>'Debe ingresar un m&iacute;nimo de 4 caracteres.','max_length'=>'Debe ingresar un m&aacute;ximo de 50 caracteres.','required'=>'Ingrese n&uacute;mero del documento.'));
    $this->validatorSchema['descripcion'] =  new sfValidatorString(array('min_length'=>4),array('min_length'=>'Debe ingresar un m&iacute;nimo de 4 caracteres.','required'=>'Ingrese una descripci&oacute;n.'));
    $this->validatorSchema['fecha_emision'] = new sfValidatorDate(array(),array('invalid'=>'Fecha incorrecta.','required'=>'Ingrese fecha de emisi&oacute;n.'));
  	
  	$this->validatorSchema->setPostValidator(
    						new sfValidatorSchemaCompare(
    								'fecha_vencimiento',
    								sfValidatorSchemaCompare::GREATER_THAN,
    								'fecha_emision',
							    	array('throw_global_error' => false),
							    	array('invalid' => 'La fecha de vencimiento ("%left_field%") debe ser posterior a la fecha de emisi&oacute;n ("%right_field%")')
							    	)
							);
  	
  }
}
