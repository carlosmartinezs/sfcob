<?php

/**
 * Contrato form.
 *
 * @package    form
 * @subpackage contrato
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ContratoForm extends BaseContratoForm
{
  public function configure()
  {
  	$dias = Funciones::getArrayDias();
  	$meses = Funciones::getArrayMeses();
  	$anios1 = Funciones::getArrayAnios(0,2);
  	$anios2 = Funciones::getArrayAnios(2,0);
  	
  	$this->widgetSchema['acreedor_id'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['fecha_inicio'] = new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios2,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a')));
  	$this->widgetSchema['fecha_termino'] = new sfWidgetFormDate(array(
	  											'format' => '%day%/%month%/%year%',
	  											'years'=>$anios1,
	  											'months'=>$meses,
	  											'days'=>$dias,
	  											'empty_values' => array(
	  														'year' => 'A&ntilde;o',
	  														'month' => 'Mes',
	  														'day' => 'D&iacute;a')));
  	$this->widgetSchema['estado'] = new sfWidgetFormInputHidden();
  	$this->validatorSchema['fecha_inicio'] = new sfValidatorDate(array(),array('invalid'=>'Fecha incorrecta.','required'=>'Ingrese fecha de inicio.'));
    $this->validatorSchema['fecha_termino'] = new sfValidatorDate(array(),array('invalid'=>'Fecha incorrecta.','required'=>'Ingrese fecha de inicio.'));
  	$this->validatorSchema['porcentaje_honorario'] = new sfValidatorNumber(
	  														array(
	  															'max' => 100,
	  															'min' => 0
	  														),
	  														array(
	  															'required'=>'Ingrese porcentaje.',
	  															'max' => '"%value%", debe ser menor o igual a 100.',
	  															'min' => '"%value%", debe ser mayor o igual a 0.',
	  															'invalid' => '"%value%", no es un n&uacute;mero.'
	  														)
  														 );
  
    $this->validatorSchema->setPostValidator(
    						new sfValidatorSchemaCompare(
    								'fecha_termino',
    								sfValidatorSchemaCompare::GREATER_THAN,
    								'fecha_inicio',
							    	array('required'=> false,'throw_global_error' => false),
							    	array('invalid' => 'La fecha de t&eacute;rmino ("%left_field%") debe ser posterior a la de inicio ("%right_field%")')
							    	)
							);
  }
}
