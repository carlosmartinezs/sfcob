<?php
class Funciones
{
	/**
	 * @param nada, o un array asociativo con campos dia, mes y anio.
	 * 
	 * @return si no se indica el array de fecha devuelve la fecha actual, como por ejemplo: 1 de Abril de 2008, si se le pasa el parámetro (p.e. : $fecha = array('dia'=>1,'mes'=>5,'anio'=>1990);) devuelve esa fecha en el formato indicado.
	 *
	 */
	public static function fecha($fecha = null){
		
		if($fecha != null)
		{
			$dia = $fecha['dia'];
			$mes = $fecha['mes'];
			$anio = $fecha['anio'];
		}
		else
		{
			$dia = date('d');
			$mes = date('m');
			$anio = date('Y');
		}
		
		switch(date("l", mktime(0, 0, 0,$mes,$dia,$anio))){
			case 'Sunday': $diaNombre = 'Domingo';break;
			case 'Monday': $diaNombre = 'Lunes';break;
			case 'Tuesday': $diaNombre = 'Martes';break;
			case 'Wednesday': $diaNombre = 'Miércoles';break;
			case 'Thursday': $diaNombre = 'Jueves';break;
			case 'Friday': $diaNombre = 'Viernes';break;
			case 'Saturday': $diaNombre = 'Sábado';break;
		}
		
		switch($mes){
			case 1:$mes = 'Enero';break;
			case 2:$mes = 'Febrero';break;
			case 3:$mes = 'Marzo';break;
			case 4:$mes = 'Abril';break;
			case 5:$mes = 'Mayo';break;
			case 6:$mes = 'Junio';break;
			case 7:$mes = 'Julio';break;
			case 8:$mes = 'Agosto';break;
			case 9:$mes = 'Septiembre';break;
			case 10:$mes = 'Octubre';break;
			case 11:$mes = 'Noviembre';break;
			case 12:$mes = 'Diciembre';break;
		}
		
		
		return $diaNombre . ', ' . $dia .' de '. $mes .' de '. $anio;
	}
	
	/**
	 * @param Nada o un string "d-m-Y"
	 * d: un dia entre 1 y 31
	 * m: un mes entre 1 y 12 
	 * Y: un año en cuatro dijitos, ej: 1990
	 * 
	 * @return String: L, d de M de Y. Ejemplo: Jueves, 4 de Junio de 2009 
	 * L: corresponde al dia de la semana.
	 * M: corresponde al mes.
	 */
    public static function fecha2($fechaString = null)
    {
		
		
		if($fechaString == null)
		{
			$fechaString = date('d-m-Y');
		}
		
		$arrFecha = split('-',$fechaString);
		$dia = $arrFecha[0];
		$mes = $arrFecha[1];
		$anio = $arrFecha[2];
		
		switch(date("l", mktime(0, 0, 0,$mes,$dia,$anio))){
			case 'Sunday': 
				$diaNombre = 'Domingo';
				break;
			case 'Monday': 
				$diaNombre = 'Lunes';
				break;
			case 'Tuesday': 
				$diaNombre = 'Martes';
				break;
			case 'Wednesday': 
				$diaNombre = 'Miércoles';
				break;
			case 'Thursday': 
				$diaNombre = 'Jueves';
				break;
			case 'Friday': 
				$diaNombre = 'Viernes';
				break;
			case 'Saturday': 
				$diaNombre = 'Sábado';
				break;
		}
		
		switch($mes){
			case 1:$mes = 'Enero';break;
			case 2:$mes = 'Febrero';break;
			case 3:$mes = 'Marzo';break;
			case 4:$mes = 'Abril';break;
			case 5:$mes = 'Mayo';break;
			case 6:$mes = 'Junio';break;
			case 7:$mes = 'Julio';break;
			case 8:$mes = 'Agosto';break;
			case 9:$mes = 'Septiembre';break;
			case 10:$mes = 'Octubre';break;
			case 11:$mes = 'Noviembre';break;
			case 12:$mes = 'Diciembre';break;
		}
		
		
		return $diaNombre . ', ' . $dia .' de '. $mes .' de '. $anio;
	}
	
	public static function verificarRut($valor){
	            $r=strtoupper(ereg_replace('\.|,|-','',$valor));
	            
                $sub_rut=substr($r,0,strlen($r)-1);
                $sub_dv=substr($r,-1);
                $x=2;
                $s=0;
                for ( $i=strlen($sub_rut)-1;$i>=0;$i-- )
                {
                	if ( $x >7 )
                	{
                    	$x=2;
                	}
                	$s += $sub_rut[$i]*$x;
                	$x++;
                }
                $dv=11-($s%11);
                if ( $dv==10 )
                {
                        $dv='K';
                }
                if ( $dv==11 )
                {
                        $dv='0';
                }
                if ( $dv==$sub_dv )
                {
                        return $r;
                }else{
                        return null;
                }
	}
	
	public static function parsearRut($rut)
	{
		$rut_tmp = '';
		
		for($i = strlen($rut)-1; $i >= 0; $i--)
		{
			if($i == strlen($rut)-2){
				
				$rut_tmp = '-'.$rut_tmp;
			}
			else if($i == strlen($rut)-5)
			{
				$rut_tmp = '.'.$rut_tmp;
			}
			else if($i == strlen($rut)-8)
			{
				$rut_tmp = '.'.$rut_tmp;
			}
			
			$rut_tmp = $rut[$i].$rut_tmp;
		}
		return $rut_tmp;
	}

	public static function generarRandomString($len = 20)
  	{
	    $string = "";
	    $pool   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\@#~€¬¡ºª!·$%&/()=?¿`+´ç,.-[]{};:_";
	    for ($i = 1; $i <= $len; $i++)
	    {
	      $string .= substr($pool, rand(0, 61), 1);
	    }
	
	    return $string;
	}
	
	public static function enviarMail($de,$para,$nombre,$texto,$asunto)
	{
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text; charset=iso-8859-1\r\n";
			$headers .= "From: ".$nombre." <".$de.">\r\n";
			$headers .= "Reply-To: ".$de."\r\n";
			mail($para,$asunto,$texto,$headers);
	}
	
	public static function diferenciaDias($fecha1,$fecha2)
	{
		//defino fecha 1
		$ano1 = $fecha1['ano'];
		$mes1 = $fecha1['mes'];
		$dia1 = $fecha1['dia'];
		
		//defino fecha 2
		$ano2 = $fecha2['ano'];
		$mes2 = $fecha2['mes'];
		$dia2 = $fecha2['dia'];
		
		//calculo timestam de las dos fechas
		$timestamp1 = mktime(0,0,0,$mes1,$dia1,$ano1);
		$timestamp2 = mktime(0,0,0,$mes2,$dia2,$ano2);
		
		//resto a una fecha la otra
		$segundos_diferencia = $timestamp1 - $timestamp2;
		//echo $segundos_diferencia;
		
		//convierto segundos en días
		$dias_diferencia = $segundos_diferencia / (60 * 60 * 24);
		
		//obtengo el valor absoulto de los días (quito el posible signo negativo)
		$dias_diferencia = abs($dias_diferencia);
		
		//quito los decimales a los días de diferencia
		$dias_diferencia = floor($dias_diferencia);
		
		return $dias_diferencia; 
	}
	public static function dateAdd($dias)
   {
      $mes = date("m");
      $anio = date("Y");
      $dia = date("d");
      $ultimo_dia = date( "d", mktime(0, 0, 0, $mes + 1, 0, $anio) ) ;
      $dias_adelanto = $dias;
      $siguiente = $dia + $dias_adelanto;
      if ($ultimo_dia < $siguiente)
      {
         $dia_final = $siguiente - $ultimo_dia;
         $mes++;
         if ($mes == '13')
         {
            $anio++;
            $mes = '01';
         }
         $fecha_final = $anio.'-'.$mes.'-'.$dia_final;         
      }
      else
      {
         $fecha_final = $anio.'-'.$mes.'-'.$siguiente;         
      }
      return $fecha_final;
   }
	
   public static function getArrayDias($desface = 1)
   {

   		if($desface < 1)
   		{
			$desface = 1;
		}
		
		if($desface > 31)
		{
			$desface = 31;
		}
		
   		$array = null;
   		
   		for($i = $desface;$i<=31;$i++)
   		{
   			
			if( $i < 10)
			{
				$array['0'.$i] = '0'.$i;
			}
			else
			{
				$array[$i] = $i;
			}
   				
   		}
   		return $array;
   }
   
	public static function getArrayAnios($desde = 0, $hasta = 0)
	{
		if($desde < 0)
		{
			$desde = 0;
		}
		
		if($hasta < 0)
		{
			$hasta = 0;
		}
		
   		$anio = date('Y');
   		
   		$array = null;
   		
   		
   		for($i = $anio-$desde ; $i <= $anio + $hasta ; $i++)
   		{
   			$array[$i] = $i;
   		}
   		return $array;
   }
   
	public static function getArrayMeses($desface = 1){

		if($desface < 1)
		{
			$desface = 1;
		}
		
		if($desface > 12)
		{
			$desface = 12;
		}
		
   		$array = null;
   		
   		for($i = $desface;$i<=12;$i++)
   		{
	   		switch($i)
	   		{
				case 1:
					$mesNombre = 'Enero';
					break;
				case 2:
					$mesNombre = 'Febrero';
					break;
				case 3:
					$mesNombre = 'Marzo';
					break;
				case 4:
					$mesNombre = 'Abril';
					break;
				case 5:
					$mesNombre = 'Mayo';
					break;
				case 6:
					$mesNombre = 'Junio';
					break;
				case 7:
					$mesNombre = 'Julio';
					break;
				case 8:
					$mesNombre = 'Agosto';
					break;
				case 9:
					$mesNombre = 'Septiembre';
					break;
				case 10:
					$mesNombre = 'Octubre';
					break;
				case 11:
					$mesNombre = 'Noviembre';
					break;
				case 12:
					$mesNombre = 'Diciembre';
					break;
			}
			
			if( $i < 10)
			{
				$array['0'.$i] = $mesNombre;
			}
			else
			{
				$array[$i] = $mesNombre;
			}
   				
   		}
   		return $array;
   }
   
   public static function ultimoDiaMes($mes=0,$anio=0)
   {
   		if($mes <= 0)
   		{
   			$mes = date('m');
   		}
   		else if($mes > 12)
   		{
   			$mes = 12;
   		}
   		
   		if($anio == 0)
   		{
   			$anio = date('Y');	
   		}
   		
	   	for ($dia=31;$dia>=28;$dia--)
	   	{
	   		if(checkdate($mes,$dia,$anio)){
	   			return $dia;
	   		}
	   	}
   }
}